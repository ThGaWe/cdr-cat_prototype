Go
Company History - [] [] Facts and Figures -
Wähle dein Land: - [] [] Career Germany - [] [] Career Benelux - [] []
Career Austria - [Brands](https://soliver-group.com/en/brands/) -
Commitments - [] [] partners - [] [] Organisation - [] [] Downloads -
Go - [s.Oliver Group](https://soliver-group.com/en/soliver-group/) -
History](https://soliver-group.com/en/soliver-group/company-history/) -
Figures](https://soliver-group.com/en/soliver-group/facts-and-figures/)
Germany](https://soliver-group.com/en/career/de/) - [Career
Benelux](https://soliver-group.com/en/career/bl/) - [Career
Austria](https://soliver-group.com/en/career/at/) -
Releases](https://soliver-group.com/en/press/press-releases/) - [Press
Contacts](https://soliver-group.com/en/press/press-contacts/) -
Sustainable fibres ================== []
Sustainable fibre strategy -------------------------- With the aim of
further expanding our sustainable fiber program, the company joined the
EU-funded [European Clothing Action Plan
we conducted fiber consumption inventories and determined the associated
fiber footprint. We summarized the results of our collaboration with
ECAP in a [case
study](https://soliver-group.com/fileadmin/pdf/verantwortung/publikationen/ecap-case-study-soliver.pdf).
In 2018, we further specified our strategy for sustainable products and
underpinned the fields of action defined in the process with targets. In
the coming years, we want to significantly increase the share of
sustainable fibers in our collections accordingly and address further
material alternatives and product innovations. Cotton ------ Cotton is
one of the most important fibers in our product portfolio. Therefore we
focus on sustainability criteria when sourcing this raw material. The
s.Oliver Group aims to increase the proportion of sustainably produced
cotton to 100 percent by 2030, predominantly in cooperation with the
Cotton made in Africa and Better Cotton initiatives. At the same time,
we use internationally recognized standards for organic cotton and,
among other things, use Lyocell in our products, which is obtained from
fabric remnants of cotton clothing from the textile industry. **ORGANIC
COTTON** We use organic and thus GMO-free cotton. In the cultivation of
organic cotton, no chemical fertilizers and pesticides are used, but
natural variants such as compost. At the same time, crop rotation must
be maintained so that different crops are alternately grown on the
agricultural land. This helps to maintain and improve soil fertility and
increases the humus content. This in turn ensures that the soil can
absorb more CO2. In addition, no chemical defoliants may be used, which
facilitate the mechanical harvesting of cotton but endanger the health
of the harvest workers and farmers. At s.Oliver, we currently use the
organic material in our textiles. One focus is on the traceability of
organic fibers in the final product. We also use the world's leading
standard for processing organic natural fibers, which defines not only
ecological and social criteria for cultivation, but also for the entire
organic cotton production chain. **COTTON MADE IN AFRICA** As part
of [Cotton made in Africa](https://www.cottonmadeinafrica.org/en/)
has been sourcing sustainable cotton from Africa since 2009. By doing
so, we help African smallholders to help themselves in improving living
and working conditions by their own efforts. Because for every textile
with the CmiA label we pay a license fee to Cotton made in Africa.
Income from licenses is reinvested in Africa and benefits small farmers
and the protection of our nature through training in sustainable cotton
cultivation. Genetic modification and artificial irrigation are
prohibited in the cultivation of Cotton made in Africa cotton and are
therefore excluded. In addition, the initiative is involved in social
projects for the village communities - for example in the areas of
female empowerment, education or health. Find out more
sustainable cotton sourcing is the [Better Cotton
Initiative](https://bettercotton.org/) (BCI). Growers licensed to sell
Better Cotton into the supply chain must meet minimum social and
environmental criteria and demonstrate continuous improvement in this
regard. In the environmental area, Better Cotton is characterized, for
example, by reduced pesticide use, better water efficiency and a
cultivation method that pays attention to preserving soil fertility and
biodiversity. An overview of the Better Cotton criteria can be found
Africa and Better Cotton initiatives is based on the principle of mass
balance. According to this, our suppliers purchase a certain amount of
cotton needed for the production of our goods as sustainable cotton.
This means that cotton certified according to the criteria of Cotton
made in Africa (CmiA) or the Better Cotton Initiative (BCI) is supplied
to cotton spinning mills in our supply chain in the corresponding
quantity. The textiles are then made from this cotton. In the process,
cotton that meets the criteria of BCI or CmiA may be interwoven with
conventional cotton, meaning that products do not necessarily consist
entirely of cotton from the initiatives. However, it is important that
the amount of cotton corresponding to the product has been sustainably
grown and enters our supply chain. Thus, the demand for the sustainably
sourced cotton reaches the producers, i.e. the farmers. Ultimately, the
total mass of cotton sourced is what matters. Hence the name mass
balance. With each awarded product, cotton farmers are supported and
nature is protected, because for each CmiA or BCI product, companies pay
fees that are reinvested in the growing regions. You can find more
information
CELLULOSE FIBERS ---------------- The pulp for our sustainably certified
viscose comes from FSC- or PEFC-certified forestry. Further processing
of the raw material into yarn takes place in a closed manufacturing
cycle that reuses resources used and thus protects the environment. In
addition, greenhouse gas emissions are greatly reduced compared to the
extraction of generic cellulose fibers. Cellulose fibers are
characterized not only by the fact that they are obtained from a
renewable raw material, but also by the fact that they are
biodegradable. **SUSTAINABLE CELLULOSE FIBERS FROM LENZING™** The
wood-based viscose fibers of the LENZING™ ECOVERO™ brand come from
certified and controlled sources and bear the EU Ecolabel due to their
environmental compatibility. The production of LENZING™ ECOVERO™ viscose
fibers causes about 50 percent less emissions and a lower water impact
than generic viscose. Lenzing not only uses certified wood as a raw
material, but also fabric remnants from the cutting of cotton clothing
in the textile industry. Using REFIBRA™ technology, the raw material is
processed into new TENCEL™ lyocell fibers. This allows the use of raw
materials and energy to be significantly reduced. TENCEL™, REFIBRA™,
LENZING™ and ECOVERO™ are trademarks of [Lenzing
AG](https://www.lenzing.com/lenzing-group). **SUSTAINABLE VISCOSE:
LIVAECO™ BY BIRLA CELLULOSE™** Likewise, our partner [Birla
Cellulose](https://www.birlacellulose.com/)™ specializes in the
extraction of fibers from wood. Only wood from certified forestry is
used for the production of cellulose fibers. In the manufacturing
process, both water consumption and greenhouse gas emissions are greatly
reduced compared to other non-certified natural fibers. The cellulose
fibers can also be traced back to their origin and are biodegradable.
Synthetic and animal fibres =========================== In order to make
the use of synthetic fibres more eco-friendly, the s.Oliver Group is
increasingly focused on using recycled fibre materials. You can find
information [here](https://soliver-group.com/en/responsibility/our-commitments/recycling/).
We also impose strict requirements and standards on our suppliers
regarding the use of animal fibres, which you can
read [here](https://soliver-group.com/en/responsibility/our-commitments/animal-welfare/).
SUSTAINABLE MANUFACTURING PROCESSES -----------------------------------
Another strategy component is in the area of manufacturing processes.
The s.Oliver Group wants to promote the use of environmentally friendly
technologies in the production of textiles and provides suppliers with
concrete guidelines for this purpose. One focus is on water-, energy-
and chemical-reduced finishing techniques in jeans production. Learn
Our commitments s.Oliver Group - [Company
History](https://soliver-group.com/en/soliver-group/company-history/) -
Figures](https://soliver-group.com/en/soliver-group/facts-and-figures/)
Career - [Career
Germany](https://soliver-group.com/en/career/de/?mreset=1&cHash=b24bfc03b1d2bd64624b9ca8d3cfd804)
Benelux](https://soliver-group.com/en/career/bl/?mreset=1&cHash=b24bfc03b1d2bd64624b9ca8d3cfd804)
Austria](https://soliver-group.com/en/career/at/?mreset=1&cHash=b24bfc03b1d2bd64624b9ca8d3cfd804)
Brands - [s.Oliver](https://soliver-group.com/en/brands/soliver/) -
LABEL](https://soliver-group.com/en/brands/soliver-black-label/) - [Q/S
designed by](https://soliver-group.com/en/brands/qs-designed-by/) -
identity](https://soliver-group.com/en/brands/comma-casual-identity/) -
BERLIN](https://soliver-group.com/en/brands/liebeskind-berlin/)
Responsibility -
Press - [Press
Releases](https://soliver-group.com/en/press/press-releases/) - [Press
Contacts](https://soliver-group.com/en/press/press-contacts/) ©2020
s.Oliver Bernd Freier GmbH & Co. KG -
