Wie Sie diese Datenschutzerklärung lesen können:
Wir bieten Ihnen verschiedene Möglichkeiten, diese Datenschutzerklärung
zu lesen. Zunächst finden Sie in diesem Abschnitt einige ganz
grundlegende Informationen. Anschließend haben wir diese
Datenschutzerklärung nach für Sie relevanten Themen sortiert und
entsprechend in einzelne Kapitel unterteilt.
Wir haben jedem Kapitel einen kurzen Überblickstext vorangestellt. In
Sofern Sie sich lediglich einen schnellen Gesamtüberblick über alle
Datenverarbeitungen verschaffen möchten, empfiehlt es sich, die
machen möchten, können Sie unterhalb des jeweiligen Überblickstextes den
vollständigen Inhalt des Kapitels lesen.
Wir haben so oft wie möglich auf Querverweise verzichtet. Dadurch
erhalten Sie alle Informationen zusammenhängend erklärt, egal in welchem
Kapitel Sie sich gerade bewegen. Wenn Sie diese Datenschutzerklärung von
Anfang an bis zum Ende lesen, werden Sie dadurch jedoch ggf.
Textwiederholungen feststellen.
Was Sie in dieser Datenschutzerklärung erfahren:
Falls Sie eine Frage zu dieser Datenschutzerklärung oder allgemein zum
Thema Datenschutz bei Zalando haben, können Sie sich jederzeit an unsere
Datenschutzbeauftragte oder den Zalando Kundenservice wenden.
1. Welche Daten verarbeitet Zalando?
Zalando bietet Ihnen verschiedenste Services, die Sie auch auf
unterschiedliche Weise nutzen können. Je nachdem, ob Sie online,
telefonisch, persönlich oder auf andere Weise mit uns in Kontakt treten
und welche Services Sie nutzen, fallen dabei verschiedene Daten aus
unterschiedlichen Quellen an. Viele der von uns verarbeiteten Daten
teilen Sie uns selbst mit, wenn Sie unsere Services nutzen oder mit uns
in Kontakt treten, beispielsweise wenn Sie sich registrieren und dafür
Ihren Namen oder Ihre E-Mail-Adresse oder Ihre Anschrift angeben. Wir
Interaktion mit unseren Services automatisch von uns erfasst werden.
Dabei kann es sich beispielsweise um Informationen darüber handeln,
Dies sind alle Informationen, mit denen wir Sie sofort oder durch
Kombination mit anderen Informationen identifizieren könnten. Beispiele:
Ihr Name, Ihre Telefonnummer oder Ihre E-Mail-Adresse. Alle
Informationen, mit denen wir Sie nicht (auch nicht durch Kombination mit
anderen Informationen) identifizieren können, gelten als
nicht-personenbezogene Daten. Nicht-personenbezogene Daten werden auch
als anonyme Daten bezeichnet. Wenn wir Ihre personenbezogenen Daten mit
anonymen Daten kombinieren, gelten alle Daten in diesem Datensatz als
personenbezogene Daten. Wenn wir aus einer Information oder einem
Datensatz zu Ihrer Person die personenbezogenen Daten löschen, dann
gelten die verbleibenden Daten in diesem Datensatz nicht mehr als
personenbezogene Daten. Dieses Verfahren wird als Anonymisierung
bezeichnet. Grundsätzlich gilt: Wenn Sie von uns aufgefordert werden,
bestimmte persönliche Informationen mitzuteilen, können Sie dies
selbstverständlich ablehnen. Sie entscheiden, welche Informationen Sie
uns geben. Möglicherweise werden wir Ihnen dann aber die gewünschten
Services nicht (oder nicht optimal) zur Verfügung stellen können. Falls
im Zusammenhang mit einem Service nur bestimmte Angaben notwendig sind
Kennzeichnung mit.
2. Wozu verwendet Zalando meine Daten?
Zalando verarbeitet Ihre Daten unter Einhaltung aller anwendbaren
Datenschutzgesetze. Dabei beachten wir selbstverständlich die Grundsätze
des Datenschutzrechts für die Verarbeitung personenbezogener Daten. Ihre
Daten verarbeiten wir daher grundsätzlich nur zu den Ihnen in dieser
Datenschutzerklärung erläuterten oder bei der Erhebung der Daten
mitgeteilten Zwecke.
In diesem Kapitel teilen wir Ihnen auch mit, auf welcher gesetzlichen
Grundlage (Rechtsgrundlage) wir zu den einzelnen Zwecken Daten
verarbeiten. Je nachdem, auf welcher Rechtsgrundlage wir Ihre Daten
Datenschutzrechten wie dem Recht auf Auskunft – besondere
Datenschutzrechte zustehen. Beispielsweise haben Sie in einigen Fällen
das Recht, der Verarbeitung Ihrer Daten zu widersprechen. Weitere
Information finden Sie unter _7. Welche Datenschutzrechte habe ich?_
Wir verwenden Ihre Daten für die Bereitstellung der Zalando Websites.
Neben den Geräte- und Zugriffsdaten, die bei jeder Nutzung dieser
Services anfallen, hängen die Art der verarbeiteten Daten sowie die
Verarbeitungszwecke insbesondere davon ab, wie Sie die über unsere
verwenden wir die bei der Nutzung unserer Services anfallenden Daten, um
herauszufinden, wie unser Online-Angebot genutzt wird. Wir nutzen diese
Informationen unter anderem für die Verbesserung unserer Services.
4. Infos zu Social-Media-Fanpages
Zalando unterhält Social-Media-Profile bei den sozialen Netzwerken von
veröffentlichen und teilen wir regelmäßig Inhalte, Angebote und
Produktempfehlungen. Bei jeder Interaktion auf unseren Fanpages oder
anderen Facebook- oder Instagram-Webseiten erfassen die Betreiber der
den Interessen und demographischen Merkmalen (etwa Alter, Geschlecht,
Region) des Fanpage-Publikums einsehen. Wenn Sie soziale Netzwerke
nutzen, werden die Art, der Umfang und die Zwecke der Verarbeitung der
Daten in sozialen Netzwerken in erster Linie von den Betreibern der
sozialen Netzwerke festgelegt.
5. Newsletter
Wir bieten Ihnen verschiedene Newsletter-Services an. Informationen zu
den von den einzelnen Newslettern behandelten Themen und weitere
Hinweise erhalten Sie, wenn Sie sich für einen Newsletter-Service
anmelden. Bei der Nutzung unserer Newsletter erfassen wir auch Geräte-
6. An wen werden meine Daten weitergegeben?
Zalando gibt Ihre Daten nur weiter, wenn das nach deutschem oder
europäischem Datenschutzrecht erlaubt ist. Wir arbeiten mit einigen
Dienstleistern besonders eng zusammen, etwa im Bereich Kundenservice
Betrieb von Rechenzentren). Diese Dienstleister dürfen Ihre Daten
grundsätzlich nur unter besonderen Bedingungen in unserem Auftrag
verarbeiten. Soweit wir diese als Auftragsverarbeiter einsetzen,
erhalten die Dienstleister nur in dem Umfang und für den Zeitraum
Zugriff auf Ihre Daten, wie dies für die Erbringung der jeweiligen
Leistung erforderlich ist.
7. Welche Datenschutzrechte habe ich?
Sie haben unter den jeweiligen gesetzlichen Voraussetzungen die
folgenden gesetzlichen Datenschutzrechte: Recht auf Auskunft (Artikel 15
DSGVO), Recht auf Löschung (Artikel 17 DSGVO), Recht auf Berichtigung
DSGVO), Recht auf Datenübertragbarkeit (Artikel 20 DSGVO),
Beschwerderecht bei einer Datenschutzaufsichtsbehörde (Artikel 77
DSGVO), Recht auf Widerruf von Einwilligungen (Artikel 7 Absatz 3 DSGVO)
sowie das Recht auf Widerspruch gegen bestimmte
Datenverarbeitungsmaßnahmen (Artikel 21 DSGVO). Die Kontaktdaten für
Ihre Anträge finden Sie unter „Ansprechpartner“.
8. Wann werden meine Daten gelöscht?
Wir werden Ihre personenbezogenen Daten so lange speichern, wie es für
die in dieser Datenschutzerklärung genannten Zwecke notwendig ist,
insbesondere zur Erfüllung unserer vertraglichen und gesetzlichen
Verpflichtungen. Ggf. werden wir Ihre personenbezogenen Daten auch zu
anderen Zwecken speichern, wenn bzw. solange das Gesetz uns die weitere
Speicherung für bestimmte Zwecke erlaubt, einschließlich für die
Verteidigung von Rechtsansprüchen. 
Wenn Sie Ihr Kunden-Konto schließen, werden wir alle über Sie
gespeicherten Daten löschen. Falls eine vollständige Löschung Ihrer
Daten aus gesetzlichen Gründen nicht möglich oder nicht erforderlich
ist, werden die betreffenden Daten für die weitere Verarbeitung
gesperrt.
Was meint Sperrung?
Wenn Daten gesperrt werden, wird durch eine Beschränkung der
Zugriffsrechte und andere technische und organisatorische Maßnahmen
sichergestellt, dass nur wenige Mitarbeiter auf die betreffenden Daten
zugreifen können. Diese Mitarbeiter dürfen die gesperrten Daten dann
Steuerprüfung für die Vorlage gegenüber dem Finanzamt).
Von der Löschung kann in den gesetzlich erlaubten Fällen abgesehen
werden, soweit es sich um anonyme oder pseudonymisierte Daten handelt
und die Löschung die Verarbeitung für wissenschaftliche Forschungszwecke
oder für statistische Zwecke unmöglich macht oder ernsthaft
beeinträchtigen würde.
9. Wie schützt Zalando meine Daten?
Ihre persönlichen Daten werden bei uns sicher durch Verschlüsselung
Wir bedienen uns dabei des Codierungssystems SSL (Secure Socket Layer).
Des Weiteren sichern wir unsere Websites und sonstigen Systeme durch
technische und organisatorische Maßnahmen gegen Verlust, Zerstörung,
Zugriff, Veränderung oder Verbreitung Ihrer Daten durch unbefugte
Personen ab.
10. Änderungen dieser Datenschutzerklärung und Ansprechpartner
verbessern, können Änderungen dieser Datenschutz-Hinweise erforderlich
werden. Daher empfehlen wir Ihnen, sich diese Datenschutzerklärung von
Zeit zu Zeit erneut durchzulesen.
Unter datenschutz@zalando.de können Sie jederzeit unser Datenschutz-Team
für allgemeine Fragen zum Datenschutz sowie zur Durchsetzung Ihrer
Rechte kontaktieren.
Für direkten Kontakt zu unserer Datenschutzbeauftragten richten Sie Ihr
Anliegen bitte postalisch an die unten genannte Adresse mit dem Hinweis
Valeska-Gert-Straße 5
Telefax: +49 (0)30 2759 46 93
E-Mail: datenschutz@zalando.de
show share options
Logo
Bereiche
Zalando im Web
