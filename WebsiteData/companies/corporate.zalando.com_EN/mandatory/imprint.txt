Logo
Meta navigation EN left
Valeska-Gert-Straße 5
Fax: +49 (0)30 2759 46 93
E-mail: legalnotice@zalando.co.uk
Notifications of major holdings (Sec. 33, 38, 39 WpHG): per MVP
procedure (+ adhoc@zalando.de)
Notifications of directors' dealings (Art.19 MAR): +49 (0)30 2000889349
MANAGEMENT BOARD:
Robert Gentz & David Schneider (both co-Chairs of the Board),
Dr. Astrid Arndt, James Freeman II, David Schröder 
CHAIRPERSON OF THE SUPERVISORY BOARD:
Responsible for editorial content: Robert Gentz
Registered with: Amtsgericht Charlottenburg, HRB 158855 B
VAT registration number: DE 260543043
WEEG-Reg.-No GER: 72754189
Logo
