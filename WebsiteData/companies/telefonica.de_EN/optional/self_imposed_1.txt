customers - Network - Investors - Partners - Responsibility - Careers -
Press - DE - You are here: [Telefónica Deutschland](/) -
protection](/responsibility/environmental-and-climate-protection.html)
Menu [Responsibility](/responsibility.html)[Human
rights](/responsibility/human-rights.html)[Responsible
business](/responsibility/responsible-business.html)[Strengthening life
in the digital
world](/responsibility/strengthening-life-in-the-digital-world.html)[Environmental
and climate
protection](/responsibility/environmental-and-climate-protection.html)[Table
of key figures](/responsibility/table-of-key-figures.html) - [Human
rights](/responsibility/human-rights.html) - [Responsible
business](/responsibility/responsible-business.html) - [Strengthening
life in the digital
world](/responsibility/strengthening-life-in-the-digital-world.html) -
protection](/responsibility/environmental-and-climate-protection.html) -
Environmental and climate protection
page]](//app-eu.readspeaker.com/cgi-bin/rsent?customerid=4522&lang=en_us&url=https%3A%2F%2Fwww.telefonica.de%2Fresponsibility%2Fenvironmental-and-climate-protection.html%3Ftree_id%3D6261)
Our ambition ------------ We harness the opportunities provided by
digitalisation to cut raw-material and energy consumption. By 2020 we
aim to have reduced our direct and indirect CO₂ emissions by 11%
compared with 2015. Our commitments --------------- **Energy & CO₂
reduction** **- 40 %** | We will reduce our energy consumption per data
volume by 40 % by 2020 compared with 2015 and annually purchase from
green electricity sources 100 % of the energy we procure and control
ourselves. **Conservation of resources** **+1** | In the years leading
up to and including 2020 we will annually implement at least one further
relevant measure for protecting resources in our structures and
processes. More information ---------------- [] **CR Report 2019** Here
targets for Corporate Responsibility for the next couple of years.
English | German - © Telefónica Germany GmbH & Co. OHG - - - - - ####
Company - [Strategy & facts](/company/strategy.html) - Data protection -
Society](/company/politics-and-society.html) - [Management & Supervisory
Board](/company/management-and-supervisory-board.html) #### Network -
network](/network/fixed-network.html) #### Investors - Company - Share -
Publications - [Annual
Report](/investor-relations-en/annual-report.html) - [General
Meeting](/investor-relations-en/general-meeting.html) - Corporate
Investment /
ESG](/investor-relations-en/sustainable-investment-esg.html) -
Relations](/investor-relations-en/contact-investor-relations.html) ####
Partners - [Wholesale](/partners/wholesale.html) #### Responsibility -
business](/responsibility/responsible-business.html) - [Strengthening
life in the digital
world](/responsibility/strengthening-life-in-the-digital-world.html) -
protection](/responsibility/environmental-and-climate-protection.html) -
Press - [Press Releases](/press/press-releases.html) - Photos - Video -
Portal](/portal/privatkunden) #### [Business](/portal/geschaeftskunden)
Semantics]](http://www.w3.org/html/logo/)
