NEW REGULATION FOR THE JOURNEY HOME TO GERMANY: No quarantine obligation
any more, not even for vacations in a risk area. Non-vaccinated persons
only need a negative antigen or PCR test to enter the country.
The brands
DATA PROTECTION
Protecting your privacy and data is very important to us. We only
collect and use your data in accordance with the provisions of the
General Data Protection Regulation (EU) 2016/679 (GDPR) and the
applicable provisions. In the following, we as the controller will
explain to you what data we collect and how we process these data.
Updated: September 2018.
1. Personal data
As defined by the GDPR, personal data means any information relating to
an identified or identifiable natural person (‘data subject’); an
identifiable natural person is one who can be identified, directly or
indirectly, in particular by reference to an identifier such as a name,
an identification number, location data, an online identifier or to one
or more factors specific to the physical, physiological, genetic,
mental, economic, cultural or social identity of that natural person.
Personal data will only be saved if it is necessary for providing the
booked services, adhering to legal guidelines or for purposes explained
in the following.
2. Anonymised data / log files / IP address
You can visit our website without providing any personal data. However,
certain anonymous data will be saved each time our website is visited,
e.g. which page or which offer was called up. This data is not personal,
however, and thus is not subject to the legal guidelines of the GDPR.
site and stores these as server log files. The following data are logged
in this manner:
source/link from which you reached the site, browser used, operating
system used, IP address used.
The data collected is used for statistical analysis and to improve the
log files at a later time should there be specific indications of
unlawful use.
Anonymous data is only collected for statistical evaluation in order to
improve our services. For further information, please take note of the
figure “Right to information / revocation rights”.
3. Purpose of collecting personal data
Personal data must however be provided if you contact us, subscribe to
our newsletter or use other services on our website that require
personal data to be given.
In accordance with legal provisions, we will normally only collect data
necessary for the service you require. If our forms ask you to provide
further information, this is always voluntary and will be marked as
such.
The temporary storage of the IP address by the system is necessary in
order to enable the delivery of the website to the user’s computer. To
do this, the IP address of the user must be stored for the duration of
the session. This is also stored in log files in order to ensure the
functionality of the website. The data is also used to optimise the
website and to ensure the security of our information technology
systems. In this connection, the data will not be analysed for marketing
purposes. Our legitimate interests in data processing according to point
We also use the personal data that we have collected to improve customer
relations and carry out our own advertising and marketing initiatives.
4. Legal basis for the processing of personal data
If we obtain the consent of the data subject for all processing of
personal data, the legal basis shall be point (a) of Article 6(1) of the
General Data Protection Regulation (GDPR).
For the processing of personal data required for the performance of a
contract to which the data subject is a party, the legal basis is point
required to perform pre-contractual measures.
If the processing of personal data is required in order to fulfil a
legal obligation to which our company is subject, the legal basis shall
be point (c) of Article 6(1) GDPR.
In the event that vital interests of the data subject or another natural
person require the processing of personal data, the legal basis shall be
point (d) of Article 6(1) GDPR.
If the processing is necessary for the purposes of the legitimate
interests pursued by our company or by a third party, except where such
interests are overridden by the interests or fundamental rights and
freedoms of the data subject, the legal basis shall be point (f) of
Article 6(1) GDPR.
5. Transfer of personal data to third parties
We will only transfer your personal data in accordance with the
applicable competition regulations and data protection regulations.
Your data will also be transferred to our subcontractors or other
service providers related to your requests (e.g. processing information
to be sent by post or email or customer service) if this is necessary
for us to fulfil our legal obligations and to provide you with the
services we owe you.
Your data will also be disclosed and sent to third parties if we are
obliged to do this by law or on the basis of a final court ruling.
You have the right to receive the personal data concerning you which you
have provided in a structured, commonly used and machine-readable
format. You also have the right to transmit this data to another
controller without being hindered by the controller to whom the personal
data was provided.
6. Storage and erasure of data
Your personal data will be stored in accordance with the purposes listed
under “Purpose of collecting personal data”. The personal data of the
data subject will be erased or blocked once the purpose ceases to apply.
Data may also be stored if this has been provided for by the European or
national legislator in Union ordinances, laws or other regulations to
which the controller is subject. The government has set down various
storage periods for data. The data will also be blocked or erased if the
retention period prescribed by the standards mentioned, unless it is
necessary to continue storing the data for the conclusion or performance
of a contract.
7. Use of cookies
We use cookies (small computer files with text information sent by the
web server to your internet browser) in order to improve your experience
when visiting our website. For example, some notices will only appear
once if you allow us to use cookies. Cookies also have an expiry date.
If you delete your cookies manually before their expiry, you will
receive a new one upon your next visit to the site if you do not block
the storage of cookies.
The technical specifications only allow cookies to be read by the server
that sent them. We assure you that no personal data will be stored in
cookies.
Unfortunately, you will only have limited use of our services if you do
not agree to the use of cookies. We therefore recommend you permanently
activate the cookies when using our website. Most internet browsers
automatically accept cookies. You can, however, deactivate cookies and
set up your internet browser to inform you as soon as cookies are sent.
The legal basis for the processing of personal data when using cookies
is point (f) of Article 6(1) GDPR.
The legal basis for the processing of personal data when using cookies
for analytical purposes is point (a) of Article 6(1) GDPR if the user
has given consent.
8. Use of Google Analytics
Google, Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA
on servers in the United States. Google will use this information for
relating to website activity and internet usage. Google may also
transfer this information to third parties where required to do so by
law, or where such third parties process the information on Google’s
behalf. Google will not associate your IP address with any other data
held by Google. You may refuse the use of cookies by selecting the
appropriate settings on your browser, however please note that if you do
this you may not be able to use the full functionality of this website.
By using this website, you consent to the processing of data about you
by Google in the manner and for the purposes set out above.
function, which means that IP addresses are only processed in a
shortened form in order to prevent direct personal identification. You
may object to the collection and storage of data at any time for the
tracking. In order to make a permanent objection, the browser used must
accept cookies. Alternatively, the data collection can be refused by
using a Google browser plug-in which prevents the information collected
by cookies (including your IP address) from being sent to Google Inc.
and used by Google Inc. You can find the relevant plug-in under the
following link: https://tools.google.com/dlpage/gaoptout?hl=en
9. Use of Google Fonts
We use Google Fonts and the Google Fonts API to display fonts and
symbols visually. When using Google Fonts, Google also collects,
processes and uses data about the use of the fonts functions by visitors
to the website, unless the data is stored on the local servers of our
online presence. Further information about data processing by Google can
be found in Google's privacy policy at
https://policies.google.com/privacy?hl=en. There you can also change
your settings in the data protection center so that you can manage and
protect your data. You can find the Google Fonts terms of use at
https://fonts.google.com/about# and
https://policies.google.com/terms?hl=en
10. Using Google Maps
We use Google Maps and the Google Maps API to display maps and locations
visually. When using Google Maps, Google also collects, processes and
uses data about the use of the fonts functions by visitors to the
website, unless the data is stored on the local servers of our online
presence. Further information about data processing by Google can be
found in Google's privacy policy at
http://www.google.com/privacypolicy.html. There you can also change your
settings in the data protection center so that you can manage and
protect your data. You can find the Google Maps terms of use
at https://www.google.com/maps/about/ and
https://policies.google.com/terms?hl=en
11. Web analysis with Adobe Analytics
This website uses Adobe Analytics, a web analytics service provided by
Adobe Systems Software Ireland Limited (“Adobe”). Adobe Analytics uses
cookies in order to analyse your usage of this website. If the
information on the usage of the website generated by the cookie is
transmitted to an Adobe server, the settings ensure that the IP address
collected by the cookie is anonymised before geo-localization is
performed and replaced by a generic IP address before storage. Adobe
will use this information on our behalf in order to analyse the use of
the website by the user in order to compile reports on website
activities and in order to provide us with more services associated with
website and Internet usage. Adobe will not associate the IP address
transmitted by your browser as part of Adobe Analytics with any other
data held by Adobe. You can prevent cookies being installed by changing
the settings of your browser software. Please note, however, that doing
this may mean you will be unable to use all the functions of this
website. You can also prevent Adobe’s collection and use of data
generated by the cookie relating to your use of the website (including
your IP address) by downloading and installing the browser plug-in
available under: http://www.adobe.com/de/privacy/opt-out.html
facebook.com, which are operated by Facebook Inc., 1601 S. California
Ave, Palo Alto, CA 94304, USA (“Facebook”). The plug-ins can be
identified by the Facebook logo (white “f” on a blue tile or a “Like”
button), or by the words “Facebook Social Plugin”. The list and
appearance of the Facebook social plug-ins can be viewed here:
https://developers.facebook.com/docs/plugins
If a website of this online presence that contains such a plug-in is
called up, the browser used will establish a direct connection with the
Facebook servers. The content of the plug-ins is transmitted directly by
Facebook to the browser used, which then integrates this in the website.
We have no control over the data that Facebook collects via this plugin
and would therefore like to clarify how we believe the system works: by
embedding the plug-in, Facebook receives the information that the
corresponding pages of our internet presence has been accessed. If you
are logged into Facebook, Facebook can associate your visit with your
Facebook account. If you interact with the plugins (such as when
pressing the “Share” button), the relevant information is transmitted
directly from your browser to Facebook and stored there. If you are not
a member of Facebook, there is still the possibility that your IP
address will be revealed to Facebook and stored there. To see the
purpose and scope of the data collection and the further processing and
use of data by Facebook and the associated rights and settings available
for the protection of your privacy, please see Facebook’s privacy
policy: http://www.facebook.com/policy.php
If you are a Facebook member and do not wish Facebook to collect data
about you via our website and link to your member data in Facebook, you
must log out from Facebook prior to visiting this online presence. It is
also possible to block Facebook Social Plugins with add-ons for the
browser used, such as the Facebook Blocker.
This online presence uses Facebook Pixel of Facebook Inc., 1601 S.
behaviour of users once they have viewed or clicked on a Facebook
advertisement. This procedure serves to analyse the effectiveness of
Facebook advertisements for statistical and market research purposes,
and can help optimise future advertisement measures.
The data collected is anonymous to us, and cannot be used to identify
the user. However, the data is stored and processed by Facebook in order
to enable a connection with the relevant user profile and so that
Facebook can use the data for its own advertisement purposes, in
accordance with the Facebook privacy policy
Facebook advertisements and those of its partners, including outside
Facebook. A cookie may also be stored on your computer for this purpose.
By visiting our website and our Facebook page, you consent to its use.
In order to refuse the use of cookies on the computer in general, the
internet browser can be set so that no cookies can be stored on the
computer in the future or any cookies already stored are deleted.
Disabling all cookies may mean that some functions on our internet pages
will no longer work. The user can also deactivate the use of cookies by
third part providers such as Facebook on the following website of the
Digital Advertising Alliance: http://www.aboutads.info/choices/
14. Use of the Google +1 button
network, which is operated by Google Inc., 1600 Amphitheatre Parkway,
Mountain View, CA 94043, USA. The button can be identified by the “+1”
button on a white or coloured background. If a website of this online
presence that contains such an interface is called up, the browser used
will establish a direct connection with the Google servers. The content
of the “+1” interface is transmitted directly by Google to the browser
used which then integrates this in the website. The provider therefore
has no control over the data collected by Google via the interface, but
assumes that the IP address is also recorded. To see the purpose and
scope of the data collection and the further processing and use of data
by Google and the associated rights and settings available for the
protection of your privacy, please see Googles privacy policy on the
If you are a Google Plus user and do not wish Google to collect data
about you via our website and link to your member data in Google, you
must log off from Google prior to visiting this online presence.
15. Use of Twitter’s tweet button
This online presence uses the “Tweet” interface of the social network
Twitter, which is operated by Twitter Inc., 750 Folsom Street, Suite
600, San Francisco, CA 94107, United States (“Twitter”). The button can
be identified by the dark blue bird and the word “Tweet” on the light
blue background. If a website of this online presence that contains such
an interface is called up, the browser used will establish a direct
interface is transmitted directly by Twitter to the browser used which
then integrates the Twitter message. The provider therefore has no
control over the data collected by Twitter via the interface, but
assumes that the IP address is also recorded. To see the purpose and
scope of the data collection and the further processing and use of data
by Twitter and the associated rights and settings available for the
protection of your privacy, please see Twitters privacy policy:
http://twitter.com/privacy
If you are a Twitter user and do not wish Twitter to collect data about
you via our website and link to your member data in Twitter, you must
log off from Twitter prior to visiting this online presence.
16. Use of XING social plug-ins
This online presence uses social media plug-ins by the social network
Xing, which is operated by XING AG, Dammtorstraße 29-32, 20354 Hamburg
stylised “X” of opposing arrows in green. When calling up this Internet
site, the browser will establish a brief connection with the XING
servers, by which the “XING share button” functions (particularly the
calculation/display of the meter) are performed. XING will not save any
personal data of the user when calling up this service. XING will in
particular not save any IP addresses. Furthermore, no analysis of the
usage behaviour will take place through the use of cookies in connection
with the “XING share button”. Users can find the latest privacy policy
concerning the “XING share button” and any additional information on
this internet page: https://www.xing.com/app/share?op=data_protection
17. Use of LinkedIn social plug-ins
This online presence uses plug-ins of the social network LinkedIn, which
is operated by the LinkedIn Corporation, 2029 Stierlin Court, Mountain
View, CA 94043, USA (“LinkedIn”). The LinkedIn plugins can be identified
by the corresponding logo or the “Recommend button” (“Recommend”).
Please note that the plug-in will establish a connection between your
relevant internet browser and the LinkedIn server when visiting our
Internet page. LinkedIn will be informed that this online presence was
visited with your IP address. If you click on the LinkedIn “Recommend
button” and you are also logged into your LinkedIn account, you can link
content from our internet pages to your profile page in your LinkedIn
profile. This will allow LinkedIn to associate your visit with our
internet page and your user account. You must know that we do not
receive any knowledge of the content of the data transmitted and the use
thereof by LinkedIn.
You can find further details on the collection of the data and on your
legal options and the settings available from LinkedIn at
http://www.linkedin.com/
18. Use of the Pinterest button
This online presence uses the “Pin it” interface of the social network
lettering on a white background, alternatively by a red and white “P” on
a white background.
If you click on the Pinterest “Pin it button” when you are logged onto
your Pinterest account, you can link the content of our pages to your
Pinterest profile. This allows Pinterest to associate the visit to our
pages with your user account. Please note that as the provider of the
pages, we do not have any knowledge of the content of the data
transmitted and the use thereof by Pinterest. You can find further
information on this in Pinterest’s policy privacy at:
http://pinterest.com/about/privacy
19. Use of the Instagram button
This online presence uses social media plug-ins of the social network
Instagram, which are operated by Instagram Inc., 1601 Willow Road, Menlo
Park, California, 94025, USA. The Instagram plug-in can be identified by
the “Instagram button” on our homepage.
If you press the Instagram button while you are logged into your
Instagram account, you can link content of our internet presence to your
Instagram profile. This allows Instagram to associate the visit of our
pages with your user account.
Please note that as the provider of the pages, we do not have any
knowledge of the content of the data transmitted and the use thereof by
Instagram. You can find further information on this in Instagram’s
policy privacy at: http://instagram.com/about/legal/privacy
20. Software solutions for optimising the online presence
This online presence employs various software solutions for optimising
evaluated and helpful information about the user’s requirements
collected in order on this basis to constantly improve the
user-friendliness and quality of the online presence. In order to
perform these analyses, aggregated and anonymous statistical data is
acquired. This data consists of connection and movement data without
personal reference, which are connected with the browser used, the
number of page views and visits, click behaviour and duration of the
individual visitor’s stay on the online presence. During the acquisition
and processing, the abbreviated IP address of the user may be used and
stored.
The data is especially evaluated for the following purposes:
In these procedures, so-called permanent cookies are used (see also
Section 7 above). Here too, you may at any time reject the storage of
these cookies by suitable browser settings, or only allow their storage
with your explicit consent. In addition, you have the option of
preventing use of data generally in all solutions employed at any time,
by selected the so-called “Opt-out” options offered by the software
solution used. However, if you opt out, you will, for example no longer
be provided with tailored recommendations.
Our online presence uses the following software solutions as described
above (clicking on each provider will take you to its Opt-out option):
Note:  Opting out is performed by means of a cookie. If this cookie is
deleted, a further opt-out will be necessary.
21. Use of Fullstory
Fullstory Inc., 818 Marietta Street, Atlanta, GA 30318, USA. Fullstory
records the user behaviour on our website. The records of user sessions
experience for the user. Fullstory stores and collects data in
anonymised form by means of cookies. Tracking (i.e. the capture of the
data generated by the cookie relating to the use of the website) may be
deactivated at any time. Please follow the instructions on
https://www.fullstory.com/optout
22. Use of trbo
This online presence uses the web analysis services of trbo GmbH,
Römerstrasse 6, 80801 Munich, Germany (http://www.trbo.com). Data is
acquired and stored from which user profiles are created with the use of
pseudonyms, in order to offer you personalised customer benefits. For
this purpose, cookies may be used which allow recognition of an internet
browser. These user profiles are used for analysing visitor behaviour
and are evaluated for the purpose of improving our offers and structure
according to the demand. The pseudonymised user profiles are not
coordinated with personal data of the persons concerned unless with
their specifically granted explicit consent. You may opt out of this at
any time by clicking on the following link:
https://track2.trbo.com/optout.php
23. Use of iAdvize
This online presence uses the iAdvize chat function in order to offer
you additional support facilities where necessary, for instance, for
advice. We do this to increase your satisfaction with our online
presence.
Data for the iAdvize service are stored on your computer with cookies.
Chat sessions are only temporarily stored on the iAdvize servers and are
automatically deleted at the end of the conversation.
In view of the quality level of the advice we strive to provide, it may
be necessary to transfer the content of the communication between you
and your advisor to other advisors in our company or allow it to be seen
by senior personnel. Further notes on the treatment of your data by
iAdvize can be found here:
http://www.iadvize.com/de/datenschutzerklaerung/
24. YouTube
Amphitheatre Parkway, Mountain View, CA 94043, USA. When embedding, we
use the “Extended data protection mode” so that a transfer of usage
information only takes place when starting the video. In this case, it
will be transmitted which specific pages of our Internet presence you
have visited and which videos are viewed. When you are logged onto your
YouTube account, you enable YouTube to associate your page access
directly with your personal profile. You can find further information on
the collection and processing of your data by YouTube in the
corresponding privacy policy www.youtube.com
If you would like to ensure that none of your data is stored by YouTube,
do not click on the embedded videos.
25. Right of access / revocation; other rights of data subjects
You have the right:
In order to withdraw your consent to data usage, to exercise your right
of access, or request the rectification, blocking or erasure or to
exercise your other rights as a data subject, please contact:
FTI Touristik GmbH
Department of Data Protection
Landsberger Straße 88
80339 Munich, Germany
Telephone: +49 (0)89 / 25 25 - 0
Email: datenschutz@fti.de
www.fti.de
These rights are of course available to you free of charge, without you
incurring higher transmission costs for your request than for the base
rates.
The Data Protection Officer of our company can be contacted as follows:
Rechtsanwalt Frank Hütten // Noll | Hütten | Dukic Rechtsanwälte GbR
c/o FTI Touristik GmbH
Landsberger Straße 88
80686 Munich, Germany
Email: dataprotectionofficer@fti.de
telephone +49 (0)89 / 25 25 - 0
In order to exercise your rights as a data subject to access,
rectification, restriction or erasure of your personal data, please do
not contact the Data Protection Officer directly, but initially contact
the department of the Data Protection Officer mentioned above, who will
process your inquiry immediately.
26. Security, questions and remarks, controller
Security also depends ultimately on your system. You should always treat
your access information as confidential, never allow your browser to
save passwords and close the browser window when you leave our website.
This makes it more difficult for third parties to access your personal
data.
Use an operating system that can manage user rights. Always set up
different user profiles, including amongst family members, and never use
the internet under administrator rights. Use security software such as
virus scanners and firewalls and always keep your system up to date.
The controller for this online presence as defined by the General Data
Protection Regulation and other national data protection laws of member
states as well as other data protection regulations.
FTI Touristik GmbH
Landsberger Straße 88
80339 Munich, Germany
Telephone: +49 (0)89 / 25 25 - 0
Email: datenschutz@fti.de
www.fti.de
Back to top
