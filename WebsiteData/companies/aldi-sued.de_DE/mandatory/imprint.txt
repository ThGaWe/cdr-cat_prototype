Angaben zum Verantwortlichen
Verantwortlich für die Datenverarbeitung sind die Gesellschaften der
Unternehmensgruppe ALDI SÜD, mit denen Ihr Unternehmen
Vertragsbeziehungen unterhält.
Verwendungszweck und Rechtsgrundlagen der Verarbeitung
Die Verarbeitung erfolgt für die folgenden Zwecke:
Die Verarbeitung erfolgt zudem, um gesetzlichen Pflichten beispielsweise
aus handels- oder steuerrechtlichen Vorschriften nachzukommen.
Sollten Sie sich an den Vertrauensanwalt wenden, erfolgt eine
Verarbeitung innerhalb der Unternehmensgruppe ALDI SÜD Deutschland nur,
sofern Sie einer Weitergabe zustimmen. Entsprechend einem internen
Löschkonzept, welches sich nach der Erforderlichkeit richtet, werden
alle personenbezogenen Daten gelöscht.
Rechtsgrundlagen der Verarbeitung sind Art. 6 Abs. 1 lit. a, b, c und f
DSGVO.
Quelle der Daten
Die Daten werden uns von Ihnen selbst, dem Unternehmen, dem Sie
angehören, oder anderen Geschäftspartnern zur Verfügung gestellt oder
stammen aus öffentlich zugänglichen Quellen.
Soweit die Unterauftragnehmer ihren Sitz in einem Drittstaat haben, wird
das angemessene Datenschutzniveau durch den Abschluss von Maßnahmen nach
Artt. 44 ff. DSGVO sichergestellt. Detaillierte Informationen erhalten
Ihre personenbezogenen Daten werden unter folgenden Voraussetzungen
gelöscht:
Deine Rechte
Du kannst jederzeit deine Rechte auf Auskunft, Berichtigung, Löschung,
Einschränkung der Verarbeitung und Datenübertragbarkeit deiner
gespeicherten Daten ausüben, sofern die Voraussetzungen nach Artt. 15
ff. DSGVO vorliegen.
DU HAST DAS RECHT, AUS GRÜNDEN, DIE SICH AUS DEINER BESONDEREN SITUATION
ERGEBEN, JEDERZEIT GEGEN DIE VERARBEITUNG DICH BETREFFENDER
PERSONENBEZOGENER DATEN, DIE AUFGRUND VON ART. 6 ABS.1 LIT. F DSGVO
ERFOLGT, WIDERSPRUCH EINZULEGEN.  
Zur Ausübung deiner Rechte kontaktiere uns bitte unter
datenschutz(at)aldi-sued.de.
Bei Zweifeln an deiner Identität haben wir das Recht, weitere
Informationen bei dir anzufordern, um deine Identität zu überprüfen. Mit
dieser Maßnahme möchten wir sicherstellen, dass deine personenbezogenen
Daten nicht an unberechtigte Personen weitergegeben oder auf deren
Verlangen hin geändert oder gelöscht werden.
Datenschutzbeauftragter der oben genannten verantwortlichen ALDI SÜD
Gesellschaften ist Herr Kay-Torsten Schuy, E-Mail:
datenschutzbeauftragter(at)aldi-sued.de. Wir weisen ausdrücklich darauf
hin, dass bei Nutzung dieser E-Mail-Adresse die Inhalte nicht
ausschließlich von unserem Datenschutzbeauftragten zur Kenntnis genommen
werden. Wenn du vertrauliche Informationen austauschen möchten, bitten
wir dich daher zunächst über diese E-Mail-Adresse um direkte
Kontaktaufnahme. 
Solltest du der Ansicht sein, dass die Verarbeitung deiner
personenbezogenen Daten gegen gesetzliche Bestimmungen verstößt, hast du
das Recht, dich an die zuständige Aufsichtsbehörde zu wenden. Du kannst
Dich hierfür an die Datenschutz-Aufsichtsbehörde des Bundeslandes
wenden, in dem du deinen Wohnsitz hast oder an die Behörde desjenigen
Bundeslandes, in der die Verantwortliche ihren Sitz hat. Hinsichtlich
unserer Online-Präsenz ist dies die Landesbeauftragte für Datenschutz
und Informationsfreiheit in Düsseldorf.
Wenn du uns eine mit S/MIME verschlüsselte E-Mail schreiben möchtest,
nutze bitte  dieses Zertifikat für datenschutz(at)aldi-sued.de. Falls du
noch kein S/MIME-Zertifikat für die verschlüsselte E-Mail-Kommunikation
besitzt, findest du hier weitere Informationen zur  sicheren
E-Mail-Kommunikation mit ALDI SÜD.
INFORMATIONEN
ANMELDUNG ZUM ALDI SÜD NEWSLETTER
Jetzt für den ALDI SÜD Newsletter anmelden und keine Angebote mehr
Ich möchte News per E-Mail erhalten und bin mit der damit verbundenen
Verarbeitung meiner personenbezogenen Daten gemäß der ALDI
SÜD-Datenschutzerklärung, Abschnitt Newsletter, einverstanden.
Postfach und bestätige deine Newsletter-Anmeldung.
Eine Anmeldung zum Newsletter ist derzeit nicht möglich. Bitte versuche
es später noch einmal. Eine Anmeldung zum Newsletter ist derzeit nicht
möglich.
Folge uns hier
Copyright © ALDI SÜD 2020
E-Mail-Adresse und Passwort stimmen nicht überein. Bitte überprüfe deine
Eingabe.
Dein Kundenkonto wurde noch nicht aktiviert
Dein Kundenkonto wurde leider noch nicht aktiviert
Du wurdest aus Sicherheitsgründen wegen Inaktivität abgemeldet. Bitte
E-Mail-Adresse
Passwort
Angemeldet bleiben
Passwort vergessen?
ODER
Vorname
E-Mail-Adresse {{ modal_disabled_email|getEmailInput(modal_email, "",
verwenden" ) }}
Passwort vergessen
Ich möchte News per E-Mail erhalten.
Ich möchte, dass die von mir abonnierten bzw. genutzten
Informationsangebote von ALDI SÜD (z. B. Webseite, App, Newsletter)
durch die Verknüpfung der von mir zur Verfügung gestellten Daten (z. B.
personalisiert werden. Ich kann meine Entscheidung im Kundenkonto
Registrieren
