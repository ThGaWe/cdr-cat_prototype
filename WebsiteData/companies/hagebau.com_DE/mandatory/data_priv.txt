hagebau Deutschland
zu hagebau Österreich wechseln
Hagebau
Profikunden
Dach + Fassade
Garten- und Landschaftsbau
Holzbau
Tiefbau + Straßenbau
Tischler + Schreiner
Trockenbau
Privatkunden
Unsere Baumärkte, Gartencenter und Fachhandelsstandorte
FLORALAND
hagebaumarkt
WERKERS WELT
hagebau Ratgeber
Boden, Wand und Decke
Energie sparen
Gartengestaltung
Hausmodernisierung
Holz im Garten
Neubau
Downloads
Themenbereich
hagebau Unternehmensgruppe
Aktuelles
hagebau in Europa
hagebau Einzelhandel
hagebau Fachhandel
Geschäftsführung und Aufsichtsrat
Leistungen
Gesellschafter werden
hagebau X
Soltauer Weg
Beteiligungen
A.R.E.N.A. SAS
Beratung- und Beteiligungsgesellschaft mbH (BBG)
hagebau connect GmbH & Co KG
hagebau IT GmbH
hagebau Logistik GmbH & Co. KG
hagebau Versicherungsdienst GmbH
Bewerbertipps
Ihre Karriere in der hagebau Zentrale
Berufserfahrene
Berufseinsteiger/Absolventen
Ausbildung
Arbeiten in der hagebau Welt
hagebau Startseite
DATENSCHUTZ
VERANTWORTLICHKEIT FÜR IHRE DATEN
Als unser Kunde erwarten Sie nicht nur von unseren Produkten und
Dienstleistungen, sondern auch bei der Verarbeitung Ihrer
personenbezogenen Daten ein hohes Maß an Qualität.
personenbezogenen Daten möglich.
Soweit personenbezogene Daten beim Besuch unserer Webseiten erhoben
werden, verarbeiten wir diese ausschließlich nach Maßgabe der
Datenschutz-Grundverordnung (VO (EU) 2016/679; DSGVO) und des
Bundesdatenschutzgesetzes vom 30. Juli 2017 (BDSG-neu), sowie des
Telemediengesetzes (TMG). Die Verarbeitung von personenbezogenen Daten
findet ausschließlich nach Maßgabe dieser Datenschutzerklärung statt.
Adresse https://www.hagebau.com. Für verlinkte Inhalte anderer Anbieter
ist die auf der verlinkten Website hinterlegte Datenschutzerklärung
maßgeblich.
Wir, das sind hagebau Handelsgesellschaft für Baustoffe mbH & Co. KG,
Celler Str. 47, 29614 Soltau vertreten durch die Geschäftsführung, siehe
Impressum, und unsere Dienstleister, die Ihre Daten in unserem Auftrag
für die unten angegebenen Zwecke verarbeiten (im Folgenden: hagebau,
wir).
Sie erreichen uns neben der oben genannten Postanschrift auch per E-Mail
unter datenschutz@hagebau.com. Unser Datenschutzbeauftragter ist unter
gleicher Postanschrift und per E-Mail unter datenschutz@gdi-mbh.eu
erreichbar.
Mit den nachfolgenden Hinweisen möchten wir Sie über die Verarbeitung
DATENSICHERHEIT
Wir treffen technische und organisatorische Sicherheitsvorkehrungen, um
Ihre personenbezogenen Daten gegen zufällige oder vorsätzliche
Manipulationen, Verlust, Zerstörung oder gegen den Zugriff
unberechtigter Personen zu schützen und den Schutz Ihrer Rechte und die
Einhaltung der anwendbaren datenschutzrechtlichen Bestimmungen
Die ergriffenen Maßnahmen sollen die Vertraulichkeit, Integrität,
Verfügbarkeit und Belastbarkeit der Systeme und Dienste im Zusammenhang
mit der Verarbeitung Ihrer Daten auf Dauer sicherstellen und bei einem
physischen oder technischen Zwischenfall rasch wiederherstellen.
Diese Seite nutzt aus Sicherheitsgründen und zum Schutz der Übertragung
vertraulicher Inhalte, wie zum Beispiel Bestellungen oder Anfragen, die
Sie an uns als Seitenbetreiber senden, eine SSL-bzw.
TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran,
dass die Adresszeile des Browsers von “http://” auf “https://” wechselt
und an dem Schloss-Symbol in Ihrer Browserzeile.
Hierzu gehört auch eine Verschlüsselung Ihrer Daten. Alle Informationen,
die Sie online eingeben, werden verschlüsselt und erst dann übermittelt.
Dadurch können diese Informationen zu keinem Zeitpunkt von unbefugten
Dritten eingesehen werden (End-to-End-Verschlüsselung). Die Protokolle
authentifizieren den Kommunikationspartner und stellen die Integrität
der transportierten Daten sicher.
Unsere Datenverarbeitung und unsere Sicherheitsmaßnahmen werden
entsprechend der technologischen Entwicklung fortlaufend verbessert.
Unsere Mitarbeiter sind selbstverständlich schriftlich zur
Vertraulichkeit und zur Einhaltung der datenschutzrechtlichen
Anforderungen der DSGVO verpflichtet.
Wir weisen darauf hin, dass im Rahmen der Datenübertragung mittels
Internet Sicherheitslücken auftreten können, die sich auch nicht durch
die technische Gestaltung dieser Website verhindern lassen. Ein
lückenloser Schutz von personenbezogenen Daten ist bei der Nutzung des
Internet nicht möglich.
VERANTWORTLICHER:
hagebau
Celler Straße 47, 29614 Soltau
Telefon: +49 5191 802-0
Telefax: +49 5191 98664-028
E-Mail: internet@hagebau.com
Geschäftsführung: Jan Buck-Emden (Vorsitz), Sven Grobrügge, Torsten
Kreft
ALS DATENSCHUTZBEAUFTRAGTER IST BEI UNS BESTELLT:
Herr Dipl.-Inform. Olaf Tenti
GDI Gesellschaft für Datenschutz und Informationssicherheit mbH
Körnerstraße 45
58095 Hagen (NRW)
Telefon: +49 2331 356832-0
E-Mail: datenschutz@gdi-mbh.eu
PERSONENBEZOGENE DATEN
Personenbezogene Daten sind alle Informationen, die sich auf eine
identifizierte oder identifizierbare natürliche Person beziehen (Art.4
Nr.1 DSGVO). Hierzu gehören Daten, die Sie uns bei Nutzung unserer
Ihre IP-Adresse.
PFLICHTANGABEN
Wenn bei der Erhebung bestimmte Datenfelder als Pflichtangaben oder
Pflichtfelder bezeichnet sind und mit einem Sternchen ( * )
gekennzeichnet sind, ist die Bereitstellung dieser Daten gesetzlich oder
vertraglich vorgeschrieben, oder für den Vertragsabschluss, die
gewünschte Dienstleistung oder den angegebenen Zweck erforderlich. Eine
Bereitstellung liegt in Ihrem Ermessen. Eine Nichtbereitstellung kann
zur Folge haben, dass der Vertrag von uns nicht erfüllt bzw. die
gewünschte Dienstleistung nicht erbracht oder der angegebene Zweck nicht
erreicht werden kann.
ALLGEMEINE ANFRAGEN UND KOMMUNIKATIONEN
Wir verarbeiten Ihre z.B. in Telefonaten, über Kontaktformulare, E-Mail-
oder Post-Anfragen oder -Korrespondenzen mitgeteilten Daten zunächst zur
Beantwortung Ihrer Anfragen (Art.6 Abs.1 f DSGVO). Dies geschieht in der
Regel mit unserem Interesse einen guten Service anzubieten bzw.
verlässlich zu arbeiten. Ggfs. von uns als Pflichtangaben bezeichnete
Angaben (z.B. bei Dateneingabefeldern) sind erforderlich, um Sie
kontaktieren zu können. Die freiwillige Angabe weiterer Daten
erleichtert uns die Kommunikation mit Ihnen.
Eine Zusammenführung der Daten mit anderen auf dieser Webseite erhobenen
Daten findet nicht statt.
Das Kontaktformular wird TLS-Technik verschlüsselt gesendet. Die
Verschlüsselung dient der Verhinderung unberechtigter Zugriffe Dritter
auf Ihre persönlichen Daten.
Ihre für diese Zwecke verarbeiteten Daten speichern wir nach
Beantwortung der Kontaktanfrage im Regelfall für drei Monate für den
Fall weiterer Nachfragen, falls es sich nicht um Handels- oder
Geschäftsbriefe handelt, die wir für sechs bzw. zehn Jahre speichern (§§
147 AO, 257 HGB, Art.6 Abs.1 c DGSVO), oder falls wir nicht aus anderen
Gründen zu einer längeren Aufbewahrung verpflichtet sind (Art.6 Abs.1 c
DSGVO), bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur
Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt.
KOMMUNIKATION IM RAHMEN VON GESCHÄFTSBEZIEHUNGEN
Wir verarbeiten Ihre im Rahmen unserer geschäftlichen Kommunikation
Anliegens bzw. zur Vertragsbegründung, -durchführung und -abwicklung
Angaben sind notwendig, da sie entweder vertraglich erforderlich oder
gesetzlich vorgeschrieben sind. Die freiwillige Angabe weiterer Daten
erleichtert uns die Vertragsdurchführung.
Ihre für unsere Geschäftsbeziehung relevanten Daten speichern wir
solange ein Aufbewahrungszweck vorliegt (d.h. regelmäßig entsprechend
bestehender Gewährleistungsfristen). Entsprechende Dokumente (z.B.
Handelsbriefe, Rechnungen) werden grundsätzlich gemäß der gesetzlichen
Anforderungen nach Abschluss des Vertrages nach den gesetzlichen
Vorschriften für sechs Jahre (§ 257 HGB, Art.6 Abs.1 c DSGVO) bzw. zehn
Jahre (§ 147 AO, Art.6 Abs.1 c DSGVO) aufbewahrt.
BEWERBUNG
Auf unserer Webseite haben Sie die Möglichkeit, sich in unserem
Unternehmen auf eine angebotene Stelle oder initiativ zu bewerben.
Wenn Sie sich bei uns bewerben, verarbeiten wir diejenigen
Informationen, die wir im Rahmen des Bewerbungsverfahrens von Ihnen
erhalten, z.B. durch Bewerbungsschreiben, Lebenslauf, Zeugnisse,
Schriftverkehr, telefonische oder mündliche Angaben. Für uns von
Relevanz sind neben Ihren Kontaktdaten insbesondere Angaben zu Ihrer
Ausbildung, Ihrer Qualifikation, Ihrer Arbeitserfahrung und Ihren
Fähigkeiten. Wir werden Sie nur nach Ihrer Eignung für die jeweilige
Stelle beurteilen, sodass Sie uns kein Foto schicken müssen.
Bei der Nutzung des Bewerbungsformulars verarbeiten wir im Rahmen Ihrer
Bewerbung die von Ihnen in den Formularfelder eingegebenen Daten
Gehaltsvorstellung, weitere freiwillige Angaben) und gegebenenfalls
hochgeladene Unterlagen zum Zwecke der Bearbeitung Ihrer Bewerbung.
Schließen wir einen Anstellungsvertrag mit einem Bewerber, werden die
Beschäftigungsverhältnisses unter Beachtung der gesetzlichen
Vorschriften gespeichert.
Wird von uns kein Anstellungsvertrag mit dem Bewerber geschlossen,
erfolgt eine Löschung der Daten 6 Monate nach Ablehnung.
Für den Fall, dass Sie in eine längere Speicherung Ihrer Bewerbungsdaten
eingewilligt haben, werden wir Ihre Daten in unseren Bewerber-Pool
werden die Daten nach Ablauf von einem Jahr gelöscht.
Der Zweck der Datenverarbeitung ist die Durchführung eines
Bewerbungsverfahrens.
Rechtsgrundlage der Datenverarbeitung im Bewerbungsverfahren und als
Bestandteil der Personalakte sind § 26 Abs. 1 S. 1 BDSG und Art. 6 Abs.
1 lit. b DSGVO und, soweit Sie eine Einwilligung erteilt haben, etwa
durch Übersendung nicht für das Bewerbungsverfahren notwendiger Angaben,
Art. 6 Abs. 1 lit. a DSGVO.
Sie haben jederzeit die Möglichkeit, ihre Einwilligung zur Verarbeitung
der personenbezogenen Daten im Rahmen des Bewerbungsverfahrens zu
widerrufen, indem Sie uns eine E-Mail an die internet@hagebau.com
senden.
Bewerbungsunterlagen, die uns über die oben genannte E-Mail Adresse
zugehen, können im Bewerbungsprozess nicht berücksichtigt werden. Bitte
bewerben Sie sich ausschließlich über unser Webportal.
Berechtigtes Interesse bei Verarbeitung auf der Basis von Art. 6 Abs. 1
lit. f DSGVO ist die Verteidigung gegen Rechtsansprüche.
Unser berechtigtes Interesse an der Speicherung besteht für diesen
Zeitraum in der Geltendmachung oder Abwehr von Ansprüchen z. B. im
Rahmen einer Beweispflicht in einem Verfahren nach dem Allgemeinen
Gleichbehandlungsgesetz (AGG).
Bewerber können die "Datenschutzhinweise für Bewerber" hier einsehen.
RECAPTCHA
Dienstes kann unterschieden werden, ob eine Formulareingabe menschlicher
Herkunft ist oder durch automatisierte maschinelle Verarbeitung
missbräuchlich erfolgt. Die Datenübermittlung in die USA erfolgt auf
Grund Ihrer erteilten Einwilligung nach Art. 6 Abs. 1 S. 1 lit. a i. V.
m. Art. 49 Abs. 1 a) DSGVO.
Zur Umsetzung der Funktionalität wird Ihre Referrer-URL, IP-Adresse, das
Verhalten der Webseitenbesucher, Informationen über Betriebssystem,
Browser und Verweildauer, Cookies, Darstellungsanweisungen, Skripte,
Informationen über das Eingabeverhalten des Nutzers sowie Mausbewegungen
Google verwendet die so erlangten Informationen unter anderem zur
Digitalisierung und zur Optimierung der eigenen verschiedenen Dienste.
Die im Rahmen von „reCAPTCHA“ übermittelte IP-Adresse wird nicht mit
anderen Daten von Google zusammengeführt, außer Sie sind zum Zeitpunkt
der Nutzung des „reCAPTCHA“-Plug-ins / also zum Zeitpunkt des Besuchs
Die Nutzungsbedingungen des Dienstes „reCAPTCHA“ finden Sie unter:
https://www.google.com/intl/de/policies/privacy/.
NUTZUNGSDATENDATEN
HOSTING
Unsere Webseite wird betrieben auf Servern von PlusServer GmbH,
Hohenzollernring 72, 50672 Köln, Deutschland (Hoster).
Wenn Sie unsere Webseite aufrufen, übermitteln Sie (aus technischer
Notwendigkeit) über Ihren Internetbrowser Daten an unseren Webserver.
Folgende Daten werden während einer laufenden Verbindung zur
Ermöglichung einer Kommunikation zwischen Ihrem Internetbrowser und
unserem Webserver verarbeitet (Art.6 Abs.1 f DSGVO):
Eine temporäre Speicherung bzw. Verarbeitung dieser sog. Server-Logdaten
ist aus Gründen der Sicherstellung der Funktionsfähigkeit bzw. der
technischen Sicherheit, insbesondere zur Abwehr und Verteidigung von
Angriffs- bzw. Schädigungsversuchen, erforderlich, und erfolgt mit
unserem entsprechenden berechtigten Interesse (Art.6 Abs.1 f DSGVO).
Die Daten werden nicht mehr personenbezogen verarbeitet (d.h. werden
anonymisiert), sobald sie für die Erreichung des Zweckes ihrer Erhebung
nicht mehr erforderlich sind. Grundsätzlich werden Daten durch Kürzung
der IP-Adresse anonymisiert bzw. gelöscht, soweit diese nicht für
Dokumentationszwecke länger vorgehalten werden müssen (z.B. zur
Dokumentation einer erteilten Einwilligung, s. oben, Art.6 Abs. 1 c,
Art.5 Abs. 2, Art.7 Abs.1 DSGVO).
Die von uns/vom Hoster gespeicherte(n) Daten werden nach drei Monaten
gelöscht.
WAS SIND COOKIES UND WOFÜR WERDEN SIE VERWENDET?
Zusätzlich zu den zuvor genannten Daten werden bei Ihrer Nutzung und
Cookies sind kleine Textdateien, die von Ihrem Browser auf Ihrem
Endgerät zur Speicherung von bestimmten Informationen abgelegt werden.
Des Weiteren werden diese Cookies dazu eingesetzt, die Nutzung unseres
Angebotes für Sie angenehmer und komfortabler zu gestalten oder zu
Die meisten, der von uns verwendeten Cookies, sind so genannte
diese gesetzten Cookies automatisch von Ihrem Browser gelöscht.
Andere Cookies verbleiben auf Ihrem Rechner und bewirken, dass wir Ihr
Endgerät bei Ihrem nächsten Besuch wiedererkennen (sog. persistente oder
Bei Ihrem nächsten Aufruf unserer Webseite mit demselben Endgerät werden
die in Cookies gespeicherten Informationen entweder durch unsere
Webseite („First Party Cookie“) oder eine andere Webseite, zu der das
Diese Cookies werden nach einer voreingestellten Zeitspanne, die sich je
nach Cookie unterscheidet, automatisch von Ihrem System gelöscht.
Durch die gespeicherten und zurückgesandten Informationen erkennt die
jeweilige Webseite, dass Sie diese mit dem Browser Ihres Endgeräts
bereits aufgerufen und besucht haben.
Diese Informationen nutzen wir, um Ihnen die Webseite gemäß Ihren
Präferenzen optimal gestalten und anzeigen zu können. Dabei wird
lediglich das Cookie selbst auf Ihrem Endgerät identifiziert.
Eine darüberhinausgehende Speicherung von personenbezogenen Daten
erfolgt nur nach Ihrer ausdrücklichen Zustimmung oder wenn dies
unbedingt erforderlich ist, um den angebotenen und von Ihnen
Funktionsweise im Folgenden erläutert werden:
werden ausschließlich von uns verwendet und dienen zum Beispiel dazu,
dass Sie als angemeldeter Nutzer bei Zugriff auf verschiedene
Unterseiten unserer Webseite stets angemeldet bleiben und so nicht jedes
Mal bei Aufruf einer neuen Seite Ihre Anmeldedaten neu eingeben müssen.
Die Nutzung von unbedingt erforderlichen Cookies auf unserer Webseite
ist ohne Ihre Einwilligung möglich.
Funktionale Cookies ermöglichen unserer Webseite, bereits getätigte
Angaben (wie z.B. registrierter Name oder Sprachauswahl) zu speichern
und Ihnen darauf basierend verbesserte und persönlichere Funktionen
anzubieten. Diese Cookies sammeln und speichern ausschließlich
anonymisierte Informationen, sodass diese nicht Ihre Bewegungen auf
anderen Webseiten verfolgen können.
Rechtsgrundlage für den Einsatz dieser Cookies ist unser berechtigtes
Interesse gem. Art. 6 Abs. 1 S. 1 lit. f DS-GVO. Unser berechtigtes
Interesse ergibt sich aus den genannten Zwecken.
Sie haben jederzeit die Möglichkeit, Cookies generell in Ihrem Browser
zu deaktivieren (siehe unten).
Bei der Deaktivierung von Cookies kann jedoch die Funktionalität dieser
Marketing- und/oder Tracking-Cookies werden erst nach Ihrer aktiven
Einwilligung gesetzt.
Rechtsgrundlage für die Datenverarbeitung ist hier Art. 6 Abs. 1 S. 1
lit. a DS-GVO.
erstellen.
Sie können zudem Cookies, die für Online-Werbung verwendet werden, auch
entwickelten Tools, wie z.B. die in den USA ansässige
https://www.aboutads.info/choices/ oder die in der EU ansässige
http://www.youronlinechoices.com/uk/your-ad-choices verwalten.
widerrufen.
Sie können Ihren Browser so einstellen, dass Sie über das Setzen von
Cookies informiert werden und Cookies nur im Einzelfall, z.B.
Third-Party-Cookies (Cookies, die durch einen Dritten gesetzt werden,
also nicht durch die eigentliche Internetpräsenz, auf der Sie sich
gerade befinden), erlauben, die Annahme von Cookies für bestimmte Fälle
oder generell ausschließen, sowie das automatische Löschen der Cookies
beim Schließen des Browsers aktivieren. Sie können mithilfe Ihres
eingeschränkt sein.
DEAKTIVIERUNG VON COOKIES/WIDERRUF IHRER EINWILLIGUNG
Sie können Ihren Webbrowser so einstellen, dass er Sie beim Setzen von
Cookies benachrichtigt oder das Setzen von Cookies teilweise oder
generell verbietet, um z.B. Webtracking abzulehnen. Durch das
Verbieten/Deaktivieren können Sie auch eine erteilte Einwilligung in das
Cookies mithilfe Ihres Browsers ganz deaktivieren, dann werden aber
sein. Sie können mit Hilfe Ihres Webbrowsers gespeicherte Cookies
jederzeit löschen.
am meisten verwendeten Browser informieren:
Wenn Sie keine abweichenden Einstellungen vorgenommen haben oder
vornehmen, verbleiben Cookies, die die erforderlichen technischen
Endgerät verbleiben (maximal 6 Monate).
AUSWERTUNG MIT ETRACKER (WEBTRACKING)
Einwilligung in unserem Auftrag durch unseren Dienstleister, die
etracker GmbH (Erste Brunnenstraße 1, 20459 Hamburg, www.etracker.com),
Optimierungszwecken für uns gesammelt und gespeichert (Art.6 Abs.1 a
DSGVO). Aus diesen Daten werden unter einem Pseudonym Nutzungsprofile
erstellt. Hierzu werden auch Cookies eingesetzt. Die eingesetzten
Die dadurch erzeugten Daten werden in unserem Auftrag von etracker
ausschließlich in Deutschland verarbeitet und gespeichert und
unterliegen damit den strengen deutschen und europäischen
Datenschutzgesetzen und -standards. etracker wurde diesbezüglich
unabhängig geprüft, zertifiziert und mit dem Datenschutz-Gütesiegel
ePrivacyseal ausgezeichnet.
Die mit den etracker-Technologien erhobenen Daten werden nicht dazu
nicht mit personenbezogenen Daten über den Träger des Pseudonyms
zusammengeführt. Auch die IP-Adresse wird nur verkürzt verarbeitet,
sodass ein Rückschluss auf den einzelnen Besucher nicht möglich ist. Der
widersprochen werden. In diesem Fall wird ein
etracker-Deaktivierungs-Cookie in Ihrem Browser abgelegt
akzeptieren muss, um dieses Cookie ablegen zu können. Wenn Sie das
Deaktivierungs-Cookie löschen, müssen Sie sich hier ggfs. erneut
entscheiden.
etracker Auswertung widersprechen
Weitere Informationen zum Datenschutz bei etracker finden Sie hier.
PROFILE IN SOZIALEN NETZWERKEN
Unsere Präsenzen auf sozialen Netzwerken und Videoplattformen, die wir
im Folgenden nennen, dienen einer aktiven und zeitgemäßen Kommunikation
mit unseren Kunden und Interessenten. Wir informieren dort über unsere
Leistungen, Produkte und interessante Sonderaktionen rund um unser
Im Folgenden geben wir Ihnen die Datenschutzhinweise gem. Art. 13
Datenschutzgrundverordnung (DSGVO) zu den von uns betriebenen Social
VIMEO
Auf unserer Seite setzen wir Komponenten des Anbieters Vimeo ein.
Der Dienst wird angeboten von Vimeo LCC, 555 West 18th Street, New York,
New York 10011, USA.
Bei jedem einzelnen Aufruf unserer Webseiten, die mit einer solchen
Komponente verlinkt sind, veranlasst diese Komponente, dass der von
Ihnen verwendete Browser eine entsprechende Darstellung der Komponente
von Vimeo herunterlädt.
Wenn Sie eine solche Vimeo-Komponente auf unseren Webseiten aufrufen und
währenddessen bei Vimeo eingeloggt sind, erkennt Vimeo durch die von der
Komponente gesammelten Informationen, welche konkrete Seite Sie besuchen
und ordnet diese Informationen Ihrem persönlichen Konto bei Vimeo zu.
Drücken Sie z.B. den „Play“-Button an oder geben Sie entsprechende
Kommentare ab, werden diese Informationen an Ihr persönliches
Mitgliedskonto bei Vimeo übermittelt und dort gespeichert.
Die Datenübermittlung in die USA erfolgt auf Grund Ihrer erteilten
Einwilligung nach Art. 6 Abs. 1 S. 1 lit. a i. V. m. Art. 49 Abs. 1 a)
DSGVO.
Darüber hinaus wird die Information, dass Sie unsere Webseiten besucht
haben, an Vimeo weitergegeben. Dies geschieht unabhängig davon, ob Sie
z.B. die Komponente anklicken und Kommentare abgeben oder nicht.
Dies können Sie verhindern, indem Sie sich vor dem Besuch unserer
Weitere Informationen zum Datenschutz, insbesondere zur Erhebung und
Nutzung der Daten durch Vimeo, werden von Vimeo unter dem folgenden Link
bereitgestellt: https://vimeo.com/privacy.
Da für die Übermittlung personenbezogener Daten in die USA kein
Angemessenheitsbeschluss der EU Kommission besteht, haben wir mit Vimeo
Standard-Datenschutzklauseln im Sinne des Art. 46 Abs. 2 lit. c) DSGVO
abgeschlossen.
YOUTUBE
Die Videoplattform YouTube wird betrieben von der der Google LLC., 1600
Amphitheatre Parkway, Mountain View, CA 94043, USA, bzw. falls Sie in
Die folgenden Hinweise betreffen nicht eine etwaige direkte Einbindung
von YouTube-Videos auf unserer Webseite.
Bei dem Besuch unseres Kanals auf YouTube können Ihre Daten für
Marktforschungs- und Werbezwecke automatisch erhoben und gespeichert
werden. Aus diesen Daten werden unter Verwendung von Pseudonymen sog.
der Regel Cookies auf Ihrem Endgerät eingesetzt. Über die Funktion von
Cookies wird im Rahmen der dortigen Datenschutzbelehrung aufgeklärt,
vgl. Sie daher bitte dort die entsprechenden Hinweise. In diesen Cookies
werden das Besucherverhalten und die Interessen der Nutzer gespeichert.
Ferner erhalten wir aus den erhobenen Daten eine statistische
Auswertung, welche Personengruppen sich für unsere einzelnen bei YouTube
eingestellten Videos interessieren. Insbesondere werden uns in diesem
Zusammenhang die Aufrufzahlen und Spielzeiten von Videos,
bereitgestellt. Hierbei werden die Daten in derart anonymisierter Form,
dass ein Rückschluss auf einzelne Personen nicht möglich ist, zu
statistischen Auswertungen bereitgestellt. Die enthaltenen Angaben
erfassen etwa den ungefähren geographischen Aufenthaltsort, die
Rechtsgrundlage für die Erhebung und Verarbeitung von Daten ist Ihre
Einwilligung im Sinne des Art. 6 Abs. 1 lit. a) DSGVO, die Sie gegenüber
Google ggf. beim Aufruf der dortigen Webseite(n) gegeben haben bzw.
geben. Sie können die erteilte Einwilligung zur Datenverarbeitung
jederzeit mit Wirkung für die Zukunft widerrufen; hierzu wenden Sie sich
bitte unmittelbar an Google. Der Widerruf der Einwilligung berührt nicht
die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung.
Die detaillierten Informationen zur Verarbeitung und Nutzung der Daten
und Ihre diesbezüglichen Rechte und Einstellungsmöglichkeiten zum Schutz
Ihrer Privatsphäre, entnehmen Sie bitte den Datenschutzhinweisen von
Google, die Sie unter dem folgenden Link finden:
https://policies.google.com/privacy?hl=de&gl=de
Da für die Übermittlung personenbezogener Daten in die USA kein
Angemessenheitsbeschluss der EU Kommission besteht, haben wir mit Google
Standard-Datenschutzklauseln im Sinne des Art. 46 Abs. 2 lit. c) DSGVO
abgeschlossen.
FANPAGE BEI FACEBOOK / FACEBOOKAUFTRITT
Das soziale Netzwerk Facebook wird betrieben von der Facebook Inc., 1
Hacker Way, Menlo Park, CA 94025, USA, bzw. falls Sie in der EU Ihren
Firmensitz oder Ihren Wohnsitz haben, Facebook Irland Ltd., 4 Grand
Bei dem Besuch unserer Onlinepräsenzen in sozialen Medien können Ihre
Daten für Marktforschungs- und Werbezwecke automatisch erhoben und
gespeichert werden. Aus diesen Daten werden unter Verwendung von
werden, um z. B. Werbeanzeigen innerhalb und außerhalb der Plattformen
die Funktion von Cookies wird im Rahmen unserer Datenschutzbelehrung
aufgeklärt, vgl. Sie daher bitte dort die entsprechenden Hinweise. In
Nutzer gespeichert. Dies dient der Wahrung unserer im Rahmen einer
Interessensabwägung überwiegenden berechtigten Interessen an einer
optimierten Darstellung unserer Leistungen und Angebote sowie der
effektiven Kommunikation mit den Kunden und Interessenten.
Rechtsgrundlage für die Verarbeitung ist daher Art. 6 Abs. 1 lit. f)
DSGVO.
Rechtsgrundlage für die Erhebung und Verarbeitung von Daten ist Ihre
Einwilligung im Sinne des Art. 6 Abs. 1 lit. a) DSGVO, die Sie gegenüber
Facebook ggf. beim Aufruf der dortigen Webseite abgegeben haben bzw.
geben. Sie können die erteilte Einwilligung zur Datenverarbeitung
jederzeit mit Wirkung für die Zukunft widerrufen; hierzu wenden Sie sich
bitte unmittelbar an Facebook. Der Widerruf der Einwilligung berührt
nicht die Rechtmäßigkeit der bis zum Widerruf erfolgten
Datenverarbeitung.
Die detaillierten Informationen zur Verarbeitung und Nutzung der Daten
durch die Anbieter auf deren Seiten sowie eine Kontaktmöglichkeit und
Ihre diesbezüglichen Rechte und Einstellungsmöglichkeiten zum Schutz
Ihrer Privatsphäre, insbesondere Widerspruchsmöglichkeiten (sog.
Opt-Out), entnehmen Sie bitte den Datenschutzhinweisen von Facebook:
https://www.facebook.com/about/privacy/
Die Widerspruchsmöglichkeit (Opt-Out) finden Sie wie folgt:
https://www.facebook.com/settings?tab=ads
Die Datenverarbeitung erfolgt auf Grundlage einer Vereinbarung zwischen
gemeinsam Verantwortlichen gemäß Art. 26 DSGVO, die Sie hier einsehen
können: https://www.facebook.com/legal/terms/page_controller_addendum
LINKEDIN (UNTERNEHMENSPROFIL)
Zu Zwecken der Personalwerbung setzten wir das berufliche bzw.
Unternehmensprofil. LinkedIn wird betrieben von der der LinkedIn
Corporation, 1000 W. Maude Ave., Sunnyvale, California 94085 USA, bzw.
falls Sie in der EU Ihren Firmensitz oder Ihren Wohnsitz haben, LinkedIn
Bei dem Besuch unseres Unternehmensprofils auf LinkedIn können Ihre
Daten für Marktforschungs- und Werbezwecke sowie zur Vermittlung von für
Sie mutmaßlich interessanten Jobangeboten automatisch von LinkedIn
erhoben und gespeichert werden. Aus diesen Daten werden unter Verwendung
die Funktion von Cookies werden Sie im Rahmen der Datenschutzbelehrung
und der Cookie-Richtlinie von LinkedIn aufgeklärt, vgl. Sie daher bitte
dort die entsprechenden Hinweise. In diesen Cookies werden das
Besucherverhalten und die Interessen der Nutzer gespeichert.
Ferner erhalten wir aus den erhobenen Daten eine statistische
Auswertung, welche Personengruppen sich für unsere Unternehmensseite
interessieren. Hierbei werden die Daten in derart anonymisierter Form,
dass ein Rückschluss auf einzelne Personen nicht möglich ist, zu
statistischen Auswertungen verarbeitet, die etwa Angaben zum ungefähren
geographischen Aufenthaltsort oder der Altersgruppe und weiteren
Falls Sie von LinkedIn um eine Einwilligung (Einverständnis) in die
Datenverarbeitung gebeten werden, z. B. mit Hilfe einer Checkbox, ist
die Rechtsgrundlage der Datenverarbeitung Art. 6 Abs. 1 lit. a) DSGVO.
widerrufen, wobei Sie sich hierzu an LinkedIn wenden müssen. Bis zum
Zeitpunkt des Widerrufs erfolgte Datenverarbeitungen bleiben rechtmäßig.
Die detaillierten Informationen zur Verarbeitung und Nutzung der Daten
durch LinkedIn sowie eine Kontaktmöglichkeit und Ihre diesbezüglichen
Rechte und Einstellungsmöglichkeiten zum Schutz Ihrer Privatsphäre,
entnehmen Sie bitte den Datenschutzhinweisen von LinkedIn, die Sie unter
dem folgenden Link finden:
https://www.linkedin.com/legal/privacy-policy?src=li-other&veh=de.linkedin.com%7Cli-other
Die Cookie-Richtlinie von LinkedIn finden Sie unter dem folgenden Link:
https://www.linkedin.com/legal/cookie-policy
Die Datenverarbeitung erfolgt auf Grundlage einer Vereinbarung zwischen
gemeinsam Verantwortlichen gemäß Art. 26 DSGVO, die Sie hier einsehen
Unabhängig von den internen, zwischen uns und LinkedIn vereinbarten
Zuständigkeiten können Sie sich mit allen datenschutzrechtlichen
Anfragen sowohl an uns bzw. unseren Datenschutzbeauftragten als auch an
LinkedIn wenden.
XING
Dammtorstraße 30, 20354 Hamburg.
Wir unterhalten auf Xing eine eigene Unternehmensseite. Diese dient
einer aktiven und zeitgemäßen Ansprache potentieller Beschäftigter in
einem professionellen Umfeld. Auf dieser Seite teilen wir auch
Informationen über unser Unternehmen, und stellen uns auf diese Weise
nach außen hin dar. Wir informieren dort über unsere Leistungen,
Produkte, interessante Sonderaktionen sowie über Möglichkeiten der
Beschäftigungen in unserem Unternehmen. Dies dient der
Falls Sie von Xing um eine Einwilligung (Einverständnis) in die
Datenverarbeitung gebeten werden, z. B. mit Hilfe einer Checkbox, ist
die Rechtsgrundlage der Datenverarbeitung Art. 6 Abs. 1. UAbs. 1 lit. a)
DSGVO.
Die detaillierten Informationen zur Verarbeitung und Nutzung der Daten
durch die Anbieter auf deren Seiten sowie eine Kontaktmöglichkeit und
Ihre diesbezüglichen Rechte und Einstellungsmöglichkeiten zum Schutz
Ihrer Privatsphäre entnehmen Sie bitte den Datenschutzhinweisen von
WEITERGABE VON DATEN ÜBER XING SOWIE LINKEDIN FÜR BEWERBUNGSUNTERLAGEN
Wir nutzen die von Ihnen angegebenen und übermittelten Daten
ausschließlich für unsere Bewerbungsrecruitings. Durch das Hochladen und
Versenden Ihrer Daten an uns erklären Sie sich ausdrücklich damit
einverstanden, dass die hagebau Handelsgesellschaft für Baustoffe mbH &
Co. KG (Adresse) diese Daten zum Zwecke der Prüfung Ihrer Bewerbung und
der Benachrichtigung gem. Art. 6 Abs. 1 lit. a, b DSGVO speichert und
verarbeitet. Eine Weitergabe der Daten an Dritte findet nicht statt.
Es steht einem Bewerber jederzeit frei, die Einwilligung mit Wirkung für
21 DSGVO). Den Widerruf richten Sie an datenschutz@hagebau.com. Wenn Sie
Fragen zur Verarbeitung Ihrer personenbezogenen Daten durch uns haben,
erteilen wir Ihnen selbstverständlich gerne unter derselben
Kontaktadresse Auskunft über die Sie betreffenden Daten (Art. 15 DSGVO).
Zudem haben Sie neben dem Widerspruch bei Vorliegen der gesetzlichen
Voraussetzungen ein Recht auf Berichtigung (Art. 16 DSGVO),
Einschränkung der Verarbeitung (Art. 18 DSGVO) und das Recht der
Datenübertragbarkeit (Art. 20 DSGVO). Es steht Ihnen frei, sich bei
Beschwerden an die zuständige Datenschutzbehörde zu wenden (Art. 77
DSGVO, § 19 BDSG).
EINSATZ VON GOOGLE MAPS
von Google Maps. Diese Inhalte werden von Google Inc., 1600 Amphitheatre
ermöglichen Ihnen die komfortable Nutzung der Karten-Funktion.
Durch das Aufrufen einer Seite mit integrierter Karte von Google Maps
wird Ihre IP-Adresse erfasst. Diese Informationen werden in der Regel an
einen Server von Google in den USA übertragen und dort gespeichert.
Google erfährt Ihre IP-Adresse, selbst wenn kein Nutzerkonto besteht,
sind, kann Google Ihr Surfverhalten direkt Ihrem persönlichen Profil
zuordnen.
Durch vorheriges Ausloggen haben Sie die Möglichkeit, dies zu
verhindern. Der Anbieter dieser Seite hat keinen Einfluss auf diese
Datenübertragung.
Zugleich kann Google Cookies auf Ihrem Endgerät ablegen, sofern Sie
nicht die Verwendung von Cookies in Ihrem Browser untersagt haben, oder
Cookies auslesen. Es können auch Standortdaten erhoben werden, wenn Sie
dies in Ihrem Browser gestatten.
Die Datenübermittlung in die USA erfolgt auf Grund Ihrer erteilten
Einwilligung nach Art. 6 Abs. 1 S. 1 lit. a i. V. m. Art. 49 Abs. 1 a)
DSGVO.
Ihre IP-Adresse wird von uns erhoben, um die Übermittlung an Google zu
ermöglichen. Sie sind zur Bereitstellung dieser Daten nicht
Bereitstellung dieser Daten ist jedoch nicht möglich.
Weitere Informationen enthält die Datenschutzerklärung von Google, diese
finden Sie unter https://www.google.com/policies/privacy/.
GOOGLE WEB FONTS
Diese Seite nutzt zur einheitlichen Darstellung von Schriftarten so
genannte Web Fonts, die von Google bereitgestellt werden. Beim Aufruf
einer Seite lädt Ihr Browser die benötigten Web Fonts in ihren
Browsercache, um Texte und Schriftarten korrekt anzuzeigen.
Servern von Google aufnehmen. Hierdurch erlangt Google Kenntnis darüber,
Die Datenübermittlung in die USA erfolgt auf Grund Ihrer erteilten
Einwilligung nach Art. 6 Abs. 1 S. 1 lit. a i. V. m. Art. 49 Abs. 1 a)
DSGVO.
Wenn Ihr Browser Web Fonts nicht unterstützt, wird eine Standardschrift
von Ihrem Computer genutzt.
Weitere Informationen zu Google Web Fonts finden Sie unter
https://developers.google.com/fonts/faq und in der Datenschutzerklärung
von Google: https://www.google.com/policies/privacy/.
WAHRNEHMUNG VON DATENSCHUTZRECHTEN
Ihnen stehen die unten genannten Rechte zu. Diese können Sie uns
gegenüber geltend machen. Zur Geltendmachung nutzen Sie bitte die oben
genannten Daten oder kontaktieren Sie uns per E-Mail an:
datenschutz@hagebau.com.
Sie haben gemäß Art. 15 DSGVO das Recht, Auskunft über Ihre von uns
verarbeiteten personenbezogenen Daten zu verlangen. Insbesondere können
Sie Auskunft über die Verarbeitungszwecke, die Kategorie der
personenbezogenen Daten, die Kategorien von Empfängern, gegenüber denen
Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer,
das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der
Verarbeitung oder Widerspruch, das Bestehen eines Beschwerderechts, die
Herkunft ihrer Daten, sofern diese nicht bei uns erhoben wurden, sowie
einschließlich Profiling und ggf. aussagekräftigen Informationen zu
deren Einzelheiten verlangen;
Berichtigung:
Sie haben gemäß Art. 16 DSGVO das Recht, unverzüglich die Berichtigung
unrichtiger oder Vervollständigung Ihrer bei uns gespeicherten
personenbezogenen Daten zu verlangen;
Löschung:
Sie haben gemäß Art. 17 DSGVO das Recht, die Löschung Ihrer, bei uns
gespeicherten personenbezogenen Daten zu verlangen, soweit nicht die
Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und
Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen
des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder
Verteidigung von Rechtsansprüchen erforderlich ist;
Einschränkung der Verarbeitung:
Sie haben gemäß Art. 18 DSGVO das Recht, die Einschränkung der
Verarbeitung Ihrer personenbezogenen Daten zu verlangen, soweit die
Richtigkeit der Daten von Ihnen bestritten wird, die Verarbeitung
unrechtmäßig ist, Sie aber deren Löschung ablehnen und wir die Daten
nicht mehr benötigen, Sie jedoch diese zur Geltendmachung, Ausübung oder
Verteidigung von Rechtsansprüchen benötigen oder Sie gemäß Art. 21 DSGVO
Widerspruch gegen die Verarbeitung eingelegt haben;
Sie haben gemäß Art. 20 DSGVO das Recht, Ihre personenbezogenen Daten,
die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und
maschinenlesebaren Format zu erhalten oder die Übermittlung an einen
anderen Verantwortlichen zu verlangen;
Widerruf Ihrer Einwilligung:
Sie haben gemäß Art. 7 Abs. 3 DSGVO das Recht, Ihre einmal erteilte
Einwilligung jederzeit uns gegenüber zu widerrufen. Dies hat zur Folge,
dass wir die Datenverarbeitung, die auf dieser Einwilligung beruhte, für
Bitte richten Sie ihren Widerruf an die oben angegebenen Daten oder per
Mail an: datenschutz@hagebau.com.
Beschwerde bei einer Aufsichtsbehörde:
Sie haben gemäß Art. 77 DSGVO das Recht, sich bei einer Aufsichtsbehörde
zu beschweren. In der Regel können Sie sich hierfür an die
Aufsichtsbehörde Ihres üblichen Aufenthaltsortes oder Arbeitsplatzes
oder unseres Kanzleisitzes / Unternehmenssitzes wenden.
Widerspruchsrecht
Sofern Ihre personenbezogenen Daten auf Grundlage von berechtigten
Interessen gemäß Art. 6 Abs. 1 S. 1 lit. f DSGVO verarbeitet werden,
haben Sie das Recht, gemäß Art. 21 DSGVO Widerspruch gegen die
Verarbeitung Ihrer personenbezogenen Daten einzulegen, soweit dafür
Gründe vorliegen, die sich aus Ihrer besonderen Situation ergeben.
Möchten Sie von Ihrem Widerrufs- oder Widerspruchsrecht Gebrauch machen,
genügt eine E-Mail an: datenschutz@hagebau.com.
Von Zeit zu Zeit ist es erforderlich, den Inhalt der vorliegenden
Hinweise zum Datenschutz für zukünftig erhobene Daten anzupassen. Wir
behalten uns daher vor, diese Hinweise jederzeit zu ändern. Wir werden
die geänderte Version der Datenschutzhinweise ebenfalls an dieser Stelle
veröffentlichen. Wenn Sie uns wieder besuchen, sollten Sie sich daher
STAND DER DATENSCHUTZERKLÄRUNG
Die ständige Entwicklung des Internets macht von Zeit zu Zeit
Anpassungen unserer Datenschutzerklärung erforderlich. Wir behalten uns
vor, jederzeit entsprechende Änderungen vorzunehmen.
Kontaktinformationen
hagebau Kundenservice
Celler Str. 47, 29614 Soltau
kundenservice@hagebau.com
Wir haben Ihre Nachricht erhalten und werden sie schnellstmöglich
bearbeiten.
Bitte füllen Sie die markierten Felder aus.
Bitte überprüfen Sie die eigegebene E-Mailadresse.
Ihre E-Mail: Thema: Bitte eingeben Fragen zum Datenschutz Ihre
Nachricht:
Weitere Seiten
Online-Shops
hagebau im Social Web
verwendet. (www.gettyimages.de)
hagebau.com weiterempfehlen
