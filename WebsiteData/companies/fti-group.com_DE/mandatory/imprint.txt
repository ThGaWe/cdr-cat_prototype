Neue Regelung für die Heimreise nach Deutschland:
Keine Quarantäne-Pflicht mehr, auch nicht bei Urlaub in einem
Risikogebiet. Zur Einreise benötigen Nicht-Geimpfte nur einen negativen
Antigen- oder PCR-Test
IMPRESSUM
Anschrift:
FTI Touristik GmbH
Landsberger Straße 88
D-80339 München
Telefon: +49 (0)89 / 25 25 - 0
Telefax: +49 (0)89 / 25 25 - 65 65
E-Mail: info@fti.de
Sitz und Registergericht: München, HRB 71745
UStIDNr.: DE129351305
Geschäftsführer:
Ralph Schiller
Constantin von Bülow
TYPO3 is a free open source Content Management Framework initially
created by Kasper Skaarhoj and licensed under GNU/GPL.
TYPO3 is copyright 1998-2014 of Kasper Skaarhoj. Extensions are
copyright of their respective owners.
Information and contribution at https://typo3.org/
FTI Touristik GmbH unterstützt die TYPO3 Association
Nach oben
