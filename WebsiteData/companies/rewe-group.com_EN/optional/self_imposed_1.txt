Logo](https://www.rewe-group.com/content/themes/reweg/img/icon-rewe_group-white.svg
Lines](https://www.rewe-group.com/en/company/structure-and-saleslines/)
Board](https://www.rewe-group.com/en/company/management-and-supervisory-board/)
Relations](https://www.rewe-group.com/en/company/creditor-relations/) -
Culture](https://www.rewe-group.com/en/company/corporate-culture/) -
Projects](https://www.rewe-group.com/en/sustainability/commitments-and-projects/)
Awards](https://www.rewe-group.com/en/sustainability/cooperations-and-awards/)
Team](https://www.rewe-group.com/en/policy/purpose-and-team/) -
Centre](https://www.rewe-group.com/en/press-and-media/media-centre/) -
Contacts](https://www.rewe-group.com/en/press-and-media/media-contacts/)
Portal](https://karriere.rewe-group.com/content/careers/?utm_source=careersite&utm_campaign=konzernzentrale)
Germany](https://karriere.rewe-group.com/search/?utm_source=careersite&utm_campaign=konzernzentrale)
jobs](https://rewe-group.jobs/entdecke-rewe-group/internationale-jobchancen/)
Your collection is here. Click on the bookmark icon to save content from
Project to packaging. [teaser background image] ### Making packaging
more environmentally friendly From protection and transport through to
storage, packaging has many important functions. From an ecological
perspective however, its production and disposal can be problematic,
which is why we have resolved to eliminate or reduce packaging wherever
possible or improve its ecological design. With our packaging
initiative, we are working intensively on more sustainable and practical
options, from innovative materials through to detailed work on specific
packaging. Our goal is to achieve 100% environmentally friendly
private-label packaging by 2030. - To field of action ### Upholding
human rights Human rights are non-negotiable for the REWE Group. In a
policy statement, we set down clear rules for upholding human rights
both in our own business activities and our global supply chains. We are
committed to bolstering human rights, improving working conditions, and
promoting fair trade within the supply chains of all our private-label
products because we can only achieve success over the long term if our
business activities are in harmony with people and the environment. - To
field of action [teaser background image] Our Climate target We want to
reduce greenhouse gas emissions per square metre of retail space to
fifty percent of 2006 levels by 2022 in Germany and Austria. ###
Improving climate protection Glazed chiller cabinets, climate-friendly
coolants, LED lighting – these are just a few of the many things we have
implemented in order to save energy at our stores and warehouses. For
us, climate protection also means cutting greenhouse gas emissions in
logistics and along the supply chain. It is our aim to make a measurable
contribution to the protection of resources and to keep the impact on
the climate and the environment as low as possible. This is why we set
our first climate goals back in 2009, and modified these again in 2013.
we have introduced a wide range of initiatives for the protection and
encouragement of biodiversity along supply chains. In this, we rely on
cooperations with strategic partners, such as Naturschutzbund
Deutschland (the Nature and Biodiversity Conservation Union, or NABU).
Together, we agree on specific areas of focus, such as marine
conservation, plastic, forest conservation and paper, resource
conservation, and promoting biodiversity. - To field of action [teaser
background image] [teaser background image] ### Fair and cooperative
agriculture As a cooperatively organized company, a collaborative
relationship with our contractual partners is important to us. We have
built trusting working relationships with our farmers and suppliers on
regional and local levels with the aim of jointly contributing to
strengthening communities and protecting the environment. Our retailers,
in particular, have deep roots in their respective regions. They believe
in agriculture that is made up of small and medium-sized businesses and
operates according to environmentally and socially conscious principles.
greater sustainability Sustainability has been an important part of our
corporate identity and business strategy for over ten years. Our
strategy for greater sustainability ### Sustainability Report 2019 to
GRI standards Since 2009, our annual online sustainability report, which
is compiled in accordance with GRI sustainability reporting standards,
has documented our sustainable commitments in the four pillars of our
sustainability strategy: Green Products, Energy, Climate & Environment,
Employees, and Social Commitment. - To sustainability report #### Join
the discussion Our Twitter feed has all the latest information from the
REWE Group and the sector, with the most important developments from the
worlds of retail, e-commerce, food, CSR and policy. To Twitter feed ####
Service [Contact us](https://www.rewe-group.com/en/contacts/) [Legal
Notice](https://www.rewe-group.com/en/legal-notice/) [Privacy
Statement](https://www.rewe-group.com/en/privacy-statement/) ####
Settings Accessibility [Privacy](#cookie-settings) [Deutsche
Seite](https://www.rewe-group.com/de/) #### Social Media ####
Certifications [[Certificate]](https://www.top-employers.com/de/)
International [rewe-group.at](https://www.rewe-group.at/)
Logo](https://www.rewe-group.com/content/themes/reweg/img/icon-rewe_group-gray.svg
Group - All rights reserved ### Accessibility ##### Enlarged font sizes
