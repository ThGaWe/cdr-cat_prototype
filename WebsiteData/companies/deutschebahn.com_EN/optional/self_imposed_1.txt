Deutsche Bahn [] [] [] GB - [] DE [] Close menu - Group - Group Group -
At a glance - At a glance At a glance - Facts & Figures - Supervisory
Board of DB AG - Board of Management - Business Units - History -
History History - DB Museum - Chronology - Collections - Topics -
Compliance - Compliance Compliance - Standards - Business Partner -
Business Partner Business Partner - DB's Code of Conduct for BP -
Competition - Starke Schiene - Press - Press Press - Press releases - DB
Schenker - DB Arriva - Press services - Press services Press services -
Filming and photography permits - Investors - Sustainability -
Sustainability Sustainability - The Green Transformation - This is
green. - Dialogue - Social commitment - Social commitment Social
commitment - Introduction - Social commitment - Social commitment Social
commitment - Reading Foundation - Off Road Kids - Culture of tolerance -
Business - Business Business - Infrastructure & Energy - Infrastructure
Deutsche Bahn as a client - Deutsche Bahn as a client Deutsche Bahn as a
client - Current award procedures - Priority requirements - Central
Service - Documents - FAQ - Invoicing - Supplier hotline - SMaRT Info -
Prequalification - Prequalification Prequalification - Infrastructure -
trains and train spare parts - general demands and services - Our
Supplier Management - Our Supplier Management Our Supplier Management -
Basic principles - Supplier qualification - Supplier appraisal -
Supplier development - Sustainability - Supplier's Award - Supplier
Registration - Supplier Innovation Award - Global Accounting Shared SC -
DB Systel - DB Systemtechnik - DB Systemtechnik DB Systemtechnik -
Technical expertise - Engineering and Construction - Approval/Tests -
Publications engl.+french - DB Training - DB Training DB Training -
About us - International Expertise - DB Rail Academy - DB Rail Academy
DB Rail Academy - About us - Courses - Our References - Sales - Sales
Sales - Passenger transport vehicle - Freight transport vehicle -
Digitalization - Digitalization Digitalization - Technology - Technology
Technology - Objective - New Technology - New Technology New Technology
Smart Mobility - Smart Mobility Smart Mobility - ioki -
On-Demand-Mobility - Autonomous Public Transport - New Work - New Work
New Work - Digital Innovation Labs - DB Digital Base - Agility and
Self-Organization - DB Intrapreneurs Green Transformation at Deutsche
Bahn ===================================== x Stage ===== [With the green
transformation we are making the Deutsche Bahn even greener and more
sustainable.] Stage ===== - ![With the green transformation we are
making the Deutsche Bahn even greener and more
sustainable.](/resource/image/5957788/stage-moderne-ikone/960/520/dda1c6536d08ca0885e64bed3c2da7f/F117CB6CF43E26E0724E848C36D67769/nachhaltigkeit.jpg)
greener and more sustainable.
Green Transformation at Deutsche Bahn
just restricted to making our entire range of products and services more
environmentally friendly: It also entails improving ecological awareness
and sustainability at Deutsche Bahn and in the way we work. This is our
way of acting responsible towards the environment and society.
Hotter summers, milder winters and unprecedented downpours are just some
of the increasingly obvious signs of climate change, and they have
become a recurring challenge for us at Deutsche Bahn, as well as for our
customers. Now is the time to act so that we can leave the coming
generations with a planet worth living on. Deutsche Bahn is a company
that has taken on responsibility. We are strengthening the rail system
in Germany – for the climate, for people and for the economy. This goal
is embedded in our Strong Rail corporate strategy, and we are putting it
into practice via our Green Transformation. We know that there is only
one way to achieve climate targets in Germany and Europe: By
successfully transferring a massive volume of traffic to green rail
services. As an international provider of mobility and logistics
services, we are the key player in the shift to new forms of mobility.
We have developed the Green Transformation as a strategy to meet our
ecological and social responsibilities. It will not only help us make
our entire range of products and services more environmentally friendly,
but it will also enhance ecological awareness and sustainability at
Deutsche Bahn and in the way we work. Within this strategy, our
ecological and social obligations go hand in hand.** **The reason is
obvious: We can only reach Strong Rail's overarching goals if we focus
on people and the environment equally when making decisions. We have
been showcasing Green Transformation in our This is green. environmental
brand, which encompasses over 150 different measures. But we want to
press ahead with our transformation, so we have identified five key
areas for action: Climate protection, nature conservation, resource
protection, noise reduction and social responsibility. **Climate
protection** Everyone at Deutsche Bahn is dedicated to an ambitious
climate protection goal: Being completely climate neutral by 2040.
and EC trains within germany by 100% renewable power.
others electricity generated from wind
power]](/resource/blob/5957806/9caeef0b951d97f56bfe1d634f83aa41/climate_protection_wind_energy-data.jpg)
its fuel-saving, emission-free
design.]](/resource/blob/5957812/4ef4e1ad72b52663e69227801ebdd663/climate_protection_shuttlebus-data.jpg)
ICE, IC and EC trains within germany by 100% renewable power.
others electricity generated from wind
power]](/resource/blob/5957806/9caeef0b951d97f56bfe1d634f83aa41/climate_protection_wind_energy-data.jpg)
its fuel-saving, emission-free
design.]](/resource/blob/5957812/4ef4e1ad72b52663e69227801ebdd663/climate_protection_shuttlebus-data.jpg)
We have established a series of stages designed to help us reach our
overall objective. For example, by 2030 we want our CO2 emissions to be
at least 50% lower than the 2006 figure, and we are also working to
increase the portion of renewably generated electricity in our traction
current mix to 80%. By 2038, all of this energy will come from renewable
sources. We have already achieved this goal for our long-distance
services: Customers on these trains have been able to enjoy journeys
powered exclusively by renewable electricity since 2018. Further
information about climate protection at Deutsche Bahn is available
at [gruen.deutschebahn.com/en/strategy/climate\_protection](https://gruen.deutschebahn.com/en/strategy/climate_protection)
healthy woodlands and species-rich habitats for granted. Over the past
few years, there has been a serious decline in the variety of plants and
animals in the world around us. Insect numbers are plummeting and rising
temperatures are putting pressure on our forests and grasslands. This in
turn robs wildlife of their habitats and undermines the entire
ecosystem.     [[Boer goats serve us well by grazing on dry grassland in
the states of Hesse and
Rhineland-Palatinate.]](/resource/blob/5957790/1a4ed098c04028351a0ad14d2daad0cd/nature_conservation_boer_goats-data.jpg)
reconstruction of the woodlands'
ecosystems.]](/resource/blob/5957796/0f22c333ed20e040927cb17f50167921/nature_conservation_bergwaldprojekt-data.jpg)
not overgrow.
states of Hesse and
Rhineland-Palatinate.]](/resource/blob/5957790/1a4ed098c04028351a0ad14d2daad0cd/nature_conservation_boer_goats-data.jpg)
reconstruction of the woodlands'
ecosystems.]](/resource/blob/5957796/0f22c333ed20e040927cb17f50167921/nature_conservation_bergwaldprojekt-data.jpg)
not overgrow.
As a company, Deutsche Bahn is committed to counteracting these
developments. We know that more rail traffic means our infrastructure
also has to grow, and it is not always possible to achieve this without
consequences for the environment. It is precisely this knowledge that
makes us so aware of our duty.  Whenever we have to disturb the
environment to create new rail facilities, we also create new
habitats. We temporarily relocate animals and plants so they are not
harmed by our construction projects. Alternatively, we create a new
permanent home for them somewhere else. Goats, wild horses and water
buffalo help us by grazing selected sites so that rare plants and
animals can thrive there. Since 2010, we have planned a total of more
than 38,000 separate conservation measures or put them into action.
Further information about nature conversation at Deutsche Bahn is
available
at [gruen.deutschebahn.com/en/strategy/nature\_conservation](https://gruen.deutschebahn.com/en/strategy/nature_conservation)
generations, we must use natural resources as wisely and sparingly as
possible. For us at Deutsche Bahn, protecting resources takes the form
of scaling back consumption. Above all, this entails recycling the waste
we generate and extending the service life of our trains. [[Our reusable
cups are 100 percent recyclable, reduce rubbish and save wood, energy
and
water.]](/resource/blob/5957818/34db6af93314a07e62637732f2f574d2/resource_protection_reusable_cups-data.jpg)
publications.
energies.
and save wood, energy and
water.]](/resource/blob/5957818/34db6af93314a07e62637732f2f574d2/resource_protection_reusable_cups-data.jpg)
publications.
energies.
In recent years, our recycling rate has been consistently higher than
our set target of 95%. We are focusing on different issues so this
figure will continue to grow. Whenever possible, we recondition physical
resources so that we can reuse them. Examples include concrete ties and
sleepers, track ballast and even the paving stones used on station
platforms. At the same time, we are extending our fleet's lifespan by
redesigning our trains and upgrading their technological features. Our
restaurants and bistros increasingly serve orders using reusable items.
We are also making our offices more sustainable by expanding digital
working methods and using recycled paper. Further information about
protecting resources at Deutsche Bahn is available
at [gruen.deutschebahn.com/en/strategy/resource\_protection](https://gruen.deutschebahn.com/en/strategy/resource_protection)
activities. Strong Rail means more trains, but this will only work if
the people who live beside railway lines are willing to accept higher
rail traffic. [[Among others with noise barriers we care for more peace
and quiet at our rail
lines.]](/resource/blob/5957798/2c53c664f14368f0cef8e9b30044dc9a/noise_reduction_noise_barriers-data.jpg)
reduce the noise up to three
decibel.]](/resource/blob/5957804/c2ac5f8d3e2f1d605b6c838eec3f970d/noise_reduction_rail_web_dampers-data.jpg)
the introduction of the whisper
brake.]](/resource/blob/5957802/4620a4c431afa7092b34bd428f319fed/noise_reduction_low_noise_brake_blocks-data.jpg)
quiet at our rail
lines.]](/resource/blob/5957798/2c53c664f14368f0cef8e9b30044dc9a/noise_reduction_noise_barriers-data.jpg)
reduce the noise up to three
decibel.]](/resource/blob/5957804/c2ac5f8d3e2f1d605b6c838eec3f970d/noise_reduction_rail_web_dampers-data.jpg)
the introduction of the whisper
brake.]](/resource/blob/5957802/4620a4c431afa7092b34bd428f319fed/noise_reduction_low_noise_brake_blocks-data.jpg)
We have responded to this fact by working with the German government to
set out noise control targets for 2030/2050. Our aim for 2030 is to
reduce exposure to noise for over 800,000 trackside residents. This
figure represents over half the total number of people currently living
near our rail routes. By 2050, we will have improved the situation for
everyone who is currently exposed to railroad noise in this way. Our
noise mitigation project. By the end of 2020, we installed quiet braking
technology in all DB Cargo freight cars and completed noise control
measures on 2,000 kilometers of rail lines that were particularly
affected. Together, these two changes cut rail noise by half compared to
Bahn is available
at [gruen.deutschebahn.com/en/strategy/noise\_reduction](https://gruen.deutschebahn.com/en/strategy/noise_reduction)
strategy, we are making a clear commitment to our social responsibility
and our role in society. This commitment takes the form of ensuring
ethical, responsible conduct toward all people, combined with
technological progress and digitalization, along our value chain and
beyond. Our awareness does not just extend to our employees but also
encompasses every stage in our sustainable value creation chain. [[We
won't let good food end up in the garbage - we donate it for a good
cause.]](/resource/blob/5957864/490bb2446da539a778ad3c75953aa5fe/social_responsibility_food_donations-data.jpg)
Rwanda.]](/resource/blob/5957860/3083bc84628c7b30f32b2d8c9f3265fd/social_responsibility_db_eco-data.jpg)
at our
Umweltforum.]](/resource/blob/5957866/1fe121e9d30812b26a2662f096d1fd99/social_resposibility_stakeholder_dialogue-data.jpg)
for a good
cause.]](/resource/blob/5957864/490bb2446da539a778ad3c75953aa5fe/social_responsibility_food_donations-data.jpg)
Rwanda.]](/resource/blob/5957860/3083bc84628c7b30f32b2d8c9f3265fd/social_responsibility_db_eco-data.jpg)
at our
Umweltforum.]](/resource/blob/5957866/1fe121e9d30812b26a2662f096d1fd99/social_resposibility_stakeholder_dialogue-data.jpg)
As one of the world's leading mobility and logistics companies and the
employer of some 330,000 people, Deutsche Bahn assumes responsibility
for its actions in Germany just as we do in every other market where we
operate. Fair business practices, supply chain transparency, human
rights and digital responsibility are just some of the topics that we
actively address throughout the Group. One key component of Strong Rail
is our HR strategy, which is based on the principle of "with people and
for people." This strategy outlines four key areas for action that we
will use to get the DB Group ready for the future: Forward-looking HR
planning, innovative recruiting, a holistic approach to management,
qualifications and transformation, plus actively shaping future work
practices. Further information about social responsibility at Deutsche
Bahn is available
at [gruen.deutschebahn.com/en/strategy/social\_responsibility](https://gruen.deutschebahn.com/en/strategy/social_responsibility)
Transformation.] ### Further information about the Green Transformation
at Deutsche Bahn >[go to external
website](https://gruen.deutschebahn.com/en) +49 180 6 996633 Footer
bahn.de](https://www.bahn.de/p/view/index.shtml) ###
deutschebahn.com](/en/privacy_policy-1207606 "Data protection notice for
deutschebahn.com") [Modern Slavery Act Statement](/en/slavery-1207622
