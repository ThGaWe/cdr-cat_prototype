DATENSCHUTZINFORMATION
Im Rahmen der Nutzung der unter www.vwfs.com abrufbaren Webseiten werden
personenbezogene Daten von Ihnen durch die für die Datenverarbeitung
referenziert, verarbeitet und für die Dauer gespeichert, die zur
Erfüllung der festgelegten Zwecke und gesetzlichen Verpflichtungen
erforderlich ist. Im Folgenden informieren wir Sie darüber, um welche
personenbezogenen Daten es sich dabei handelt, auf welche Weise sie
verarbeitet werden und welche Rechte Ihnen diesbezüglich zustehen,
insbesondere im Hinblick auf die Datenschutz-Grundverordnung (EU)
2016/679 (DSGVO).
1. VERARBEITUNG PERSONENBEZOGENER DATEN UND ZWECKE DER VERARBEITUNG
BEIM BESUCH DER WEBSEITEN
sind. Wir erfahren nur den Namen Ihres Internet Service Providers, die
Website, von der aus Sie uns besuchen und die Webseiten, die Sie bei uns
besuchen. Diese Informationen werden zu statistischen Zwecken
ausgewertet, Sie bleiben als einzelner Benutzer hierbei anonym. Darüber
Analysedienste ein. Nähere Erläuterungen dazu erhalten Sie unter den
Ziffern 3 und 4 dieser Datenschutzinformation.
2. DRITTLANDÜBERMITTLUNG
Der Verantwortliche kann sich im Rahmen dieser Geschäftsbeziehung auch
Informations- und Kommunikationstechnologie) mit Sitz außerhalb des
europäischen Wirtschaftsraumes (EWR) bedienen. Die Übermittlung Ihrer
Daten erfolgt hierbei unter Einhaltung der besonderen Voraussetzungen
der Art. 44 - 49 DSGVO, wobei das angemessene Schutzniveau entweder
durch einen Angemessenheitsbeschluss der europäischen Kommission gemäß
Art. 45 DSGVO oder abgeschlossene EU-Standardvertragsklauseln gemäß Art.
46 Abs. 2 lit. c und d DSGVO gewährleistet wird. Die
Kommission abrufen und einsehen oder direkt beim Verantwortlichen
erfragen und in Kopie erhalten.
3. COOKIES
Wir setzen auf unserer Seite Cookies ein. Hierbei handelt es sich um
kleine Dateien, die Ihr Browser automatisch erstellt und die auf Ihrem
Endgerät (Laptop, Tablet, Smartphone o.ä.) gespeichert werden, wenn Sie
unsere Seite besuchen. Cookies richten auf Ihrem Endgerät keinen Schaden
an, enthalten keine Viren, Trojaner oder sonstige Schadsoftware.
In dem Cookie werden Informationen abgelegt, die sich jeweils im
Zusammenhang mit dem spezifisch eingesetzten Endgerät ergeben. Dies
bedeutet jedoch nicht, dass wir dadurch unmittelbar Kenntnis von Ihrer
Der Einsatz von Cookies dient einerseits dazu, die Nutzung unseres
Angebots für Sie angenehmer zu gestalten. So setzen wir sogenannte
Session-Cookies ein, um zu erkennen, dass Sie einzelne Seiten unserer
Website bereits besucht haben.
Darüber hinaus setzen wir ebenfalls zur Optimierung der
Benutzerfreundlichkeit temporäre Cookies ein, die für einen bestimmten
festgelegten Zeitraum auf Ihrem Endgerät gespeichert werden. Besuchen
automatisch erkannt, dass Sie bereits bei uns waren und welche Eingaben
und Einstellungen sie getätigt haben, um diese nicht noch einmal
eingeben zu müssen.
statistisch zu erfassen und zum Zwecke der Optimierung unseres Angebotes
für Sie auszuwerten (siehe Ziffer 4). Diese Cookies ermöglichen es uns,
bei einem erneuten Besuch unserer Seite automatisch zu erkennen, dass
Sie bereits bei uns waren. Diese Cookies werden nach einer jeweils
definierten Zeit, spätestens jedoch nach 24 Monaten, automatisch
gelöscht.
Die durch Cookies verarbeiteten Daten sind für die genannten Zwecke zur
Wahrung unserer berechtigten Interessen sowie der Dritter nach Art. 6
Abs. 1 S. 1 lit. f DSGVO erforderlich.
Browser jedoch so konfigurieren, dass keine Cookies auf Ihrem Computer
gespeichert werden oder stets ein Hinweis erscheint, bevor ein neuer
Cookie angelegt wird. Die vollständige Deaktivierung von Cookies kann
4. TRACKING/WEBANALYSE
Die im Folgenden aufgeführten und von uns eingesetzten
Tracking-Maßnahmen werden auf der Grundlage des Art. 6 Abs. 1 S. 1 lit.
f DSGVO durchgeführt. Mit den zum Einsatz kommenden Tracking-Maßnahmen
wollen wir eine bedarfsgerechte Gestaltung und die fortlaufende
Optimierung unserer Webseite sicherstellen. Zum anderen setzen wir die
Tracking-Maßnahmen ein, um die Nutzung unserer Webseite statistisch zu
erfassen. Weiter nutzen wir die Daten für die optimierte Ausspielung von
Werbeinhalten. Diese  Interessen sind als berechtigt im Sinne der
vorgenannten Vorschrift anzusehen. Die jeweiligen
der entsprechenden Tracking-Tools zu entnehmen.
ADOBE ANALYTICS
der Adobe Systems Software Ireland Limited, 4-6 Riverwalk, Citywest
In diesem Zusammenhang werden pseudonymisierte Nutzungsprofile erstellt
und Cookies verwendet. Die durch den Cookie erzeugten Informationen über
werden an einen Server von Adobe in Irland übertragen und dort
gespeichert.
auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen
und um weitere mit der Websitenutzung und der Internetnutzung verbundene
Dienstleistungen zu Zwecken der Marktforschung und bedarfsgerechten
Gestaltung dieser Internetseiten zu erbringen.
Auch werden diese Informationen gegebenenfalls an Dritte übertragen,
sofern dies gesetzlich vorgeschrieben ist oder soweit Dritte diese Daten
im Auftrag verarbeiten. Es wird in keinem Fall Ihre IP-Adresse mit
anderen Daten von Adobe zusammengeführt. Die IP-Adressen werden
anonymisiert, so dass eine Zuordnung nicht möglich ist (IP-Masking).
Sie können die Installation der Cookies durch eine entsprechende
Einstellung der Browser-Software verhindern. Wir weisen jedoch darauf
Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten
IP-Adresse) sowie die Verarbeitung dieser Daten durch Adobe verhindern,
indem Sie die Erfassung durch Adobe Analytics widersprechen. Es wird ein
Opt-out-Cookie gesetzt, das die zukünftige Erfassung Ihrer Daten beim
Löschen Sie die Cookies in diesem Browser, müssen Sie das Opt-out-Cookie
erneut setzen.
Finden Sie hier weitere Informationen zum Datenschutz im Zusammenhang
mit Adobe Analytics.
5. DATENSICHERHEIT
Alle von Ihnen persönlich übermittelten Daten werden mit dem allgemein
verschlüsselt übertragen. TLS ist ein sicherer und erprobter Standard,
der z.B. auch beim Onlinebanking Verwendung findet. Sie erkennen eine
sichere TLS-Verbindung unter anderem an dem angehängten s am http (also
https://..) in der Adressleiste Ihres Browsers oder am Schloss-Symbol im
unteren Bereich Ihres Browsers.
Wir bedienen uns im Übrigen geeigneter technischer und organisatorischer
Sicherheitsmaßnahmen, um Ihre Daten gegen zufällige oder vorsätzliche
Manipulationen, teilweisen oder vollständigen Verlust, Zerstörung oder
gegen den unbefugten Zugriff Dritter zu schützen. Unsere
Sicherheitsmaßnahmen werden entsprechend der technologischen Entwicklung
fortlaufend verbessert.
6. BETROFFENENRECHTE
Sie haben das Recht:
Möchten Sie von Ihren Betroffenenrechten Gebrauch machen, dann genügt
eine E-Mail an: betroffenenrechte.FSAG@vwfs.com - darüber hinausgehender
Sie haben das Recht, gemäß Art. 21 DSGVO Widerspruch gegen die
Verarbeitung Ihrer personenbezogenen Daten einzulegen, soweit dafür
Gründe vorliegen, die sich aus Ihrer besonderen Situation ergeben oder
sich der Widerspruch gegen allgemeine oder auf Sie zugeschnittene
Direktwerbung richtet. Im letzteren Fall haben Sie ein generelles
Widerspruchsrecht, das ohne Angabe einer besonderen Situation von uns
umgesetzt wird.**
VERANTWORTLICHER
Postanschrift des Verantwortlichen und des Datenschutzbeauftragten:
Volkswagen Financial Services AG
Gifhorner Straße 57
38112 Braunschweig
Möchten Sie von Ihrem Widerspruchsrecht Gebrauch machen, genügt eine
E-Mail an widerspruch.FSAG@vwfs.com
8. AKTUALITÄT UND ÄNDERUNG DIESER DATENSCHUTZINFORMATION
Diese Datenschutzinformation ist aktuell gültig und hat den Stand April
2019.
aufgrund geänderter gesetzlicher bzw. behördlicher Vorgaben kann es
notwendig werden, diese Datenschutzinformation zu ändern. Die jeweils
unter https://www.vwfs.com/data-privacy-policy.html von Ihnen abgerufen
und ausgedruckt werden.
Es ist ein technischer Fehler aufgetreten, bitte versuchen Sie es noch
einmal.
OK
