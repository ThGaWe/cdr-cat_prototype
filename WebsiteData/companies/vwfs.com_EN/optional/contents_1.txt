Notice](/en/imprint.html) - [Data Privacy
Policy](/en/data-privacy-policy.html) - [Consumer
Arbitration](/en/consumer-arbitration.html) - [Volkswagen
AG](https://www.volkswagenag.com/#) ꀲ - [Legal
Notice](/en/imprint.html) - [Data Privacy
Policy](/en/data-privacy-policy.html) - [Consumer
Arbitration](/en/consumer-arbitration.html) - [Volkswagen
AG](https://www.volkswagenag.com/#) ꀲ - [About Us](/en/about-us.html) -
AG](/en/companies/volkswagen-financial-services-ag.html) - [Volkswagen
Leasing
GmbH](/en/companies/volkswagen-financial-services-ag/volkswagen-leasing-gmbh.html)
GmbH](/en/companies/volkswagen-financial-services-ag/volkswagen-financial-services-digital-solutions-gmbh.html)
AG](/en/companies/volkswagen-financial-services-ag/volkswagen-autoversicherung-ag.html)
N.V.](/en/companies/volkswagen-financial-services-ag/volkswagen-financial-services-n-v-.html)
GmbH](/en/companies/volkswagen-financial-services-ag/volkswagen-payment-s-a-volkswagen-payment-systems-gmbh.html)
AG](/en/companies/volkswagen-financial-services-ag/volkswagen-versicherung-ag.html)
GmbH](/en/companies/volkswagen-financial-services-ag/carmobility-gmbh.html)
Greenwheels](/en/companies/volkswagen-financial-services-ag/collect-car-bv-greenwheels.html)
GmbH](/en/companies/volkswagen-financial-services-ag/euro-leasing-gmbh.html)
Fleetlogistics](/en/companies/volkswagen-financial-services-ag/fleetcompany-gmbh-fleetlogistics.html)
GmbH](/en/companies/volkswagen-financial-services-ag/logpay-financial-services-gmbh.html)
GmbH](/en/companies/volkswagen-financial-services-ag/mobility-trader-holding-gmbh.html)
Inc.](/en/companies/volkswagen-financial-services-ag/paybyphone.html) -
GmbH](/en/companies/volkswagen-financial-services-ag/voya-gmbh.html) -
GmbH](/en/companies/volkswagen-bank-gmbh/volkswagen-financial-services-digital-solutions-gmbh.html)
GmbH](/en/companies/porsche-financial-services-gmbh.html) - [VW Credit
Inc.](/en/companies/vw-credit-inc-.html) - [VW Credit Canada
Inc.](/en/companies/vw-credit-canada-inc-.html) - [Business
Fields](/en/business-fields.html) -
Mobility](/en/corporate-responsibility/empowering-social-mobility.html)
System](/en/corporate-responsibility/whistleblower-system.html) -
Financial Services
AG](/en/investor-relations/volkswagen-financial-services-ag.html) - [Key
Data](/en/investor-relations/volkswagen-financial-services-ag/key-data.html)
Reports](/en/investor-relations/volkswagen-financial-services-ag/annual-reports.html)
Reports](/en/investor-relations/volkswagen-financial-services-ag/interim-reports-and-half-yearly-financial-reports.html)
Reports](/en/investor-relations/volkswagen-financial-services-ag/disclosure-reports.html)
Publications](/en/investor-relations/volkswagen-financial-services-ag/mandatory-publications.html)
Information](/en/investor-relations/volkswagen-financial-services-ag/additional-information.html)
GmbH](/en/investor-relations/volkswagen-bank-gmbh.html) - [Key
Data](/en/investor-relations/volkswagen-bank-gmbh/key-data.html) -
Reports](/en/investor-relations/volkswagen-bank-gmbh/annual-reports.html)
Reports](/en/investor-relations/volkswagen-bank-gmbh/interim-reports-and-half-yearly-financial-reports.html)
Reports](/en/investor-relations/volkswagen-bank-gmbh/disclosure-reports.html)
Publications](/en/investor-relations/volkswagen-bank-gmbh/mandatory-publications.html)
Information](/en/investor-relations/volkswagen-bank-gmbh/additional-information.html)
GmbH](/en/investor-relations/volkswagen-leasing-gmbh.html) - [Key
Data](/en/investor-relations/volkswagen-leasing-gmbh/key-data.html) -
Reports](/en/investor-relations/volkswagen-leasing-gmbh/annual-reports.html)
Reports](/en/investor-relations/volkswagen-leasing-gmbh/interim-reports-and-half-yearly-financial-reports.html)
Information](/en/investor-relations/volkswagen-leasing-gmbh/additional-information.html)
N.V.](/en/investor-relations/volkswagen-financial-services-n-v-.html) -
Reports](/en/investor-relations/volkswagen-financial-services-n-v-/annual-reports.html)
Reports](/en/investor-relations/volkswagen-financial-services-n-v-/half-yearly-financial-reports.html)
Information](/en/investor-relations/volkswagen-financial-services-n-v-/additional-information.html)
Ltd.](/en/investor-relations/volkswagen-financial-services-australia-pty-ltd--.html)
Reports](/en/investor-relations/volkswagen-financial-services-australia-pty-ltd--/annual-reports.html)
Information](/en/investor-relations/volkswagen-financial-services-australia-pty-ltd--/additional-information.html)
Reports](/en/investor-relations/volkswagen-financial-services-australia-pty-ltd--/annual-reports1.html)
Ltd.](/en/investor-relations/volkswagen-financial-services-japan-ltd-.html)
Reports](/en/investor-relations/volkswagen-financial-services-japan-ltd-/annual-reports.html)
Information](/en/investor-relations/volkswagen-financial-services-japan-ltd-/additional-information.html)
Reports](/en/investor-relations/annual-reports.html) - [Interim Reports
Reports](/en/investor-relations/interim-reports-and-half-yearly-financial-reports.html)
Releases](/en/media/press-releases.html) -
Board](/en/media/images/management-board.html) -
AG](/en/companies/volkswagen-financial-services-ag.html) - [Volkswagen
Leasing
GmbH](/en/companies/volkswagen-financial-services-ag/volkswagen-leasing-gmbh.html)
GmbH](/en/companies/volkswagen-financial-services-ag/volkswagen-financial-services-digital-solutions-gmbh.html)
AG](/en/companies/volkswagen-financial-services-ag/volkswagen-autoversicherung-ag.html)
N.V.](/en/companies/volkswagen-financial-services-ag/volkswagen-financial-services-n-v-.html)
GmbH](/en/companies/volkswagen-financial-services-ag/volkswagen-payment-s-a-volkswagen-payment-systems-gmbh.html)
AG](/en/companies/volkswagen-financial-services-ag/volkswagen-versicherung-ag.html)
GmbH](/en/companies/volkswagen-financial-services-ag/carmobility-gmbh.html)
Greenwheels](/en/companies/volkswagen-financial-services-ag/collect-car-bv-greenwheels.html)
GmbH](/en/companies/volkswagen-financial-services-ag/euro-leasing-gmbh.html)
Fleetlogistics](/en/companies/volkswagen-financial-services-ag/fleetcompany-gmbh-fleetlogistics.html)
GmbH](/en/companies/volkswagen-financial-services-ag/logpay-financial-services-gmbh.html)
GmbH](/en/companies/volkswagen-financial-services-ag/mobility-trader-holding-gmbh.html)
Inc.](/en/companies/volkswagen-financial-services-ag/paybyphone.html) -
GmbH](/en/companies/volkswagen-financial-services-ag/voya-gmbh.html) -
GmbH](/en/companies/volkswagen-bank-gmbh/volkswagen-financial-services-digital-solutions-gmbh.html)
GmbH](/en/companies/porsche-financial-services-gmbh.html) - [VW Credit
Inc.](/en/companies/vw-credit-inc-.html) - [VW Credit Canada
Inc.](/en/companies/vw-credit-canada-inc-.html) - [Business
Fields](/en/business-fields.html) -
Mobility](/en/corporate-responsibility/empowering-social-mobility.html)
System](/en/corporate-responsibility/whistleblower-system.html) -
Financial Services
AG](/en/investor-relations/volkswagen-financial-services-ag.html) - [Key
Data](/en/investor-relations/volkswagen-financial-services-ag/key-data.html)
Reports](/en/investor-relations/volkswagen-financial-services-ag/annual-reports.html)
Reports](/en/investor-relations/volkswagen-financial-services-ag/interim-reports-and-half-yearly-financial-reports.html)
Reports](/en/investor-relations/volkswagen-financial-services-ag/disclosure-reports.html)
Publications](/en/investor-relations/volkswagen-financial-services-ag/mandatory-publications.html)
Information](/en/investor-relations/volkswagen-financial-services-ag/additional-information.html)
GmbH](/en/investor-relations/volkswagen-bank-gmbh.html) - [Key
Data](/en/investor-relations/volkswagen-bank-gmbh/key-data.html) -
Reports](/en/investor-relations/volkswagen-bank-gmbh/annual-reports.html)
Reports](/en/investor-relations/volkswagen-bank-gmbh/interim-reports-and-half-yearly-financial-reports.html)
Reports](/en/investor-relations/volkswagen-bank-gmbh/disclosure-reports.html)
Publications](/en/investor-relations/volkswagen-bank-gmbh/mandatory-publications.html)
Information](/en/investor-relations/volkswagen-bank-gmbh/additional-information.html)
GmbH](/en/investor-relations/volkswagen-leasing-gmbh.html) - [Key
Data](/en/investor-relations/volkswagen-leasing-gmbh/key-data.html) -
Reports](/en/investor-relations/volkswagen-leasing-gmbh/annual-reports.html)
Reports](/en/investor-relations/volkswagen-leasing-gmbh/interim-reports-and-half-yearly-financial-reports.html)
Information](/en/investor-relations/volkswagen-leasing-gmbh/additional-information.html)
N.V.](/en/investor-relations/volkswagen-financial-services-n-v-.html) -
Reports](/en/investor-relations/volkswagen-financial-services-n-v-/annual-reports.html)
Reports](/en/investor-relations/volkswagen-financial-services-n-v-/half-yearly-financial-reports.html)
Information](/en/investor-relations/volkswagen-financial-services-n-v-/additional-information.html)
Ltd.](/en/investor-relations/volkswagen-financial-services-australia-pty-ltd--.html)
Reports](/en/investor-relations/volkswagen-financial-services-australia-pty-ltd--/annual-reports.html)
Information](/en/investor-relations/volkswagen-financial-services-australia-pty-ltd--/additional-information.html)
Reports](/en/investor-relations/volkswagen-financial-services-australia-pty-ltd--/annual-reports1.html)
Ltd.](/en/investor-relations/volkswagen-financial-services-japan-ltd-.html)
Reports](/en/investor-relations/volkswagen-financial-services-japan-ltd-/annual-reports.html)
Information](/en/investor-relations/volkswagen-financial-services-japan-ltd-/additional-information.html)
Reports](/en/investor-relations/annual-reports.html) - [Interim Reports
Reports](/en/investor-relations/interim-reports-and-half-yearly-financial-reports.html)
Releases](/en/media/press-releases.html) -
Board](/en/media/images/management-board.html) -
customers.
environmentally compatible mobility. Our products connect people, they
help conserve resources, and they help make processes simple and
efficient. We create mobility concepts, develop solutions for fleet
management and offer attractive financial products and services.
Volkswagen Financial Services are aware of their responsibility as
lenders. That's why Volkswagen Bank GmbH has been a signatory to the
voluntary **"Responsible Lending for Consumers"** code of practice since
installment and framework credits. The company has a fundamental
interest in granting credit responsibly. High standards therefore apply
to the granting of consumer credit or the issuing of loans. The Code
provides an overview of these standards and contains a number of
consumer-friendly regulations, some of which go beyond existing
statutory requirements. Read more
TRANSPORT.]
Cost-effective, flexible, convenient and environmentally compatible:
finding a vehicle via an app, using it, and then returning it after use
is the way **car sharing works with Greenwheels** – the largest car
sharing provider in the Netherlands and a subsidiary of Volkswagen
Financial Services AG. According to a recent study, car sharing
customers make use of a broader mix of transport modes. The intermodal
flexibility preferred by Greenwheels' customers also results in a more
efficient individual CO₂ footprint. Read more
THROUGHOUT EUROPE.]
become the market leader in the domain of e-mobility by 2025 and plans
to electrify its entire model range by 2030. Volkswagen Financial
Services support this goal by offering their customers tailor-made
services like the **LogPay Charge&Fuel Card**. This combined card for
fueling and charging enables our customers to access more than 145,000
charging points throughout Europe. In addition, Volkswagen Financial
Services and the German Nature and Biodiversity Conservation Union
is to offer fleet customers all e-mobility services from a single source
and at the same time invest in climate protection projects. More
information is available at
MOBILITY IN FUTURE.]
Volkswagen Financial Services are also developing new mobility solutions
in the corporate customer segment. While leasing helps ensure that fleet
customers are always bringing the latest technology to the market as a
result of the short leasing terms in their contracts, **intermodal
mobility solutions** are also becoming increasingly relevant. Various
modes of transport (car rental, car sharing, plane, bus, train, ride
hailing, local public transport, etc.) can be intelligently linked, for
example to plan what is the most CO₂-efficient business trip. An initial
solution is scheduled for the beginning of 2021.     Read more
PARKING SPACE WILL BE AVAILABLE.]
Another important field is **Smart Parking**. Volkswagen Financial
Services and its subsidiary [PayByPhone
Inc.](https://www.paybyphone.com/) have been operating in this market
for several years. The focus here is not only on the digital payment of
parking tickets, but also on delivering solutions in the field of
predictive parking services. These can forecast the next vacant parking
space using intelligent algorithms and therefore reduce the volume of
traffic searching for parking spaces and the emissions that result –
both for cars and for trucks. Read more More Information 
Notice](/en/imprint.html) - [Data Privacy
Policy](/en/data-privacy-policy.html) - [Consumer
Arbitration](/en/consumer-arbitration.html) - [Volkswagen
AG](https://www.volkswagenag.com/#) © Volkswagen Financial Services 2021
Error ----- There was a technical error, please try again. OK
