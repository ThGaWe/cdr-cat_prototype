We use cookies to improve your experience. If you'd like to continue
exploring our site, please accept our Cookie Policy.[Read
more](/en/company/legal/privacy)
Chinese](/zh/company/sustainability/towards-circularity/our-strategy)Here
to Shop? - Company - [About us](/en/company/corporate/about-us) -
Voices](/en/company/corporate/esprit-cares/esprit-voices) - [Charity
Day](/en/company/corporate/esprit-cares/charity-day) - [YES
Center](/en/company/corporate/esprit-cares/yes-center) -
Contact](/en/company/press/press-contact) - Sustainability - [Towards
Circularity](/en/company/sustainability/towards-circularity/our-strategy)
Reports](/en/company/sustainability/towards-circularity/sustainability-reports)
Smart](/en/company/sustainability/design-smart/more-sustainable-materials)
Materials](/en/company/sustainability/design-smart/more-sustainable-materials)
Welfare](/en/company/sustainability/design-smart/animal-welfare) -
Responsibly](/en/company/sustainability/produce-responsibly/transparency)
People](/en/company/sustainability/produce-responsibly/responsible-for-people)
Act](/en/company/sustainability/produce-responsibly/uk-anti-slavery-act)
Sustainably](/en/company/sustainability/ship-sell-sustainably/carbon-footprint)
Footprint](/en/company/sustainability/ship-sell-sustainably/carbon-footprint)
Life](/en/company/sustainability/extend-product-life/care) -
and Recycle](/en/company/sustainability/reuse-recycle/recycling) -
Garments](/en/company/sustainability/reuse-recycle/collecting-garments)
Management](/en/company/sustainability/reuse-recycle/managing-the-life-cycle-of-garments)
new](/en/company/investor-relations/whats-new) - [Investor
Resources](/en/company/investor-relations/investor-resources/financial-highlights)
Highlights](/en/company/investor-relations/investor-resources/financial-highlights)
Materials](/en/company/investor-relations/investor-resources/results-materials)
Listings](/en/company/investor-relations/investor-resources/stock-listings)
Coverage](/en/company/investor-relations/investor-resources/analyst-coverage)
Contact](/en/company/investor-relations/investor-resources/investors-contact)
Reports](/en/company/investor-relations/publications/annual-interim-reports)
Reports](/en/company/sustainability/towards-circularity/sustainability-reports)
Shareholders](/en/company/investor-relations/publications/circular-to-shareholders)
Filings](/en/company/investor-relations/publications/others) -
Governance](/en/company/investor-relations/corporate-governance/introduction-governance)
Governance](/en/company/investor-relations/corporate-governance/introduction-governance)
Directors](/en/company/investor-relations/corporate-governance/board-of-directors)
Committees](/en/company/investor-relations/corporate-governance/board-committees)
Bye-laws](/en/company/investor-relations/corporate-governance/memorandum-of-association-and-new-bye-laws)
Engagement](/en/company/investor-relations/corporate-governance/shareholders-engagement)
Certificates](/en/company/investor-relations/corporate-governance/notice-of-replacement-of-the-companys-lost-share-certificates)
HGB](/en/company/investor-relations/corporate-governance/declaration-ss-289f-hgb)
Shop?](//www.esprit.com/interface/service-styleid-redirect.php?style_id=3038)
Circularity](/en/company/sustainability/towards-circularity/our-strategy)
has long been based on the linear model of take, make, waste. It’s time
to go circular. Circular fashion Design Smart More Sustainable
Produce Responsibly Transparent Supply ChainHow We Work With
Our SuppliersResponsible For The PlanetResponsible For People Ship and
Shell Sustainably Carbon FootprintReducing And Handling Waste Extend
Product's Life CareRepair Reuse and Recycle 2020 Circular Fasion
System CommiitmentRecyclingGarment CollectionManagement The Life-Cycle
of Garment Esprit was born in the 1960s, an era known for the
revolutionary spirit of its people, and the cultural shifts they
inspired. Fifty years later, we believe that now is the time for the
next revolution; a revolution in terms of how we interact with our
environment, how we support our fellow global citizens, and how we
create and care for the things we love. ### We close the loop Inclusive
circularity rests on the respectful use of both material resources and
human resources. In an inclusive, circular economy, growth will be based
on human capital instead of the extraction of natural resources. Waste
is no longer an option. Ensuring that our actions at Esprit embody this
philosophy means rethinking every step of the fashion life cycle. So we
are re-examining the way we approach design, material selection,
manufacturing, and distribution, and align with new streams to support
care, collection, resale, recycling, and reutilization. Moving towards
circularity, Esprit incorporates the United Nations Sustainable
Development Goals in our sustainability strategy. The [Sustainable
Goals](https://www.un.org/sustainabledevelopment/sustainable-development-goals/)
are an internationally recognized framework that aims to address three
elements: economic growth, social inclusion and environmental
protection. This is part of a broader endeavor to eradicate poverty and
strengthen peace and freedom. - #### Shop -
information](/en/company/legal/internet-technology-information) -
information](/en/company/legal/supplier-information) - [Corporate
Ethics](/en/company/corporate/corporate-ethics) - [Compliance
helpline](/en/company/legal/compliance-helpline) -
worldwide](/en/company/corporate/esprit-worldwide) Follow
us[](https://www.facebook.com/esprit)[](https://www.instagram.com/esprit/)[](https://www.youtube.com/user/esprit)[](https://www.snapchat.com/add/espritofficial)[](https://twitter.com/espritofficial)[](https://www.pinterest.de/espritofficial/)
us[](https://www.linkedin.com/company/esprit/)[](https://www.xing.com/companies/esprit)
