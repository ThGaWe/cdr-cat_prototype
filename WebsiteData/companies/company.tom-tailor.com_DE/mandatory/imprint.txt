Impressum - TOM TAILOR Group

IMPRESSUM


TOM TAILOR GmbH 
Garstedter Weg 14
22453 Hamburg
Deutschland

T +49 (0)40 589 56-0
F +49 (0)40 589 56-398
E info@tom-tailor.com

Vertreten durch die Geschäftsführer: 
Dr. Gernot Lenz, Christian Werner und Marcus Brüning
 

Vorsitzende des Aufsichtsrates: 
Dr. Junyang (Jenny) Shao
 

Registergericht: 
Amtsgericht Hamburg
HRB 94278
Umsatzsteuer-ID-Nr.: DE 118546414