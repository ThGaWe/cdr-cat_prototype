Logo](/xbcr/der-net/logo.png)](/de/ "Zurück zur Startseite")
Group](/de/unternehmen/der-touristik-group/) - [Vision & Mission
Statement](/de/unternehmen/der-touristik-group/vision-und-mission-statement/)
Europe](/de/unternehmen/management/management-board-central-europe/) -
Europe](/de/unternehmen/divisions/central-europe/) - [Northern
Europe](/de/unternehmen/divisions/northern-europe/) - [Eastern
Europe](/de/unternehmen/divisions/eastern-europe/) -
Group](/de/unternehmen/rewe-group/) -
Energiemanagement](/de/nachhaltigkeit/umwelt-und-natur/umweltschutz-und-energiemanagement/)
Engagement](/de/nachhaltigkeit/gesellschaftliches-engagement/) -
Engagement](/de/nachhaltigkeit/gesellschaftliches-engagement/gemeinnuetziges-engagement/)
Reisen](/de/nachhaltigkeit/kunden-und-produkte/nachhaltig-reisen/) -
Hotels](/de/nachhaltigkeit/kunden-und-produkte/zertifizierte-hotels/) -
Afrika](/de/nachhaltigkeit/kunden-und-produkte/kaza-projekt-afrika/) -
Food](/de/nachhaltigkeit/kunden-und-produkte/sustainable-food/) -
Reiseangebote](/de/nachhaltigkeit/partner-in-den-laendern/zertifizierte-reiseangebote/)
Unternehmenskultur](/de/nachhaltigkeit/mitarbeiter/vielfaeltige-unternehmenskultur/)
Ausgleich](/de/nachhaltigkeit/mitarbeiter/gesundheit-und-ausgleich/) -
Marken - - [Reiseveranstalter](/de/marken/reiseveranstalter/) -
Fluglinie](/de/marken/hotelmarken-und-fluglinie/) - [Business
Travel](/de/marken/business-travel/) - Media - -
als Arbeitgeber](/de/karriere/wir-als-arbeitgeber/) - [Familie &
Beruf](/de/karriere/wir-als-arbeitgeber/familie-und-beruf/) - [Vorsorge
Vergünstigungen](/de/karriere/wir-als-arbeitgeber/vorsorge-und-verguenstigungen/)
Gesundheit](/de/karriere/wir-als-arbeitgeber/fitness-und-gesundheit/) -
Karriereverständnis](/de/karriere/wir-als-arbeitgeber/personalentwicklung-und-karriereverstaendnis/)
Bereiche](/de/karriere/berufserfahrene/kaufmaennische-bereiche/) -
Absolventen](/de/karriere/studenten-und-absolventen/) -
Studium](/de/karriere/schueler/duales-studium/) -
Natur](/de/nachhaltigkeit/umwelt-und-natur/) >  Klimaschutz ### Unser
Klima schützen #### DER Touristik Group Maßnahmen ![Baum im
Wasser](/xbcr/der-net/Teaser_Klimaschutz_Baum_im_Wasser_696x200.jpg) ###
Eine der drängendsten Herausforderungen unserer Zeit ist der
Klimawandel. Auch im Tourismus entstehen die für den Klimawandel
mitverantwortlichen CO2-Emissionen, vor allem durch Transportmittel wie
Touristik Group arbeitet an vielen Stellschrauben, um CO2-Emissionen zu
senken, so zum Beispiel durch Umweltmanagementsysteme in den Hotels. Als
Teil der REWE Group sind die Verwaltungsstandorte und Reisebüros in
Deutschland Teil des zentralen Energiemanagementsystems des
Novair](/xbcr/der-net/NovAir_Apollo_web_rdax_300x200.JPG) ### Unsere
eigene Charter-Airline, die schwedische Fluggesellschaft Novair,
arbeitet seit Jahren konsequent daran, Emissionen und Lärm zu
reduzieren. Sie gilt heute als eine der treibstoffeffizientesten
Fluglinien in Europa. Unter anderem fliegt sie seit Sommer 2017 mit dem
neuen Airbus A321neo, einem um 20 Prozent effizienteren Flugzeug im
Vergleich zum Vorgängermodell. Auch die Kompensation von Emissionen bei
Urlaubsreisen und Geschäfts­flügen zählt zu den wesentlichen Maßnahmen
im Bereich Klimaschutz. Dabei gleichen wir die unvermeidlichen
Emissionen durch Investitionen in Klimaschutzprojekte an anderer Stelle
wieder aus. Einige Geschäftsbereiche engagieren sich hier sehr aktiv. In
der Schweiz beispielsweise besteht eine Partnerschaft zwischen DER
Touristik Suisse und myclimate. Kunden haben die Möglichkeit, die
Emissionen ihres Fluges oder ihrer Kreuzfahrt zu kompensieren. Adventure
die Rundreisenanbieter Shoestring, Koning Aap, YourWay2GO und Entdeck
die Welt – kompensieren den CO2-Ausstoß aller Reisen in vollem Umfang.
Hier werden nicht nur die An- und Abreise per Flugzeug berücksichtigt,
sondern auch die Fortbewegung an Land sowie die Unterkunft. Zudem werden
kompensiert. Weitere Informationen finden Sie unter: ###
Trinkwasserinitiative hilft dem Klimaschutz #### Sauberes Trinkwasser
unterstützt die DER Touristik Foundation in Kooperation mit myclimate
Deutschland ein Klimaschutzprojekt in Afrika. Die Aktion startete am 1.
August 2018 in den rund 500 DER Reisebüros in ganz Deutschland. Kunden
können in den Filialen – unabhängig von der Buchung einer Reise – durch
eine Spende die Versorgung von Menschen in Uganda mit sauberem
Trinkwasser unterstützen und gleichzeitig zum Klimaschutz beitragen. Mit
den Spenden werden Wasserfilter für ärmere Privathaushalte und Schulen
finanziert. Durch den Einsatz der Filter entfällt das traditionelle
Abkochen des Wassers auf Feuerstellen, das derzeit noch von rund 40
Prozent der Bevölkerung Ugandas praktiziert wird. Durch den Einsatz der
Filtersysteme kann eine Schule im Schnitt jährlich rund 130 Tonnen
Feuerholz einsparen. Die Abholzung schützenswerter Wälder, die CO2
speichern können, wird verringert. Zudem entfällt auf diese Weise der
Trinkwasser für
Uganda](/xbcr/der-net/Teaser%20Klimaschutzprojekt%20Versuch.jpg)
Touristik der REWE Group - Drucken -
Portals erforderlich sind. Diese sind aktiv, sobald Sie das Portal
verwenden und können nicht deaktiviert werden. Weitere Informationen
stehen Ihnen auf unserer [Datenschutzseite](/de/datenschutz/) zur
Verfügung. OK x
