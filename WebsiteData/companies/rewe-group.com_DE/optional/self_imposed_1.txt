Logo](data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E
Logo](https://www.rewe-group.com/content/themes/reweg/img/icon-rewe_group-white.svg
und
Vertriebslinien](https://www.rewe-group.com/de/unternehmen/struktur-und-vertriebslinien/)
Aufsichtsrat](https://www.rewe-group.com/de/unternehmen/vorstand-und-aufsichtsrat/)
Relations](https://www.rewe-group.com/de/unternehmen/creditor-relations/)
Projekte](https://www.rewe-group.com/de/nachhaltigkeit/engagements-und-projekte/)
Auszeichnungen](https://www.rewe-group.com/de/nachhaltigkeit/kooperationen-und-auszeichnungen/)
und
Team](https://www.rewe-group.com/de/politik/selbstverstandnis-und-team/)
Positionen](https://www.rewe-group.com/de/politik/positionen/) -
Arbeitgeber](https://karriere.rewe-group.com/content/wir-als-arbeitgeber/?utm_source=careersite&utm_campaign=konzernzentrale)
Hier befindet sich Ihre Sammlung. Die Inhalte unserer Website können Sie
mit einem Klick auf das Symbol speichern und diese später hier abrufen.
Suchen Suchen Suchvorschläge Themen [Bienenhotel] [Bienenhotel]
und Touristikkonzern sind wir uns unserer besonderen Verantwortung
gegenüber Menschen und Umwelt bewusst. Nachhaltige Unternehmensführung
bedeutet für uns daher nicht nur, das Beste für Mitarbeitende sowie
Kundinnen und Kunden zu ermöglichen, sondern geht viel weiter. ### Wir
sind uns unserer Verantwortung bewusst und handeln nachhaltig Wir bei
der REWE Group wissen: Unser Handeln hat Auswirkungen auf Mensch, Tier
und Umwelt. Daher fördern wir nachhaltige Sortimente und achten auf
einen fairen Umgang mit unseren Partnernorganisationen und Lieferanten.
Wir handeln umwelt- und klimabewusst, setzen uns für eine zukunftsfähige
Gesellschaft ein und übernehmen Verantwortung für unsere Mitarbeitenden.
Nachhaltiges Handeln ist für uns kein Trend, sondern ein wesentlicher
Teil unserer Geschäftsstrategie. Unser Klimaziel Wir möchten unsere
Treibhausgasemissionen für Deutschland und Österreich pro Quadratmeter
Verkaufsfläche bis 2022 gegenüber 2006 halbieren. Green Buildings Mit
unserem Green Building-Konzept verfolgen wir als REWE Group einen neuen
Standard für nachhaltige Supermärkte. [teaser background image] [teaser
background image] [teaser background image] [teaser background image]
Nachhaltigkeits­bericht 2019 Bei unserem Engagement legen wir besonderen
Wert auf Transparenz: Der aktuelle Nachhaltigkeitsbericht beschreibt die
Aktivitäten des vergangenen Jahres und wirft einen Blick auf zukünftige
Herausforderungen. - Zum Nachhaltigkeitsbericht [teaser background
image] [teaser background image] #### Kooperationen und Aus­zeichnungen
Zu unseren Kooperationen und Auszeichnungen [teaser background image]
Eigenmarken-Schokolade aus einem Projekt zur Unterstützung und Förderung
der wirtschaftlichen Unabhängigkeit ghanaischer Kakao-Farmer:innen ein.
treibt den Ausstieg aus dem millionenfachen Kükentöten voran. Auf welche
Alternativen REWE und PENNY setzen, und warum es eine Branchenlösung
braucht. - Zur Story [teaser background image] [teaser background image]
Bei REWE und PENNY bieten wir immer mehr Obst und Gemüse unverpackt an.
Nachhaltigkeitsziele - Eier ohne Kükentöten #### Diskutieren Sie mit Das
unserem Twitter-Kanal informieren wir über aktuelle Entwicklungen in der
REWE Group sowie der Branche. Zum Twitter-Kanal #### Service
Einstellungen Barrierefreiheit [Privatsphäre](#cookie-settings) [English
page](https://www.rewe-group.com/en/) #### Social Media #### Zertifikate
Deutschland [rewe.de](https://www.rewe.de/)
International [rewe-group.at](https://www.rewe-group.at/)
Touristik [dertouristik.com](http://www.dertouristik.com/de/)
Group
Logo](data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E
Logo](https://www.rewe-group.com/content/themes/reweg/img/icon-rewe_group-gray.svg
Group - All rights reserved ### Barrierefreiheit ##### Große Schriften
