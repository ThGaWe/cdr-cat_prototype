BlaBlaCar - [Newsroom](/newsroom) - [What’s new](/newsroom/news-list) -
Story](https://blog.blablacar.com/blog/inside-story) - [Media
resources](/newsroom/download-center) About Us ======== **We bring
freedom, fairness and fraternity to the world of travel** \- BlaBlaCar
mission statement BlaBlaCar is the world’s leading community-based
travel network enabling over 90 million members to share a ride across
road, connecting members looking to carpool or to travel by bus, and
making travel more affordable, sociable and convenient. BlaBlaCar’s
environmentally and human-friendly mobility network saves 1.6M tons of
CO2 and enables 120M human connections every year. **HOW IT ALL
STARTED** When Fred was trying to get home to his family in the French
countryside for Christmas 2003, he struggled as he had no car and all
the trains were full. After begging his sister to pick him up, it was on
the road when Fred noticed the sheer number of people driving alone. It
hit him that all those empty seats in existing cars could be the
beginning of a new travel network. Over the next decade, together with
co-founders Francis and Nicolas, the trio took this simple idea and
built it into the world’s leading carpooling platform, connecting
millions of people going the same way. Learn about our first decade of
building BlaBlaCar in our[ Inside
Story](https://blog.blablacar.com/blog/inside-story)! **OUR KEY NUMBERS
kilometers shared by the community since BlaBlaCar’s creation **CURIOUS
ABOUT OUR IMPACTS? **BlaBlaCar doubles the occupancy rate of cars whilst
operating a carbon-saving network. In total, 1.6 million tonnes of CO2
were saved by BlaBlaCar carpoolers in 2018, thanks to the relative
efficiency of filled cars versus alternative modes of transport,
members’ improved driving behaviors whilst carpooling, and the informal
carpooling inspired by BlaBlaCar outside the platform. Find our more in
our dedicated report, **[Zero Empty
Seats](http://blog.blablacar.fr/ZeroEmptySeats-pdf)**.  Carpooling also
creates a unique space, enabling exchanges between people who might have
never met otherwise but who come together to share a ride. – [**Bringing
Closer**](https://blog.blablacar.com/newsroom/news-list/study-social-impacts-carpooling)
is the largest study conducted on the social impacts of carpooling and
reveals the social ties created by carpooling. – In [**Entering the
Trust Age**](https://blog.blablacar.com/trust), a study conducted
jointly with NYU Stern, you can learn about the digital trust tools that
have enabled BlaBlaCar to create trust at scale and enable millions of
people to share long-distance journeys. **FIND OUT MORE ABOUT
BLABLACAR** [Founders](https://blog.blablacar.com/about-us/founders)
Story](https://blog.blablacar.com/blog/inside-story) Become the part of
Jobs](https://blog.blablacar.com/dreamjobs) - [Newsroom](/newsroom)
Terms & Privacy Policy  & Cookie settings © 2021 BlaBlaCar By using our
content & ads tailored to your interests. Read our policy on cookies
usage OK Offer a ride
