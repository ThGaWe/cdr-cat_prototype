Mail [Icon Domain & Mail] Domain & Mail Online seriös auftreten mit
Wunsch-Domain und Mail-Postfach. [Domain Ihre Wunschadresse im Netz mit
Top-Endungen wie .de & .com.](/domains/) [Mail Ihre seriöse E-Mail mit
eigener Domain – sicher und werbefrei.](/mail/) [Mail-Archivierung Ihre
rechtskonforme, automatische Archivierung für Ihr
Business.](/mail-archivierung/) [SSL-Zertifikate Schützen Sie Ihre
Ihre Business Lösung für Postfächer und Office
Vorkenntnisse zur Homepage.](/homepage-baukasten/) [Webshop Schnell und
ohne Vorwissen online verkaufen.](/webshop/) [Homepage erstellen lassen
Lassen Sie sich Ihre Seite professionell
erstellen.](/homepage-baukasten/homepage-erstellen-lassen/) - Hosting &
WordPress [Icon Hosting] Hosting Schnell, sicher und leistungsstark:
Hosting & WordPress von Profis für Profis. [WordPress-Hosting Im
Handumdrehen zur eigenen, sicheren
WordPress-Seite.](/hosting/wordpress-hosting/) [Hosting Ideal für
professionelle und blitzschnelle Websites.](/hosting/) -
Online-Marketing [Icon Online-Marketing] [](/online-marketing/)
Online-Marketing Optimieren Sie die Performance und behalten Sie den
marketingRadar Ihr perfekter Einstieg ins
Online-Marketing](/online-marketing/marketingradar/) [STRATO
rankingCoach Schnell und einfach zu Top-Plätzen bei Google &
Co.](/online-marketing/rankingcoach/) [STRATO listingCoach Lassen Sie
sich per Klick in mehr als 30 Top-Verzeichnisse
eintragen.](/online-marketing/listingcoach/) [STRATO adCoach Erstellen
Sie einfach effektive Werbekampagnen bei
Google.](/online-marketing/adcoach/) - Server [Icon Server] [](/server/)
Server Schneller geht's nicht: 100% Marken-Power vom V-Server bis zum
eigenen System. Alle Server [V-Server Linux Kraftvolle Linux V-Server
mit SSD-Power & Plesk.](/server/linux-vserver/) [V-Server Windows
RAM-optimierte Windows V-Server mit Windows 2019
Datacenter.](/server/windows-vserver/) [Managed Server 100%
Server-Leistung ohne Administrationsaufwand.](/server/managed-server/)
Hosting.](/server/linux-root-server/) [Dedicated Server Windows Voller
Marken-Hardware.](/server/windows-root-server/) [Limited Hardware
Einzelne Server zum günstigen Preis.](/server/limited-hardware/)
ServerCloud.](/server/servercloud/) [Cyber Protect Die integrierte
Backup-Funktion](/server/cyber-protect/) - Cloud-Speicher [Icon
Cloud-Speicher] Cloud-Speicher Dateien online sichern & automatische
Backups in der deutschen Cloud speichern. [HiDrive Speichern Sie Ihre
Daten sicher in der deutschen Cloud.](/cloud-speicher/) [HiDrive
Business Das Online-Laufwerk für die sichere Arbeit im
Team.](/cloud-speicher/hidrive-business/) [HiDrive S3 Der Objektspeicher
für Ihre Firmendaten.](/cloud-speicher/hidrive-s3/) - Business Solutions
Ihre maßgeschneiderte Managed Hosting Lösung. [Managed Hosting Lassen
Sie sich zu Ihrer Cloud- und Hosting-Lösung
beraten.](/business-solutions/) [Business Cloud Skalierbare Public &
Private Business Cloud Lösungen.](/business-solutions/cloud-loesungen/)
konfiguriert.](/business-solutions/business-server/) [Managed Services
Erfahrene Experten managen Ihre Systeme ganz nach
Bedarf.](/business-solutions/managed-services/) [Network & Security
Sicherer Datenverkehr all Ihrer
Systemkomponenten.](/business-solutions/network-security/) [Storage &
Backup Clevere NAS, SAN und Backup-Lösungen für Ihre
Daten.](/business-solutions/storage-backup/) - Login - Hilfe & Kontakt -
Sicherheit Optimale Sicherheit bei STRATO
Infrastruktur - Zuverlässige Schutz Ihrer Daten - Maximale Sicherheit
für Ihre STRATO-Produkte STRATO - Ihr zuverlässiger Partner in puncto
Sicherheit ------------------------------------------------------- Wir
haben hohe Ansprüche, um Ihnen ein sicheres Umfeld rund um die STRATO
Produkte anzubieten. Angefangen von der Infrastruktur mit hochsicheren
Rechenzentren, über verschlüsselte Daten bis hin zu sicher entwickelten
Produkten. Wir gewähren Ihnen auf dieser Seite einen detaillierten
Infrastruktur - Produktsicherheit - Datensicherheit - Rechtssicherheit
Infrastruktur ------------- [Infrastruktur] Infrastruktur -------------
folgen höchsten Sicherheitsstandards und gehören zu den sichersten
weltweit. STRATO betreibt zwei getrennte Rechenzentren an zwei
verschiedenen geografischen Standorten in Deutschland mit strengen
Zugangsbeschränkungen. Wir bieten eine redundante Datenspeicherung
innerhalb unserer Rechenzentren in verschiedenen Brandabschnitten, eine
batteriegestützte, unterbrechungsfreie Stromversorgung, Vorkehrungen
gegen Stromausfälle für einen autonomen Betrieb, ein automatisches
Löschsystem durch Unterdrückung von Sauerstoff gegen äußere Einflüsse,
verdeckte Standorte der Rechenzentren sowie moderne Schutzmaßnahmen
gegen Angriffe von außen. So können Sie nicht nur mit einer hohen
Leistungsfähigkeit Ihrer STRATO Produkte rechnen, sondern auch mit
größtmöglicher Sicherheit. #### Standort Deutschland Alle Rechenzentren
von STRATO sind “hosted in Germany”, so dass die Daten nach deutschem
Recht gespeichert und verarbeitet werden. So können wir sicherstellen,
dass unsere Produkte und Ihre Daten bei uns sicher aufgehoben sind.
Zusätzlich zu den strengen gesetzlichen Auflagen, haben wir uns zu
weiteren Maßnahmen verpflichtet, um für die Sicherheit Ihrer Daten zu
sorgen – zum Beispiel mit einer TÜV-Zertifizierung. Inzwischen betreibt
STRATO weitaus mehr als vier Millionen Internetpräsenzen und mehr als
IT-Branche verfügt STRATO bei ausgewählten Server-Produkten über
Service-Level-Agreements, kurz SLAs, um Ihnen die Verfügbarkeit und
Zuverlässigkeit unserer Produkte gewährleisten und zusichern zu können.
Sie beinhalten z.B. schnelle Reaktionszeiten, die priorisierte
Bearbeitung von Anfragen und Verfügbarkeiten von bis zu 99,99 % im
Monatsmittel - je nach Produkt auch hinzubuchbar. #### Zertifizierung
nach ISO 27001 STRATO ist bereits seit 2004 mit dem TÜV-Siegel nach ISO
Cloud-Services ausgezeichnet. Zudem umfasst diese Zertifizierung
zahlreiche Sicherheitsmaßnahmen in der IT-Infrastruktur selbst, in der
Sekundärtechnik und in der Prozesskette. STRATO wird jedes Jahr aufs
Neue nach ISO 27001 zertifiziert. Produktsicherheit -----------------
Produkte an, um Ihre Sicherheit noch gezielter zu gewährleisten. Anbei
BackupControl] Mit dieser professionellen Backup-Lösung sichern Sie sich
angelegt. Sie können aus einer Vielzahl an Sicherungskopien wählen, die
zu unterschiedlichen Zeitpunkten angelegt wurden. So stellen Sie
versehentlich überschriebene oder gelöschte Inhalte kurzerhand und
einfach wieder her. #### SiteLock Scan + Repair [SiteLock Scan + Repair]
SiteLock Scan + Repair ist ein cloud-basiertes Sicherheits-Tool. Es
behebt es automatisch mögliche Bedrohungen, sodass Ihre Website
dauerhaft geschützt bleibt. #### Cyber Protect by Acronis [Cyber Protect
by Acronis] Cyber Protect ist eine integrierte
Anti-Malware-Sicherheitslösung mit Backup-Funktion für den besten Schutz
Ihrer Systeme und Daten. Sie profitieren von praktischen
Verwaltungsfunktionen, mit denen Sie Ransomware und weitere Gefahren
abwehren und die Stände Ihrer Geräte und Systeme sichern können. ####
Trusted Shops [Trusted Shops] Durch das Gütesiegel “Trusted Shops”
Kunden auf der sicheren Seite sind. Bereits über 5.000 erfolgreiche
Online-Händler bieten somit einen sorgenfreien Einkauf im Netz. Nutzen
auch Sie die Bekanntheit des Internet-Gütesiegels Nr. 1. Bei STRATO
können Sie sich ohne großen Aufwand und zu ermäßigten Konditionen
zertifizieren lassen. [Backup Control] Datensicherheit ---------------
HiDrive dank Zero-Knowledge-Verschlüsselung einen sicheren Weg, Ihre
Dateien in der Cloud Software so abzulegen, dass sie kein anderer öffnen
oder lesen kann. Nicht nur Dateien benötigen Schutz. Auch
Zugriffe wie der STRATO Kunden-Login selbstverständlich verschlüsselt
und mit einer Zwei-Faktor-Authentifizierung versehen. ####
SSL-Zertifikate / DigiCert Die Sicherheit der Datenübertragung im
Internet spielt eine wichtigere Rolle. Insbesondere wenn es sich um
persönliche Daten oder Zahlungsinformationen handelt, sollte dies
ausreichend gesichert werden. Die SSL-Zertifikate werden eingesetzt, um
den Austausch solcher Daten zu schützen. SSL (Secure Sockets Layer) ist
ein Verschlüsselungsprotokoll zur Datenübertragung zwischen Server und
Client. Der Datenaustausch erfolgt ausschließlich verschlüsselt und die
werden. Gemäß der Datenschutz-Grundverordnung (DSGVO) müssen alle Daten,
persönliche Daten, usw.) verschlüsselt übertragen werden. Somit ist der
Einsatz von SSL-Zertifikaten eine Pflicht. DigiCert ist weltweit
führender Anbieter digitaler Zertifikate mit einzigartiger Erfahrung in
Bereichen der Online-Sicherheit. Zusammen mit DigiCert bietet STRATO
bietet Ihnen die Möglichkeit, Ihre Identität mittels der
Zwei-Faktor-Authentifizierung durch zwei unabhängige Komponenten
nachzuweisen. Beispielsweise können Sie eine
Sie funktioniert ganz einfach für alle Logins, auch bei Nutzung von
HiDrive mit Netzwerkprotokollen. Gesteuert wird die
Zwei-Faktor-Authentifizierung durch die Authenticator-App, die auf Ihrem
mobilen Gerät installiert wird. #### Passwortschutz Zur Nutzung der
verschiedenen Dienste bei STRATO gibt es unterschiedliche Passwörter. So
können Sie zum Beispiel Ihrem Webmaster-Zugriff auf den
SFTP-Speicherplatz Ihres Paketes gewähren, jedoch den Zugriff auf die
Vertragsdaten unterbinden. Vergeben und ändern Sie Ihre Passwörter je
nach Produkt direkt in Ihrem Kunden-Login. #### Spamfilter Dank dem
integrierten mehrstufigen Spam- und Virenschutz sorgt der Spamfilter
dafür, dass unerwünschte E-Mails gar nicht erst in Ihrem STRATO Postfach
eingehen. Rechtssicherheit ---------------- [Rechtssicherheit] ####
Internet-Service-Provider seit 2018 konform der
Datenschutzgrundverordnung, kurz DSGVO. Bereits zuvor war STRATO als
STRATO Mail-Archivierung Damit E-Mails Ihres Unternehmens
revisionssicher archiviert werden können, bietet STRATO das Produkt
Seite. Sie profitieren von einer manipulationssicheren und vollständigen
Archivierung Ihrer Nachrichten und Dateien sowie von einem
kontrollierten Zugriff auf das Archiv. Die Mail-Archivierung von STRATO
ist nach IDW PS 880 zertifiziert. Sowohl die Softwareentwicklung, als
auch die Software selbst, erfüllen die Anforderungen der Rechts- und
Manipulationssicherheit. Damit erfüllt die Mail-Archivierung von STRATO
alle gesetzlichen Anforderungen in puncto Ordnungsmäßigkeit, Sicherheit
und Kontrolle. #### janolaw Da Rechtssicherheit bei STRATO von großer
Bedeutung ist, bietet STRATO gemeinsam mit janolaw eine optimale
Widerrufsbelehrung, eine Datenschutzerklärung und ein Impressum. Nach
jeder Änderung der Rechtslage durch Urteile oder Gesetzesänderungen
werden die Dokumente von janolaw automatisch über eine Schnittstelle
aktualisiert. Schützen Sie sich vor unnötigen Abmahnungen. [janolaw]
Allgemeine Infos [Über uns](/ueber-uns/) [Presse](/press/)
Vergleich](/cloud-speicher/cloud-speicher-vergleich/) [E-Mail Adresse
erstellen](/mail/e-mail-adresse-erstellen/) [Newsletter
erstellen](/webshop/newsletter-erstellen/) [Opt Out](/webshop/opt-out/)
Baukasten HTML](/homepage-baukasten/homepage-baukasten-html/)
Server](/server/root-server/) [VPS](/server/vps/)  
Anbieter](/cloud-speicher/deutsche-cloud-anbieter/)
kaufen](/domains/domain-kaufen/) [WordPress Homepage
erstellen](/hosting/wordpress-hosting/wordpress-homepage-erstellen/)
bearbeiten](/hosting/wordpress-hosting/wordpress-seiten-bearbeiten/)
STRATO im Social Web [ STRATO bloggt](https://www.strato.de/blog/) [
Facebook](https://www.facebook.com/strato/) [
Twitter](https://twitter.com/STRATO_ag) [
YouTube](https://www.youtube.com/user/stratoDE/) Karriere bei STRATO [
Karriere Portal](/karriere/) [
LinkedIn](https://de.linkedin.com/company/strato-ag) [
Xing](https://www.xing.com/pages/strato) Die perfekte Domain finden:
Jetzt prüfen [STRATO Logo] Copyright © STRATO AG |
