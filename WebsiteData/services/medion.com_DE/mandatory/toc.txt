AGB - ALLGEMEINE GESCHÄFTSBEDINGUNGEN
Die MEDION AG hat ein Business-Modell entwickelt, das führenden
europäischen Handelskonzernen attraktive Marktsegmente im
Non-Food-Bereich erschließt und gleichzeitig weltweit agierenden
Markenherstellern neue Absatzkanäle öffnet.
Die Gesellschaft unterstützt sowohl Handel als auch Hersteller bei
punktuellen Verkaufsaktionen von Produkten aus den Bereichen
PC/Multimedia, Unterhaltungs- und Haushaltselektronik und
Kommunikationstechnik.
§ 1 ALLGEMEINE BEDINGUNGEN
der vertraglichen Beziehung zwischen der MEDION AG und dem
Besteller/Käufer und richten sich gleichermaßen an Verbraucher und
Unternehmer. Ist der Besteller Kaufmann, werden abweichende oder
ergänzende Bedingungen des Kunden auch dann nicht Vertragsbestandteil,
wenn ihnen nicht gesondert widersprochen wird. Allgemeine
Geschäftsbedingungen eines solchen Bestellers werden nur dann
verbindlich, wenn sie ausdrücklich von der MEDION AG schriftlich
genehmigt wurden.
der Bundesrepublik Deutschland (ausgenommen deutsche Inseln).
gelten nur für den deutschen Markt. Bestellungen von Kunden der MEDION
AG sind verbindliche Angebote. Die MEDION AG kann das Angebot des
Bestellers nach Wahl durch unmittelbare Zusendung der Ware oder durch
Auftragsbestätigung innerhalb einer Frist von acht Tagen annehmen. Die
Bestellbestätigung, welche der Käufer im Anschluss an die Bestellung per
E-Mail erhält, stellt ausdrücklich keine Annahme dar, sondern dient
lediglich der Erfüllung der Informationspflichten aus § 312d, Abs. 1,
Nr. 3 BGB. Die MEDION AG weist daraufhin, dass die Liefermöglichkeit
vorbehalten ist und bleibt. Die Bestellung des Käufers wird zu den
Leistungsgegenstand stellen eine Produktbeschreibung dar.
Zustimmung der MEDION AG übertragbar.
angeboten werden, wird die MEDION AG im Hinblick auf diese Leistungen
und Waren nicht selbst Vertragspartner des Verbrauchers. Diese Verträge
werden zwischen den Endverbrauchern und den jeweiligen
Kooperationspartnern geschlossen. Es gelten die AGB der
Kooperationspartner.  
§ 2 WIDERRUFSBELEHRUNG
WIDERRUFSRECHT
SIE HABEN DAS RECHT, BINNEN VIERZEHN TAGEN OHNE ANGABE VON GRÜNDEN
DIESEN VERTRAG ZU WIDERRUFEN.
DIE WIDERRUFSFRIST BETRÄGT VIERZEHN TAGE AB DEM TAG AN DEM SIE ODER EIN
VON IHNEN BENANNTER DRITTER, DER NICHT DER BEFÖRDERER IST, DIE LETZTE
WARE IN BESITZ GENOMMEN HABEN BZW. HAT.
UM IHR WIDERRUFSRECHT AUSZUÜBEN, MÜSSEN SIE UNS DER MEDION AG, AM
ZEHNTHOF 77, 45092 ESSEN, TELEFON: 0201/22099555 , FAX: 0201/8388-519,
E-MAIL: REVOCATION@MEDION.COM MITTELS EINER EINDEUTIGEN ERKLÄRUNG (Z.B.
EIN MIT DER POST VERSANDTER BRIEF, TELEFAX ODER E-MAIL) ÜBER IHREN
ENTSCHLUSS, DIESEN VERTRAG ZU WIDERRUFEN, INFORMIEREN. SIE KÖNNEN DAFÜR
DAS MUSTER-WIDERRUFSFORMULAR VERWENDEN, DAS JEDOCH NICHT VORGESCHRIEBEN
IST.
ABSENDEN.
WIDERRUFSFOLGEN
WENN SIE DIESEN VERTRAG WIDERRUFEN, HABEN WIR IHNEN ALLE ZAHLUNGEN, DIE
WIR VON IHNEN ERHALTEN HABEN, EINSCHLIESSLICH DER LIEFERKOSTEN (MIT
AUSNAHME DER ZUSÄTZLICHEN KOSTEN, DIE SICH DARAUS ERGEBEN, DASS SIE EINE
ANDERE ART DER LIEFERUNG ALS DIE VON UNS ANGEBOTENE, GÜNSTIGSTE
STANDARDLIEFERUNG GEWÄHLT HABEN), UNVERZÜGLICH UND SPÄTESTENS BINNEN
VIERZEHN TAGEN AB DEM TAG ZURÜCKZUZAHLEN, AN DEM DIE MITTEILUNG ÜBER
IHREN WIDERRUF DIESES VERTRAGS BEI UNS EINGEGANGEN IST. FÜR DIESE
RÜCKZAHLUNG VERWENDEN WIR DASSELBE ZAHLUNGSMITTEL, DAS SIE BEI DER
URSPRÜNGLICHEN TRANSAKTION EINGESETZT HABEN, ES SEI DENN, MIT IHNEN
WURDE AUSDRÜCKLICH ETWAS ANDERES VEREINBART; IN KEINEM FALL WERDEN IHNEN
WEGEN DIESER RÜCKZAHLUNG ENTGELTE BERECHNET.
WIR KÖNNEN DIE RÜCKZAHLUNG VERWEIGERN, BIS WIR DIE WAREN WIEDER
ZURÜCKERHALTEN HABEN ODER BIS SIE DEN NACHWEIS ERBRACHT HABEN, DASS SIE
DIE WAREN ZURÜCKGESANDT HABEN, JE NACHDEM, WELCHES DER FRÜHERE ZEITPUNKT
IST.
SIE HABEN DIE WAREN UNVERZÜGLICH UND IN JEDEM FALL SPÄTESTENS BINNEN
VIERZEHN TAGEN AB DEM TAG, AN DEM SIE UNS ÜBER DEN WIDERRUF DIESES
VERTRAGS UNTERRICHTEN, AN UNS – MEDION AG, 45092 ESSEN ZURÜCKZUSENDEN
ODER ZU ÜBERGEBEN. DIE FRIST IST GEWAHRT, WENN SIE DIE WAREN VOR ABLAUF
DER FRIST VON VIERZEHN TAGEN ABSENDEN.
WIR TRAGEN DIE KOSTEN DER RÜCKSENDUNG DER WARE.
SIE MÜSSEN FÜR EINEN ETWAIGEN WERTVERLUST DER WAREN NUR AUFKOMMEN, WENN
DIESER WERTVERLUST AUF EINEN ZUR PRÜFUNG DER BESCHAFFENHEIT,
EIGENSCHAFTEN UND FUNKTIONSWEISE DER WAREN NICHT NOTWENDIGEN UMGANG MIT
IHNEN ZURÜCKZUFÜHREN IST.
DAS WIDERRUFSRECHT BESTEHT NICHT BEI DEN FOLGENDEN VERTRÄGEN:
widerrufen, sind Sie auch an den Darlehensvertrag nicht mehr gebunden ,
sofern beide Verträge eine wirtschaftliche Einheit bilden. Dies ist
insbesondere dann anzunehmen, wenn wir gleichzeitig Ihr Darlehensgeber
sind oder wenn sich Ihr Darlehensgeber im Verhältnis zu Ihnen
hinsichtlich der Rechtsfolgen der Widerrufs oder der Rückgabe in unsere
Rechte und Pflichten aus dem finanzierten Vertrag ein. Letzteres gilt
nicht, wenn der vorliegende Vertrag den Erwerb von Finanzinstrumenten
Wollen Sie eine vertragliche Bindung so weitgehend wie möglich
vermeiden, machen Sie von Ihrem Widerrufsrecht Gebrauch und widerrufen
Sie zudem den Darlehensvertrag, wenn Ihnen auch dafür ein Widerrufsrecht
zusteht.
Hier erhalten Sie das Muster-Widerrufsformular**
Formular aus und senden Sie es zurück)
§ 3 LIEFERZEITEN UND LIEFERFRISTEN
MEDION AG und dem Besteller kann mündlich erfolgen und wird von der
MEDION AG schriftlich bestätigt.
Liefergegenstand das Werk verlassen hat oder bei Abholung durch den
Besteller die Lieferung versandbereit ist und dies dem Besteller
mitgeteilt wird.
und aufgrund von Ereignissen, die dem Verkäufer die Lieferung wesentlich
erschweren oder unmöglich machen. Hierzu gehören insbesondere Maßnahmen
im Rahmen von rechtmäßigen Arbeitskämpfen sowie Streik und Aussperrung
im eigenen Betrieb, Arbeitskampfmaßnahmen in Drittbetrieben, sofern die
MEDION AG kein Abwendungsverschulden trifft. Des Weiteren bei
unvorhergesehenen Ereignissen wie Krieg, Blockade, Mobilmachung. Bei
Vorliegen dieser und wertverwandter Voraussetzungen ist die MEDION AG
berechtigt vom Vertrag ganz oder teilweise zurückzutreten. In diesen
Fällen wird der Kunde unverzüglich über die Nichtverfügbarkeit der Waren
informiert. Es gelten die unter § 2 genannten Widerrufsfolgen.
§ 4 SHOPPING-ACCOUNTS
passwortgeschützten direkten Zugang für künftige Bestellungen im Shop
ein. Der Besteller verpflichtet sich, dieses Passwort vertraulich zu
behandeln und keinem unbefugten Dritten zugänglich zu machen. MEDION AG
§ 5 ANGEBOTE UND AKTIONEN
Rabatte aus Angeboten und Aktionen sind nicht miteinander kombinierbar
und gelten nur, solange der Vorrat reicht. Eine Barauszahlung ist nicht
möglich.
§ 6 PREIS- UND ZAHLUNGSBEDINGUNGEN
unserer Lieferanten) inkl. der jeweils gültigen gesetzlichen
Mehrwertsteuer als vereinbart.
abweichend behalten wir uns vor, für einzelne Produkte reduzierte
Versandkosten zu berechnen. Im Falle einer Großgutversendung als Teil
einer Bestellung (Fracht- oder Sperrgut) berechnen wir insgesamt EUR
19,95.
ist ausgeschlossen, sofern es sich um Ansprüche aus einem anderen
Vertragsverhältnis handelt. Beruht der Gegenanspruch auf demselben
Vertragsverhältnis, ist die Zurückbehaltung von Zahlungen nur zulässig,
wenn es sich um unbestrittene oder rechtskräftig festgestellte
Gegenansprüche handelt.
erklären, wenn es sich um unbestrittene oder rechtskräftig festgestellte
Forderungen handelt.
§ 7 EIGENTUMSVORBEHALT
an der von ihr gelieferten Ware bis zur Erfüllung sämtlicher, ihr aus
der Geschäftsverbindung gegen den Besteller zustehenden Ansprüche,
gleichgültig aus welchem Rechtsgrunde, vor. Bei allen übrigen Bestellern
behält sich die MEDION AG das Eigentum an der gelieferten Ware bis zur
vollständigen Kaufpreiszahlung vor.
Sachen, die nicht im Eigentum der MEDION AG stehen, erwirbt MEDION einen
Miteigentumsanteil an der neuen Sache im Verhältnis der verarbeiteten
Vorbehaltsware zu dem Artikel zur Zeit der Bearbeitung.
Zahlungsverzug, ist die MEDION AG berechtigt, die Vorbehaltsware
zurückzunehmen oder ggf. die Abtretung der Herausgabeansprüche des
Käufers gegen Dritte zu verlangen.
Forderungen wird der Besteller auf das Eigentum der MEDION AG hinweisen
und diese sofort unter Übergabe der für die Intervention notwendigen
Unterlagen benachrichtigen. Die Kosten der Intervention trägt der
Besteller.
§ 8 GEWÄHRLEISTUNG
mitteilen. Nach der Mängelanzeige wird das Produkt auf Kosten der MEDION
AG an diese übersandt.
Im unternehmerischen Geschäftsverkehr ist die Gewährleistung auf ein
Jahr begrenzt und die MEDION AG ist berechtigt, das Produkt nach ihrer
Wahl zu reparieren oder kostenfreien Ersatz zu stellen.
Abnutzung der Ware sowie Mängel, die durch unsachgemäße Handhabung
entstanden sind.
§ 9 GEFAHRÜBERGANG
Die Gefahr des zufälligen Untergangs und der zufälligen Verschlechterung
der Sache gehen auf den Besteller über, sobald die bestellte Ware von
der MEDION AG der jeweils ausgesuchten Transportperson übergeben wurde
oder Zwecks Versendung das Lager der MEDION AG verlassen hat. Ist der
Besteller Verbraucher (vgl. § 13 BGB), geht die Gefahr des zufälligen
Untergangs und der zufälligen Verschlechterung mit der Übergabe an den
Besteller oder eine empfangsberechtigte Person über.
§ 10 HAFTUNG
entstehende Schäden haftet die MEDION AG lediglich, soweit diese auf
vorsätzlichem oder grob fahrlässigem Handeln oder auf schuldhafter
Verletzung einer wesentlichen Vertragspflicht durch die MEDION AG oder
Erfüllungsgehilfen (z. B. dem Zustelldienst) der MEDION AG beruhen. Eine
darüber hinausgehende Haftung auf Schadensersatz ist ausgeschlossen. Die
Bestimmungen des Produkthaftungsgesetzes bleiben unberührt.
die Haftung der MEDION AG auf den voraussehbaren Schaden begrenzt.
Stand der Technik nicht fehlerfrei und/oder jederzeit verfügbar
gewährleistet werden. Die MEDION AG haftet daher weder für die ständige
und ununterbrochene Verfügbarkeit des Online-Handelssystems noch für
technische und elektronische Fehler während des Bestellvorgangs, auf die
MEDION AG keinen Einfluss hat, insbesondere nicht für die verzögerte
Bearbeitung oder Annahme von Angeboten.
selbst verantwortlich. Es wird darauf hingewiesen, dass bei Reparaturen
eine Löschung der Festplatte erforderlich sein kann.
§ 11 SCHLUSSBESTIMMUNGEN
erhaltenen personenbezogenen Daten i.S.d. Bundesdatenschutzgesetzes.
Vollkaufmann ist, ein Besteller keinen allgemeinen Gerichtsstand im
Inland hat, ein Besteller nach Vertragsabschluss seinen Wohnsitz oder
gewöhnlichen Aufenthalt in das Ausland verlegt hat oder sein Wohnsitz
oder gewöhnlicher Aufenthaltsort zum Zeitpunkt der Klageerhebung
unbekannt ist.
Vorschriften des UN-Kaufrechts.
STAND AGB_WEBSHOP_DE-DE_2017_0629_V1.5
Ja, ich möchte den kostenlosen MEDION Newsletter erhalten. Die Abmeldung
ist jederzeit mittels in den Mailings enthaltenem Abmeldelink oder per
E-Mail an DATENSCHUTZ@MEDION.COM möglich. Weitere Informationen dazu
findest Du in unserer DATENSCHUTZERKLÄRUNG.
Du kannst uns auch hier besuchen
Rechtliches
Produktvergleich
