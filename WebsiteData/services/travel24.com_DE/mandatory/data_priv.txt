HEUTE ERREICHBAR (9-20 UHR)
HOME
heute erreichbar (9-20 Uhr)
1.  Startseite
2.  Datenschutz
DATENSCHUTZ UND SICHERHEIT
DATENSCHUTZ
Hiermit informieren wir Sie über die Verarbeitung Ihrer
personenbezogenen Daten durch die Travel24.com AG und die Ihnen nach der
Datenschutz-Grundverordnung (DS-GVO) zustehenden Rechte.
VERANTWORTLICHER FÜR DIE DATENVERARBEITUNG
Travel24.com AG
Salomonstr. 25a
D – 04103 Leipzig
Deutschland
Tel. +49 341 355 727 05
Fax. +49 341 355 727 99
support@travel24.com 
Vorstand: Ralf Dräger
für www.travel24.com
Diese Erklärung zum Datenschutz erläutert, welche gegebenenfalls
personenbezogenen Daten wir oder Dritte während Ihres Besuchs auf
verwendet werden. Entsprechendes gilt, wenn wir im Rahmen der
Bereitstellung unserer Mediendienste (Webseiten) weitere
personenbezogene Daten zur Nutzung eines Telemediendienstes erheben bzw.
verwenden.
1. DATENSCHUTZBEAUFTRAGTER
Unseren Datenschutzbeauftragten erreichen Sie per Post unter der o.g.
Adresse mit dem Zusatz „Datenschutzbeauftragter“ oder per E-Mail unter
anfragen-dsb@travel24.com.
2. DATENERHEBUNG UND –VERARBEITUNG
2.1 Buchung
Buchen Sie bei uns, schließen Sie mit uns einen Vertrag, für dessen
Erfüllung personenbezogene Daten wie Name/Namenszusätze, Adressdaten,
Kontakt-, Alters- und Zahlungsdaten von Ihnen und den Reisenden benötigt
und im Buchungsformular oder telefonisch erhoben werden. 
Mit dem Geburtsdatum stellt der Reiseveranstalter sicher, dass Sie als
Vertragspartner volljährig und geschäftsfähig sind. Anhand des Alters
werden ebenfalls die Reisepreise ermittelt. 
Die Daten werden an den jeweiligen Reiseveranstalter übergeben. Für
diese Übermittlung werden Dienstleister beauftragt, die auf Weisung von
uns tätig sind und denen es gesetzlich und vertraglich untersagt ist,
die Daten für eigene Zwecke zu nutzen.
2.1.1 Zwecke und Rechtsgrundlagen der Datenverarbeitung
Wir verarbeiten Ihre personenbezogenen Daten aufgrund der
datenschutzrechtlich relevanten Bestimmungen der
Datenschutz-Grundverordnung (DS-GVO), des Bundesdatenschutzgesetzes
Sie wünschen von uns die Abgabe eines Angebotes bzw. Erbringung einer
Dienstleistung durch unser Unternehmen. Ihre bereitgestellten Daten
werden zur Bearbeitung vorvertraglicher Maßnahmen bzw. zur Erfüllung
eines Vertrages benötigt. Rechtsgrundlage dieser Verarbeitungen ist Art.
6 Abs. 1b).
Darüber hinaus verarbeiten wir Ihre personenbezogenen Daten zur
Erfüllung gesetzlicher Verpflichtungen wie z.B. aufsichtsrechtlicher
Vorgaben, handels- und steuerrechtlicher Aufbewahrungspflichten. Als
Rechtsgrundlage dienen in diesem Fall die jeweiligen gesetzlichen
Regelungen i.V.m. Art. 6 Abs 1c) DS-GVO. 
Zur Sicherstellung der Systemsicherheit verarbeiten wir folgende Daten:
IP vom User, Datum/Uhrzeit vom Request, Request Size, http Status, User
Agent (Browser & ggf. OS) angefragte Domain, Name der Datei. Die Daten
werden gespeichert und nach Wegfall des Zweckes gelöscht.
Rechtsgrundlage der Verarbeitung ist Art. 6 Abs. 1f) DS-GVO.
Zum Zwecke der Direktwerbung verarbeiten wir Vorname, Nachname,
Anschrift und E-Mailadresse des Reiseanmelders. 
Sollten wir Ihre personenbezogenen Daten für einen oben nicht genannten
Zweck verarbeiten wollen, werden wir Sie im Rahmen der gesetzlichen
Bestimmungen darüber zuvor informieren.
2.1.2 Kategorien von Empfängern der personenbezogenen Daten
Ihre personenbezogenen Daten werden, sofern erforderlich, auf Grundlage
von Art. 28 DS-GVO und Art. 6 Abs. 2 an folgende Kategorien von
Empfängern übermittelt.
bereit)
einem Angebot oder zum Vertrag)
Darüber hinaus können wir Ihre personenbezogenen Daten an weitere
Kategorien von Empfängern übermitteln, wie etwa an Behörden zur
Erfüllung gesetzlicher Mitteilungspflichten (z.B.
Sozialversicherungsträger, Finanz- und Sozialbehörden oder
Strafverfolgungsbehörden, Gerichte).
2.2 Newsletter / E-Mail-Marketing
Wir versenden Newsletter, E-Mails und weitere elektronische
Benachrichtigungen mit werblichen Informationen (nachfolgend
gesetzlichen Erlaubnis. Wir bitten den Empfänger explizit um eine
Bestätigung zur Anmeldung zum Newsletterempfang und prüfen, ob die
Anmeldung tatsächlich über den Adressinhaber erfolgt ist. Diese
Rückversicherung per E-Mail ist das sog. Double-Opt-In-Verfahren und
gehört aufgrund fehlender werblicher Inhalte nicht zum Newsletter. Wir
protokollieren neben der E-Mail-Adresse u.a. den Zeitpunkt der ersten
Interaktion und die Bestätigung des Klicks auf den Anmeldelink in der
Double-Opt-In-E-Mail. 
Die Newsletter können Inhalte über die Travel24.com AG und deren
Produkte und Angebote enthalten sowie ergänzende Angebote zum Thema
Reisen und besondere Aktionen, u.a. auch von Partnern. Wie oft der
Newsletter versendet wird, ist abhängig von der Saison und vom Anlass.
Abgleich der Empfängeradressen mit den Kundendaten vorgenommen.
Mailings im Rahmen der Vertragsbeziehung (wie z.B. Buchungsbestätigung,
Service-E-Mails, technische Hinweise) gehören nicht zu den Newslettern. 
Es ist jederzeit kostenfrei möglich, den Newsletter abzubestellen. Dazu
können Sie entweder dieses Formular nutzen oder uns eine E-Mail
an SUPPORT@TRAVEL24.COM senden. Wir bitte Sie zu beachten, dass die
Umsetzung der Anfrage bis zu drei Werktage in Anspruch nehmen kann. 
2.2.1 Zwecke und Rechtsgrundlagen der Datenverarbeitung
Wir verarbeiten Ihre personenbezogenen Daten (E-Mail-Adresse, Name,
Standort, Browser, Datum der Anmeldung, Datum der Bestätigung des DOI)
aufgrund der datenschutzrechtlich relevanten Bestimmungen der
Datenschutz-Grundverordnung (DS-GVO), des Bundesdatenschutzgesetzes
Ihre Daten werden zum Zweck der Zusendung von Werbung und
Produktinformationen per E-Mail auf Grundlage Ihrer Einwilligung
verarbeitet. Rechtsgrundlage für diese Verarbeitung ist Art. 6 Abs. 1 a)
DS-GVO. 
Darüber hinaus verarbeiten wir Ihre personenbezogenen Daten zur
Erfüllung gesetzlicher Verpflichtungen wie z.B. aufsichtsrechtlicher
Vorgaben, handels- und steuerrechtlicher Aufbewahrungspflichten. Als
Rechtsgrundlage dienen in diesem Fall die jeweiligen gesetzlichen
Regelungen i.V.m. Art. 6 Abs 1c) DS-GVO. 
Sollten wir Ihre personenbezogenen Daten für einen oben nicht genannten
Zweck verarbeiten wollen, werden wir Sie im Rahmen der gesetzlichen
Bestimmungen darüber zuvor informieren.
2.2.2 Kategorien von Empfängern der personenbezogenen Daten
Ihre personenbezogenen Daten werden, sofern erforderlich, zur
Leistungserbringung auf Grundlage von Art. 28 DS-GVO und Art. 6 Abs. 2
an folgende Kategorien von Empfängern übermittelt.
E-Mail-Marketing und -Versendetool 
Darüber hinaus können wir Ihre personenbezogenen Daten an weitere
Kategorien von Empfängern übermitteln, wie etwa an Behörden zur
Erfüllung gesetzlicher Mitteilungspflichten (z.B.
Sozialversicherungsträger, Finanz- und Sozialbehörden oder
Strafverfolgungsbehörden, Gerichte).
2.3 Kontaktformulare 
2.3.1 Zwecke und Rechtsgrundlagen der Datenverarbeitung
Wir verarbeiten Ihre personenbezogenen Daten aufgrund der
datenschutzrechtlich relevanten Bestimmungen der
Datenschutz-Grundverordnung (DS-GVO), des Bundesdatenschutzgesetzes
Ihre Daten werden zur Bearbeitung Ihres Begehrens bzgl. der Durchführung
vorvertraglicher Maßnahmen, Erfüllung eines Vertrages oder zur Wahrung
unserer berechtigten Interessen oder eines Dritten verarbeitet.
Rechtsgrundlage dieser Verarbeitungen ist Art. 6 Abs. 1b) und Art. 6
Abs. 1f) DS-GVO. 
Unser berechtigtes Interesse an der Datenverarbeitung besteht in der
Bearbeitung Ihres Begehrens insofern sich dieses nicht auf die
Durchführung vorvertraglicher Maßnahmen oder Erfüllung eines Vertrages
stützt. Dieser Verarbeitung stehen Ihre schutzwürdigen Interessen,
Grundrechte und Grundfreiheiten nicht entgegen, da die Angabe der Daten
auf Grund Ihrer freien Entscheidung erfolgt und Ihre Betroffenenrechte
gewährleistet werden.
Darüber hinaus verarbeiten wir Ihre personenbezogenen Daten zur
Erfüllung gesetzlicher Verpflichtungen wie z.B. aufsichtsrechtlicher
Vorgaben, handels- und steuerrechtlicher Aufbewahrungspflichten. Als
Rechtsgrundlage dienen in diesem Fall die jeweiligen gesetzlichen
Regelungen i.V.m. Art. 6 Abs. 1c) DS-GVO. 
Sollten wir Ihre personenbezogenen Daten für einen oben nicht genannten
Zweck verarbeiten wollen, werden wir Sie im Rahmen der gesetzlichen
Bestimmungen darüber zuvor informieren.
2.3.2 Kategorien von Empfängern der personenbezogenen Daten
Ihre personenbezogenen Daten werden, sofern erforderlich, auf Grundlage
von Art. 28 DS-GVO und Art. 6 Abs. 2 an folgende Kategorien von
Empfängern übermittelt.
IT-Dienstleister (stellen Informations- und Telekommunikationsdienste
bereit)
Versanddienstleister (Zustellung von Dokumenten)
Kundenservicedienstleister (Dienstleister zur Bearbeitung Ihrer Anliegen
und Fragen)
Darüber hinaus können wir Ihre personenbezogenen Daten an weitere
Kategorien von Empfängern übermitteln, wie etwa an Behörden zur
Erfüllung gesetzlicher Mitteilungspflichten (z.B.
Sozialversicherungsträger, Finanz- und Sozialbehörden oder
Strafverfolgungsbehörden, Gerichte).
2.4 Dauer der Datenspeicherung
Wir löschen Ihre personenbezogenen Daten sobald sie für die oben
genannten Zwecke nicht mehr erforderlich sind. Dabei kann es vorkommen,
dass personenbezogene Daten für die Zeit aufbewahrt werden, in der
Ansprüche gegen unser Unternehmen geltend gemacht werden. Zudem
speichern wir Ihre personenbezogenen Daten soweit wir dazu gesetzlich
verpflichtet sind. Entsprechende Nachweis- und Aufbewahrungspflichten
ergeben sich, unter anderem aus dem Handelsgesetzbuch, der
Abgabenordnung und den Sozialgesetzbüchern und betragen demnach bis zu
10 Jahren.
2.5 Betroffenenrechte
Sie können unter der o.g. Adresse Auskunft über die zu Ihrer Person
gespeicherten Daten verlangen. Darüber hinaus können Sie unter
bestimmten Voraussetzungen die Berichtigung oder die Löschung Ihrer
Daten verlangen. Ihnen kann weiterhin ein Recht auf Einschränkung der
Verarbeitung Ihrer Daten sowie ein Recht auf Herausgabe bzw. Übertragung
der von Ihnen bereitgestellten Daten in einem strukturierten, gängigen
und maschinenlesbaren Format zustehen.
Darüber hinaus haben Sie ein Widerspruchsrecht gegen die Verarbeitung
Ihrer personenbezogenen Daten.
Sie haben das Recht, sich mit einer Beschwerde an eine
Datenschutzaufsichtsbehörde zu wenden.
2.6 Datenbereitstellung
Die Bereitstellung Ihrer Daten ist nicht gesetzlich oder vertraglich
vorgeschrieben. Sie sind nicht verpflichtet die personenbezogenen Daten
bereit zu stellen. Bei Nichtbereitstellung Ihrer Daten ist jedoch eine
Erstellung eines Angebotes bzw. die Erstellung eines Beratungsvertrages
leider nicht möglich.
3. ERHEBEN UND VERWENDEN VON IP-ADRESSEN
Einem Computer, der eine Verbindung mit dem Internet herstellt, wird
durch einen Internetdienstanbieter eine eindeutige Nummer zugewiesen.
Diese ist die Internet Protocol (IP)-Adresse. Je nach Dienst des Nutzers
kann diesem durch den Provider jedes Mal eine andere Adresse zugewiesen
werden, wenn der Nutzer eine Verbindung mit dem Internet herstellt. Da
IP-Adressen üblicherweise in länderbasierten Blöcken zugewiesen werden,
kann eine IP-Adresse oftmals dazu verwendet werden, das Land zu
identifizieren, in dem mit diesem Computer eine Verbindung mit dem
unser Web-Server temporär folgendes Serverprotokoll auf: die IP-Adresse,
den Namen der abgerufenen Datei, Datum und Uhrzeit des Abrufs, die
Browsertyp nebst Version, das Betriebssystem des Nutzers, die Referrer
URL (die zuvor besuchte Seite) und den anfragenden Provider. Die
Erhebung und Verwendung dieser Protokolldaten erfolgt durch uns zum
Zweck der Systemsicherheit und der funktionsgerechten Gestaltung der
Webseite. Soweit dies nicht der Fall ist, werden IP-Adressen durch uns
vor einer weiteren Verwendung so gekürzt, dass ein eventueller
Personenbezug nicht mehr möglich ist. Treten Sie mit uns in ein
Vertragsverhältnis, erfolgt die Erhebung und Verwendung der
Protokolldaten für Zwecke der Vertragsdokumentation und der Begegnung
von Missbrauch. Soweit in unsere Webseiten Inhalte Dritter, wie zum
Beispiel Videos, Kartenmaterial, RSS-Feeds, Grafiken oder Social Plugins
von anderen Diensteanbietern (Webseiten) eingebunden werden, setzt dies
technisch immer voraus, dass diese Diensteanbieter (“Dritt-Anbieter”)
die IP-Adresse der Nutzer wahrnehmen. Denn ohne die IP-Adresse könnten
sie ihre Inhalte nicht an den Browser des jeweiligen Nutzers senden. Die
IP-Adresse ist damit für die Darstellung dieser Inhalte erforderlich.
Wir haben keinen Einfluss darauf, falls die Dritt-Anbieter die
IP-Adresse z.B. für statistische oder andere Zwecke verwenden. Soweit
uns dies bekannt ist, klären wir die Nutzer nachfolgend darüber auf.
4. COOKIES
4.1 Eigene Cookies
Ihrem Endgerät. Ein Cookie ist eine kleine Datei, die eine Zeichenkette
enthält, welche an Ihren Computer bzw. das Endgerät gesendet wird, wenn
diese Ihren Browser mithilfe des Cookies erkennen. Cookies können
ermöglichen es uns, unsere Webseiten nach Ihren Präferenzen zu
hat aber nur Zugriff auf die Informationen, die Sie selbst
Zugang zu anderen Informationen auf Ihrem Computer erlangen. Sie können
Ihren Browser jedoch vor oder nach Ihrem Besuch unserer Webseite
wenn ein Cookie gesendet wird. Standardmäßig lässt sich das Setzen von
Cookies durch das Browser-Programm Ihres Browsers verwalten, auch so,
dass gar keine Cookies gesetzt werden dürfen oder dass Cookies wieder
gelöscht werden. Ihr Browser verfügt möglicherweise auch über eine
Funktion für das anonyme Surfen. Diese Funktionen Ihres Browsers zu
nutzen, haben Sie jederzeit selbst in der Hand. Deaktivieren Sie das
Setzenlassen von Cookies in Ihrem Browser standardmäßig, kann es sein,
dass unsere Webseiten nicht richtig funktionieren. 
Soweit in unsere Webseiten Inhalte Dritter eingebunden werden, setzten
diese Dritt-Anbieter unter Umständen Cookies, was Sie in Ihrem Browser
durch Vornahme bestimmter Einstellungen standardmäßig ebenfalls
verhindern können. Deaktivieren Sie das Setzenlassen von Cookies in
Ihrem Browser standardmäßig, kann es sein, dass auch die Webseiten der
Dritt-Anbieter nicht richtig funktionieren.
diese Technologien, um für Sie das Internetangebot interessanter zu
gestalten. Diese Technik ermöglicht es, Internetnutzer, die sich bereits
Cookie-Technologie und einer Analyse des vorherigen Nutzungsverhaltens.
Diese Analyse erfolgt unter einem Pseudonym und es werden keine
Nutzungsprofile mit Ihren personenbezogenen Daten zusammengeführt. Nach
Maßgabe der nachstehenden Erläuterungen können Sie der Erstellung von
Nutzungsprofilen widersprechen.
Weitere Informationen zum Widerspruch gegen die Erstellung dieser
unter https://www.facebook.com/ads/website_custom_audiences.
4.3. Cookie Zustimmung widerrufen
Sie können auch nach bereits vollzogener Cookie Zustimmung Ihre
Entscheidung hier jederzeit widerrufen.
Auf unserer Webseite sind für Statistik- und Werbezwecke sowie zur
Integration sozialer Netzwerke Inhalte von dritten Anbietern integriert.
Diese ermöglichen die Nutzung und Optimierung der Webseite. **
Auf Travel24.com ist Google Analytics, einen Webanalysedienst der Google
im Einsatz. 
einen Server von Google in den USA übertragen und dort gespeichert. Auf
unserer Webseite ist die IP-Anonymisierung aktiv, d.h. Ihre IP-Adresse
wird von Google innerhalb von Mitgliedstaaten der Europäischen Union
oder in anderen Vertragsstaaten des Abkommens über den Europäischen
Wirtschaftsraum zuvor gekürzt und anonymisiert. 
Die im Rahmen von Google Analytics von Ihrem Browser übermittelte
IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt. 
Websiteaktivitäten für uns zusammenzustellen und um weitere mit der
Websitenutzung und der Internetnutzung verbundene Dienstleistungen
gegenüber der Travel24.com AG zu erbringen. In diesen Zwecken liegt auch
unser berechtigtes Interesse an der Datenverarbeitung. 
Die Rechtsgrundlage für den Einsatz von Google Analytics ist § 15 Abs. 3
TMG bzw. Art. 6 Abs. 1 lit. f DS-GVO. 
Die von uns gesendeten und mit Cookies, Nutzerkennungen (z. B. User-ID)
oder Werbe-IDs verknüpften Daten werden nach 26 Monaten automatisch
gelöscht. Die Aufbewahrungsdauer der Nutzer-ID wird bei jeder neuen
Aktivität eines Nutzers zurückgesetzt. D.h. Das Ablaufdatum wird dadurch
auf die aktuelle Zeit plus die Aufbewahrungsdauer festgelegt. Die
Löschung von Daten, deren Aufbewahrungsdauer erreicht ist, erfolgt
automatisch einmal im Monat. Nähere Informationen zu Nutzungsbedingungen
und Datenschutz finden Sie
unter https://www.google.com/analytics/terms/de.html bzw.
unter https://policies.google.com/?hl=de.
Sie können die Speicherung der Cookies durch eine entsprechende
Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch
darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche
können darüber hinaus die Erfassung der durch das Cookie erzeugten und
Google sowie die Verarbeitung dieser Daten durch Google verhindern,
indem Sie das Browser-Add-on herunterladen und installieren. Das
Deaktivierungs-Add-on für Browser von Google Analytics verhindert aber
nicht, dass Informationen an uns oder an andere von uns gegebenenfalls
eingesetzte Webanalyse-Services übermittelt werden.
Wenn Sie unsere Seite über ein mobiles Endgerät (Smartphone oder Tablet)
besuchen, müssen Sie stattdessen diesen Link klicken, um das Tracking
unterbinden. Dies ist auch als Alternative zu obigem Browser-Add-On
möglich. Durch Klicken des Links wird ein Opt-Out-Cookie in Ihrem
Browser gesetzt, das nur für diesen Browser und diese Domain gültig ist.
Wenn Sie die Cookies in diesem Browser löschen, wird auch das
Opt-Out-Cookie gelöscht, so dass Sie den Link erneut klicken müssen.
Die Webseite verwendet den Google Tag Manager. Der Tag Manager ist ein
Tool, welches dazu dient, andere Tags zu integrieren, welche ihrerseits
Daten erfassen können. Der Tag Manager selbst erfasst keine
personenbezogenen Daten und greift nicht auf Daten Dritter zu. Wenn auf
Domain- oder Cookie-Ebene eine Deaktivierung vorgenommen wurde, bleibt
diese für alle Tracking-Tags bestehen, die mit Google Tag Manager
implementiert werden. Weitere Hinweise von Google zu diesem Tool finden
Sie hier: https://marketingplatform.google.com/about/tag-manager/
Diese Webseite verwendet Tools für “Online-Marketing” der Firma Google
Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA. Damit
Webseite gestoßen ist. Google verwendet Cookies, die auf Ihrem Computer
gesetzt, wenn Sie auf eine von Google geschaltete Anzeige klicken. Diese
Cookies verlieren nach 30 Tagen ihre Gültigkeit und dienen nicht der
persönlichen Identifizierung. Wenn Sie das “Conversion-Tracking”
Ihr Recht auf Opt-Out wahrnehmen über die Seite:
www.networkadvertising.org/choices/. Falls Sie mehr über diese Methoden
erfahren möchten oder wissen möchten, welche Möglichkeiten Sie haben,
damit diese Informationen nicht von Google verwendet werden können,
klicken Sie hier:
https://www.google.com/settings/u/0/ads/authenticated 
Bestandteil unseres Webseitenangebotes sind Social Plugins ("Plugins")
des sozialen Netzwerkes facebook.com, welches von der Facebook Inc.,
1601 S. California Ave, Palo Alto, CA 94304, USA betrieben wird
Social Plugin" gekennzeichnet. Die Liste und das Aussehen der Facebook
Social Plugins kann hier eingesehen werden:
developers.facebook.com/plugins. Wenn Sie eine Webseite unseres
Internetauftritts aufrufen, die ein solches Plugin enthält, baut Ihr
und von diesem in die Webseite eingebunden. Auf diesen Datenaustausch
und weitere Verarbeitungen durch Facebook – bei zugleich aktivem
Facebook-Account – haben wir keinen Einfluss. Informationen dazu
vermittelt die Seite von Facebook
unter: http://www.facebook.com/help/186325668085084/
Sind Sie zum Zeitpunkt Ihres Besuches auf unserer Webseite bei Facebook
Facebook-Konto zuordnen, unabhängig davon, ob Sie den Facebook-Button
anklicken oder nicht. Falls Sie kein Mitglied von Facebook sind, besteht
trotzdem die Möglichkeit, dass Facebook Ihre IP-Adresse in Erfahrung
bringt und speichert. Informationen über Zweck und Umfang der
Datenerhebung und der weiteren Verarbeitung und der Nutzung der Daten
Privatsphäre und Ihrer Daten können Sie den Datenschutzrichtlinien von
unter http://www.facebook.com/policy.php finden. Facebook informiert
zudem unter http://de-de.facebook.com/privacy/explanation.php über
Einstellungen, mit denen Sie Ihre Privatsphäre schützen können.
Ausloggen bei Facebook 
Wenn Sie nicht wünschen, dass Facebook, Google und Twitter über unsere
Webseite Daten über Sie erheben und verwenden und ggf. mit Ihren bei
Facebook, Google+ und Twitter gespeicherten Mitgliedsdaten verknüpft,
loggen Sie sich bitte vor dem Besuch unserer Webseite bei Facebook,
Google+1 und Twitter bzw. aus Ihrem jeweiligen Profil aus. Falls Sie
die Möglichkeit, dass Facebook Ihre IP-Adresse in Erfahrung bringt und
speichert, wenn auch diese Daten nicht mit Mitgliedsdaten verknüpft
werden können.
Die wesentlichen Aspekte der Vereinbarung über die gemeinsame
Verantwortlichkeit von Facebook und Travel24.com AG nach Art. 26
Datenschutzgrundverordnung (DS-GVO) finden sie
unter https://www.facebook.com/legal/terms/page_controller_addendum. 
5.5 Uptain Plugin
Java-Script Plugin der uptain GmbH ("uptain-Plugin). Dies erlaubt uns
eine Analyse Ihrer Benutzung der Webseite und eine Verbesserung der
Kundenansprache (z.B. durch ein Dialogfenster). Hierzu erheben wir
Informationen über Ihr Nutzungsverhalten, d.h. Bewegung des Cursors,
Verweildauer, angeklickte Links und ggf. gemachte Angaben.
Rechtsgrundlage der Verarbeitung ist unser berechtigtes Interesse an
Direktmarketing und der Bereitstellung unserer Webseite. Die uptain GmbH
ist dabei als Auftragsverarbeiter strikt an unsere Weisungen gebunden.
Die erhobenen Informationen werden nicht an Dritte weitergegeben, außer
wir sind dazu gesetzlich verpflichtet. Soweit die vom uptain-Plugin
erhobenen Informationen personenbezogene Daten enthalten, werden diese
unmittelbar nach Ihrem Besuch unserer Webseite gelöscht.
SIE KÖNNEN DEN EINSATZ DES UPTAIN-PLUGINS JEDERZEIT ÜBER FOLGENDEN LINK
DEAKTIVIEREN: HIER KLICKEN ZUM DEAKTIVIEREN
5.6 Axel Springer Teaser Ad
Auf unseren Webseiten ist das Tracking von Ad Up, einem Technologie- und
Dienstleistungsanbieter der Axel Springer Teaser Ad GmbH
anonymisierter und/oder pseudonymer Daten kann Ad Up anschließend für
lassen.
ermittelt.
erhalten sie unter https://www.adup-tech.com/datenschutz/. Über den dort
vorhandenen Button „Opt-Out-Cookie aktivieren“ haben Sie auch die
Möglichkeit, in Ihrem Browser einen Opt-Out-Cookies zu setzen und damit
Ad Up in diesem Browser zu deaktivieren.
5.7 Verwendung des Live-Chat-Systems LiveZilla
Wir verwenden auf unserer Website das Chat System der LiveZilla GmbH
Das System dient dem Zweck der Kommunikation zwischen Ihnen und der
Travel24.com AG. Dabei werden unter anderem Daten zu Marketing- und
Optimierungszwecken gesammelt und gespeichert. Aus diesen Daten können
unter einem Pseudonym Nutzungsprofile erstellt werden.
Internet-Browsers ermöglichen. Die mit den LiveZilla-Technologien
erhobenen Daten werden ohne die gesondert erteilte Zustimmung des
zu identifizieren und nicht mit personenbezogenen Daten über den Träger
des Pseudonyms zusammengeführt.
Die Verarbeitung erfolgt auf Grundlage des Art. 6 (1) f) DSGVO aus dem
berechtigten Interesse an direkter Kundenkommunikation und an der
Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit
dieser auf Art. 6 (1) f DSGVO beruhenden Verarbeitung Sie betreffender
personenbezogener Daten zu widersprechen.
Sie können den Widerspruch ausüben, indem Sie die Speicherung der
Cookies durch eine entsprechende Einstellung Ihrer Browser-Software
verhindern. Wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall
5.8 Datenschutzerklärung e-hoi
der e-domizil GmbH, Taunusstr. 21, D-60329 Frankfurt a.M. Deren
Datenschutzerklärung ist hier zu
finden: https://www.e-hoi.de/datenschutz.html
5.9 Datenschutzerklärung atraveo
Im Bereich Ferienwohnungen kooperieren wir mit der Firma Wolters Reisen
GmbH
Bremer Straße 61, 28816 Stuhr, Deutschland. Deren Datenschutzererklärung
ist hier zu finden: https://fewo.travel24.com/datenschutz
6. DATENGEHEIMNIS
Unsere bei der Datenverarbeitung beschäftigten Personen sind zur Wahrung
der Vertraulichkeit auf das Datengeheimnis verpflichtet worden.
7. IHRE RECHTE 
Wir möchten Sie nun darüber informieren, welche Rechte Ihnen zustehen,
wenn wir personenbezogene Daten von Ihnen verarbeiten.
Sie können von uns eine Bestätigung darüber verlangen, ob
personenbezogene Daten, die Sie betreffen, von uns verarbeitet werden.
Liegt eine solche Verarbeitung vor, können Sie von uns Auskunft über
alle in Art. 15 DS-GVO aufgeführten Informationen verlangen.
Ihnen steht das Recht zu, Auskunft darüber zu verlangen, ob die Sie
betreffenden personenbezogenen Daten in ein Drittland oder an eine
internationale Organisation übermittelt werden. In diesem Zusammenhang
können Sie verlangen, über die geeigneten Garantien gemäß Art. 46 DS-GVO
im Zusammenhang mit der Übermittlung unterrichtet zu werden.
Sie haben ein Recht auf Berichtigung und/ oder Vervollständigung, sofern
die verarbeiteten personenbezogenen Daten, die Sie betreffen, unrichtig
oder unvollständig sind. Wir haben die Berichtigung unverzüglich
vorzunehmen.
Sie können die Einschränkung der Verarbeitung der Sie betreffenden
personenbezogenen Daten verlangen unter den in Art. 18 Abs. 1 DS-GVO
genannten Voraussetzungen.
Wurde die Verarbeitung der Sie betreffenden personenbezogenen Daten
eingeschränkt, dürfen diese Daten – von ihrer Speicherung abgesehen –
nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder
Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer
anderen natürlichen oder juristischen Person oder aus Gründen eines
wichtigen öffentlichen Interesses der Union oder eines Mitgliedstaats
verarbeitet werden.
Wurde die Einschränkung der Verarbeitung nach den o.g. Voraussetzungen
eingeschränkt, werden Sie von dem Verantwortlichen unterrichtet bevor
die Einschränkung aufgehoben wird.
Sie können von uns verlangen, dass die Sie betreffenden
personenbezogenen Daten unverzüglich gelöscht werden, und wir sind
verpflichtet, diese Daten unverzüglich zu löschen, sofern einer der in
Art. 17 Abs. 1 DS-GVO genannten Gründe vorliegt.
Haben wir die Sie betreffenden personenbezogenen Daten öffentlich
gemacht und sind wir gemäß Art.17 Abs. 1 DS-GVO zu deren Löschung
verpflichtet, so treffen wir unter Berücksichtigung der verfügbaren
Technologie und der Implementierungskosten angemessene Maßnahmen, auch
technischer Art, um für die Datenverarbeitung Verantwortliche, die die
personenbezogenen Daten verarbeiten, darüber zu informieren, dass Sie
als betroffene Person von ihnen die Löschung aller Links zu diesen
personenbezogenen Daten oder von Kopien oder Replikationen dieser
personenbezogenen Daten verlangt haben.
Das Recht auf Löschung besteht nicht, soweit die Verarbeitung aus den
Gründen des Art. 17 Abs. 3 DS-GVO - z.B. zur Verteidigung von
Rechtsansprüchen oder zur Erfüllung gesetzlicher Aufbewahrungspflichten
Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der
Verarbeitung gegenüber uns geltend gemacht, sind wir verpflichtet, allen
Empfängern, denen die Sie betreffenden personenbezogenen Daten
offengelegt wurden, diese Berichtigung oder Löschung der Daten oder
Einschränkung der Verarbeitung mitzuteilen, es sei denn, dies erweist
sich als unmöglich oder ist mit einem unverhältnismäßigen Aufwand
verbunden.
Ihnen steht das Recht zu, über diese Empfänger unterrichtet zu werden.
Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die
uns bereitgestellt haben, in einem strukturierten, gängigen und
maschinenlesbaren Format zu erhalten. Außerdem haben Sie das Recht diese
Daten einem anderen Verantwortlichen zu übermitteln, sofern die
Voraussetzungen des Art. 20 Abs. 1 DS-GVO vorliegen.
In Ausübung dieses Rechts haben Sie ferner das Recht, zu erwirken, dass
die Sie betreffenden personenbezogenen Daten direkt von uns einem
anderen Verantwortlichen übermittelt werden, soweit dies technisch
machbar ist. Freiheiten und Rechte anderer Personen dürfen hierdurch
nicht beeinträchtigt werden.
Das Recht auf Datenübertragbarkeit gilt nicht für eine Verarbeitung
personenbezogener Daten, die für die Wahrnehmung einer Aufgabe
erforderlich ist, die im öffentlichen Interesse liegt oder in Ausübung
Sie haben das Recht, aus Gründen, die sich aus ihrer besonderen
Situation ergeben, jederzeit gegen die Verarbeitung der Sie betreffenden
personenbezogenen Daten, die aufgrund von Art. 6 Abs. 1 lit.n e oder f
DS-GVO erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese
Bestimmungen gestütztes Profiling.
Wir verarbeiten in diesem Fall die Sie betreffenden personenbezogenen
Daten nicht mehr, es sei denn, wir können zwingende schutzwürdige Gründe
für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und
Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung,
Ausübung oder Verteidigung von Rechtsansprüchen.
Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um
Direktwerbung zu betreiben, haben Sie das Recht, jederzeit Widerspruch
gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten zum
Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling,
soweit es mit solcher Direktwerbung in Verbindung steht.
Widersprechen Sie der Verarbeitung für Zwecke der Direktwerbung, so
werden die Sie betreffenden personenbezogenen Daten nicht mehr für diese
Zwecke verarbeitet.
Sie haben die Möglichkeit, im Zusammenhang mit der Nutzung von Diensten
der Informationsgesellschaft – ungeachtet der Richtlinie 2002/58/EG –
Ihr Widerspruchsrecht mittels automatisierter Verfahren auszuüben, bei
denen technische Spezifikationen verwendet werden.
Unbeschadet eines anderweitigen verwaltungsrechtlichen oder
gerichtlichen Rechtsbehelfs steht Ihnen das Recht auf Beschwerde bei
einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres
Aufenthaltsorts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen
Verstoßes, zu, wenn Sie der Ansicht sind, dass die Verarbeitung der Sie
betreffenden personenbezogenen Daten gegen die DS-GVO verstößt.
Die Aufsichtsbehörde, bei der die Beschwerde eingereicht wurde,
unterrichtet den Beschwerdeführer über den Stand und die Ergebnisse der
Beschwerde einschließlich der Möglichkeit eines gerichtlichen
Rechtsbehelfs nach Art. 78 DS-GVO.
Bei uns buchen sie sicher und günstig
Bequem bezahlen
Folgen Sie uns
