1.  AGB
ALLGEMEINE GESCHÄFTSBEDINGUNGEN DER TRAVEL24.COM
A. VORBEMERKUNG
Die nachfolgenden Bestimmungen regeln das Verhältnis zwischen dem Nutzer
Bevor Sie diese Website nutzen und oder die von uns angebotenen
Dienstleistungen in Anspruch nehmen, lesen Sie bitte die nachfolgenden
B. RECHTSVERHÄLTNIS ZWISCHEN DEM NUTZER UND TRAVEL24.COM AG BZW. DEM
LEISTUNGSERBRINGER
I. VERMITTLUNGSVERTRAG
Wir treten ausschließlich als Vermittler der auf unserem Portal
angebotenen Beförderungsleistungen, Pauschalreisen, Unterkünften,
gelten ausschließlich für den zwischen Ihnen und uns geschlossenen
Vermittlungsvertrag.
Unsere Pflichten beschränken sich auf die Vermittlungsleistung. Wir
haben insoweit lediglich den Status eines Reisebüros. Insbesondere
veranstalten wir keine eigenen Reisen im Sinne der §§ 651a ff. BGB oder
erbringen die sonstigen vermittelten Leistungen. Die ordnungsgemäße
Erfüllung der vermittelten Leistung als solche gehört nicht zu den
Vertragspflichten. Ausnahmen bestehen nur in dem in Abschnitt E
geregelten Umfang.
Sofern im Rahmen des Buchungsvorgangs die Möglichkeit besteht,
Sonderwünsche anzugeben, sind diese stets unverbindlich. Wir werden
diese an den Leistungserbringer weiterleiten, können jedoch keine
Garantie dafür übernehmen, dass diesen Wünschen entsprochen wird.
II. HAUPTVERTRAG
Der die Hauptleistung betreffende Vertrag (Reisevertrag,
Beförderungsvertrag, Mietvertrag, Versicherungsvertrag) kommt zwischen
Versicherer, Mietwagenanbieter, Hotel; im Folgenden:
Buchungsanfrage übermittelte Nachricht stellt lediglich eine
Zusammenfassung und Eingangsbestätigung der verbindlichen
Buchungsanfrage dar.
Das Vertragsverhältnis betreffend die Hauptleistung wird durch die
Allgemeinen Geschäftsbedingungen (auch: Beförderungsbedingungen /
Versicherungsbedingungen) geregelt, die der Leistungserbringer vorgibt
und die wir Ihnen im Buchungsverlauf zur Kenntnis bringen. Bitte lesen
Sie diese Bedingungen sorgfältig durch, da sich hieraus
vertragsrelevante Informationen (z.B. Zahlungs-, Umbuchungs-,
Rücktrittsbedingungen, Haftungsregelungen usw.) ergeben.
C. ALLGEMEINE GESCHÄFTSBEDINGUNGEN FÜR DIE VERMITTLUNG VON
REISELEISTUNGEN
I. PRÜFEN UND BERICHTIGEN VON BUCHUNGSDATEN
1.  Prüfen Sie vor Abschluss der Buchungsanfrage alle Daten auf deren
2.  Unüberlegte Änderungswünsche können ggf. zu erheblichen Mehrkosten
3.  Bitte überprüfen Sie auch die in der Bestätigung der Buchungsanfrage
II. GEBÜHREN UND EINZUG DES REISEPREISES
1.  Für unsere Vermittlungstätigkeit sowie nachgelagerte
2.  Die Zahlungsabwicklung erfolgt entweder durch den Leistungserbringer
3.  Die akzeptieren Zahlungsmittel hängen im Fall des Direktinkassos vom
4.  Für den Fall der Rückgabe oder Nichteinlösung einer (SEPA-)
III. ALLGEMEINE UMBUCHUNGS- UND STORNIERUNGSBEDINGUNGEN
1.  Die Änderung der gebuchten Leistung (insbesondere Umbuchung,
2.  Wir verlangen – mit Ausnahme der in Punkt IV (Besondere Regelungen
IV. BESONDERE REGELUNGEN BEI FLUGBUCHUNGEN
1.  Wir erheben für einen Teil des Flugvermittlungsangebots Gebühren für
2.  Darüber hinaus erheben wir Gebühren für Dienstleistungen des
3.  Wir bitten Sie, bei jeder Änderung der Buchung Ihren Wunsch direkt
4.  Soweit Änderungen auf Ihren Wunsch von uns abgewickelt werden,
5.  Für den Fall der nicht rechtzeitigen Zahlung, insbesondere auch,
6.  Bei Nichterscheinen bzw. Nichtantritt des Fluges ermächtigen Sie uns
7.  Die Ziffern 2 bis 5 gelten ausschließlich für durch uns vermittelte
8.  Bitte beachten Sie die Tarifbestimmungen der jeweiligen
V. BESONDERE REGELUNGEN BEI HOTELBUCHUNGEN
1.  Wir erheben für einen Teil des Hotelvermittlungsangebots Gebühren
2.  Die Bestätigung der Reservierung wird Ihnen unmittelbar nach
3.  Abhängig vom Leistungserbringer können Ihnen weitere Unterlagen per
4.  Über die Einzelheiten der Stornierungs- und Änderungsmöglichkeiten
5.  Die von uns angezeigte Hotel-Klassifizierung entspricht den Angaben
VI. BESONDERE REGELUNGEN FÜR DIE VERMITTLUNG VON MEHREREN
EINZELLEISTUNGEN
Auf unserer Buchungsstrecke bieten wir Ihnen teilweise die Möglichkeit,
flexibel Einzelleistungen nach Ihren Wünschen zu kombinieren. Sie
beauftragen uns in diesem Fall, verschiedene Reiseleistungen bei
verschiedenen Leistungsträgern zu buchen. Hierbei werden wir die
jeweiligen Leistungsträger vor Abschluss der Buchung hinreichend
kennzeichnen. In diesem Fall kommt kein Reisevertrag zwischen Ihnen und
uns zustande, sondern mehrere Verträge über die Bereitstellung der
Einzelleistungen mit den jeweiligen Leistungsträgern. Wir sind
ausschließlich Vermittler hinsichtlich jeder einzelnen Reiseleistung.
Dies gilt auch, wenn die einzelnen Leistungen auf einer gemeinsamen
Rechnung zusammengefasst werden.
Sie werden ausdrücklich darauf hingewiesen, dass Ihnen aus der
Aufteilung der Reiseleistungen auf verschiedene Leistungsträger auch
Nachteile erwachsen können. So kann bei einer Aufteilung der gewünschten
Leistungen auf verschiedene Leistungsträger der Fall eintreten, dass
kein Leistungsträger als Reiseveranstalter anzusehen ist und Ihnen weder
für die gesamten noch für einzelne Leistungen ein Sicherungsschein von
den Leistungsträgern ausgehändigt wird. Sie haben in diesem Falle also
keine Absicherung gegen die Insolvenz des Leistungsträgers.
Zudem wird bei einer evtl. Schlechtleistung eines Leistungsträgers
hinsichtlich der von diesem geschuldeten Leistung dann
Berechnungsgrundlage für eine Minderung des Reisepreises in aller Regel
nur der an diesen abgeführte Reisepreis sein, nicht jedoch der insgesamt
für die Reise aufgewendete Betrag.
VII. HINWEISE AUF PASS-, VISA, DEVISEN- UND GESUNDHEITSBESTIMMUNGEN
Für die Einhaltung etwaiger Pass-, Visa-, Devisen- und/oder
Gesundheitsbestimmungen im jeweiligen Reisezielland sind Sie selbst
verantwortlich. Wir empfehlen Ihnen sich daher möglichst frühzeitig
hierüber bei den dazu autorisierten Stellen (Konsulate, Botschaften
etc.) zu informieren. Dies gilt insbesondere im Hinblick auf die Frage,
ob bei planmäßigen Zwischenlandungen ein Visum für das Transitland
erforderlich ist.
VIII. HAFTUNG UND VERJÄHRUNG
1.  Hinsichtlich der einzelnen Angaben und Bedingungen zu den
2.  Unsere Haftung auf Schadensersatz aus dem Vermittlungsvertrag ist
3.  Etwaige Ansprüche gegen uns verjähren abweichend von § 195 BGB
4.  Die obigen Haftungsbeschränkungen gelten nicht bei Verletzung des
IX. NEWSLETTER
Zu den Angaben, die Sie uns im Rahmen dieses Vertragsabschlusses (Kauf
einer Ware oder der Bestellung einer Dienstleistung) machen, gehört auch
Ihre E-Mail-Anschrift. Wir möchten Ihnen unseren Newsletter zusenden,
der Sie regelmäßig über unsere Angebote informiert, soweit Sie der
Verwendung Ihrer E-Mail-Adresse für diese Zwecke nicht widersprochen
haben werden.
Sie können den Text der nachstehend von Ihnen eingeholten und zu
erteilenden Einwilligung jederzeit über eine E-Mail an unsere Anschrift
abmeldung@travel24.com an eine von Ihnen bestätigte E-Mail-Anschrift
abrufen und Sie können außerdem der Verwendung Ihrer E-Mail-Anschrift
für diese Zwecke jederzeit widersprechen, also den Newsletterbezug
abbestellen, ohne dass hierfür andere als die Übermittlungskosten nach
den Basistarifen entstehen. Diese beiden Hinweise werden wir in jedem
unserer Newsletter wiederholen.
Ferner können Sie diese Einwilligung auch jederzeit frei, ohne
Begründung, durch entsprechende Erklärung durch eine E-Mail an unsere
widerrufen; Sie erhalten auch in diesem Falle dann diesen Newsletter
nicht mehr.
Sie willigen mit folgender Erklärung darin ein, dass wir Ihre
E-Mail-Anschrift für die Versendung unseres Newsletters nutzen dürfen:
Ja, ich möchte von der Travel24.com AG regelmäßig Angebote über das
Themengebiet Reisen per E-Mail erhalten. Diese Einwilligung in die
Nutzung meiner E-Mail-Adresse zu diesem Zwecke durch die Travel24.com AG
Newsletter werde ich auf mein Recht zum Widerruf hingewiesen sowie
darauf, wo ich ihn einlegen kann.
X. BEWERTUNG VON REISELEISTUNGEN
a) Sie verpflichten sich keine Inhalte einzustellen, die:
gewaltverherrlichend und/oder pornographischer Art sind;
strafbarer/ rechtswidriger Art sind;
Urheber, Kennzeichen, Patent, Marken- oder Leistungsschutzrechte,
Persönlichkeitsrechte oder Eigentumsrechte, zu deren Weitergabe er nicht
berechtigt ist;
Funktionsweise fremder Datenverarbeitungsanlagen, insbesondere Computern
zu beeinträchtigen.
b) Sie verpflichten sich:
insbesondere bei Hotelketten die Region bzw. den Ort und evtl.
Namenszusätze anzugeben.
selbst die zu bewertende Leistung in Anspruch genommen, insbesondere im
betreffenden Hotel seinen Urlaub verbracht haben.
Beschäftigter, Eigentümer oder Betreiber des zu bewertenden Hotels sind
oder in ähnlicher Weise mit diesem in Verbindung stehen, z.B. ein
Familienangehöriger der genannten Gruppen sind. Gleiches gilt für
Mitarbeiter der Reiseunternehmen und deren Familienangehörige.
diese Leistung von Beschäftigten, Eigentümern oder Betreibern der zu
bewertenden Reiseleistung oder Mitarbeitern von Reiseunternehmen eine
Vergütung angeboten wird.
Falschaussagen zu Reiseleistungen, hiermit in Zusammenhang stehenden
Personen, Freizeitmöglichkeiten und sonstigen dortigen Gegebenheiten/
Ereignissen zu machen, die die Entscheidung anderer Urlauber
beeinflussen könnten.
hauptsächlich zu dem Zweck abzugeben, anderen Reisewilligen/Urlaubern
eine Möglichkeit zu geben, sich ein
aussagekräftigeres/objektiveres/umfassenderes Bild bezüglich einer
Reiseleistung und damit zusammenhängender Gegebenheiten machen zu
können. Dasselbe gilt für das Hochladen von Hotel-/ Urlaubsbildern.
Waren, Dienstleistungen und/oder Unternehmen zu machen
verfassen
gleich auf welche Art und Weise zu sammeln, zu verwenden oder zu
veröffentlichen.
1.  Verstoßen Sie gegen diese Bedingungen oder besteht insoweit in
2.  Mit der Veröffentlichung von Inhalten (wie z.B. Hotelbewertungen,
3.  Falls Sie selbst nicht Inhaber der Rechte an einem von Ihm
4.  Wir sind außerdem berechtigt, im Umfeld zu den öffentlich zugänglich
5.  Sie erklären sich damit einverstanden, dass seine Bilder und/oder
D. ALLGEMEINE GESCHÄFTSBEDINGUNGEN FÜR DIE VERMITTLUNG VON
VERSICHERUNGEN
1.  Wir vermitteln die angebotenen Reiseversicherungen als
2.  Die angebotenen Versicherungen sind hinsichtlich Versicherungsumfang
3.  Sämtliche Angaben, zu deren Mitteilung wir nach § 11 der Verordnung
4.  Im Zuge dieser Vermittlung und zu Abrechnungszwecken werden die
5.  Der Versicherungsvertrag kommt nur zwischen Ihnen und dem
6.  Die Regelungen zu Haftung und Verjährung (Abschnitt C.VIII) gelten
E. ALLGEMEINE GESCHÄFTSBEDINGUNGEN FÜR EIGENE LEISTUNGEN
1.  Soweit wir eigene, kostenpflichtige Leistungen anbieten, entsteht
2.  Auf die Kosten wird im Rahmen der Buchung hingewiesen; die
F. DATENSCHUTZ
I. VERANTWORTLICHE STELLE NACH BUNDESDATENSCHUTZGESETZ
Die Travel24.com AG ist, was die für die Durchführung dieses Vertrages
erforderliche Verarbeitung Ihrer Daten betrifft, die verantwortliche
Stelle im Sinne des Bundesdatenschutzgesetzes.
II. WEITERGABE, ÜBERMITTLUNG UND SPEICHERUNG VON DATEN IM RAHMEN DER
VERTRAGSVERHÄLTNISSE
1.  Wir weisen Sie vorsorglich darauf hin, dass Sie das Recht haben,
2.  Wir verwenden die von Ihnen mitgeteilten Daten zur Erbringung Ihrer
3.  Eine Weitergabe Ihrer Daten erfolgt an das mit der Durchführung des
4.  Bei der Übermittlung Ihrer Zahlungsangaben /-daten verwenden wir die
a. Bei Verwendung der Software erscheint in der Adresszeile Ihres
b. Die SSL verschlüsselten Webseiten verwenden verifizierte und
signierte Zertifikate, welche beispielsweise über ein „Schlosssymbol“ in
der Adressleiste Ihres Browsers zu erkennen sind.
1.  Ihre Daten werden auf unsere Veranlassung nicht in Staaten außerhalb
2.  Spätestens wenn unsere Kenntnis Ihrer personenbezogener Daten für
III. SICHERHEITSBESTIMMUNGEN IM HINBLICK AUF BARGELDLOSE ZAHLUNGEN
1.  Wir stellen zur Abwicklung von bargeldlosen Zahlungen, z.B. per
2.  Hierbei stellen wir sicher, dass die Erhebung, Speicherung und
3.  Wir werden Sie nie auffordern Ihre vollständigen Kreditkartendaten
4.  Für Schäden, die sich aus einer unaufgeforderten Zusendung von
IV. PERSÖNLICHE BENUTZERPROFILE
Sie haben auf unserer Website die Möglichkeit, sich mit Ihrer
E-Mail-Adresse und einem frei wählbaren Passwort zu registrieren. Für
alle Neubuchungen erfolgt eine automatische Registrierung bei der
Buchung, sofern Sie noch nicht über persönliche Login-Daten verfügen. In
Ihren persönlichen Bereich können Sie Ihre persönlichen Kundendaten
verwalten, sowie nützliche Hinweise und Informationen zu Ihren
Reisebuchungsdaten einsehen. Bitte beachten Sie, dass wir Zahlungsdaten
nicht in Ihrem persönlichen Bereich abspeichern. Ihre gespeicherten
Daten können Sie jederzeit innerhalb dieses persönlichen durch Passwort
geschützten Bereiches einsehen und ändern. Selbstverständlich können Sie
Ihr Registrierungskonto auch jederzeit vollständig löschen. Hierbei
sollten Sie jedoch beachten, dass somit das Leistungsangebot auf das
eines nicht registrierten Mitgliedes beschränkt wird. Wir behalten uns
das Recht vor, Informationen über registrierte Mitglieder, welche die
Nutzungsbedingungen verletzt haben oder deren Zugang eingestellt wurde,
V. DATENSCHUTZUNTERRICHTUNG
Eine gesonderte Unterrichtung zum Datenschutz, die erläutert, welche
gegebenenfalls personenbezogenen Daten wir oder Dritte allein aufgrund
Informationen verwendet werden, finden Sie hier.
G. EUROPÄISCHE ONLINE- STREITBEILEGUNGS-PLATTFORM
Wir weisen gem. § 36 VSBG darauf hin, dass wir nicht an einem
Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle
teilnehmen.
H. SCHLUSSBESTIMMUNGEN
1.  Bei einem Verstoß gegen die hier angeführten Nutzungsbedingungen
2.  Alle Ansprüche aus dem mit uns bestehenden Vermittlungsvertrag
3.  Weiterhin stimmen Sie hiermit der Zuständigkeit der deutschen
4.  Falls sich eine der hier enthaltenden Bestimmungen, als unwirksam
Travel24.com AG
Salomonstr. 25a
D-04103 Leipzig
Deutschland
Vorstand: Ralf Dräger
Registergericht: Amtsgericht Leipzig
Registernummer: HRB 25538
Umsatzsteuer-ID-Nr.: DE 178111737
Versicherungsvermittlerregister–Nr.: D-PEZ6-30WKS-02
Zuständige Aufsichtsbehörde: IHK Leipzig, Goerdelerring 5, 04109
Leipzig,www.leipzig.ihk.de
STAND: JULI 2020
Bei uns buchen sie sicher und günstig
Bequem bezahlen
Folgen Sie uns
