telefonisch an +49 (0)30 300 137 300 oder nutze die FlixBus Google
Assistant App.
de
IMPRESSUM
HERAUSGEBER
FlixMobility GmbH
KONTAKT
Anschrift: FlixMobility GmbH, Friedenheimer Brücke 16, 80639 München
Tel: +49 (0)30 300 137 300.
E-Mail: service@flixbus.de
Mobiltelefonen aus von den Gebühren des jeweiligen Anbieters abhängig
sind.)
GESCHÄFTSFÜHRER
Daniel Krauss
HANDELSREGISTER
Amtsgericht München, HRB 197620
UST-ID
VERANTWORTLICH I.S.D. § 18 ABS. 2 MSTV
Daniel Krauss
Friedenheimer Brücke 16
E-Mail: service@flixbus.de
HAFTUNG FÜR INHALTE
Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die
Richtigkeit, Vollständigkeit und Aktualität der Inhalte können wir
jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir gemäß § 7
Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen
Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als
Diensteanbieter jedoch nicht verpflichtet, übermittelte oder
gespeicherte fremde Informationen zu überwachen oder nach Umständen zu
forschen, die auf eine rechtswidrige Tätigkeit hinweisen.
Verpflichtungen zur Entfernung oder Sperrung der Nutzung von
Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt.
Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der
Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von
entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend
entfernen.
Die Europäische Kommission stellt eine Plattform für die
außergerichtliche Online-Streitbeilegung (OS-Plattform) bereit, die
unter www.ec.europa.eu/consumers/odr aufrufbar ist. Wir nehmen an einem
Streitschlichtungsverfahren vor einer Verbraucherschlichtungsstelle
teil. Eine Liste mit den Kontaktdaten der anerkannten
Streitschlichtungsstellen finde Sie unter
https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.adr.show.
Hierzu verweisen wir auf die Schlichtungsstelle öffentlicher
Personenverkehr e.V. hin (www.soep-online.de).
FlixBus bei:
App
Hilfe
