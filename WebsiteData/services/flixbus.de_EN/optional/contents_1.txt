please give us a call at [+44(0)1491502156](tel:+44(0)1491502156) or
refer to the [FlixBus Google Assistant
App](https://assistant.google.com/services/invoke/uid/0000009ac7aaea7a).
FlixBus](https://cdn.flixbus.de/assets/images-20180806/flixbus_logo.svg
account](https://shop.flixbus.co.uk/user/profile) - Route Map - Plan
Your Journey Plan Your Journey - [Timetables & Stops](/coach) - [Night
buses](/night-bus) - [Holiday
destinations](https://www.flixbus.co.uk/holiday-destinations) - [Travel
tips](/travel-tips) - [Rent a
bus](https://charter.flixbus.com/?utm_source=schedules&utm_medium=Link&utm_campaign=Flixbus.com-Link)
Info](/info) - [How to book](/service/book-bus-ticket) - [On
board](/service/on-board) - [Manage my
booking](/service/change-cancel-a-booking) - [Bus app](/service/bus-app)
points](/service/ticket-sales-points) - [Lost &
found](https://help.flixbus.com/s/article/PSSP-I-left-something-on-the-coach-What-should-I-do?language=en_GB)
About FlixBus - [Safety](/company/safety) -
satisfaction](/company/customer-satisfaction) -
Help - En-Gb - Home - Home - [Timetables & Stops](/coach) - [Night
buses](/night-bus) - [Holiday
destinations](https://www.flixbus.co.uk/holiday-destinations) - [Travel
tips](/travel-tips) - [Rent a
bus](https://charter.flixbus.com/?utm_source=schedules&utm_medium=Link&utm_campaign=Flixbus.com-Link)
Info](/info) - [How to book](/service/book-bus-ticket) - [On
board](/service/on-board) - [Manage my
booking](/service/change-cancel-a-booking) - [Bus app](/service/bus-app)
points](/service/ticket-sales-points) - [Lost &
found](https://help.flixbus.com/s/article/PSSP-I-left-something-on-the-coach-What-should-I-do?language=en_GB)
Partners](/company/partners/bus-partners) - [Travel Agencies &
Partners](/company/partners/travel-agencies-and-ticket-outlets) -
Centre](https://corporate.flixbus.com/flixbus/medienbibliothek/) -
Coverage](/company/press-room/press-coverage) - Home En-Gb - Български -
Lithuanian - Latvian - Македонски - Norsk (bokmål) - Nederlands -
Slovenščina - Shqip - Srpski - Svenska - Українська - 中文 Home -
Slovenščina - Shqip - Srpski - Svenska - Українська - 中文
account](https://shop.flixbus.co.uk/user/profile) #### §§ geoTextTitle
FlixBus - [Safety](/company/safety) -
satisfaction](/company/customer-satisfaction) -
Partners](/company/partners/bus-partners) - [Travel Agencies &
Partners](/company/partners/travel-agencies-and-ticket-outlets) -
Coverage](/company/press-room/press-coverage) About FlixBus
experience the world FlixBus is a young mobility provider that has been
changing the way millions of people travel in Europe over the past 3
years. As a combination of tech-startup, e-commerce-platform and
transportation company, FlixBus was able to establish Europe's largest
intercity bus network in the shortest amount of time. Thanks to a
user-friendly booking system and an extensive route network, we can
offer travellers the opportunity to experience the world no matter their
budget. Our green buses comply with the highest safety and environmental
standards, so that we can offer a sustainable and convenient alternative
to private transport. We provide the highest level of comfort. ####
FlixBus Experience – our concept for success Our success can be
attributed to the digitization of traditional bus travel. With
technological advancements like our e-ticketing system, the
Tracking, we have been revolutionizing the bus travel industry. Through
smart network planning and dynamic price management, we provide our
customers with great offers. We also rely closely on partnerships with
small and medium-sized enterprises: our regional bus partners – often
family-owned companies with decades of experience, are responsible for
our green FlixBus fleet. This is where the innovation and start-up
spirit of FlixBus meets the experience and know-how of traditional SMEs.
young entrepreneurs in Munich with the goal of making sustainable bus
travel both comfortable and affordable. At the same time, "MeinFernbus"
began operating bus routes from Berlin throughout Germany.  After the
end of the rail monopoly in 2013, these two start-ups established
themselves as the leaders of the long-distance bus market and gained a
strong position against international competitors. Since the merge of
our teams in Munich and Berlin, we are the clear market leader in
Germany. #### As of 2015, FlixBus has also been expanding
internationally: we’ve established domestic long-distance bus networks
in France, Italy, Austria, the Netherlands and Croatia, as well as the
regular cross-border services to Scandinavia, Spain, England and the
region of Central and Eastern Europe. Today, passengers can benefit from
Europe's largest long-distance bus network. Currently, an international
team of nearly 1,000 employees throughout Munich, Berlin, Paris, Milan,
Zagreb and Copenhagen as well as thousands of our partner's bus drivers
from all over Europe, stand behind the green mobility provider. Each
individual contributes to making FlixBus better with every day. Do you
have the personality and team spirit to help us conquer the European
intercity bus market? Then apply here, we might have the right job for
you. #### ### FlixFacts [FlixBus passengers] **100 million customers
since launch** 100 million passengers have already travelled with us, 40
million in 2017 alone. [European network] **2,500+ destinations, 36
countries** FlixBus connects 2,500+ destinations in 36 countries, a
number which is growing nearly every day. [FlixBus connections]
often departing every 30 minutes. ### Founders
the international operations and expansion. Back in 2009, together with
Jochen, he discussed and brainstormed the opportunities that a
deregulated market could bring. As a fan of alternative mobility
concepts, he doesn’t own a car and cycles to the office. [] **Jochen
Engert ** Jochen is responsible for Marketing, Sales and Finance. Before
founding FlixBus, he and André travelled the world as strategy
consultants. During his studies he organized ski bus trips to the Alps,
which was his first experience with the industry.
Mobile and Software Development. During his studies he founded a
successful IT start-up with his schoolmate André. He eventually
sacrificed his long time job at Microsoft – and his car – to create
Europe's largest long-distance bus provider. - #### Travel Options -
destinations](/holiday-destinations) - [Rent a
bus](https://charter.flixbus.com/?utm_source=footer&utm_medium=Link&utm_campaign=Flixbus.com-Link)
Berlin](/coach/berlin) - [Coach Amsterdam](/coach/amsterdam) - [Coach
London](/coach/london) - [Coach Marseille](/coach/marseille) - [Coach
Milan](/coach/milan) - [Coach Munich](/coach/munich) - #### Bus Routes -
Szczecin](/bus-routes/berlin-szczecin) - [Bus Amsterdam -
Paris](/bus-routes/amsterdam-paris) - [Bus Dresden -
Berlin](/bus-routes/dresden-berlin) - [Bus Lille -
Paris](/bus-routes/paris-lille) - [Bus Paris -
Milan](/bus-routes/paris-milan) - [Bus Paris -
London](/bus-routes/paris-london) - #### FlixBus - [About
FlixBus](/company/about-flixbus) -
room](/company/press-room) - [Privacy](/privacy) - [General conditions
for business and
booking](/general-terms-business-and-booking-conditions) - [General
terms and conditions of carriage -
Selection](/general-conditions-of-carriage-selection) - [Legal
Notice](/legal-notice) - [Photo
credits](https://www.flixbus.com/photo-credits) - #### Customer Service
app](/service/bus-app) - [Passenger rights](/passenger-rights) - [Manage
my booking](https://shop.flixbus.co.uk/rebooking/login) #### FlixBus on:
