please give us a call at +44(0)1491502156 or refer to the FlixBus Google
Assistant App.
Login
En-Gb
Home
Login
Help
§§ geoTextTitle §§
§§ geoLinkText §§
Home ǀ
Privacy Policy
PRIVACY POLICY
FOREWORD
We, FlixMobility GmbH, including our subsidiaries (hereinafter referred
to jointly as: “FLIXBUS”, “WE” or “US”) would like to inform you here
about data protection at FlixBus.
Data protection regulations for the protection of the person affected by
data processing (we refer to you as a data subject hereinafter, as well
2016/679; hereinafter referred to as “GDPR”). Insofar as we decide on
the purposes and means of data processing either alone or jointly with
others, this primarily includes the obligation to inform you
transparently about the type, scope, purpose, duration and legal basis
of the processing (cf. Art. 13 and 14 GDPR).
The purpose of this policy (hereinafter: “privacy policy”), is to inform
you about the way in which we process your personal data.
Our privacy policy is modular in structure. It consists of one part
containing general information about all processing of personal data and
processing situations that come into effect each time our website is
accessed (Clause 1 General information) and a special part, the content
of which only refers to the processing situation specified therein with
the name of the respective offer or product, in particular when you
visit our websites, which is described in more detail here (Clause 2
Special information).
You can find further legal information here:
Terms and conditions of booking (TCB)
Terms and conditions of carriage (TCC)
Regarding FlixTrain:
Fare conditions of FlixTrain GmbH
Terms and conditions of carriage of FlixTrain GmbH (TCC)
For job applicants:
Data protection information for job applicants
1.    General information
1.1    Definition of terms
This privacy policy is based on the following definitions of terms, as
outlined in Art. 4 GDPR:
1.2    Name and address of the controller
The person responsible for the processing of your personal data (Art.
4(7) GDPR) is:
FlixMobility GmbH
Friedenheimer Brücke 16
80639 Munich
Telephone: +49 (0)30 300 137 300
Email: info@flixbus.co.uk
Further information about our company can be found in the legal notice.
1.3    Contact details of the data protection officer
Our company data protection officer is available to you at any time to
answer all your questions and as a contact person on the subject of data
protection.
Her contact details are:
FlixMobility GmbH
Dr. Theresia Gondro
Friedenheimer Brücke 16
80639 Munich
Email: data.protection@flixbus.com
For general questions about FlixBus, please contact info@flixbus.co.uk.
1.4    Legal basis of data processing
The processing of personal data is permitted if at least one legal basis
listed below is complied with:
For processing carried out by us, we specify the applicable legal basis
under Clause 2. Processing can also be based on more than one legal
basis.
1.5    Categories of recipients
Under certain conditions, we transmit your personal data to our
subsidiaries, or personal data from our subsidiaries is transferred to
us, to the extent that is permissible.
As with any major company, we also use external domestic and foreign
service providers to handle our business transactions and work with
partner companies at home and abroad. These include, for example:
The service providers and partner companies must provide guarantees that
suitable technical and organisational measures are implemented by them
in such a way that the processing meets legal requirements and the
rights of the data subjects are safeguarded.
We transmit personal data to public bodies and institutions (e.g.
police, public prosecutor’s office, supervisory authorities) if there is
a corresponding obligation/authorisation.
For processing carried out by us, we specify the categories of the data
recipients under Clause 2.
1.6    Requirements for the transfer of personal data to third countries
As part of our business relationships, your personal data may be shared
with or disclosed to third parties, who may also be located outside the
European Economic Area (EEA), i.e. in third countries.
Insofar as it is necessary, we will inform you in the relevant sections
of Clause 2 about the respective details of the transfer to third
countries in connection with the processing carried out by us.
The European Commission certifies that some third countries have data
protection that is comparable to the EEA standard by means of so-called
adequacy decisions (a list of these countries and a copy of the adequacy
decisions can be downloaded from:
http://ec.europa.eu/justice/data-protection/international-transfers/adequacy/index_en.html).
However, in other third countries to which personal data may be
transferred, there may not be a consistently high level of data
protection due to a lack of legal provisions. If this is the case, we
ensure that data protection is adequately guaranteed.
This is possible, for example, via binding company regulations (referred
to as “binding corporate rules”), standard contractual clauses of the
European Commission for the protection of personal data, certificates
and recognised codes of conduct.
Insofar as it is necessary for your booking and the associated providing
and processing of transport services, the transmission of personal data
required for this to third countries is permitted in accordance with
Art. 49 para. 1(b) GDPR.
Please contact our data protection officer if you would like more
detailed information on this topic.
1.7    Storage duration and data erasure
The storage period of the personal data collected depends on the purpose
for which we process the data. The data will be stored for as long as
this is necessary to achieve the intended purpose.
In the case of processing carried out by us, we specify in Clause 2 how
long the data will be stored by us. If no explicit storage period is
specified below, your personal data will be erased or blocked as soon as
the purpose or legal basis for the storage no longer applies.
However, storage can take place beyond the specified time in the event
of a(n) (imminent) legal dispute with you, or if other legal proceedings
are initiated, or if storage is stipulated by statutory provisions to
which we as the controller are subject. If the storage period prescribed
by statutory provisions expires, the personal data will be blocked or
erased unless further storage by us is required and there is a legal
basis for this.
1.8    Automated decision making (including profiling)
We do not intend to use any personal data collected from you for any
processes involving automated decision-making (including profiling). If
we wish to implement these procedures, we will inform you of this
separately in accordance with statutory provisions.
1.9    No obligation to provide personal data
We do not fundamentally make the conclusion of contracts with us
dependent on you providing us with personal data beforehand. In
principle, there is also no statutory or contractual obligation to
provide us with your personal data; however, we may only be able to
provide certain offers to a limited extent, or not at all, if you do not
provide the data required for this.
1.10    Statutory obligation to transmit certain data
Under certain circumstances, we may be subject to a special statutory or
legal obligation to provide personal data to third parties, in
particular public bodies.
1.11    Data security
We use suitable technical and organisational measures to collect your
data, taking into consideration the latest technology, the
implementation costs and the nature, the scope, the context and the
purpose of the processing, as well as the existing risks of a data
breach (including the probability and effect of such an event), in order
to protect the data subject against accidental or intentional
manipulation, partial or complete loss or destruction, or against
unauthorised access by third parties (e.g. we use TLS encryption for our
websites). Our security measures are continuously being improved to take
into account technological developments.
We will be happy to provide you with further information about this upon
request. Please contact our data protection officer or our CISO (chief
information security officer) in this regard.
His contact details are:
FlixMobility GmbH
Tobias Hadem
Friedenheimer Brücke 16
80639 Munich
Email: it-security@flixbus.com
1.12    Your rights
You may assert your rights as a data subject regarding your personal
data at any time, in particular by contacting us using the contact
details provided in Clause 1.2. Data subjects have the following rights
under the GDPR:
RIGHT TO INFORMATION
You can request information in accordance with Art. 15 GDPR about your
personal data processed by us. In your request for information, you
should clarify your concern to make it easier for us to compile the
necessary data. Upon request, we will provide you with a copy of the
data that are the subject of the processing. Please note that your right
to information may be limited under certain circumstances in accordance
with statutory provisions.
RIGHT TO RECTIFICATION
If the information relating to you is not (any longer) correct, you may
request a correction in accordance with Art. 16 GDPR. If your data is
incomplete, you may request completion.
RIGHT TO ERASURE
You may request the erasure of your personal data in accordance with the
provisions of Art. 17 GDPR. Your right to erasure depends, among other
things, on whether the data relating to you are still required by us to
perform our statutory duties.
RIGHT TO RESTRICTION OF PROCESSING
In accordance with the provisions of Art. 18 GDPR, you have the right to
demand a restriction of the processing of the data relating to you.
RIGHT TO DATA PORTABILITY
In accordance with the provisions of Art. 20 GDPR, you have the right to
receive the data that you have provided to us in a structured,
commonly-used and machine-readable format, or to request the
transmission to another controller.
RIGHT TO OBJECT:
IN ACCORDANCE WITH ART. 21 PARA. 1 GDPR, YOU HAVE THE RIGHT TO OBJECT TO
THE PROCESSING OF YOUR DATA AT ANY TIME FOR REASONS RELATING TO YOUR
PARTICULAR SITUATION. YOU CAN OBJECT TO RECEIVING ADVERTISING AT ANY
TIME WITH EFFECT FOR THE FUTURE, IN ACCORDANCE WITH ART. 21 PARA. 2 GDPR
RIGHT TO APPEAL
If you are of the opinion that we have not complied with the provisions
of data protection regulations when processing your data, you can
complain to a data protection supervisory authority about the processing
of your personal data, such as to the data protection supervisory
authority to whom we are responsible:
Bavarian State Office for Data Protection Supervision, Promenade 18,
91522 Ansbach
RIGHT TO WITHDRAW CONSENT
You can withdraw your consent to the processing of your data at any time
with future effect. This also applies to declarations of consent that
were issued before the GDPR came into force, i.e. before 25/05/2018.
2.    Special information
2.1    Visiting our websites
Information about FlixBus and the services we offer can be found in
particular at https://www.flixbus.co.uk/ / https://www.flixtrain.com/
including the associated subpages (hereinafter referred to jointly as
data is processed.
2.1.1    Provision of websites
When using the websites for information purposes, we collect, store and
process the following categories of personal data:
Log data: when you visit our websites, a log data record (referred to as
We use IT service providers for hosting our websites and for statistical
evaluations of the log data.
The processing of the log data serves statistical purposes and improves
the quality of our websites, in particular the stability and security of
the connection.
The legal basis is Art. 6 para. 1(f) GDPR. Our legitimate interest is to
2.1.2    Contact forms
When using contact forms, the data transmitted in this way are processed
the time of transmission, subject matter of the enquiry).
The processing of contact form data takes place in order to process
enquiries, and depending on the basis and the subject matter of your
enquiry, either on the legal basis of Art. 6 para. 1(b) GDPR, if it
concerns a contract-related enquiry, or in other cases on the legal
basis of Art. 6 para. 1(f) GDPR, our legitimate interest is to process
contact enquiries.
We use customer service providers for job processing to answer enquiries
made via our contact forms.
In addition, we store the contact form data as well as the respective IP
address in order to comply with our obligations to provide evidence, to
ensure compliance with and documentation of legal obligations, in order
to be able to clarify any possible misuse of your personal data and to
ensure the security of our systems.
The legal basis is Art. 6 para. 1(c) or (f) GDPR.
2.1.3    Booking, providing and processing of transport services
When booking tickets for transport services, we collect, store and
process the following categories of personal data:
You also have the option of providing a contact telephone number in case
of delays or changes in the itinerary of your trip (optional).
These data are processed for the booking, providing and processing of
transport services, including customer service, as well as for the
fulfilment of legal obligations.
The legal basis is Art. 6 para. 1(b), (c) GDPR.
We also use some of these data for product recommendations, see Clause
2.1.4 and for the newsletter, see Clause 2.1.5 and the customer account,
When booking tickets for international transport services, the following
categories of personal data are also collected depending on the place of
departure and arrival:
These data are processed for the booking, providing and processing of
transport services, including customer service, as well as for the
fulfilment of legal obligations.
We pass on the above-mentioned data to the respective carrier or
carriers, as well as to public bodies if there is a corresponding
obligation/authorisation.
The legal basis is Art. 6 para. 1(b) or (c) GDPR.
The necessary payment data will be transmitted to a payment service
provider for the secure processing of the payments initiated by you.
Our payment service providers are:
The legal basis is Art. 6 para. 1(b) or (f) GDPR.
For certain bookings, we also use the technologies and services of
Distribusion Technologies GmbH as processors (Wattstrasse 10, 13355
Berlin, telephone: +49-30-3465507-50, email:
dataprotection@distribusion.com).
2.1.4    Product recommendation
To the extent permitted, we may use the email address received in
connection with the booking or transport service to send you regular
offers by email for products from our range similar to those already
purchased.
We use external customer service providers as processors to send product
recommendations.
You will receive these product recommendations from us regardless of
whether you have subscribed to a newsletter or have consented to
marketing communication by email. We would like to provide you in this
way with information about products from our range that you might be
interested in, based on your recent purchases with us.
The legal basis is Art. 6 para. 1(f) GDPR; our legitimate interest is to
inform you about our product range and to suggest certain products to
you.
You can object to the use of your email address for this purpose at any
time by using the unsubscribe link of the product recommendation, or by
sending a message to unsubscribe@flixbus.com.
2.1.5    Newsletter
If you also register for the newsletter via our registration link, we
ask you to consent to the processing of your data (email address, first
and last name, place of residence) in order to send you our newsletter
by email on a regular basis.
As part of your subscription to the newsletter, we also obtain your
consent that we may personalise the content of our newsletter according
to your needs and interests.
To register for our newsletter, we use the so-called double opt-in
procedure. This means that after you have registered we will send an
email to the email address you provided, asking you to confirm that you
wish to receive the newsletter. If you do not confirm your registration
within 24 hours, your information will be blocked and automatically
erased after one month.
The newsletter may concern all goods, services, products, offers and
promotions provided by the controller (Clause 1.2) and/or by companies
affiliated with the controller, or by partner companies.
The legal basis is Art. 6 para. 1(a) GDPR.
In addition, we store the IP addresses you use and the times of
registration and confirmation. The purpose of the procedure is to prove
that you are registered and, if necessary, to be able to clarify any
possible misuse of your personal data. If we process your personal data
for this purpose, this is done on the basis of our legitimate interests
in ensuring compliance with and documentation of legal requirements.
The legal basis is Art. 6 para. 1(f) GDPR; our legitimate interest is to
be able to prove consent.
You can revoke the use of your email address at any time by using the
unsubscribe link of the newsletter or by sending a message to
unsubscribe@flixbus.com. The legality of the data processing operations
already carried out remains unaffected by the revocation.
We use external IT service providers who act as processors to distribute
the newsletter.
2.1.6    Customer account
You have the option of creating a personal customer account with us. In
the password-protected area of the customer account, you can manage your
bookings conveniently and have your data stored for future journeys.
To create a customer account, the following mandatory information is
collected:
We use this data to manage your customer account and to create invoices.
In addition, you can also enter a mobile phone number in your customer
account so that we can contact you in the event of a delay or a change
in the itinerary of your trip (optional).
You may also choose to store the following additional data in your
customer account (optional):
These data are used to manage your customer account and to issue
invoices, and can also be used to send personalised product
recommendations (Clause 2.1.4) and – if you have registered for this
purpose – to send newsletters (Clause 2.1.5).
The legal basis is Art. 6 para. 1(a), (b) or (f) GDPR.
If you have given your consent, so-called persistent cookies are stored
on your device with the function “remain logged in”, their purpose is to
ensure that you do not have to log in again during subsequent visits to
our website. This function is not available to you if you have
deactivated the storage of such cookies in your browser settings.
The legal basis is Art. 6 para. 1(f) GDPR.
You can update or delete the customer account – and thus also your
stored personal data – at any time in your personal customer account.
2.1.7  Use of cookies, plugins and other services
When you visit our website, information in the form of a cookie (small
text file) is saved on your terminal. It saves information about how you
use the website (identification ID, visit date, etc.). With the use of
cookies, we make it easier for you to use our site with various service
functions (such as the recognition of previous visits), and
consequently, we can better adapt the site to your needs.
The use of technically necessary cookies is based on Art. 6 (1) lit. f
GDPR. Our legitimate interest is to provide you with the specific
functionalities of our websites, to improve them, and to ensure the
security and integrity of our websites.
You can prevent cookies from being saved and to delete cookies already
existing by changing your browser settings accordingly. The help
function of most browsers tells you how to make these settings. However,
if you do not accept cookies, the service features of the Internet offer
may be impaired. Therefore, we recommend leaving the cookie function
turned on.
Comprehensive information on how this can be accomplished on a number of
Network Advertising Initiative and/or Digital Advertising Alliance. You
can also find information there about how to delete cookies from your
computer as well as general information about cookies. We use different
types of cookies:
Transient cookies, which are also known as in-memory cookie or "session
cookies", are cookies that are deleted after you leave our website and
close the browser. E.g. these cookies usually save language settings or
the content of the order.
Persistent or permanent cookies remain stored even after the browser is
closed. This enables e.g. saving the login status or any search terms
that were entered. We use such cookies, among other things, to measure
the reach or for marketing purposes. Persistent cookies are
automatically deleted after a specified period, which may vary depending
on the cookie. However, you can delete these cookies at any time in the
security settings of your browser.
In addition to so-called "first-party cookies", which we set as
controllers for data processing, "third-party cookies" are also used,
which are offered by other providers.
As part of consent management (consent banner), we give you the
opportunity to decide between the use of cookies and the use of
comparable technologies. You can find a detailed overview with
comprehensive information about the services used and access to your
consent settings, including the possibility of revocation here.
2.2    Customer service
When you contact our customer service, we collect the personal data that
you provide to us on your own initiative. For example, you can send this
to us by email, telephone or letter. Your personal data will only be
used in order to contact you, or for the purpose for which you have
provided us with this data, e.g. for processing your enquiries,
technical administration or customer administration.
This data (including information on means of communication such as email
address, telephone number) is provided on a voluntary basis. We use the
data to process your concern, to fulfil legal obligations if necessary,
and for administrative purposes.
The legal basis is Art. 6 para. 1(b), (c) or (f) GDPR.
In the case of a telephone enquiry, your data is also processed by
telephone applications and in part also via a voice dialogue system in
order to support us in the distribution and processing of enquiries.
For our customer service, we use external customer service providers as
processors.
2.3    Presence on social media channels
We have a presence on social media (currently: Facebook, Instagram,
LinkedIn and Twitter). To the extent we have control over the processing
of your data, we ensure that the applicable data protection regulations
are complied with.
In addition to us, the following are responsible for the company’s
presence within the meaning of the GDPR and other data protection
regulations:
Facebook (Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal
Harbour, Dublin 2, Ireland)
Instagram (Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal
Harbour, Dublin 2, Ireland)
Twitter (Twitter International Company, One Cumberland Place, Fenian
Street, Dublin 2, D02 AX07, Ireland)
LinkedIn (LinkedIn Ireland Unlimited Company, Gardner House Wilton
Place, Dublin 2, Ireland)
We would like to point out that your data may be processed outside the
European Union.
The legal basis for the processing of your personal data by us is Art. 6
para. 1(f) GDPR. Our legitimate interest is effective information and
communication.
Further information about data protection laws in relation to our
corporate presence on social media channels can be found here:
https://www.facebook.com/notes/flixbus/facebook-fanpage-datenschutzrichtlinie/2107777085936747/
Instagram: https://www.facebook.com/policy.php
LinkedIn: https://www.linkedin.com/legal/privacy-policy
Pay with
App
Help
