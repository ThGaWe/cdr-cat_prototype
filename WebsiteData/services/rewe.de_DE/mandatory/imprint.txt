Einkaufsliste
https://www.rewe.de/service/impressum/
REWE.deServiceImpressum
IMPRESSUM
Telefon: 0221 / 177 39 777
E-Mail: kundenmanagement@rewe.de
FRAGEN ZUR ONLINEBESTELLUNG: kundenservice@rewe.de
FRAGEN ZU UNSEREN PRODUKTEN:qualitaetssicherung@rewe.de
FRAGEN ZUR BEWERBUNG:www.karriere.rewe.de
Anfragen von Personaldienstleistern und Kooperationspartnern:
bewerbung@rewe-group.com
ANGABEN GEMÄSS §5 TMG:
VERANTWORTLICHE FÜR DIE WEBSEITE
und unsere Auftritte bei YouTube, Facebook, Twitter, Instagram,
Pinterest, Google+, Snapchat
REWE MARKT GMBH
Domstraße 20, D-50668 Köln
Tel: 0221/149-0
Fax: 0221/149-9000
Email: impressum@rewe.de
Geschäftsführer: Dr. Daniela Büchel, Christoph Eltze, Peter Maly,
Telerik Schischmanow
Registergericht, Sitz: Amtsgericht Köln (HRB 66773)
Umsatzsteueridentifikationsnummer: DE 812 706 034
VERANTWORTLICHER/R I.S.D § 18 ABS. 2 MSTV:
Clemens Bauer, Domstraße 20, D-50668 Köln
HINWEIS ZUR STREITBEILEGUNG
Nach geltendem Recht sind wir verpflichtet, unsere Kunden auf die
Existenz der europäischen Online-Streitbeilegungs-Plattform
hinzuweisen: 
http://ec.europa.eu/odr 
Die REWE Markt GmbH nimmt nicht an Streitbeilegungsverfahren einer
Verbraucherschlichtungsstelle teil und ist dazu auch nicht verpflichtet.
AGB
Versandkosten, Pfand und optionaler Servicegebühren.
verfügbar. In Einzelfällen können die Preise zudem von den online
bestellbaren Angeboten unter https://shop.rewe.de abweichen.
Rezept aus „Meine Rezepte“ entfernen?
