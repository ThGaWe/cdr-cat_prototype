Hilfe
Zur Kategorie Aktionen
Zur Kategorie Baumarkt
Zur Kategorie Möbel und Einrichtung
Zur Kategorie Sport, Freizeit und Hobby
Zur Kategorie Multimedia und Technik
Emarsys Web Extend
Funktionale Cookies sind für die Funktionalität des Webshops unbedingt
erforderlich. Diese Cookies ordnen Ihrem Browser eine eindeutige
zufällige ID zu damit Ihr ungehindertes Einkaufserlebnis über mehrere
Seitenaufrufe hinweg gewährleistet werden kann.
Gerätezuordnung
Login Token
Cache Ausnahme
zielgerichtet und individuell über mehrere Seitenaufrufe und
Google AdSense
POPUPCHECK
Tracking Cookies helfen dem Shopbetreiber Informationen über das
Hotjar
Yandex Metrica
Emarsys Web Extend
Personalisierung
Informationen über die Verwendung der Webseite von Nutzern, um
anschließend Werbung und/oder Inhalte in anderen Zusammenhängen, in
Trbo Onsite-Personalisierung
Trbo Onsite-Personalisierung
Criteo Retargeting
Trbo Onsite-Personalisierung
Live Chats) auf der Webseite zur Verfügung zu stellen. Informationen,
die über diese Service Cookies gewonnen werden, können möglicherweise
Userlike
Userlike
Zendesk
Push-Nachrichten
Informationen
IMPRESSUM
SEITENBETREIBER DER NORMA24.DE UND VERTRAGSPARTNER FÜR DEN ONLINESHOP
NORMA24 Online-Shop GmbH & Co. KG
Manfred-Roth-Str. 7
Registergericht Fürth HRA 9775
USt-IdNr.: DE 281 146 018
KONTAKTMÖGLICHKEITEN
E-MAIL
Bitte teilen Sie uns in Ihrer E-Mail Folgendes mit, damit wir Ihre
Wollen Sie von Ihrem Widerrufsrecht gebraucht machen? -
widerruf.n24@norma24.de
Wollen Sie Ware außerhalb des Widerrufs zurücksenden?
Fragen zu Lieferzeit oder Versandstatus Ihrer getätigten Bestellung?
Haben Sie Reklamationen bzgl. bereits erhaltener Ware?
Haben Sie Fragen zu Zahlungsthemen (z.B. Paypal) / offenen Gutschriften?
Haben Sie sonstige Fragen? - kundenservice.n24@norma24.de
TELEFON
Tel.: +49 (0) 911-247 9999 7 (je nach Tarif fallen die üblichen
Festnetz-Konditionen an)
Montag - Freitag 8:00 - 18:00 Uhr
Persönlich haftende Gesellschafterin:
NORMA Warenhandels GmbH,
Registergericht Nürnberg HRB 5000
Geschäftsführer: Gerd Köber, Robert Tjón
USt-IdNr.: DE132748925
Detailansicht drucken
NORMA24 wurde durch unabhängige Institute zertifiziert.
Abonnieren Sie unseren Newsletter
Eine Abmeldung des Newsletters ist jederzeit über den Abmeldelink in
jeder Mail oder als E-Mail an kundenservice.n24@norma24.de möglich.
der gesetzlichen Mehrwertsteuer. Irrtümer durch Schreib-, Programmier-
und Datenübertragungsfehler sind vorbehalten.
Copyright © 2021 by NORMA24 Online-Shop GmbH Co. KG
Aktiv Inaktiv
Funktionale Cookies sind für die Funktionalität des Webshops unbedingt
erforderlich. Diese Cookies ordnen Ihrem Browser eine eindeutige
zufällige ID zu damit Ihr ungehindertes Einkaufserlebnis über mehrere
Seitenaufrufe hinweg gewährleistet werden kann.
Das Session Cookie speichert Ihre Einkaufsdaten über mehrere
Seitenaufrufe hinweg und ist somit unerlässlich für Ihr persönliches
Das Cookie ermöglicht es einen Merkzettel sitzungsübergreifend dem
Benutzer zur Verfügung zu stellen. Damit bleibt der Merkzettel auch über
mehrere Browsersitzungen hinweg bestehen.
Gerätezuordnung:
Die Gerätezuordnung hilft dem Shop dabei für die aktuell aktive
Displaygröße die bestmögliche Darstellung zu gewährleisten.
Das CSRF-Token Cookie trägt zu Ihrer Sicherheit bei. Es verstärkt die
Absicherung bei Formularen gegen unerwünschte Hackangriffe.
Login Token:
Der Login Token dient zur sitzungsübergreifenden Erkennung von
Benutzern. Das Cookie enthält keine persönlichen Daten, ermöglicht
jedoch eine Personalisierung über mehrere Browsersitzungen hinweg.
Cache Ausnahme:
Das Cache Ausnahme Cookie ermöglicht es Benutzern individuelle Inhalte
unabhängig vom Cachespeicher auszulesen.
Cookies Aktiv Prüfung:
Das Cookie wird von der Webseite genutzt um herauszufinden, ob Cookies
vom Browser des Seitennutzers zugelassen werden.
Das Cookie wird verwendet um die Cookie Einstellungen des
Seitenbenutzers über mehrere Browsersitzungen zu speichern.
Das Cookie wird für Zahlungsabwicklungen über Amazon eingesetzt.
Das Cookie speichert die Herkunftsseite und die zuerst besuchte Seite
des Benutzers für eine weitere Verwendung.
Speichert welche Cookies bereits vom Benutzer zum ersten Mal akzeptiert
wurden.
Cloudflare Cookies werden genutzt um dem Nutzer eine möglichst hohe
Performance über mehrere Seitenaufrufe zu bieten. Zudem tragen sie zur
Sicherheit der Seitennutzung bei.
Aktiv Inaktiv
zielgerichtet und individuell über mehrere Seitenaufrufe und
der Webseite effektiv zu erfassen. Diese Informationen werden vom
Seitenbetreiber genutzt um Google AdWords Kampagnen gezielt einzusetzen.
Aktiv Inaktiv
Dienste von Facebook einbinden, personalisierte Werbeangebote aufgrund
des Nutzerverhaltens anzuzeigen.
Aktiv Inaktiv
Google AdSense:
Aktiv Inaktiv
POPUPCHECK:
Speichert anonymisiert, ob im laufenden Quartal mit dem derzeit
genutzten Gerät an einer Umfrage der Österreichischen Webanalyse (ÖWA)
teilgenommen wurde.
Aktiv Inaktiv
Aktiv Inaktiv
Tracking Cookies helfen dem Shopbetreiber Informationen über das
Google Analytics wird zur der Datenverkehranalyse der Webseite
eingesetzt. Dabei können Statistiken über Webseitenaktivitäten erstellt
und ausgelesen werden.
Aktiv Inaktiv
Das Cookie stellt Tracking-Funktionalitäten für eine erfolgreiche
Einbindung von Awin Partnerprogrammen zur Verfügung.
Aktiv Inaktiv
Speichert einen Client-Hash für die Österreichische Webanalyse (ÖWA) zur
Optimierung der Ermittlung der Kennzahlen Clients und Visits. Der Cookie
ist maximal 1 Jahr lang gültig.
Aktiv Inaktiv
Das Bing Ads Tracking Cookie wird verwendet um Informationen über die
Aktiv Inaktiv
Hotjar:
Der Seitenbenutzer wird dabei über das Cookie über mehrere Seitenaufrufe
identifiziert und sein Verhalten analysiert.
Aktiv Inaktiv
Diese Cookie dient zur Anzeige von personalisierten Produktempfehlungen
im Webshop.
Aktiv Inaktiv
Aktiv Inaktiv
Aktiv Inaktiv
Yandex Metrica
Aktiv Inaktiv
Emarsys Web Extend
Aktiv Inaktiv
Personalisierung
Aktiv Inaktiv
Informationen über die Verwendung der Webseite von Nutzern, um
anschließend Werbung und/oder Inhalte in anderen Zusammenhängen, in
Trbo Onsite-Personalisierung:
Das Cookie wird verwendet zur Onsite-Persoonalisierung der Seiteninhalte
und zur Seitenanalyse für Optimierungsmaßnahmen.
Aktiv Inaktiv
Criteo Retargeting:
Das Cookie dient dazu personalisierte Anzeigen auf dritten Webseiten auf
Basis angesehener Seiten und Produkte zu ermöglichen.
Aktiv Inaktiv
Trbo Onsite-Personalisierung
Aktiv Inaktiv
Aktiv Inaktiv
Live Chats) auf der Webseite zur Verfügung zu stellen. Informationen,
die über diese Service Cookies gewonnen werden, können möglicherweise
Userlike:
Userlike stellt einen Live Chat für Seitenbenutzer zur Verfügung. Über
das Cookie wird die Funktion der Anwendung über mehrere Seitenaufrufe
hinweg sicher gestellt.
Aktiv Inaktiv
Zendesk:
Zendesk stellt einen Live Chat für Seitenbenutzer zur Verfügung. Über
das Cookie wird die Funktion der Anwendung über mehrere Seitenaufrufe
hinweg sicher gestellt.
Aktiv Inaktiv
Tawk stellt einen Live Chat für Seitenbenutzer zur Verfügung. Über das
Cookie wird die Funktion der Anwendung über mehrere Seitenaufrufe hinweg
sicher gestellt.
Aktiv Inaktiv
Push-Nachrichten:
Push-Nachrichten dienen zur Verbesserung der zielgerichteten
Kommunikation mit den Besuchern der Webseite. Über diesen Dienst können
den Nutzern Benachrichtigungen über Produktneuheiten, Aktionen, etc.
angezeigt werden.
Aktiv Inaktiv
Funktionalität bieten zu können. Sie können Ihre Auswahl der Verwendung
