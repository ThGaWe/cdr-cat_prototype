Setzen Sie zunächst einen Filter und Sie erhalten maßgeschneiderte
Informationen.
IMPRESSUM
Informationen gemäß den Anforderungen des Telemediengesetzes (TMG)
Verantwortlich
DATEV eG, Nürnberg
vertreten durch
Vorsitzender des Aufsichtsrates: Nicolas Hofmann
E-Mail: info@datev.de
Registergericht Nürnberg, GenReg Nr. 70
Umsatzsteuer-Identifikationsnummer (USt-IdNr.)
Verantwortlicher Ansprechpartner
DATEV IV
Virnsberger Straße 63
E-Mail: claus.fesel@datev.de
Rechtliche Hinweise, Qualitätssicherung, Haftungsausschluss
vollständige Informationen bereitzustellen und ändert oder ergänzt diese
daher bei Bedarf laufend und ohne vorherige Ankündigung. Dennoch müssen
wir für Korrektheit, Aktualität und Vollständigkeit jede Gewähr, Haftung
oder Garantie ausschließen. Dies gilt auch für alle Verweise, so
anbietet. DATEV kann für die Inhalte solcher externer
Internet-Auftritte, die Sie mittels eines Links oder sonstiger Hinweise
erreichen, keine Verantwortung übernehmen. Ferner haftet DATEV nicht für
direkte oder indirekte Schäden (inkl. entgangener Gewinne), die auf
Informationen zurückgeführt werden können, die auf diesen externen
Internet-Auftritten stehen.
Grafiken, Ton-, Video- und Animationsdateien unterliegen dem
Urheberrecht und anderen Gesetzen zum Schutz geistigen Eigentums. Der
verbreitet, verändert oder auf sonstige Art genutzt werden. DATEV
iStockphoto.com.
Windows, Windows 2000, Windows XP, Windows Vista, Windows 7, Windows
Server 2003, Windows Server 2008, Windows 8, Windows Server 2012, Word,
Redmond, Washington. Android™ ist eine Marke der Google Inc., CA 94043,
USA. Apple, Mac, Mac OS, das Apple Logo, iPad und iPhone sind Marken der
Apple Inc., die in den USA und weiteren Ländern eingetragen sind. App
Store ist eine Dienstleistungsmarke der Apple Inc. ACL und das ACL Logo
sind angemeldete Marken der ACL Services Ltd., Suite 1500, 980 Howe
Street, Vancouver BC, Canada 2015. Lotus, Notes und Lotus Notes sind
sind Marken der jeweiligen Firmen.
Hier finden Sie Informationen zu den Online-Datenschutzprinzipien von
DATEV.
Unternehmen](/web/de/m/ueber-datev/das-unternehmen/?stat_Mparam=int_footer_azg_das-unternehmen
Presse](/web/de/m/presse/?stat_Mparam=int_footer_azg_presse " Presse ")
Veranstaltungen](/web/de/aktuelles/veranstaltungen/?stat_Mparam=int_footer_azg_messen-veranstaltungen
DATEV-Newsletter](/web/de/aktuelles/newsletter/?stat_Mparam=int_footer_azg_newsletter-abonnieren
Lösungen für Unternehmen [ DATEV
Mittelstand](/web/de/datev-shop/komplettloesungen/datev-mittelstand-faktura-mit-rechnungswesen/?stat_Mparam=int_footer_azg_datev-mittelstand
Branchenlösungen](/web/de/loesungen/unternehmer/loesungen-fuer-branchen/?stat_Mparam=int_footer_azg_branchenloesungen
online](/web/de/loesungen/unternehmer/rechnungswesen/buchfuehrung-erstellen-un/datev-unternehmen-online-un/?stat_Mparam=int_footer_azg_digitalisierung-mit-duo
Personalwirtschaft](/web/de/loesungen/unternehmer/lohn-und-personal/?stat_Mparam=int_footer_azg_personalwirtschaft
Service DATEV Hilfe-Center [
Self-Service](/web/de/service/antworten-finden/?stat_Mparam=int_footer_azg_self-service
Service](/web/de/service/experten-fragen/?stat_Mparam=int_footer_azg_persoenlicher_service
Download-Bereich](/web/de/service/software-auslieferung/download-bereich/?stat_Mparam=int_footer_azg_download-bereich
Software-Auslieferung](/web/de/service/software-auslieferung/?stat_Mparam=int_footer_azg_software-auslieferung
Online-Anwendungen [ DATEV Unternehmen
online](/web/de/mydatev/online-anwendungen/datev-unternehmen-online/?stat_Mparam=int_footer_azg_duo
Steuern](/web/de/mydatev/online-anwendungen/datev-meine-steuern/ " DATEV
Meine Steuern ") [ DATEV
SmartTransfer](/web/de/mydatev/online-anwendungen/datev-smarttransfer/?stat_Mparam=int_footer_azg_smarttransfer
DATEV - Kontakt
Kontaktieren Sie uns
AGB
Sie Verwenden einen veralteten Browser oder den IE11 im
Kompatiblitätsmodus. Bitte deaktivieren Sie diesen Modus oder nutzen Sie
