
<!DOCTYPE html>
<html lang="DE" class="datev-styleguide datev-redesign datev-current standardpage">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Verhaltenskodex - Code of Business Conduct</title>
<link rel="apple-touch-icon" href="/web/de/media/tech_datev/layout_datev/images_datev/icons_datev/favicons_datev/apple-touch-icon.png" sizes="180x180">
<link rel="icon" href="/web/de/media/tech_datev/layout_datev/images_datev/icons_datev/favicons_datev/favicon-32x32.png" sizes="32x32" type="image/png">
<link rel="icon" href="/web/de/media/tech_datev/layout_datev/images_datev/icons_datev/favicons_datev/favicon-16x16.png" sizes="16x16" type="image/png">
<link rel="mask-icon" href="/web/de/media/tech_datev/layout_datev/images_datev/icons_datev/favicons_datev/favicon.ico" color="#90D034">
<link rel="icon" href="/web/de/media/tech_datev/layout_datev/images_datev/logo_datev/5c3ccfa02d0a370222b028ee_logo_2x.png">
<meta name="stat.pid" content="datev.de">
<meta name="stat.anw" content="FirstSpirit_datev.de">
<meta name="stat.pageid" content="datev.de_%C3%9Cber+DATEV_Das+Unternehmen_Compliance_Verhaltenskodex">
<meta name="stat.has-children" content="false">
<meta name="stat.req" content="seite">
<meta id="nm-event-url" name="nm-event-url" content="/statistik/nm.event.css?nm.appl.anw=FirstSpirit_datev.de&amp;nm.appl.req=seite&amp;nm.appl.pageid=datev.de_%C3%9Cber+DATEV_Das+Unternehmen_Compliance_Verhaltenskodex">
<meta name="date-modified" content="2020-11-13T13:00:05+01:00">
<meta name="release-time" content="
never released
">
<meta name="revision-id" content="4249881">
<meta name="keywords" content="">
<meta name="description" content="Die Wettbewerbsfähigkeit unserer Kunden zu fördern, ist das erklärte Ziel von DATEV. Um diesem Ziel gerecht zu werden, entwickeln wir das Unternehmen zu dem europäischen Dienstleister für alle Informations- und Kommunikationsanforderungen der steuerberatenden, rechtsberatenden und wirtschaftsprüfenden Berufe.">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2">
<meta property="og:title" content="Verhaltenskodex - Code of Business Conduct" />
<meta property="og:description" content="Die Wettbewerbsfähigkeit unserer Kunden zu fördern, ist das erklärte Ziel von DATEV. Um diesem Ziel gerecht zu werden, entwickeln wir das Unternehmen zu dem europäischen Dienstleister für alle Informations- und Kommunikationsanforderungen der steuerberatenden, rechtsberatenden und wirtschaftsprüfenden Berufe." />
<meta property="og:image" content="https://www.datev.de/web/de/media/tech_datev/layout_datev/images_datev/logo_datev/5c3ccfa02d0a370222b028ee_logo_2x.png" />
<meta property="og:type" content="website" />
<script src="/web/de/media/system/javascript/js_berufsgruppen.js"></script>
<link rel="stylesheet" type="text/css" href="/web/de/media/system/style/css/jquery-fancybox-min.css" />
<link rel="stylesheet" type="text/css" href="/web/de/media/tech_datev/layout_datev/css_datev/internal_css_datev/datevcurrent-css.css" />
<link rel="stylesheet" type="text/css" href="/web/de/media/tech_datev/layout_datev/css_datev/internal_css_datev/shared-css.css" />
<link rel="stylesheet" type="text/css" href="/web/de/media/tech_datev/layout_datev/css_datev/internal_css_datev/general-css.css" />
<link rel="stylesheet" type="text/css" href="../../../../../media/tech_datev/layout_datev/css_datev/internal_css_datev/css_print_current.css" media="print" />
<script src="https://apps.datev.de/analytics/fd71f4fdb4e9/67bc534acbde/launch.min.js" async></script>
</head>
<body id="body" class="basis basis--flipped">
<div class="page__container header-with-breadcrumb" data-anim-class="anim-margin">
<header class="main-header main-header--breadcrumb" data-anim-class="anim-height" data-navigation-data-url="/web/de/datev.de/Technisches/Technische-Navigations-JSON.json
">
<div class="nav-flap-selector" data-anim-class="anim-margin">
<div class="container nav-flap-selector__container">
<div class="row nav-flap-selector__lead">
<div class="col">
<p>Wählen Sie zunächst eine Berufsgruppe und Sie erhalten maßgeschneiderte Informationen.</p>
</div>
<div class="col-12 nav-flap-selector__lead-checkbox">
<div class="checkbox-with-link ">
<input type="checkbox" name="flapAllowCookies" id="flapAllowCookies" checked>
<label for="flapAllowCookies">Berufsgruppenauswahl speichern. <a target="_blank" href="../../../datenschutz/informationen-nach-artikel-13-und-14-ds-gvo/">Cookie-Hinweis</a></label>
</div>
</div>
</div>
<div class="row nav-flap-selector__list-wrapper">
<ul class="nav-flap-selector__list" id="flap-selector-template">
<li class="list-seperator--tablet"></li>
<li>
<div class="datev-button datev-button--white datev-button--heading-font" >
<span>Placeholder</span>
</div>
</li>
</ul>
</div>
<div class="nav-flap-selector__close">
<span></span>
<span></span>
</div>
</div>
<div class="nav-flap-selector__flap" data-anim-class="anim-transform-opacity">
<div class="nav-flap-selector__flap-left">
<span></span>
</div>
<div class="nav-flap-selector__flap-label" data-default="Berufsgruppe wählen">
<span class="hidden" id="bg-flap-label-choose-pg">Berufsgruppe auswählen</span>
<span id="bg-flap-loader">Lädt...</span>
<span class="hidden" id="bg-flap-label">Berufsgruppe auswählen</span>
</div>
<div class="nav-flap-selector__flap-right">
<span></span>
</div>
</div>
</div>
<div class="main-header__wrapper" data-anim-class="anim-height">
<div class="main-header__container container">
<div class="main-header__row row">
<div class="main-header__mobile-full" data-anim-class="anim-height-padding">
<div class="nav-brand" data-anim-class="anim-height-margin">
<a class="nav-brand__link nav-brand__logo" id="datevLogo" href="/web/de/startseite/index.jsp" data-anim-class="anim-top">
<img class="change-to-anim-width-height" src="/web/de/media/tech_datev/layout_datev/images_datev/logo_datev/5c3ccfa02d0a370222b028ee_logo_2x.png" >
</a>
<a class="nav-brand__link nav-brand__claim" id="datevLogo" href="/web/de/startseite/index.jsp" data-anim-class="anim-opacity-delayed">
Zukunft gestalten.<br>Gemeinsam.
</a>
</div>
<a class="nav-search__mobile" href="/web/de/suche/">
<i class="nav-search__mobile--item"></i>
</a>
<div class="nav-hamburger">
<span></span>
<span></span>
<span></span>
<span></span>
</div>
</div>
<ul class="nav-entries" data-anim-class="anim-margin">
<li class="nav-entries__item has-children " data-dynamic-navigation="false" data-id="aktuelles_1">
<a href="/web/de/aktuelles/" class="link-toggle-menu first-level" data-toggle-class="menu-open">
<span>Aktuelles<i class="icon-plus-close icon-toggle" data-toggle-class="icon-plus-close--rotate"></i></span>
</a>
<div class="nav-entries__active-pin"></div>
<ul class="nav-mega-menu mega-menu-loader" data-anim-class="anim-top-max-height">
<div class="container bg-datev-darkgray">
<div class="row">
<div class="datev-loader"></div>
</div>
</div>
</ul>
</li>
<li class="nav-entries__item has-children " data-dynamic-navigation="true" data-id="loesungen">
<a class="link-toggle-menu first-level" data-toggle-class="menu-open">
<span>Lösungen<i class="icon-plus-close icon-toggle" data-toggle-class="icon-plus-close--rotate"></i></span>
</a>
<div class="nav-entries__active-pin"></div>
<ul class="nav-mega-menu mega-menu-loader" data-anim-class="anim-top-max-height">
<div class="container bg-datev-darkgray">
<div class="row">
<div class="datev-loader"></div>
</div>
</div>
</ul>
</li>
<li class="nav-entries__item has-children " data-dynamic-navigation="true" data-id="wissen">
<a class="link-toggle-menu first-level" data-toggle-class="menu-open">
<span>Wissen<i class="icon-plus-close icon-toggle" data-toggle-class="icon-plus-close--rotate"></i></span>
</a>
<div class="nav-entries__active-pin"></div>
<ul class="nav-mega-menu mega-menu-loader" data-anim-class="anim-top-max-height">
<div class="container bg-datev-darkgray">
<div class="row">
<div class="datev-loader"></div>
</div>
</div>
</ul>
</li>
<li class="nav-entries__item" data-dynamic-navigation="false" data-id="datev_shop">
<a href="/web/de/datev-shop/" class="first-level">
<span>DATEV-Shop<i class="icon-arrow icon-arrow--right"></i></span>
</a>
</li>
<li class="nav-entries__item has-children " data-dynamic-navigation="false" data-id="service">
<a href="/web/de/service/" class="link-toggle-menu first-level" data-toggle-class="menu-open">
<span>Service<i class="icon-plus-close icon-toggle" data-toggle-class="icon-plus-close--rotate"></i></span>
</a>
<div class="nav-entries__active-pin"></div>
<ul class="nav-mega-menu mega-menu-loader" data-anim-class="anim-top-max-height">
<div class="container bg-datev-darkgray">
<div class="row">
<div class="datev-loader"></div>
</div>
</div>
</ul>
</li>
<li class="nav-entries__item has-children " data-dynamic-navigation="false" data-id="mydatev">
<a href="/web/de/mydatev/" class="link-toggle-menu first-level" data-toggle-class="menu-open">
<span>MyDATEV<i class="icon-plus-close icon-toggle" data-toggle-class="icon-plus-close--rotate"></i></span>
</a>
<div class="nav-entries__active-pin"></div>
<ul class="nav-mega-menu mega-menu-loader" data-anim-class="anim-top-max-height">
<div class="container bg-datev-darkgray">
<div class="row">
<div class="datev-loader"></div>
</div>
</div>
</ul>
</li>
<li class="nav-entries__item nav-entries__item--mobile {{#isActive}}active{{/isActive}}">
<a href="../../../datev-im-web/impressum/">
<span>Impressum<i class="icon-arrow icon-arrow--right"></i></span>
</a>
</li>
<li class="nav-entries__item nav-entries__item--mobile {{#isActive}}active{{/isActive}}">
<a href="../../../datenschutz/">
<span>Datenschutz<i class="icon-arrow icon-arrow--right"></i></span>
</a>
</li>
<li class="nav-entries__item nav-entries__item--mobile {{#isActive}}active{{/isActive}}">
<a href="../../agb/">
<span>AGB<i class="icon-arrow icon-arrow--right"></i></span>
</a>
</li>
<li class="nav-entries__item nav-entries__item--mobile {{#isActive}}active{{/isActive}}">
<a href="../../../datev-im-web/kontakt/">
<span>Kontakt<i class="icon-arrow icon-arrow--right"></i></span>
</a>
</li>
</ul>
<div class="nav-cart-search">
<div class="nav-cart-search__item nav-cart-search__cart">
<a href=/web/de/anwendung/shop/index.jsp>
<img src="/web/de/media/tech_datev/layout_datev/images_datev/icons_datev/icons_general_datev/icon_shopping_cart.svg" alt="Warenkorb"/>
<div class="nav-cart-search__cart-counter"></div>
</a>
</div>
<div class="nav-cart-search__item nav-cart-search__login">
<a href="">
<img src="/web/de/media/tech_datev/layout_datev/images_datev/icons_datev/icons_general_datev/login.png" alt="Login"/>
</a>
</div>
<div class="nav-cart-search__item nav-cart-search__logout">
<a href="">
<img src="/web/de/media/tech_datev/layout_datev/images_datev/icons_datev/icons_general_datev/logout.png" alt="Logout"/>
</a>
</div>
<form
id="form-search"
action="/web/de/suche/"
autocomplete="off"
class="search"
accept-charset="UTF-8">
<div class="nav-cart-search__item nav-cart-search__search">
<input
name="query"
placeholder="Suche"/>
<button type="submit"><i></i></button>
</div>
</form>
</div>
<div class="main-header__mobile-overlay"></div>
</div>
<div class="main-header__breadcrumb-row row">
<div class="main-header__breadcrumb container">
<ul class="new-breadcrumb list--reset" id="breadcrumb">
<li class="with-link">
<a href="/web/de/startseite/index.jsp">Startseite</a>
<span class="new-breadcrumb__separator"><i class="icon-arrow icon-arrow--right"></i></span>
</li>
<li class="with-link">
<a href="/web/de/m/ueber-datev/" >Über DATEV</a>
<span class="new-breadcrumb__separator"><i class="icon-arrow icon-arrow--right"></i></span>
</li>
<li class="with-link">
<a href="/web/de/m/ueber-datev/das-unternehmen/" >Das Unternehmen</a>
<span class="new-breadcrumb__separator"><i class="icon-arrow icon-arrow--right"></i></span>
</li>
<li class="with-link">
<a href="/web/de/m/ueber-datev/das-unternehmen/compliance/" >Compliance</a>
<span class="new-breadcrumb__separator"><i class="icon-arrow icon-arrow--right"></i></span>
</li>
<li class="without-link">
<span>
Verhaltenskodex
</span>
<span class="new-breadcrumb__separator"><i class="icon-arrow icon-arrow--right"></i></span>
</li>
</ul>
</div>
</div>
</div>
</div>
</header>
<div id="mega-menu-template" style="display: none;">
<ul class="nav-mega-menu" data-anim-class="anim-top-max-height" id="mega-menu-navigation">
<div class="container bg-datev-darkgray">
<div class="row">
<li class="nav-mega-menu__item nav-mega-menu--show-mobile has-children" id="sub-menu-item-template">
<a href="url" class="link-toggle-menu link-toggle-menu-desktop" data-toggle-class="menu-open">
<span class="title"></span>
<i class="icon-arrow icon-arrow--right"></i>
<i class="icon-plus-close icon-toggle" data-toggle-class="icon-plus-close--rotate"></i>
</a>
<a href="url" class="link-toggle-menu link-toggle-menu-mobile" data-toggle-class="menu-open">
<span class="title">title</span>
<i class="icon-arrow icon-arrow--right"></i>
<i class="icon-plus-close icon-toggle" data-toggle-class="icon-plus-close--rotate"></i>
</a>
<ul class="nav-mega-menu-sub">
<li class="nav-mega-menu__sub-item">
<a href="url">
<span class="title">title</span>
<i class="icon-arrow icon-arrow--right"></i>
</a>
</li>
</ul>
</li>
</div>
</div>
</ul>
<ul class="nav-mega-menu nav-mega-menu--alt" id="mega-menu-pg">
<div class="container bg-datev-darkgray">
<div class="row justify-content-center">
<div class="nav-mega-menu__lead">
<h2>DATEV bietet Lösungen für ...</h2>
<p>Wählen Sie zunächst eine Berufsgruppe und Sie erhalten maßgeschneiderte Informationen.</p>
</div>
<li class="nav-mega-menu__item">
<a href="#" class="button--white">
<span>title</span>
<i class="icon-arrow icon-arrow--right"></i>
</a>
</li>
</div>
</div>
</ul>
</div>
<section id="content" class="page__content datev-current-page__content" data-anim-class="anim-margin">
<section class="nav" id="menu">
<form id="form-search-1" action="/web/de/suche/" autocomplete="off" accept-charset="UTF-8" class="search hidden"> <input type="search" name="query" placeholder="Suche" maxlength="1024" class="search"/> </form>
<nav class="sidebar contentBox navigation" id="sub-nav">
<ul>
<li class="hidden"> <a class="" href="/web/de/aktuelles/">Aktuelles</a> </li> <li class="hidden"> <a class="" href="/web/de/loesungen/steuerberater/kanzleimanagement/">Lösungen</a> </li> <li class="hidden"> <a class="" href="/web/de/wissen/steuerberater/seminare-und-kanzlei-beratungen/">Wissen</a> </li> <li class="hidden"> <a class="" href="/web/de/datev-shop/">DATEV-Shop</a> </li> <li class="hidden"> <a class="" href="/web/de/service/">Service</a> </li> <li class="hidden"> <a class="" href="/web/de/mydatev/">MyDATEV</a> </li> <li class="hidden"> <a class="" href="/web/de/top-themen/">Top-Themen</a> </li> <li class="hidden"> <a class="" href="/web/de/m/kontakt/">Kontakt</a> </li> <li class="hidden"> <a class="" href="/web/de/m/presse/">Presse</a> </li> <li> <a class=" selected test true" href="/web/de/m/ueber-datev/">Über DATEV</a> <ul> <li> <a class=" selected test true" href="/web/de/m/ueber-datev/das-unternehmen/">Das Unternehmen</a> <ul> <li> <a class="" href="/web/de/m/ueber-datev/das-unternehmen/fakten-und-informationen/">Fakten &amp; Informationen </a> </li> <li> <a class="" href="/web/de/m/ueber-datev/das-unternehmen/organe-und-geschichte/">Organe &amp; Geschichte</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/das-unternehmen/innovation-sicherheit-und-qualitaet/">Innovation, Sicherheit &amp; Qualität</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/das-unternehmen/unternehmensfuehrung-und-compliance/">Unternehmensführung &amp; Compliance</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/das-unternehmen/nachhaltigkeit-und-csr/">Nachhaltigkeit &amp; CSR</a> </li> </ul> </li> <li> <a class="" href="/web/de/m/ueber-datev/standorte/">Standorte</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/mitglied-werden/">Mitglied werden</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/wechsel-zu-datev/">Wechsel zu DATEV</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/karriere-bei-datev/">Karriere bei DATEV</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/international/">DATEV international</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/das-unternehmen/datev-public-affairs/">DATEV Public Affairs</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/das-unternehmen/kanzleinachfolge/">Kanzleinachfolge</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/datev-im-web/">DATEV im Web</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/datenschutz/">Datenschutz</a> </li> <li> <a class="" href="/web/de/m/ueber-datev/datev-oekosystem/">DATEV-Ökosystem </a> </li> </ul> </li> <li class="hidden"> <a class="" href="/web/de/m/marktplatz/">Marktplatz</a> </li>
</ul>
<ul id="meta-sub" class="hidden">
<li class="cart"> <a class="cartlink" href="/web/de/anwendung/shop/index.jsp"> Warenkorb </a> </li>
</ul>
<div class="clearfix"></div>
</nav>
</section>
<span id="toggle"></span>
<section id="main-content">
<article>
<h1>Verhaltenskodex - Code of Business Conduct</h1>
</article>
<article >
<h2>Präambel</h2>
<p>Die Wettbewerbsfähigkeit unserer Kunden zu fördern, ist das erklärte Ziel von DATEV. Um diesem Ziel gerecht zu werden, entwickeln wir das Unternehmen zu dem europäischen Dienstleister für alle Informations- und Kommunikationsanforderungen der steuerberatenden, rechtsberatenden und wirtschaftsprüfenden Berufe.</p>
<p>Unser Anspruch ist im DATEV-Markenversprechen wiedergegeben: „DATEV kümmert sich zuverlässig und tatkräftig um ihre Partner und ist mit erstklassigen, sicheren und zukunftsorientierten Lösungen Wegbereiter für deren persönlichen Erfolg.&#34; Eine wesentliche Voraussetzung, um dieses Versprechen einzulösen, aber auch, um Schaden vom Unternehmen abzuwenden, ist die Einhaltung der rechtlichen Bestimmungen und der ergänzend dazu eingeführten verbindlichen internen Regelungen.</p>
<p>Unser Code of Business Conduct fasst das grundlegende Wertekonzept des Unternehmens zusammen; alle weiteren internen Richtlinien müssen mit ihm im Einklang stehen.</p>
<p>DATEV bekennt sich ohne jede Einschränkung zu gesetzmäßigem Handeln. Wir erwarten von allen Mitarbeitern ein Verhalten, das jederzeit im Einklang mit den anwendbaren Rechtsvorschriften und den verbindlichen internen Richtlinien sowie den Bestimmungen des Arbeitsvertrages steht.</p>
<p>Soweit in diesem Code of Business Conduct der Begriff „Mitarbeiter&#34; verwendet wird, sind damit alle für DATEV tätigen Personen gemeint, einschließlich Vorstand, Leitende Angestellte und andere Führungskräfte. Alle Mitarbeiter werden über die Verbindlichkeit interner Richtlinien informiert; das Intranet ist hierfür ein wesentliches Hilfsmittel.</p>
<p>DATEV wirkt darauf hin, dass die Regeln dieses Code of Business Conduct auch in ihren Beteiligungen angewendet werden, sofern dies der Größe und Komplexität der jeweiligen Unternehmen angemessen ist. Bei ausländischen Beteiligungen ist jeweils auch das nationale Recht zu beachten.</p>
<p>Auch von ihren Geschäftspartnern erwartet DATEV ein rechtskonformes Verhalten.</p>
<div class="clearfix"></div>
</article>
<article >
<h2>Art. 1 Verwirklichung des Genossenschaftsgedankens und Grundsätze unternehmerischen Handelns</h2>
<ol class="number" start="1">
<li >
Als Genossenschaft unterstützt DATEV in besonderer Weise die beruflichen Interessen der Mitglieder und achtet ihre Rechte. Alle Mitarbeiter beachten bei ihrem unternehmerischen Handeln den Förderauftrag nach Gesetz und Satzung. Für den Geschäftsbetrieb der Genossenschaft ist die Wahrung des Berufsgeheimnisses der Mitglieder gemäß den dafür geltenden Vorschriften unabdingbar.
</li><li >
Nachhaltigkeit ist für DATEV und ihre Mitarbeiter ein Leitgedanke für das gesamte unternehmerische Handeln. DATEV versteht Nachhaltigkeit als den Ausgleich zwischen den drei Dimensionen Ökonomie, Ökologie und Soziales.<br>
</li><li >
DATEV bekennt sich zu guter und verantwortungsvoller Unternehmensführung und orientiert sich dabei auch am Corporate Governance Kodex für Genossenschaften des DGRV - Deutscher Genossenschafts- und Raiffeisenverband e.V..
</li>
</ol>
<div class="clearfix"></div>
</article>
<article >
<h2>Art. 2 Datenschutz und Datensicherheit</h2>
<ol class="number" start="1">
<li >
Für DATEV als berufsständischen DV-Dienstleister haben Datensicherheit und Datenschutz oberste Priorität und sind von grundlegender Bedeutung. DATEV steht für außergewöhnlich hohe Standards in diesem Bereich. Dies gilt in besonderer Weise für personenbezogene Daten, aber auch für Geschäftsdaten. Allen Mitarbeitern obliegt in diesem Zusammenhang eine besondere Verantwortung.
</li><li >
Alle Mitarbeiter wahren die strikte Vertraulichkeit von Mitglieds- und Mandantendaten, insbesondere von Auftragsdaten.
</li><li >
DATEV verlangt von allen Geschäftspartnern die Einhaltung der Verpflichtungen zum Datenschutz und zur Datensicherheit sowie zur Wahrung der Vertraulichkeit.
</li>
</ol>
<div class="clearfix"></div>
</article>
<article >
<h2>Art. 3 Finanzen, Unternehmenseigentum, Wahrheitspflicht</h2>
<ol class="number" start="1">
<li >
Die einschlägigen gesetzlichen Vorschriften und allgemein geltenden Standards zur Buchführung und Bilanzierung (insbesondere GoB) sowie für finanzielle Transaktionen (insbesondere das Vier-Augen-Prinzip) sind einzuhalten. Alle Mitarbeiter sichern die finanzielle Substanz des Unternehmens durch Beachtung der internen Kompetenz- und Unterschriftsregelungen.
</li><li >
Geschäfte mit nahestehenden Unternehmen und Personen erfolgen zu marktüblichen Bedingungen; die betreffenden internen Regelungen werden eingehalten.
</li><li >
DATEV ergreift alle erforderlichen Maßnahmen, um Geldwäsche und Terrorismusfinanzierung in ihrem Einflussbereich zu unterbinden. Embargovorschriften werden beachtet.
</li><li >
Jeder Mitarbeiter geht verantwortungsvoll mit dem Unternehmenseigentum um und vermeidet dessen Schädigung. Anlagen und Einrichtungen des Unternehmens dürfen nur dienstlich genutzt werden, sofern eine andere Nutzung nicht ausdrücklich zugelassen wird.
</li><li >
Alle Aufzeichnungen und Berichte über geschäftliche Vorgänge müssen vollständig und wahrheitsgemäß sein.
</li><li >
DATEV bekennt sich zur Einhaltung steuerrechtlicher Vorschriften und erwartet von allen Mitarbeitern ein Verhalten, das jederzeit im Einklang mit den anwendbaren steuerrechtlichen Vorschriften und den verbindlichen internen Richtlinien steht.
</li><li >
Durch ein internes Kontrollsystem (IKS), ein Tax Compliance Management System (TCMS) und ein aktives Risikomanagement sorgt DATEV für ordnungsgemäße interne Abläufe und vermeidet bestandsgefährdende Risiken.<br>
</li>
</ol>
<div class="clearfix"></div>
</article>
<article >
<h2>Art. 4 Integrität</h2>
<ol class="number" start="1">
<li >
Integrität bestimmt das Verhältnis zu Kunden, Lieferanten und sonstigen Geschäftspartnern. Es wird getragen von Respekt und Wertschätzung und entspricht kaufmännischen Grundsätzen. Beauftragungen erfolgen frei von sachfremden Erwägungen und persönlichen Interessen. Vereinbarungen sind vollständig schriftlich zu dokumentieren.<br>
</li><li >
Kein Mitarbeiter darf unzulässige Zuwendungen, gleich in welcher Form, anbieten oder annehmen. Auf die diesbezüglichen internen Regelungen wird ausdrücklich hingewiesen. Direkte oder indirekte Zuwendungen an Amtsträger und Beamte im In- und Ausland sind strikt verboten.<br>
</li><li >
Sofern Mitarbeiter Insiderinformationen über einen börsennotierten Kunden, Lieferanten oder sonstigen Partner haben, beachten sie die gesetzlichen Insiderregeln.<br>
</li><li >
Spenden und Sponsoring der DATEV müssen transparent, dokumentiert und auf ihre rechtliche Zulässigkeit geprüft sein. Sie bedürfen in jedem Einzelfall einer Vorstandsentscheidung.<br>
</li><li >
Kein Mitarbeiter darf Dritte zu unrechtmäßigen Handlungen veranlassen oder wissentlich an solchen Handlungen Dritter mitwirken.<br><br>
</li>
</ol>
<div class="clearfix"></div>
</article>
<article >
<h2>Art. 5 Arbeitsschutz, Betriebliche Mitbestimmung, Kollegialität, Menschenwürde</h2>
<ol class="number" start="1">
<li >
DATEV respektiert alle Mitarbeiter und achtet ihre Rechte. Hierzu zählt in besonderer Weise der Arbeits- und Gesundheitsschutz. Arbeitsprozesse, Betriebsstätten und -mittel müssen den anwendbaren gesetzlichen Vorschriften entsprechen.<br>
</li><li >
Die Verschiedenheit der Menschen und ihrer Talente ist eine Stärke des Unternehmens. DATEV fördert die Mitarbeiter nach ihren individuellen Möglichkeiten. Dabei werden die Belange der Schwerbehinderten besonders berücksichtigt. Besetzungsentscheidungen werden nach sachlichen Kriterien getroffen.<br>
</li><li >
Eine kollegiale Unternehmenskultur ist Voraussetzung für unternehmerischen Erfolg. Führungskräfte arbeiten mit ihren Mitarbeitern vertrauensvoll zusammen. Diskriminierungen aus Gründen der Rasse, wegen der ethnischen Herkunft, des Geschlechts, der Religion oder Weltanschauung, einer Behinderung, des Alters oder der sexuellen Identität sowie Belästigungen werden nicht toleriert.<br>
</li><li >
DATEV sieht die betriebliche Mitbestimmung mit einem verantwortungsbewussten Betriebspartner als Chance.<br>
</li><li >
Das Unternehmen ist auf den vollen Arbeitseinsatz der Mitarbeiter angewiesen. Nebentätigkeiten sowie Organtätigkeiten für andere Unternehmen dürfen die Interessen von DATEV nicht verletzen und müssen angezeigt werden. Die Zulassung als Berufsträger bedarf der Zustimmung von DATEV.<br>
</li><li >
DATEV achtet die international anerkannten Menschenrechte und lehnt menschenunwürdige Praktiken, wie z. B. Zwangsarbeit oder Kinderarbeit ab. Mit Unternehmen und Institutionen, die solche Praktiken anwenden oder zulassen, arbeitet DATEV nicht zusammen. DATEV erwartet, dass unsere Zulieferer international geltende soziale Standards einhalten.<br>
</li>
</ol>
<div class="clearfix"></div>
</article>
<article >
<h2>Art. 6 Produktinnovation, Produktsicherheit, Umweltschutz</h2>
<ol class="number" start="1">
<li >
DATEV-Produkte stehen für Qualität. Alle Beteiligten stellen daher sicher, dass unsere Produkte bei unseren Kunden Mehrwert schaffen und keine Schäden anrichten.<br>
</li><li >
DATEV schützt eigenes und achtet fremdes geistiges Eigentum.<br>
</li><li >
Die Mitarbeiter gehen verantwortlich mit natürlichen Ressourcen um. Unser Engagement geht über die Beachtung der rechtlichen Anforderungen zum Umweltschutz hinaus.<br>
</li>
</ol>
<div class="clearfix"></div>
</article>
<article >
<h2>Art. 7 Verhalten im Wettbewerb</h2>
<ol class="number" start="1">
<li >
DATEV überzeugt durch ihre Produkte und Service und achtet die Wettbewerbsregeln. Die Mitarbeiter wenden keine unrechtmäßigen Praktiken im Wettbewerb an und setzen hierfür auch keine Anreize.<br>
</li><li >
Alle Mitarbeiter wahren die Vertraulichkeit von Informationen, auch über Dritte. Kein Mitarbeiter von DATEV gibt unbefugt und auf unrechtmäßige Weise Geschäftsgeheimnisse oder sonstige interne Informationen an Wettbewerber oder sonstige Dritte weiter.<br>
</li><li >
Eine Tätigkeit bei einem Wettbewerber, die Beteiligung daran und das Betreiben eines Wettbewerbsunternehmens sind mit einer Tätigkeit als Mitarbeiter bei DATEV unvereinbar.
</li>
</ol>
<div class="clearfix"></div>
</article>
<article >
<h2>Art. 8 Schlussvorschriften</h2>
<ol class="number" start="1">
<li >
Alle Mitarbeiter müssen diesen Code of Business Conduct beachten. Die Führungskräfte tragen für die Einhaltung des Code of Business Conduct in ihrem Zuständigkeitsbereich besondere Verantwortung.
</li><li >
Jeder Mitarbeiter hat das Recht, seine Führungskraft auf mögliche Verstöße im Unternehmen gegen Gesetze oder verbindliche interne Richtlinien, einschließlich diesen Code of Business Conduct, hinzuweisen. Sofern DATEV darüber hinaus ein Hinweisgebersystem zur Verfügung stellt, können damit Fehlverhalten oder Schadenseintritt auch außerhalb der bestehenden Meldewege unter namentlicher Nennung des Hinweisgebers oder gegebenenfalls anonym an den Compliance-Officer gemeldet werden, ohne dass dem Hinweisgeber daraus ein Nachteil entsteht. Dabei werden die Rechte des Betroffenen gewahrt.
</li><li >
Festgestellte Verstöße gegen diesen Code of Business Conduct werden angemessen sanktioniert; hierbei werden die Rechte des Betriebsrats beachtet. Sie können außerdem Strafanzeigen, externe Ermittlungen und zivilrechtliche Verfahren zur Folge haben. DATEV arbeitet mit den zuständigen staatlichen Stellen nach den geltenden Gesetzen zusammen.
</li><li >
Der Vorstand kann diesen Code of Business Conduct ändern und ergänzen.
</li><li >
Der Compliance Officer überwacht die Einhaltung dieses Code of Business Conduct. Bei Zweifelsfällen zur Anwendung und Auslegung können sich alle Mitarbeiter jederzeit an ihre Führungskraft oder an den Compliance Officer wenden (<a class="mailto" href="mailto:compliance%40datev.de?subject=Compliance-Anfrage" title="compliance@datev.de?subject=Compliance-Anfrage">compliance<span>@</span>datev.de</a> beziehungsweise Tel. +49 911 319-41161).
</li>
</ol>
<p>Nürnberg, Januar 2010</p>
<div class="clearfix"></div>
</article>

</section>
</section>
<div class="clearfix"></div>
<footer class="main-footer">
<div class="main-footer__container container">
<div class="main-footer__wrapper main-footer__nav-wrapper">
<div class="main-footer__row main-footer__nav-row row module-content">
<div class="link-box">
<a href="/web/de/m/ueber-datev/" title=" Über DATEV " class="  "> <span> Über DATEV </span> </a>
<a href="/web/de/m/ueber-datev/das-unternehmen/?stat_Mparam=int_footer_azg_das-unternehmen" title=" Das Unternehmen " class="  "> <span> Das Unternehmen </span> </a>
<a href="https://www.datev.de/web/de/karriere/" class="extern  " title="Karriere" target="_blank" > <span> Karriere </span> </a>
<a href="/web/de/m/presse/?stat_Mparam=int_footer_azg_presse" title=" Presse " class="  "> <span> Presse </span> </a>
<a href="/web/de/aktuelles/veranstaltungen/?stat_Mparam=int_footer_azg_messen-veranstaltungen" title=" Messen und Veranstaltungen " class="  "> <span> Messen und Veranstaltungen </span> </a>
<a href="https://www.datev-magazin.de/?stat_Mparam=int_footer_azg_datev-magazin" class="extern  " title="DATEV magazin" target="_blank" > <span> DATEV magazin </span> </a>
<a href="https://www.datev.de/web/de/m/marktplatz/?stat_Mparam=int_footer_azg_datev-marktplatz" class="extern  " title="DATEV-Marktplatz" target="_blank" > <span> DATEV-Marktplatz </span> </a>
<a href="/web/de/aktuelles/newsletter/?stat_Mparam=int_footer_azg_newsletter-abonnieren" title=" DATEV-Newsletter " class="  "> <span> DATEV-Newsletter </span> </a>
<a href="https://www.datev-community.de/" class="extern  " title="DATEV-Community" target="_blank" > <span> DATEV-Community </span> </a>
</div>
<div class="link-box">
<a>Lösungen für Unternehmen</a>
<a href="/web/de/datev-shop/komplettloesungen/datev-mittelstand-faktura-mit-rechnungswesen/?stat_Mparam=int_footer_azg_datev-mittelstand" title=" DATEV Mittelstand " class="  "> <span> DATEV Mittelstand </span> </a>
<a href="/web/de/loesungen/unternehmer/loesungen-fuer-branchen/?stat_Mparam=int_footer_azg_branchenloesungen" title=" Branchenlösungen " class="  "> <span> Branchenlösungen </span> </a>
<a href="/web/de/loesungen/unternehmer/rechnungswesen/buchfuehrung-erstellen-un/datev-unternehmen-online-un/?stat_Mparam=int_footer_azg_digitalisierung-mit-duo" title=" Digitalisierung mit DATEV Unternehmen online " class="  "> <span> Digitalisierung mit DATEV Unternehmen online </span> </a>
<a href="/web/de/loesungen/unternehmer/lohn-und-personal/?stat_Mparam=int_footer_azg_personalwirtschaft" title=" Personalwirtschaft " class="  "> <span> Personalwirtschaft </span> </a>
<a href="https://www.trialog-magazin.de/?stat_Mparam=int_footer_azg_trialog" class="extern  " title="TRIALOG-Magazin" target="_blank" > <span> TRIALOG-Magazin </span> </a>
</div>
<div class="link-box">
<a>Service</a>
<a href="https://apps.datev.de/help-center/" class="extern  " title="DATEV Hilfe-Center" target="_blank" > <span> DATEV Hilfe-Center </span> </a>
<a href="/web/de/service/antworten-finden/?stat_Mparam=int_footer_azg_self-service" title=" Self-Service " class="  "> <span> Self-Service </span> </a>
<a href="/web/de/service/experten-fragen/?stat_Mparam=int_footer_azg_persoenlicher_service" title=" Persönlicher Service " class="  "> <span> Persönlicher Service </span> </a>
<a href="/web/de/service/software-auslieferung/download-bereich/?stat_Mparam=int_footer_azg_download-bereich" title=" Download-Bereich " class="  "> <span> Download-Bereich </span> </a>
<a href="https://www.datev-status.de?stat_Mparam=int_footer_azg_datev-status" class="extern  " title="Wartungen/Störungen" target="_blank" > <span> Wartungen/Störungen </span> </a>
<a href="/web/de/service/software-auslieferung/?stat_Mparam=int_footer_azg_software-auslieferung" title=" Software-Auslieferung " class="  "> <span> Software-Auslieferung </span> </a>
<a href="https://www.smartexperts.de/suche/?stat_Mparam=int_footer_azg_smartexperts" class="extern  " title="SmartExperts" target="_blank" > <span> SmartExperts </span> </a>
<a href="https://download.datev.de/download/fbo-kp/datev_fernbetreuung_online.exe" class="extern  " title="Fernbetreuung online" target="_blank" > <span> Fernbetreuung online </span> </a>
</div>
<div class="link-box">
<a>Online-Anwendungen</a>
<a href="/web/de/mydatev/online-anwendungen/datev-unternehmen-online/?stat_Mparam=int_footer_azg_duo" title=" DATEV Unternehmen online " class="  "> <span> DATEV Unternehmen online </span> </a>
<a href="/web/de/mydatev/online-anwendungen/datev-meine-steuern/" title=" DATEV Meine Steuern " class="  "> <span> DATEV Meine Steuern </span> </a>
<a href="/web/de/mydatev/online-anwendungen/datev-smarttransfer/?stat_Mparam=int_footer_azg_smarttransfer" title=" DATEV SmartTransfer " class="  "> <span> DATEV SmartTransfer </span> </a>
<a href="https://www.datev.de/meinfiskal?stat_Mparam=int_footer_azg_meinfiskal" class="extern  " title="DATEV MeinFiskal" target="_blank" > <span> DATEV MeinFiskal </span> </a>
<a href="https://www.datev.de/ano/?stat_Mparam=int_footer_azg_ano" class="extern  " title="DATEV Arbeitnehmer online" target="_blank" > <span> DATEV Arbeitnehmer online </span> </a>
</div>
<div class="hotline-box">
<span class="headline">DATEV - Kontakt</span>
<div class="datev-button datev-button--black-alpha-25-transparent" >
<span>Kontaktieren Sie uns</span>
<a href="https://www.datev-bot.de/kbot-widget/bots/datev/IWph8QFCtJATT8_nrW4HhJt9VEdZA9wR5t5tX95RLV4=/bot.html" target="_blank"></a>
</div>
</div><!-- /.hotline-box -->
</div><!-- /.row -->
</div>
<div class="main-footer__row main-footer__footer-row row module-content">
<div class="main-footer__footer-row-wrapper">
<div class="main-footer__footer-row-item footer-row-copyright">
<p>&copy; 2021 DATEV eG</p>
</div>
<div class="main-footer__footer-row-item footer-row-item__links">
<ul class="linklist ">
<li>
<a href="/web/de/m/ueber-datev/datev-im-web/impressum/" title="Impressum"><span> Impressum </span></a>
</li>
<li>
<a href="/web/de/m/ueber-datev/datenschutz/" title="Datenschutz"><span> Datenschutz </span></a>
</li>
<li>
<a href="/web/de/m/ueber-datev/das-unternehmen/agb/" title="AGB"><span> AGB </span></a>
</li>
<li>
<a href="/web/de/m/ueber-datev/datev-im-web/kontakt/" title="Kontakt"><span> Kontakt </span></a>
</li>
</us>
</div>
<div class="main-footer__footer-row-item footer-row-item__social">
<ul class="linklist linklist--icon-list">
<li>
<a href="http://www.datev-blog.de" title="blogs" target="_blank"> <i class="icon-share icon-share--blogs"></i> </a>
</li>
<li>
<a href="http://www.facebook.com/dateveg" title="facebook" target="_blank"> <i class="icon-share icon-share--facebook"></i> </a>
</li>
<li>
<a href="http://www.twitter.com/datev" title="twitter" target="_blank"> <i class="icon-share icon-share--twitter"></i> </a>
</li>
<li>
<a href="http://www.youtube.com/datev" title="youtube" target="_blank"> <i class="icon-share icon-share--youtube"></i> </a>
</li>
<li>
<a href="http://www.xing.com/communities/groups/datev-verbindet-erfahrungsaustausch-zwischen-kanzleigruendern-und-inhabern-8249-1006527" title="xing" target="_blank"> <i class="icon-share icon-share--xing"></i> </a>
</li>
</ul>
</div>
</div>
</div><!-- /.row -->
</div><!-- /.container-->
</footer><!-- /.main-footer -->
</div>

<script src="/web/de/media/tech_datev/layout_datev/js_datev/ext_js_datev/jquery-3-3-1.js"></script>
<script>
let secureDomains = ["//secure11.datev.de","login.datev.de","//secure8.datev.de","//secure4.datev.de","//secure.datev.de","www.secure.datev.de"];
</script>
<script src="/web/de/media/tech_datev/layout_datev/js_datev/internal_js_datev/shared.js"></script>
<script src="/web/de/media/tech_datev/layout_datev/js_datev/internal_js_datev/general.js"></script>
<div id="ie-alert" class="ie-alert">
<div class="container">
<p>Sie Verwenden einen veralteten Browser oder den IE11 im Kompatiblitätsmodus. Bitte deaktivieren Sie diesen Modus oder nutzen Sie einen anderen Browser!</p>
</div>
</div>
<script src="../../../../../media/tech_datev/layout_datev/js_datev/internal_js_datev/ie_warning_layer.js"></script>
<script src="/web/de/media/system/javascript/js_general.js"></script>
<script src="/web/de/media/system/javascript/js_basis.js"></script>
<script src="/web/de/media/system/javascript/jquery-fancybox-min.js"></script>
<script src="/web/de/media/system/javascript/js_general-4.js"></script>
</body>
</html>