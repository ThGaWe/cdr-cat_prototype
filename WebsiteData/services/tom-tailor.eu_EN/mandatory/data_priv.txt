New
Men
Legal info
The following explains which personal data is recorded by TOM TAILOR
E-Commerce GmbH during your visit to our internet pages, for what
purposes, and how this data is used.
Lawfulness of processing
To the extent that we obtain your consent for processing of personal
data, the statutory basis is provided by Article 6 (1) sentence 1 letter
a) General Data Protection Regulation (GDPR).
Article 6 (1) sentence 1 letter b) GDPR serves as the statutory basis
for the processing of personal data necessary to fulfil a contract to
which you are a party. This also applies for processing that is
necessary to perform pre-contractual measures.
Article 6 (1) sentence 1 letter c) GDPR serves as the statutory basis
where a processing of personal data is necessary to meet a legal
obligation to which TOM TAILOR E-Commerce GmbH is subject.
Article 6 (1) sentence 1 letter f) GDPR serves as the statutory basis
for any processing of personal data necessary to uphold legitimate
interests of TOM TAILOR E-Commerce GmbH.
Data deletion / period of retention
Your personal data will be deleted as soon as the purpose for recording
it ceases to apply. It may be kept longer if this is provided for by
European or national law in EU decrees, laws or other regulations to
which TOM TAILOR E-Commerce GmbH is subject. Data will also be deleted
when a storage term which has been set by the listed standards ends,
except if there is a need to keep the data for a contract still to be
concluded or performed.
Creation of logfiles
Each time you access the internet pages of TOM TAILOR E-Commerce GmbH
information transmitted by your browser is stored automatically but only
temporarily. The log file so created records the browser type and
version, the operating system, the name and URL of the file accessed
address) as well as date and time of the request to the server. This
data is not linked or combined with data from other sources. The data is
stored and processed uniquely for the purpose of system security and
optimising the internet offer.
In order to collect and store data we use so-called cookies. These are
data packages that your browser stores in your end appliance at our
request. They do not cause any damage there. They do not contain any
executable code and therefore no viruses and do not allow us to spy on
you. There are two kinds of cookies; temporary, or session, cookies, and
persistent cookies.
Session cookies are deleted automatically when you close your browser.
They record a so-called session ID with which the different requests of
your browser can be assigned to the session. This enables your computer
to be recognised when you return to our website. The use of session
The statutory basis for processing your personal data using session
cookies is Article 6 (1) letter f) GDPR.
Persistent cookies are deleted automatically after a prescribed period,
which may vary depending on the cookie. You can delete the cookies at
any time in the security settings of your browser. The cookies enable us
to track your usage for the aforementioned purposes and in the scope
appropriate. They are also intended to enable you to surf better on our
purposes in the internet, for example, in order to offer you
personalized advertising or to assess the success of our advertising
measures. The use of persistent cookies is based on our legitimate
interest in online direct marketing, web analysis and the improvement of
our online impact. The statutory basis for the processing the data is
Article 6 (1) letter f) GDPR.
You can set your internet browser so that our cookies cannot be stored
on your end appliance or else to delete cookies which have already been
stored. If you do not accept cookies, this can lead to restrictions in
the functioning of the internet pages.
On the basis of our legitimate interest, we also integrate cookies by
third party suppliers. In this case, third parties store the
corresponding data packages in your browser or the packages are sent to
the third parties. As a rule, you can prevent the use of third-supplier
cookies by changing the settings in your browser.
Alternatively, you can object to the use of cookies on our site here.
If you have set your browser to block external scripts or use plugins
for that purpose, which are e.g. adblockers or script blockers, the
Cookie Preferences button is not displayed. In this case you prevent the
data processes already. Therefore, need to contradict the data
processing with third-party cookies is not necessary any more.
The following cookies of third party suppliers are used
For web analysis and to improve our online impact:
Use of re-targeting technologies
In order to offer you personalised and thus more relevant advertising we
also make use of re-targeting technologies. With re-targeting we address
users who have already visited our shop and are interested in our range.
With re-targeting, you are shown the advertisements on pages of
advertising partners the content of which is oriented on how you have
surfed on our online shop. Here, too, there is use of cookies from third
party suppliers. We do not use any other personal data for re-targeting
purposes apart from the cookie data and, possibly, your IP address.
The cookies of third party suppliers which are used are listed below:
The current partners of Criteo:
An up-to-date overview of all advertising partners of Criteo is
available here:
https://www.criteo.com/privacy/criteo-works-with-the-following-platforms/
The statutory basis for processing your personal data using cookies from
third-suppliers is Article 6 (1) sentence 1 letter f) GDPR.
Again, you can reject the use of retargeting technologies here
Use of Google Analytics
Our internet page uses Google Analytics, a web analysis service of
Google Inc. Google Analytics uses so-called cookies – these are text
files that are stored on your computer and make it possible to analyse
how you use the website. The information generated by the cookie about
your use of the website is usually transmitted to a Google server in the
USA, where it is stored. On our internet site we use the code extension
website. Using this extension, Google first abbreviates your IP address
within the member states of the European Union and in the other
signatory countries of the Treaty of the European Economic Area.
Google will use this information exclusively on our instructions in
order to evaluate your usage of our website, in order to collate reports
the internet. The abbreviated IP address that it transmitted by your
browser in connection with Google Analytics is not connected with other
data of Google. You can prevent the storage of cookies by changing the
setting in your browser software; but we would point out that, in this
case, you may not be able to use all the functions of the website in
full.
You can also prevent Google Analytics recording by clicking on the
following link: https://tools.google.com/dlpage/gaoptout?hl=en. This
sets an opt-out cookie that prevents your data being recorded when
visiting this website in future.
Further information is provided in the Google data protection
information at https://www.google.com/intl/en/policies/privacy/.
The statutory basis for processing your personal data using cookies from
third-party entities is Article 6 (1) sentence 1 letter f) GDPR.
Use of Exactag
Exactag GmbH collects and stores data on this website and its subpages
for marketing and optimisation purposes. From this data user profiles
can be created. For this purpose, cookies and a technique that is called
fingerprint can be used. Cookies are small text files that are stored
locally in the cashe of the visitor's web browser. The fingerprint
technology stores environment variables of the Internet browser in a
database, without storing user-related data such as an IP address.
Cookies and/or fingerprints allow for the recognition of the Internet
browser. The data collected by the Exactag technology is, without the
explicit consent of the person concerned, not used to identify the user
personally nor aggregated with any personal data. To ensure this
exclusion from data storage, a cookie is set in your browser. This
cookie is named "exactag_new_ccoptout" and is set by "exactag.com". It
may not be deleted as long as the storage of the data is contradicted.
If you want to object to the storage of your anonymously collected
visitor data for the future, please click this Link:
https://m.exactag.com/ccoo.aspx?campaign=8fcb0a023c83ed1d935692c16e44fd27&url=https://www.exactag.com/bestaetigung.
Linking to websites of various social networks
At the bottom of our website you can see links to the websites of some
social networks. Information on the storage and use of your data as well
as your rights and possibilities of changing the settings for the
protection of your privacy is contained in the data protection
information of the network operator concerned.
Through its website, TOM TAILOR E-Commerce GmbH offers you the
possibility of contacting the customer service for any questions about
online orders, invoices or returns. You can enter into contact with TOM
TAILOR E-Commerce GmbH by making a phone call or arranging to be called
back. In connection with establishing contact, where this is necessary
in order to process your query, personal data such as name, date of
birth, e-mail address and telephone number are recorded. This data is
processed as part of the pre-contractual measures which are necessary in
order to perform the service being offered. The data is erased
afterwards when it is not needed in order to fulfil a contract or for
further pre-contractual measures.
Alternatively, contact can be made via the e-mail address provided. In
this case, the personal data of the user which is transmitted with the
email is recorded.
The statutory basis for processing your personal data is Article 6 (1)
sentence 1 letter a) GDPR.
Registration
registering with your personal data. Here the data is entered into a
menu and transmitted to us for us to store in computer memory. This data
is not passed on to any third parties. Data such as your name, address,
e-mail address, date of birth and telephone number is recorded during
the registration process. The reason for registering is to make certain
contents and performances available for you on the website. This data is
processed on the basis of your consent. The consent is obtained during
registration by drawing attention to this data protection declaration.
In the event of the declaration of consent being withdrawn, the data is
deleted.
The statutory basis for processing your personal data is Article 6 (1)
sentence 1 letter a) GDPR.
The internet site of TOM TAILOR E-Commerce GmbH offers the option of
subscribing to a newsletter free of charge. During registration for the
newsletter the data entered into the input mask is transmitted to us and
stored. The data concerned is your name, date of birth, and e-mail
address (only this last is a mandatory field). The data is collected so
that the newsletter can be delivered. This data is processed on the
basis of your consent. The consent is obtained during registration by
drawing attention to this data protection declaration. In the case of
withdrawal of consent, the data is deleted unless there is a need for
retain it, for example, due to statutory regulations.
If you are already a customer of the online shop, or will become one in
future, we should like to adapt, i.e. edit, this newsletter to fit your
individual interests and product preferences, which we do by creating a
personal user profile using the information given by you and stored in
your customer account as well as through your automatically generated
user and transaction data (for details here see the passage on cookies).
When you register for the newsletter, you give your consent for TOM
TAILOR E-Commerce GmbH to create and use the aforementioned personal
user profile in order to send you a personalised newsletter.
The statutory basis for processing your personal data is Article 6 (1)
sentence 1 letter a) GDPR.
Consent to use of data
I AGREE, FOR THE PURPOSES OF OPTIMIZING OFFERS AND PRODUCT INFORMATION
ACCORDING TO INTEREST AND DEMAND, TO MY CLIENT-BASED DATA (SURNAME,
FIRST NAME, TITLE, BILLING AND DELIVERY ADDRESS, E-MAIL ADDRESS,
TELEPHONE NUMBER, DATE OF BIRTH, CUSTOMER NUMBER, PAYMENT DATA) BEING
PROCESSED AND USED AND COLLATED IN A DATABASE WITH USAGE AND TRANSACTION
DATA (E.G. TIME OF ORDER, INFORMATION ABOUT PURCHASED/RETURNED GOODS,
INCLUDING THEIR INDIVIDUAL AND TOTAL PRICES, DISCOUNTS, PARTICIPATION IN
PROMOTIONS AND USE OF VOUCHERS) WHEN AN ORDER IS PLACED VIA THE E-SHOP.
DATA STORED IN THE USER PROFILE SHALL BE ALIGNED WITH THE TOM TAILOR
E-SHOPS PRODUCT RANGE IN ORDER TO PRESENT TO THE CUSTOMER AN OPTIMIZED
RANGE ACCORDING TO INTEREST AND DEMAND, AS WELL AS TO PROVIDE
INFORMATION ABOUT PRODUCTS VIA THE NEWSLETTER.
I CAN REVOKE MY FUTURE CONSENT TO THE ABOVEMENTIONED COLLECTION,
PROCESSING AND USE OF MY DATA AT ANY TIME VIA THE LINK PROVIDED IN EVERY
NEWSLETTER OR BY CONTACTING e-shop.eu@tom-tailor.com OR TOM TAILOR
E-Commerce GmbH, Postfach 1700, 31817 Springe, Germany.
The statutory basis for processing your personal data is Article 6 (1)
sentence 1 letter a) GDPR.
Individual advertising
We should like to offer you individual product offers. For this, we use
the information you have provided and the data contained in your
customer account as well as automatically generated usage data (for
details, see the notes here about cookies) in order to create a user
profile. By analysing and evaluating this information we are able to
improve our websites and our internet offer and to give you product
recommendations better aligned to you as an individual. The use of your
personal data for our advertising purposes involves personal product
recommendations on our website.
With your order and your acceptance of this data protection declaration
you give your consent to a user profile being created as described for
the purpose of making personal recommendations on our website.
The statutory basis for processing your personal data is Article 6 (1)
sentence 1 letter f) GDPR.
Rights of the person affected (i.e. the “data subject”)
If your personal data is processed, you are, in the meaning and
terminology of the GDPR a “data subject” (i.e. an affected person) and
you have claim from us to the rights set out below.
INFORMATION: You have the right at all times, free of charge, to demand
access to – or confirmation of – the data kept about you and to receive
a copy of this data.
CORRECTION: You have the right to demand your personal data be corrected
or added to if it is incorrect or incomplete.
RESTRICTION OF PROCESSING: You have the right to demand that the
processing be restricted if any of the following preconditions applies:
DELETION: You have the right to have your personal data deleted without
delay if any of the following applies and if the processing is not
necessary.
TRANSFERABILITY OF DATE: You have the right to receive from us the
personal data you have given us in a structured, current and
machine-readable format. You also have the right to send this data to
another controller without impediment. In exercising this right, you
also have the right for your personal data to be sent directly by us to
another controller if this is technically feasible. This shall not
infringe the freedoms and rights of other persons.
LODGING AN OBJECTION: You have the right to lodge an objection at any
time against the processing of your personal data which occurs “only” on
account of legitimate interests of ourselves or third parties (Article 6
the personal data unless we can demonstrate compelling reasons
warranting protection which outweigh your interests, rights and freedoms
or the processing serves the assertion, exercise or defence of legal
claims.
You also have the possibility of rejecting the use of your data for
targeted advertising, for instance via Customer Match, at Google under
https://www.google.com/settings/u/0/ads/authenticated
Under https://www.facebook.com/settings?tab=ads§ion=oba&view you can
unsubscribe the use of your data for targeted advertising at Facebook.
WITHDRAWAL OF CONSENT: You have the right to withdraw your consent under
data protection law at any time. The withdrawal of consent does not
affect the legality of the processing which has occurred on account of
the consent in the period until the withdrawal.
In order to exercise your rights, consult please generally the
aforementioned controller since there, too, your rights are to be
implemented. Otherwise, in particular if your concern requires enhanced
confidentiality, you can contact the DATA PROTECTION OFFICER:
You can contract the data protection officer of TOM TAILOR E-Commerce
GmbH at the following postal address: TOM TAILOR E-Commerce GmbH, Data
Protection Officer, Garstedter Weg 14, 22453 Hamburg, or by E-Mail at
datenschutz@tom-tailor.com.
Right to lodge a complaint with a regulator
Without prejudice to any other legal redress pertaining to
administrative law or the courts, you have the right to complain to a
regulator, in particular in the member state of your place of residence,
your workplace or the place of the supposed breach if you are of the
opinion that the processing of your personal data violates the GDPR.
The data protection regulator responsible for us is:
Der Hamburgische Beauftragte für Datenschutz und Informationsfreiheit
Prof. Dr. Johannes Caspar
Ludwig-Erhard-Straße 22, 7. OG, 20459 Hamburg
Tel.: 040 / 428 54 - 4040
Fax: 040 / 428 54 - 4000
E-Mail: mailbox@datenschutz.hamburg.de
your address for Fashion & Lifestyle..
Please choose your delivery country from the following list:
select country
Do you have any questions?
Legal Info
