us](/corporate/about-us/) - [Brand and
Fashion](/corporate/about-us/brand-and-fashion/) -
Report](/corporate/about-us/brand-and-fashion/fashion-report/) -
choice](/corporate/responsibility/positive-choice/) -
Rights
Protection](/corporate/responsibility/positive-choice/human-rights-protection/)
Africa](/corporate/responsibility/positive-product/cotton-made-in-africa/)
List](/corporate/responsibility/positive-view/supplier-list/) -
Policies](/corporate/responsibility/positive-partners/code-of-conduct-policies/)
Programme](/corporate/responsibility/positive-partners/social-programme/)
Finder](/corporate/career/job-finder/) - [FAQ](/corporate/career/faq/) -
Leavers](/corporate/career/school-leavers/) -
Entry](/corporate/career/students/job-entry/) -
Collaborations](/corporate/career/students/university-collaborations/) -
Entry](/corporate/career/graduates/job-entry/) -
Entry](/corporate/career/professionals/job-entry/) -
Environment](/corporate/career/work-environment/) - [Corporate
Culture](/corporate/career/work-environment/corporate-culture/) -
list](/corporate/press/press-mailing-list/) - [Shop
online](https://www.bonprix.de/) -
Company](/corporate/our-company/) - [About us](/corporate/about-us/) -
Report](/corporate/about-us/brand-and-fashion/fashion-report/) -
choice](/corporate/responsibility/positive-choice/) -
Rights
Protection](/corporate/responsibility/positive-choice/human-rights-protection/)
Africa](/corporate/responsibility/positive-product/cotton-made-in-africa/)
List](/corporate/responsibility/positive-view/supplier-list/) -
Policies](/corporate/responsibility/positive-partners/code-of-conduct-policies/)
Programme](/corporate/responsibility/positive-partners/social-programme/)
Finder](/corporate/career/job-finder/) - [FAQ](/corporate/career/faq/) -
Leavers](/corporate/career/school-leavers/) -
Entry](/corporate/career/students/job-entry/) -
Collaborations](/corporate/career/students/university-collaborations/) -
Entry](/corporate/career/graduates/job-entry/) -
Entry](/corporate/career/professionals/job-entry/) -
Environment](/corporate/career/work-environment/) - [Corporate
Culture](/corporate/career/work-environment/corporate-culture/) -
list](/corporate/press/press-mailing-list/) -
back](/corporate/responsibility/positive-product/) - [Our
Responsibility](/corporate/responsibility/ "Responsibility") - [positive
product](/corporate/responsibility/positive-product/ "positive product")
tip-top condition, making packaging a cornerstone of storage and
shipping. As well as using sustainable packaging materials, we are now
also examining dyes and adhesives more closely. Our goal: 100%
sustainable packaging by 2025. **Shipping packaging.** Our shipping
boxes are made from 100% FSC® certified cardboard. Our mailing bags are
made from at least 80% post-consumer recycled plastics and carry the
German eco certification “[Blue Angel](https://www.blauer-engel.de/en)”
packaging with the aim of reducing materials.  **Product packaging.**
Since 2017, we’ve been working with our suppliers to replace polybags
used for textile packaging with more sustainable alternatives. We
started by using recycled bags made from production waste for some of
our product ranges. But we’ve come a long way since then. In 2021, we
switched to using recycled polybags made from household waste. Following
a successful test phase across our markets, all product packaging will
be made from certified post-consumer recycled plastics from 2022. In
cooperation with [Fashion for Good](https://fashionforgood.com/), we
carried out research into ground-breaking new alternatives for recycled
product packaging.  The very first circular solution for polybags was
identified as part of a pilot project with representatives from the
industry. Innovative technology by the company Cadel Deinking removes
dye and glue from post-consumer waste so the transparency and quality of
recycled polybags is on par with their conventional counterparts. Our
logistics hubs are currently examining how we can use insights from the
pilot going forward.  We test new packaging ideas whenever we can. Our
homeware collection offers bedlinen wrapped in so-called self fabric
packaging. These small bags are made from bedlinen production cut-offs
and can be reused by customers, saving 2.5 tonnes of plastic annually. 
We hope to roll out the idea across other product groups soon. []
centrally by the Otto Group. Our paper strategy was geared at upping the
percentage of FSC® certified paper to 60% by 2020, but we exceeded our
target when a figure of 64% was recorded in 2019.  - [positive
choice](/corporate/responsibility/positive-choice/) - [positive
product](/corporate/responsibility/positive-product/) - [positive
view](/corporate/responsibility/positive-view/) - [positive
partners](/corporate/responsibility/positive-partners/) - [positive
making](/corporate/responsibility/positive-making/) - [Shop
online](https://www.bonprix.de/) -
Twitter - Instagram [to top](#) bonprix und Partner brauchen für
einzelne Datennutzungen Deine
um Dir unter anderem Informationen zu Deinen Interessen anzuzeigen. Mit
Klick auf diese Webseite, auf einen Link oder auf "OK" gibst Du diese
Einwilligung. Deine Einwilligung kannst du hier ablehnen. OK
