IMPRESSUM
limango GmbH
Georg-Muche-Straße 1
Für Fragen rund um Deine Bestellung stehen wir Dir gerne zur Verfügung.
Montag - Freitag von 8:00 - 19:00 Uhr
Empfang/Zentrale:
Fax: 089 - 2 48 85 1259
Bildernachweis
Geschäftsführer: Sven van den Bergh, Frank Losem, Martin Oppenrieder,
Marcel de Vries
Eingetragen im Handelsregister
Nr. HRB 138105 am Amtsgericht München
USt-Id-Nr. DE217771895
limango übernimmt keine Haftung für fremde Inhalte, die über einen Link
von Webseiten der limango GmbH erreicht werden können.
Hinweis: Die EU-Kommission bietet die Möglichkeit zur
Online-Streitbeilegung auf einer von ihr betriebenen Online-Plattform.
Diese Plattform ist über den externen Link
https://ec.europa.eu/consumers/odr/ zu erreichen.
