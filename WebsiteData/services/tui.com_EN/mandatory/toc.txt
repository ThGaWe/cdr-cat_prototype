Shortlisted holidays
Our website uses cookies to improve your experience. To find out more,
please read our Cookie Notice. Close
Website; (B) Our Supply Of Products And Services; and (C) Our Contact
With You.
with its Registered Office at Wigmore House, Wigmore Lane, Luton, LU2
9TN (company number 2830117) and which trades under a number of brands.
We are a member of the TUI AG group of companies. We are fully bonded by
the Civil Aviation Authority under the Air Travel Organisers Licence
Association and our membership number is V5126.
PLEASE ENSURE THAT YOU REVIEW ALL OF THE FOLLOWING SECTIONS OF OUR
WEBSITE TERMS AND CONDITIONS CAREFULLY BEFORE USING OUR WEBSITE – YOUR
USE OF AND ACCESS TO ANY PAGE OR PART OF OUR WEBSITE INDICATES THAT YOU
AGREE TO COMPLY WITH AND BE BOUND BY ALL OF THESE TERMS AND CONDITIONS.
PERMITTED TO ACCESS OR USE OUR WEBSITE.
AUTHORISED USE
times be reasonable and not abusive); or
our products and services; or
services; or
manage your preferences or to get into contact with us about your
bookings or travel arrangements; or
services.
NON-AUTHORISED USE
You agree that you shall not:
unlawful. In particular, you are not allowed to copy (whether by
printing off, storing on disk or in any other way), distribute
individual page for your own personal use; or
the rights of any other party or which breaches any standards,
regulations or codes published by any relevant authority; or
other systems and/or to the personal data, information or booking data
of other users and customers; or
such as brands, designs, data, content, copyright material and
trademarks, with rights in the United Kingdom and/or other countries and
which belong to us or to companies within the TUI AG group or which have
been licensed to us. Other product and company names mentioned and third
party content displayed on this Website are proprietary to their
respective owners. You are not licensed to use any of the marks on our
any of these marks.
Unless otherwise stated, we own (or are licensed to use) the
intellectual property rights in the content and information in this
images, logos, videos, maps, podcasts, blogs, customer reviews,
graphics, design, underlying source code and software. Subject to the
or in part, from this Website may not be reproduced, copied,
republished, downloaded, posted, broadcast or transmitted in any form or
medium without our and/or the appropriate owner's prior written
permission.
acknowledged:
LINKS
If you wish to include a link to our Website, unless you have our
written permission, you may only do so to our homepage.
websites operated by companies or brands which are members of the TUI AG
group of companies or external websites operated by our selected
suppliers or partners or other third party websites. Occasionally as you
browse, book travel arrangements or use the functionality on this
Website we may provide you with links or connect you to websites which
may be our branded or third party branded sites. These links or
connections are made available so you can search for and purchase
additional services, find out further information on our services and
your personal travel arrangements quickly and easily and find out about
the products and services of our other group companies and brands.
Please note that we are not responsible for nor do we endorse the
content of these websites and your access and use of these websites will
be subject to the terms and conditions of those websites.
We have made available various services, tools and functionality on this
you how you can use these services, tools and functionality to find out
about and book our products (as well as those provided by our selected
partners), view content provided by us, interact with us and submit and
ACCOUNT SET-UP
When you make a booking with us an account will be created for you. You
will receive an email after you make your booking asking you to activate
your account. If you do not have a booking with us you can register for
an account by providing your name and email address and creating a
password. If you make a booking with us after you have registered for an
account you will be able to view that booking in your account as long as
you use the same email address you registered with when you make your
booking. You are responsible for keeping your password safe and secure
and for all activities that take place on your account.
YOUR BOOKINGS
The lead passenger will be able to view details of current and historic
bookings, including cancelled bookings, in your account.
SHORTLISTS
You can use the shortlist feature to store up to 10 holidays. These will
stay in your shortlist until you remove them. This allows you to view
the saved holidays whenever you are logged in to your account. The
prices of the holidays may go up or down and you will be shown the most
up to date price each time you view the holiday.
B: OUR SUPPLY OF PRODUCTS AND SERVICES
Normally when you purchase our products and/or services and book travel
will make it clear to you that TUI UK is acting as an agent for another
travel provider. If you book a flight only that is operated by TUI
Airways then your contract is with them. See our Booking Terms and
Conditions for the terms and conditions relevant to the purchase of
travel products and holidays from us.
In addition, when you book any products or services from us, you will do
Conditions and such further terms and conditions as may be applied by
the hotel or cruise ship where you will be staying (together, the
entire agreement and understanding between you and TUI UK in relation to
their subject matter. By proceeding with a booking, you acknowledge that
you have read and understood all of the Conditions of Contract and agree
to be bound by them. However, if you do not or cannot accept our
immediately. These Conditions of Contract may vary from time to time. By
browsing this Website you accept that you are bound by the Conditions of
Contract current at the time when you browse or book.
Please note that TUI UK may provide accommodation, transport from a wide
range of third party accommodation providers and carriers including
airlines, ships and railways. The provision of these arrangements will
be subject to the terms and conditions of each provider or carrier as
part of your Conditions of Contract with us (either as agent for the
provider or carrier or as part of our holiday package as you will see in
section 1 of the Booking Terms and Conditions). Other holiday and travel
related services such as travel insurance, foreign exchange and travel
money cards are supplied by our selected service providers and are
available for you to purchase through our own branded websites or
directly via the supplier website – the supply of these products and
services to you shall be subject to the terms and conditions of each
service provider and not us. You can find information on most of these
suppliers and their services on our Extras page. We cannot list all of
the supplier's and their terms and conditions here but you will be able
to find the terms and conditions or conditions of carriage for these
suppliers on their own web sites - you will need to have read them
carefully before you book and/or travel. If you are unable to find these
then please Contact Us.
All details provided by you or collected when you use this Website will
be used and held by us in accordance with our Privacy Notice. 
found in our Cookies Notice.
D. GENERAL
These terms and conditions do not affect your statutory rights.
UPDATES AND CHANGES
We may remove or make changes to the products, information, content,
liability and without notice to you. We also reserve the right to
at any time and without notice if we consider that you have breached any
of our Terms and Conditions. We may also change or modify all of any
parts of our Terms and Conditions at any time and such changes or
modifications shall be effective immediately upon their publication. You
should review these regularly to ensure you are familiar with the most
constitutes your acceptance of the updated terms and conditions and you
agree to by bound by the updated terms and conditions.
DISCLAIMER
To the maximum extent permitted by law, we disclaim all warranties,
whether express or implied by statute, custom or usage relating to the
losses or damages whatsoever, whether in contract, tort (including
negligence), or otherwise arising from this website, or from any
GOVERNING LAW
deal with any disputes which may arise between you and us, and that
English law shall be the applicable law.
LAST UPDATED: March 2016 © TUI UK Limited 2016
Related Links
Call us on: 0203 451 2688
Some images shown throughout this website do not represent current
operational guidelines or health and safety measures such as face masks
and physical distancing requirements.
Cruisedeals.co.uk has been brought into our overall cruise offering,
including TUI River Cruises
To top
Our top deals tailored to you, straight to your inbox Sign up for offers
Shop Finder Ask a question? Contact us
We're part of TUI Group - one of the world's leading travel companies.
And all of our holidays are designed to help you Discover Your Smile.
Registered address: Wigmore House, Wigmore Lane,
Luton,Bedfordshire,United Kingdom, LU2 9TN
Holiday Types
Summer Holidays 2020 Summer Holidays 2021 Winter Holidays 2020 Winter
Sun Holidays Cheap Holidays Luxury Holidays Family Holidays All
inclusive holidays Package Holidays Beach holidays Discount Codes City
Breaks Last minute Holidays Villa Holidays Short Breaks Accessible
Algarve holidaystest Benidorm holidays Canary Islands holidays Crete
holidays Cyprus holidays Gran Canaria holidays Ibiza holidays Lanzarote
holidays Las Vegas Holidays Majorca holidays Menorca holidays Mexico
Holidays New York Holidays Tenerife holidays
Mid/Long haul
Aruba holidays Bali Holidays Vietnam Holidays Cape Verde holidays
Egypt holidays Florida holidays Jamaica Holidays Mauritius holidays Sri
Lanka Holidays Thailand holidays Sharm El Sheikh Holidays Los Cabos
Short Haul
Benidorm Holidays Portugal holidays Spain holidays Turkey Holidays Zante
holidays Morocco holidays Croatia holidays Greece holidays Malta
holidays Bulgaria holidays Italy holidays Lapland holidays
Thailand Flights Alicante Flights Lanzarote Flights Ibiza Flights
Florida Flights Orlando Flights Spain Flights Cyprus Flights Malta
Flights Portugal Flights Turkey Flights Goa Flights Cancun Flights
Barbados Flights Popular Flight Routes
Mediterranean Cruises Canary Islands and Atlantic Central America
Dubrovnik Barcelona All Inclusive Family Cruises Last Minute Cruises
Cruises Cruise Deals Cruises from the UK Croatia Cruises from
Southampton Summer Cruises 2020 River Cruise Deals
Hotels
Hotels in Tenerife Hotels in Gran Canaria Hotels in Lanzarote Hotels in
Fuerteventura Hotels in Majorca Hotels in Menorca Hotels in Ibiza Hotels
in Cancun Hotels in Riviera Maya Hotels in Playacar Hotels in Playa Del
Carmen Hotels in Paphos Hotels in Protaras Hotels in Marmaris Hotels in
Bodrum Hotels in Alanya Hotels in Kusadasi
More from TUI
About TUI MyTUI app Cookies Notice Manage Cookie Preferences Privacy
Affiliates Discover Lakes & Mountains Discover Weddings App Store for
Ios Google Play Store Travel Money Holiday Budget Calculator TUI Group
First Choice Holiday Brochures Travel After Brexit Holiday Weather
All the flight-inclusive holidays on this website are financially
protected by the ATOL scheme. When you pay you will be supplied with an
ATOL Certificate. Please ask for it and check to ensure that everything
you booked (flights, hotels and other services) is listed on it. If you
do receive an ATOL Certificate but all the parts of your trip are not
listed on it, those parts will not be ATOL protected. Some of the
flights on this website are also financially protected by the ATOL
will provide you with information on the protection that applies in the
case of each flight before you make your booking. If you do not receive
an ATOL Certificate then the booking will not be ATOL protected. Please
see our booking conditions for information, or for more information
about financial protection and the ATOL Certificate go to:
www.caa.co.uk. ATOL protection does not apply to the other holiday and
travel services listed on this website.
