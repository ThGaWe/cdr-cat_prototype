sein. Bitte aktivieren Sie Ihre JavaScript-Einstellung in Ihrem Browser.
Wir bitten um Verständnis.
Reiseplanung und Reisebuchung zur Verfügung.
Auf Grund der aktuellen Situation kommt es in unserem Service Center zu
einem sehr hohen Aufkommen.
Bitte beachten Sie, dass wir Ihnen ausschließlich bei Buchungen helfen
können, die online auf DERTOUR.de oder telefonisch mit unserem Service
Team geplant oder abgeschlossen werden.
Login
Anmeldung
E-Mail Adresse oder Benutzername
Passwort
Passwort vergessen
Neu bei DERTOUR?
Registrieren Sie sich jetzt, um zukünftig Reisen zu speichern und zu
verwalten.
Reiseplan
DER Touristik Deutschland GmbH
Emil-von-Behring-Str. 6
Tel.: +49 69 9588-00
Fax: +49 69 9588-1010
service@dertour.de
Sitz und Amtsgericht: Köln - HRB 53152
Umsatzsteuer-Identifikationsnummer: DE811177889
Geschäftsführer: Dr. Ingo Burmester (Sprecher), Mark Tantz, Stephanie
Inhaltlich Verantwortlicher gemäß § 18 Abs. 2 MStV:
DER Touristik Online GmbH
Emil-von-Behring-Str. 2
Tel. +49 69 9588-00
service@dertour.de
Icons made by Vignesh Oviyan, Dave Gandy, Scott de Jonge, Daniel Bruce,
Vaadin, Gregor Cresnar, Freepik, Google, Icomoon, Zurb from
www.flaticon.com are licensed by CC 3.0 BY.
COPYRIGHT:
Die vorliegenden Inhalte des Internetauftritts von DERTOUR sind
urheberrechtlich geschützt. Alle Rechte sind vorbehalten. Mit Ausnahme
der angebotenen Downloads zur Verwendung in der Originalfassung verstößt
die Verwendung der Texte und Abbildungen, auch auszugsweise, ohne die
vorherige schriftliche Zustimmung von DERTOUR gegen die Bestimmungen des
Urheberrechts und ist damit rechtswidrig. Dies gilt insbesondere auch
für alle Verwertungsrechte wie die Vervielfältigung, die Übersetzung
werden eingetragene Marken, Handelsnamen und Logos verwendet. Auch wenn
diese an den jeweiligen Stellen nicht als solche gekennzeichnet sind,
gelten die entsprechenden gesetzlichen Bestimmungen.
Kontaktdaten der Beschwerde- und Schlichtungsstelle (Ombudsmann) für
Versicherungsvermittlungen:
Versicherungsombudsmann e. V.,
Postfach 080632, 10006 Berlin,
Telefon: 0800 3696000, Fax: 0800 3699000
E-Mail: beschwerde@versicherungsombudsmann.de
Homepage: www.versicherungsombudsmann.de
DERTOUR Services
DERTOUR Online-Kataloge
DERTOUR Newsletter
E-Mail Adresse
Ich möchte den personalisierten Newsletter erhalten und akzeptiere
die Datenschutzhinweise
Unser DERTOUR Service Team hilft Ihnen jederzeit bei Ihrer Reisebuchung
Buchung.
Sind Sie sich nicht sicher, ob Sie Ihre Reise mit DERTOUR über ein
Reisebüro oder weitere Online-Anbieter gebucht haben?
Dann erhalten Sie hier mehr Informationen dazu
Liken Sie uns auf Facebook und verpassen Sie keine Neuigkeiten und
Gewinnspiele mehr.
