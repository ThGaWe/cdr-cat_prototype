Rhein-Main-VerkehrsverbundLogo: RMV, Rhein-Main-Verkehrsverbund
RMV-Servicetelefon 069 / 24 24 80 24Täglich 24 Stunden für Sie da!
MEINRMV
Login
E-Mail
PasswortVergessen?_ / _Registrieren
1.  Verbindungssuche
2.  Nächste Abfahrt
3.  Live
Von
Nach
Haltestelle[]
Abfahrtszeiten an einer Haltestelle mit aktuellen Verkehrshinweisen.
Von
Nach
DATENSCHUTZERKLÄRUNG
Die Rhein-Main-Verkehrsverbund GmbH nimmt den Schutz Ihrer
personenbezogenen Daten sehr ernst. Wir möchten, dass Sie wissen, wann
wir welche Daten erheben und wie wir sie verwenden. Nachfolgend
informieren wir Sie über Art, Umfang und Zweck der Erhebung,
Verarbeitung und Nutzung („Verarbeitung“) personenbezogener Daten
hierüber zugänglichen Dienste.
Name und Anschrift des für die Verarbeitung Verantwortlichen
Verantwortlicher im Sinne der Datenschutz-Grundverordnung ist die:
Rhein-Main-Verkehrsverbund GmbH
Alte Bleiche 5
65719 Hofheim am Taunus
Tel.: (0 61 92) 294-0
Fax: (0 61 92) 294-900
E-Mail: datenschutz
rmv
de
Darüber hinaus nutzen auch immer mehr unserer lokalen Partner die
Möglichkeit, sich über unseren Internetauftritt mit eigenen Inhalten zu
präsentieren, um alle wichtigen Informationen zum lokalen und regionalen
Nahverkehr über einen Kanal verfügbar zu machen. Soweit dabei unsere
lokalen Partner personenbezogene Daten verarbeiten, erfolgt dies IN
DEREN EIGENER VERANTWORTUNG und auf Grundlage EIGENER
DATENSCHUTZERKLÄRUNGEN. Diese finden Sie
Im Zuge der Weiterentwicklung unserer Webseiten und der Implementierung
auch Änderungen dieser Datenschutzerklärung erforderlich werden. Daher
empfehlen wir Ihnen, sich diese Datenschutzerklärung ab und zu erneut
durchzulesen.
Zugriff auf das Internetangebot
Jeder Zugriff auf das Internetangebot der Rhein-Main-Verkehrsverbund
GmbH ("RMV") wird in einer Protokolldatei gespeichert. Folgende Daten
werden gespeichert:
Die Verarbeitung dieser Daten erfolgt, um Ihnen die Inanspruchnahme
unserer Dienste und den Verbindungsaufbau zu ermöglichen, zur
Gewährleistung der IT-Sicherheit und zur technischen Sitzungssteuerung.
Dies sowie die Speicherung von IP-Adressen in Logfiles liegt in unserem
berechtigten Interesse an der Datenverarbeitung, ohne dass es
unverhältnismäßig die Grundrechte und Grundfreiheiten der Nutzer unseres
Internetangebots beeinträchtigen würde. Die Verarbeitung erfolgt
insoweit auf Grundlage von Art. 6 Abs. 1 f) DSGVO. Für das Hosting der
Die Daten werden gelöscht, wenn sie zu den vorgenannten Zwecken nicht
mehr erforderlich sind – spätestens jedoch zwei Monate nach ihrer
Erhebung, es sei denn, gesetzliche Aufbewahrungsfristen sehen eine
längere Frist vor. Eine Speicherung dieser Daten zusammen mit anderen
personenbezogenen Daten des Nutzers findet nicht statt.
Einsatz von Cookies
Auf unserem Internetangebot verwenden wir sogenannte "Cookies", bei
denen es sich um kleine Textdateien handelt, die Informationen zur
technischen Sitzungssteuerung beinhalten und die bei Benutzung unseres
Internetangebots unter Umständen von einem Web-Server auf den Speicher
Ihres Browsers übertragen werden. Die in den Cookies enthaltenen
Informationen haben den Zweck, unsere Angebote für Sie möglichst
komfortabel zu gestalten. Sogenannte "Tracking-Cookies" zur
statistischen Auswertung der Nutzung unserer Webseite kommen nicht zum
Einsatz. Der Umfang des Einsatzes von Cookies ist aufgrund unserer
berechtigten Interessen nach Art. 6 Abs. 1 f) DSGVO gerechtfertigt.
Sie können unser Internetangebot auch ohne Cookies nutzen, indem Sie in
Ihrem Browser die Cookie-Speicherung deaktivieren. Folgen Sie hierzu
bitte den jeweiligen Anleitungen des von Ihnen genutzten Browsers. Bitte
beachten Sie, dass bei Deaktivierung von Cookies unter Umständen mit
einer eingeschränkten Darstellung des Internetangebots und mit einer
eingeschränkten Benutzerführung zu rechnen ist.
Auf unserem Internetangebot befinden sich Verlinkungen auf die
Unternehmensseiten des RMV von Facebook und Twitter. Sie erkennen diese
an den jeweiligen Unternehmens-Logos. Es handelt sich hierbei
ausschließlich um Verlinkungen, durch die Sie leichter unsere Kanäle auf
den entsprechenden Webseiten auffinden können. Wir möchten Sie darauf
aufmerksam machen, dass wir in unserem Internetangebot keine sogenannten
der jeweiligen Anbieter herstellen, wenn sie aufgerufen werden. Eine
Datenverarbeitung findet in diesem Zusammenhang mithin erst statt, wenn
Sie den entsprechenden Verlinkungen auf unserem Internetangebot folgen.
Beim Aufruf der jeweiligen Netzwerke und Plattformen gelten die
Geschäftsbedingungen und die Datenverarbeitungsrichtlinien deren
jeweiligen Betreiber.
Soweit nicht anders im Rahmen unserer Datenschutzerklärung angegeben,
verarbeiten wir die Daten der Nutzer, sofern diese mit uns innerhalb der
sozialen Netzwerke und Plattformen kommunizieren, z.B. Beiträge auf
unseren Onlinepräsenzen verfassen oder uns Nachrichten zusenden.
Soweit wir bei Nutzung unserer Facebook-Seiten ("RMVdialog", "RMVsmiles
Bonusprogramm" und "BuBa") personenbezogene Daten der Nutzer
verarbeiten, erfolgt dies auf Grundlage unserer berechtigten Interessen
an einer zeitgemäßen und unterstützenden Informations- und 
Art. 6 Abs. 1 lit. f. DSGVO.
Bitte beachten Sie hierzu auch unsere spezielle Datenschutzerklärung für
RMV-Facebook-Seiten .
Nachfolgend finden Sie zunächst die ausführlichen Pflichtinformationen
zu den zentralen Diensten, die Sie über das MEINRMV-KUNDENPORTAL
erreichen:
Pflichtinformation nach Art. 13 der Datenschutzgrundverordnung
MEINRMV: Informationen nach Art. 13 der Datenschutzgrundverordnung zu
den von Ihnen unter meinRMV erhobenen Daten. 
RMV-HANDYTICKET: Informationen nach Art. 13 der
Datenschutzgrundverordnung zu Ihrem Vertragspartner Offenbacher
Verkehrs-Betriebe GmbH bei Nutzung des Zusatzdienstes. 
RMVsmart: Informationen nach Art. 13 der Datenschutzgrundverordnung bei
Nutzung von RMVsmart.
RMVSMART-IN/OUT: Informationen nach Art. 13 der
Datenschutzgrundverordnung bei Nutzung von RMVsmart-In/Out.
RMV-TICKETSHOP UND ETICKET RHEINMAIN-SERVICES: Informationen nach Art.
13 der Datenschutzgrundverordnung zu Ihrem Vertragspartner bei Nutzung
des TicketShops sowie bei den eTicket RheinMain-Services
BONITÄTSPRÜFUNG: Wünscht der Kunde bei der Nutzung der kostenpflichtigen
Zusatzdienste (RMV-HandyTicket /RMV-TicketShop) die Bezahlung mittels
SEPA-BASIS-Lastschriftverfahrens, erfolgt eine Bonitätsprüfung zur
Beurteilung der Kreditwürdigkeit des Bestellers. In diesem Fall erfolgt
eine Übermittlung Ihrer Daten (Name, Adresse und ggfs. Geburtsdatum) zum
Zweck der Bonitätsprüfung an die infoscore Consumer Data GmbH (ICD),
Rheinstr. 99, 76532 Baden-Baden. Rechtsgrundlage dieser Übermittlung ist
Artikel 6 Absatz 1 Buchstabe f der DSGVO. Übermittlungen auf der
Grundlage dieser Bestimmungen dürfen nur erfolgen, soweit dies zur
Wahrnehmung berechtigter Interessen unseres Unternehmens oder Dritter
erforderlich ist und nicht die Interessen der Grundrechte und
Grundfreiheiten der betroffenen Person, die den Schutz personenbezogener
Daten erfordern, überwiegen. Detaillierte Informationen zur ICD gemäß
Art. 14 Europäische Datenschutzgrundverordnung ("EU-DSGVO"), d. h.
Informationen zum Geschäftszweck, zu Zwecken der Datenspeicherung, zu
oder Berichtigung etc. finden Sie in den. Informationen nach Art. 14 der
Datenschutzgrundverordnung über die infoscore Consumer Data GmbH.
RMVSMILES: Informationen nach Art. 13 der Datenschutzgrundverordnung zu
den von Ihnen bei der Teilnahme an RMVsmiles erhobenen Daten.
RMV-AUSWEISPRÜFUNG: Informationen nach Art. 13 der
Datenschutzgrundverordnung bei Nutzung der RMV-Ausweisprüfung.
E-Mail-Services
Unter Angabe Ihrer E-Mail-Adresse haben Sie die Möglichkeit
Mit Aktivierung des jeweiligen Mail-Services erfolgt die Verarbeitung
zum genannten Zweck. Rechtsgrundlage hierfür ist jeweils Art. 6 Abs. 1
b) DSGVO. Ihre uns im Zusammenhang mit den Mail-Services zur Verfügung
gestellte Mailadresse wird spätestens am Tag nach dem Versand der
jeweiligen E-Mail (Verbindungsfahrplan; Haltestellenaushang) bzw. am Tag
nach Beendigung der Verbindung, für die eine Info-Mail aktiviert war,
gelöscht.
Einzelauskunft für barrierefreie Fahrten
EINE BARRIEREFREIE REISEKETTE" Voreinstellungen zu Ihrer konkreten
Verbindungsanfrage vorgenommen werden, die eine besondere Kategorie
personenbezogener Daten im Sinne des Datenschutzes darstellen, da sie
Rückschlüsse auf Ihre Gesundheitsdaten ermöglichen (Art. 9 Absatz 1
DSGVO). Mit der Aktivierung der entsprechend Ihrer Angaben
personalisierten Verbindungssuche willigen Sie ein, dass wir Ihre
Informationen zur barrierefreien Reisekette ausschließlich zum Zwecke
der Beauskunftung Ihrer Anfrage verarbeiten dürfen. Die Einwilligung
bildet nach Art. 9 Abs. 2 lit a) DSGVO dann die Rechtsgrundlage für die
Verarbeitung. Die Daten werden nur für die Dauer der Beauskunftung
verarbeitet und im Anschluss sofort wieder gelöscht.
Online-Serviceportal für Anfragen, Anregungen und Kritik
Wenn Sie die Links zu unserem Online-Serviceportal für Anfragen,
Anregungen und Kritik nutzen, werden Sie auf www.rmv.de/kundenanliegen
weitergeleitet. Dieses Online-Serviceportal fungiert als zentrale
Beschwerdeplattform des RMV, um die unterschiedlichen Anliegen an den
jeweils richtigen Ansprechpartner weitergegeben zu können. Richtige
Stelle ist beispielsweise eine Lokale Nahverkehrsorganisation (LNO) oder
eines der Verkehrsunternehmen (VU). Für den Betrieb des Serviceportals
bedient sich der RMV seiner 100%igen Tochtergesellschaft, der
Rhein-Main-Verkehrsverbund Servicegesellschaft mbH (rms) in Frankfurt.
Informationen nach Art. 13 der Datenschutzgrundverordnung zu den von
Ihnen erhobenen Daten für das Serviceportal gibt es eine separate
Datenschutzerklärung unter
https://www.rmv.de/c/de/rechtliche-hinweise/datenschutz/datenschutzgrundverordnung/online-serviceportal.
Kontaktinformationen
Wenn Sie uns personenbezogene Daten im Rahmen der Kommunikation (bspw.
per E-Mail, Brief oder Austausch von Visitenkarten) mitteilen, werden
diese von uns verarbeitet. Hierbei handelt es sich insbesondere um Ihre
Kontakt- und Adressdaten, wie z.B. Name, Anschrift und E-Mail-Adresse.
Die Verarbeitung Ihrer Daten erfolgt auf der Grundlage von Art. 6 Abs. 1
lit. f DSGVO, da für uns ein berechtigtes wirtschaftliches Interesse
besteht, im Rahmen des Geschäftsverkehrs entstandene Kontakte auch über
den Erstkontakt hinaus zu pflegen und zum Aufbau einer
Geschäftsbeziehung zu nutzen und hierfür mit den Betroffenen in Kontakt
zu bleiben. 
RMV-10-Minuten-Garantie
Um einen Antrag auf Entschädigung gemäß den Bedingungen der
RMV-10-MINUTEN-GARANTIE bearbeiten zu können, müssen auch die hierfür
erforderlichen Daten erfasst und verarbeitet werden Die Informationen
nach Art. 13 der Datenschutzgrundverordnung zur RMV-10-Minuten-Garantie
haben wir Ihnen in einem eigenen Dokument zusammengefasst.
RMV-Gewinnspiel
Wenn Sie an einem RMV-GEWINNSPIEL teilnehmen, fragen wir Sie nach Ihrem
Namen und einigen anderen persönlichen Informationen, die wir zur
Durchführung des Gewinnspiels, insbesondere zur Gewinnermittlung, der
Gewinnbenachrichtigung und Zusendung/Lieferung des Gewinns benötigen.
Die Verarbeitung Ihrer Daten erfolgt auf Grundlage des Art. 6 Abs. 1
lit. b DSGVO. Für die Auslieferung des Gewinns kann sich der RMV eines
Versenders bedienen. Die übermittelten Daten werden im Übrigen nicht an
Dritte weitergegeben. Alle personenbezogenen Daten in Zusammenhang mit
einem RMV-Gewinnspiel werden unmittelbar nach der Ermittlung der
Gewinner gelöscht.
Anforderung von Infomaterial oder Werbeartikeln
Wenn Sie Infomaterial oder Werbeartikel per E-Mail an freizeit
rmv
de bestellen, benötigen wir Ihren Namen und Ihre Adresse für den
Versand. Ihre Daten werden auf Grundlage von Art. 6 Abs. 1 b) DSGVO
verarbeitet. Für den Versand bedient sich der RMV eines damit
beauftragten Lageristen. Unmittelbar nach dem Versand der bestellten
Artikel wird Ihre E-Mail inklusive aller Kontaktdaten gelöscht.
Abonnement unseres Kundenmagazins RMVmobil
Wenn Sie ein kostenloses Abonnement unseres Kundenmagazins RMVmobil per
E-Mail an mobil
rmv
de bestellen, benötigen wir Ihren Namen und Ihre Adresse für den
Versand. Diese von Ihnen übermittelten personenbezogenen Informationen
werden in eine Abonnentenliste aufgenommen, aus der Sie sich jederzeit
wieder streichen lassen können. Die Verarbeitung Ihrer Daten erfolgt auf
Grundlage von Art. 6 Abs. 1 b) DSGVO. Für den Versand des Kundenmagazins
bedient sich der RMV eines damit beauftragten Lageristen.
Bestellung von Fahrplanbüchern und RMV-Unterrichtsmaterialien
Wenn Sie mit dem Bestellschein Fahrpläne bestellen möchten, benötigen
wir die Angabe Ihres Namens, Straße, Postleitzahl, Ort, Datum der
Bestellung und Ihre Unterschrift. Für die Rechnungsstellung, Abwicklung
und Versand bedient sich der RMV des damit beauftragten Lageristen RMS
Logistics UG, Industriestraße 2, 65779 Kelkheim, welcher auch als
Empfänger des Bestellscheines auf dem Bestellschein aufgeführt ist.
Wenn Sie mit dem Fax-Bestellschein die kostenlosen
RMV-Unterrichtsmaterialien bestellen möchten, benötigen wir die Angabe
Ihres Namens sowie Namen und Adresse Ihrer Schule. Für den Versand
bedient sich der RMV in der Regel des damit beauftragten Lageristen RMS
Logistics UG, Industriestraße 2, 65779 Kelkheim.
Presseverteiler
Wenn Sie die Pressemitteilungen des RMV per E-Mail erhalten möchten, um
stets über anstehende Veranstaltungen und alle wichtigen Informationen
rund um den RMV und seine Partner informiert zu werden, können Sie sich
durch eine E-Mail an pressestelle
rmv
de mit dem Betreff "Anmeldung Presseverteiler" in den E-Mail-Verteiler
aufnehmen lassen. Rechtsgrundlage für die Verarbeitung der Daten ist
Art. 6 Abs. 1 lit. a) DSGVO (Einwilligung). Selbstverständlich können
Sie sich mit dem Betreff "Abmeldung Presseverteiler" genauso einfach
auch wieder abmelden.
Weitergabe personenbezogener Daten an Dritte
Daten, die beim Zugriff auf das Internetangebot des RMV protokolliert
worden sind, werden an Dritte nur übermittelt, soweit wir durch oder
aufgrund Gesetz oder vollziehbarer Anordnung eines Gerichts, einer
Behörde oder sonstigen Stelle nach öffentlichem Recht dazu verpflichtet
sind oder die Weitergabe im Falle von Angriffen auf die
Internetinfrastruktur des RMV zur Rechts- oder Strafverfolgung
erforderlich ist. Eine Weitergabe zu anderen nicht kommerziellen oder
kommerziellen Zwecken erfolgt nicht.
Betroffenenrechte
Nach der EU-Datenschutzgrundverordnung stehen Ihnen folgende Rechte zu:
Werden Ihre personenbezogenen Daten verarbeitet, so haben Sie das Recht,
Auskunft über die zu Ihrer Person gespeicherten Daten zu erhalten (Art.
15 DSGVO).
Sollten unrichtige personenbezogene Daten verarbeitet werden, steht
Ihnen ein Recht auf Berichtigung zu (Art. 16 DSGVO).
Liegen die gesetzlichen Voraussetzungen vor, so können Sie die Löschung
oder Einschränkung der Verarbeitung verlangen sowie Widerspruch gegen
die Verarbeitung einlegen (Art. 17, 18 und 21 DSGVO).
Wenn Sie in die Datenverarbeitung eingewilligt haben oder ein Vertrag
zur Datenverarbeitung besteht und die Datenverarbeitung mithilfe
automatisierter Verfahren durchgeführt wird, steht Ihnen gegebenenfalls
ein Recht auf Datenübertragbarkeit zu (Art. 20 DSGVO).
Sie können nach Artikel 21 DSGVO Widerspruch gegen die Verarbeitung
Ihrer Daten einlegen, sofern und soweit wir diese Verarbeitung auf die
Rechtsgrundlage des überwiegenden berechtigten Interesses (Artikel 6
Absatz 1 Buchstabe f DSGVO) stützen (beispielsweise bei Nutzung unserer
Unternehmenseiten bei Facebook), wenn in Ihrer Person bzw. aus Ihrer
konkreten Situation Gründe ergeben, die in Ihrem Einzelfall gegen die
Verarbeitung sprechen. 
Sollten Sie von Ihren oben genannten Rechten Gebrauch machen, prüft der
RMV, ob die gesetzlichen Voraussetzungen hierfür erfüllt sind.
Weiterhin besteht ein Beschwerderecht bei:
Der Hessische Datenschutzbeauftragte
Postfach 3163
65021 Wiesbaden
Poststelle@datenschutz.hessen.de
Telefon: +49 611 1408 – 0
Telefax: +49 611 1408 – 611
Automatisierte Entscheidungsfindung
Die Entscheidung über die Zulassung des SEPA-Basis-Lastschriftverfahrens
im Zusammenhang mit dem "RMV-HandyTicket" und dem "RMV-TicketShop"
richtet sich nach dem Ergebnis der Bonitätskontrolle, das einen
Forderungsausfall als unwahrscheinlich erscheinen lassen muss.
Dauer der Datenspeicherung
Soweit zuvor keine besonderen Fristen genannt wurden, werden Ihre
personenbezogenen Daten routinemäßig gelöscht, wenn sie nicht mehr zur
Vertragserfüllung notwendig sind [Art. 17 Abs. 1 lit. a) DSGVO] und auch
nicht mehr gesetzlichen Aufbewahrungsfristen unterliegen [Art. 17 Abs. 1
lit.e) DSGVO].
Wir bieten Ihnen die Möglichkeit, sich bei uns zu bewerben (z. B. per
E-Mail oder via Online-Bewerbermanagementsystem). Im Folgenden
informieren wir Sie über Umfang, Zweck und Verwendung Ihrer im Rahmen
des Bewerbungsprozesses erhobenen personenbezogenen Daten. Wir
versichern, dass die Erhebung, Verarbeitung und Nutzung Ihrer Daten in
gesetzlichen Bestimmungen erfolgt und Ihre Daten streng vertraulich
behandelt werden.
UMFANG UND ZWECK DER DATENERHEBUNG
Wenn Sie uns eine Bewerbung zukommen lassen, verarbeiten wir Ihre damit
verbundenen personenbezogenen Daten (z. B. Kontakt- und
Kommunikationsdaten, Bewerbungsunterlagen, Notizen im Rahmen von
etc.), soweit dies zur Entscheidung über die Begründung eines
Beschäftigungsverhältnisses erforderlich ist. Rechtsgrundlage hierfür
ist § 26 BDSG-neu nach deutschem Recht (Anbahnung eines
Beschäftigungsverhältnisses), Art. 6 Abs. 1 lit. b DSGVO (allgemeine
Vertragsanbahnung) und – sofern Sie eine Einwilligung erteilt haben –
Art. 6 Abs. 1 lit. a DSGVO. Die Einwilligung ist jederzeit widerrufbar.
Ihre personenbezogenen Daten werden innerhalb unseres Unternehmens
ausschließlich an Personen weitergegeben, die an der Bearbeitung Ihrer
Bewerbung beteiligt sind.
Sofern die Bewerbung erfolgreich ist, werden die von Ihnen eingereichten
Daten auf Grundlage von § 26 BDSG-neu und Art. 6 Abs. 1 lit. b DSGVO zum
Zwecke der Durchführung des Beschäftigungsverhältnisses in unseren
Datenverarbeitungssystemen gespeichert.
Bei einer Bewerbung über unser Online-Bewerbermanagementsystem werden
Ihre personenbezogenen Daten bei der Firma INFO GmbH - Institut für
Organisationen, Kloster Arnsburg, 35423 Lich, gespeichert.
ABSCHLUSS EINES VERTRAGS ÜBER AUFTRAGSVERARBEITUNG
Wir haben mit dem Anbieter INFO GmbH einen Vertrag zur
Auftragsverarbeitung abgeschlossen und setzen die strengen Vorgaben der
deutschen Datenschutzbehörden bei der Nutzung von dem Portal der INFO
GmbH vollständig um. Näheres entnehmen Sie den Datenschutzbestimmungen
von der INFO GmbH unter: https://www.info-home.org/de/datenschutz.html
AUFBEWAHRUNGSDAUER DER DATEN
Stellenangebot ablehnen oder Ihre Bewerbung zurückziehen, behalten wir
uns das Recht vor, die von Ihnen übermittelten Daten auf Grundlage
unserer berechtigten Interessen (Art. 6 Abs. 1 lit. f DSGVO) bis zu 6
Monate ab der Beendigung des Bewerbungsverfahrens (Ablehnung oder
Zurückziehung der Bewerbung) bei uns aufzubewahren. Anschließend werden
die Daten gelöscht und die physischen Bewerbungsunterlagen vernichtet.
Die Aufbewahrung dient insbesondere Nachweiszwecken im Falle eines
Rechtsstreits. Sofern ersichtlich ist, dass die Daten nach Ablauf der
6-Monatsfrist erforderlich sein werden (z. B. aufgrund eines drohenden
oder anhängigen Rechtsstreits), findet eine Löschung erst statt, wenn
Aufbewahrung kann außerdem stattfinden, wenn Sie eine entsprechende
Einwilligung (Art. 6 Abs. 1 lit. a DSGVO) erteilt haben oder wenn
gesetzliche Aufbewahrungspflichten der Löschung entgegenstehen.
AUFNAHME IN DEN BEWERBER-POOL
Sofern wir Ihnen kein Stellenangebot machen, besteht ggf. die
Möglichkeit, Sie in unseren Bewerber-Pool aufzunehmen. Im Falle der
Aufnahme werden alle Dokumente und Angaben aus der Bewerbung in den
Bewerber-Pool übernommen, um Sie im Falle von passenden Vakanzen zu
kontaktieren.
Die Aufnahme in den Bewerber-Pool geschieht ausschließlich auf Grundlage
Ihrer ausdrücklichen Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Die
Abgabe der Einwilligung ist freiwillig und steht in keinem Bezug zum
laufenden Bewerbungsverfahren. DER BETROFFENE KANN SEINE EINWILLIGUNG
JEDERZEIT WIDERRUFEN. In diesem Falle werden die Daten aus dem
Bewerber-Pool unwiderruflich gelöscht, sofern keine gesetzlichen
Aufbewahrungsgründe vorliegen. Die Daten aus dem Bewerber-Pool werden
spätestens zwei Jahre nach Erteilung der Einwilligung unwiderruflich
gelöscht.
Für weitere Informationen zu unserem Webangebot wenden Sie sich bitte an
den Webmaster der RMV GmbH unter webmaster
rmv
de.
Gültigkeit und Aktualität der Datenschutzerklärung
Diese Datenschutzerklärung hat den Stand vom 08.06.2021.
RMV-Servicetelefon 069 / 24 24 80 24Täglich 24 Stunden für Sie da!
Wir haben die Antwort
Sie haben Fragen? Schauen Sie nach, ob die Antworten auf Ihre Fragen
dabei sind.
RMV von A bis Z
Schirm vergessen? JahresAbo ändern? Oder ein anderes Anliegen? Wir
helfen Ihnen gerne weiter.
RSS-Feeds
Neuigkeiten rund um den RMV liefern Ihnen unsere RSS-Feeds: topaktuell
und kostenlos.
RSS-Feeds
Folgende Fragen könnten Sie interessieren:
Suchergebnisse für "":
Keine passenden Fragen gefunden
