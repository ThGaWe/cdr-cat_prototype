Skip to main content
Help and contactFree delivery and returns100-day return policy
en
CUSTOMER SERVICE HOTLINE: 030 20 21 98 00
From Monday to Sunday 8am-6pm
HELP & CONTACT
https://en.zalando.de/faq/
ZALANDO.DE IS BROUGHT TO YOU BY:
Valeska-Gert-Straße 5
Germany
Telefax: +49 (0)30 2759 46 93
E-Mail: legalnotice@zalando.de
BANK DETAILS
Zalando Payments GmbH
IBAN: DE86210700200123010101
BIC: DEUTDEHH210
Bank: Deutsche Bank
MANAGEMENT BOARD:
Robert Gentz & David Schneider (both co-chairs of the board), Rubin Ritter, Dr. Astrid Arndt, James Freeman II, David Schröder
CHAIRPERSON OF THE SUPERVISORY BOARD:
Registered at Amtsgericht Charlottenburg Berlin, HRB 158855 B
Value Added Tax-ID: DE 260543043
Tax Number: 37/132/45004
Responsible for contents of Zalando SE: Robert Gentz
LEGAL NOTE
Link to the platform of the European Commission according to Regulation
on consumer ODR. We are neither obligated nor willing to participate in
a dispute settlement procedure at a consumer arbitration board.
Your inbox
But make it fashion
Your email address
Manage your preferences
What are you mostly interested in?
Women’s fashion
Men’s fashion
Fashion updates
Recommendations
Offers and sales
Brands you follow
Size Reminder Confirmation
See more (you’ll need to sign in)
To learn how we process your data, visit our Privacy Notice. You can
unsubscribe at any time without costs.
See all help topics
Paying by invoice
Track your parcel
Report a damaged item
Discover Zalando Plus
Delivery information
Return an order
Find the right size
About gift cards and vouchers
Redeem a Gift Card
Visit our corporate site
Newsroom
Our partners
Our payment methods
Our promises
Free delivery and returns
Flexible payment options
Partner Up
Connect your stores
Learn more
ImprintTerms & ConditionsPrivacy NoticeData preferences
AppstoreEnPlaystoreEn
You can also find us on
All prices include VAT
The crossed out price indicates the manufacturer’s recommended retail
price.
