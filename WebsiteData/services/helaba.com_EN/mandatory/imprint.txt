Client Portals
IMPRINT
Service provider identifi­ca­tion in accor­dance with sec­tion 5 of the Tele­me­dien­ge­setz (Ger­manTele­me­dia Act)
On behalf of
LANDESBANK HESSEN-THÜRINGEN GIROZENTRALE
Anstalt des öffentlichen Rechts
MAIN TOWER
Germany
T +49 69 91 32-01
F +49 69 29 15-17
Germany
T +49 3 61/2 17-71 00
F +49 3 61/2 17-71 01
Commercial Register
Amtsgericht Frankfurt am Main, HRA 29821, Amtsgericht Jena, HRA 102181
Board of Managing Directors
Federal Financial Supervisory Authority
EUROPÄISCHE ZENTRALBANK (EZB)
Sonnemannstraße 20
Germany
www.ecb.europa.eu
BUNDESANSTALT FÜR FINANZDIENSTLEISTUNGSAUFSICHT
Graurheindorfer Straße 108
Germany
GermanyVAT identification number
DE 114 104 159
BANK CLEARING CODE (ALPHABETICAL ORDER)
BANK IDEN­TIFIER CODE (BIC)
LEGAL ENTITY IDEN­TIFIER (LEI)
DIZES5CFO5K3I5R58746
GIIN (GLOBAL INTER­ME­DIARY IDEN­TI­FI­CA­TION NUM­BER)
GERMANY X1I0C3.00000.LE.276
FRANCE X1I0C3.00000.BR.250
UNITED KINGDOM X1I0C3.00000.BR.826
SWEDEN X1l0C3.00000.BR.752
Edito­rial note
The information provided on our websites is thoroughly examined by us.
Nevertheless, we cannot assume any liability or guarantee for the
completeness, correctness as to contents or up-to-dateness of the data
and information provided therein.
Please note that the pages contain only non-binding information which in
particular may not be considered investment recommendations and which do
not replace individualized consulting and advice with additional,
up-to-date information.
We are checking the links to other websites and pages of other service
providers at regular intervals. However, we cannot assume any liability
for those sites and pages, their completeness, the correctness or
topicality of their contents or for any changes and amendments made to
them.
How we protect the privacy of your personal data as visitor of our
Learn more
GDPR - Data pro­tec­tion at Helaba
How we protect the privacy of your personal data in general
Learn more
Online Editors
Dr. Astrid Keiner
Julia Himbert
T +49 69/9132 26-77
Julia Himbert
Marco Pfohl
T +49 69/9132 41-62
Online form
meerdesguten
BRAND IDENTITY
Rheingaustraße 184
Germany
T +49 611 236 341-0
F +49 611 236 341-20
Processing of personal data based on the EU General Data Protection Regulation
The following information serves as an overview for our customers
regarding the processing of your personal data by Helaba and your rights
according to Data Protection Regulations.
Do you have questions about Data Protection or the elicitation,
processing and / or usage of your personal data? We look forward to
hearing from you – send us an e-mail to
dtnschtzhlbd
Notice about the arbitration service and the European Online Dispute Resolution (ODR) platform
To settle disputes with the bank, consumers have the option of calling
on the services of the consumer arbitration body of the Association of
German Public Banks (Bundesverband Öffentlicher Banken Deutschlands,
VÖB). Helaba participates in dispute resolution proceedings brought
before this recognised consumer arbitration body.
Complaints must be sent in text form (e.g. by email, facsimile or as a
letter) to:
Verbraucherschlichtungsstelle beim Bundesverband Öffentlicher Banken
Deutschlands (VÖB)
Postfach 11 02 72
Germany
E-Mail: mbdsmnnvb-kbsd
Telefax: +49 30 81 92-2 99
Further details are governed by the "Rules of Procedure for the
Settlement of Complaints within the Association of German Public Banks
European Online Dispute Resolution (ODR) platform
The European Commission has established an online arbitration platform.
As a consumer, you may use this platform to settle disputes out of court
arising from a purchase or service agreement concluded online.
Complaints can be submitted directly to
http://ec.europa.eu/consumers/odr/.
You can contact Helaba on this issue by sending an email to
Your choice
Some are necessary to help our website work properly, and can't be
switched off. And some are optional, but support us in order to improve
your experience during your visit.
Are you happy to accept cookies to improve our website?
necessary cookies
statistic cookies
select all
confirm selection
You can find out more in our data policy.
