Client Portals
Home
Data protection at helaba.com
DATA PROTECTION AT HELABA.COM
Helaba appreciates your visit to our website and your interest in our
bank and its products and services. We take the protection of your
personal data seriously and comply with all data protection regulations.
Set privacy settings
No personal data is collected, processed or used when you visit any of
our webpages, unless you have given Helaba your express consent to
process your data. For this reason, whenever you order any of our
newsletters, use our job application form to upload or send application
documentation or contact us by email or by using a contact form, we will
ask you in advance for your express consent and will inform you of your
rights in respect of the provisions of the EU General Data Protection
Regulation (GDPR), the German Federal Data Protection Act (BDSG) as well
as the Thuringian Data Protection Act (ThürDSG) and the Hessian Data
Protection and Freedom of Information Act (HDSIG).
Protecting your data
We want you to feel at ease not only when visiting our site. The
protection of your privacy is an important issue for us in general. Read
more about the general protection of your data. 
Data protection and revocation declaration
Die Helaba erhebt und speichert in ihren Server-Protokolldateien
automatisch vom Browser übermittelte Informationen. Diese Daten können
von der Helaba nicht bestimmten Personen zugeordnet werden. Sie werden
auch nicht mit anderen Datenquellen zusammengeführt oder dazu verwendet,
Nutzern der Webseite personalisierte Werbung bereitzustellen.
Occasionally, Helaba makes use of cookies when users visit its website.
Cookies are small text files that are stored on your computer and saved
by your browser. Their purpose is to make our range of services more
user-friendly, for example so that you do not have to re-confirm an
automatically generated disclaimer many times. Cookies that we use are
end of your visit to our website. Our cookies do not cause any damage to
your computer and do not contain any viruses.
If you do not wish to allow any cookies to be set on your computer, you
can choose to block them at any time in the privacy settings of your
browser or delete cookies that have already been generated. It is
possible that, in this case, you will not be able to make use of all the
functions of our website.
Numerous websites and servers use cookies. Many cookies contain a
so-called cookie ID. A cookie ID is a unique identification code of a
cookie that consists of a string of characters which websites and
servers can use to identify the specific web browser on which the cookie
has been saved. This enables the websites and servers that a user visits
to distinguish between an individual user’s browser from other web
browsers that contain different cookies. A particular web browser can be
recognised and identified by means of the unique cookie ID.
Subscribing to our Newsletters (only in German)
You are able to subscribe to a regular email with content on our website
that is updated on a regular basis, such as economic research
publications or press releases. In order to subscribe, you have to enter
your email address on our form https://newsletter.helaba.de and send it.
Please ensure your email address is correct, so that you receive the
emails to which you have subscribed.
In the next step, we will send you a confirmation link. Your
registration for the subscription will not be complete until you have
clicked on this link. This so-called double opt-in process ensures that
a) you are the owner of the email address provided and b) you yourself
have subscribed to the newsletter using our form.
The email address supplied when registering for the newsletter is used
exclusively for the purposes of sending the subscribed content or to
provide information about changes to our content as well as technical or
legal data. Your email address is not passed on to third parties. At the
time of your registration to our newsletter, we save the IP address
allocated by your internet service provider as well as the date and time
of your registration. This data is necessary in order to be able to
identify any improper use of your email address at a later time. You may
cancel any existing subscriptions at any time using this
http://newsletter.helaba.de.
In order to send you the newsletters offered on our webpages that you
have ordered, we will ask you for your contact data using an order form.
There are clear definitions of what information is mandatory and what is
voluntary. The data saved and used on the basis of Article 6(1)(a) of
the GDPR for the exclusive purpose of sending the newsletters. No
information is passed on to third parties. By entering your contact data
on the form, you are giving us consent to save your data as well as to
used it in order to send the requested newsletters.
WEB TRACKING
Our webpages only use analytical services to optimise the online
information we provide. Data collected in this way, such as IP address,
date or time of the request, contents of the page accessed or the
browser used do not enable any users to be identified. Analysis by
Helaba of a user’s data is not intended to identify any individuals or
conduct any profiling, in order to, for instance, send online
Web tracking with etracker
The provider of this website uses the services of etracker GmbH,
Hamburg, Germany (www.etracker.com) to analyse usage data. Here, cookies
are used which enable the statistical analysis of the use of this
website by its visitors as well as the display of usage-relevant content
or advertising. Cookies are small text files that are stored by the
Internet browser on the user's device. etracker cookies do not contain
any information that could identify a user.
The data generated with etracker is processed and stored by etracker
solely in Germany by commission of the provider of this website and is
thus subject to strict German and European data protection laws and
standards.In this regard, etracker was checked, certified and awarded
with the ePrivacyseal data protection seal of approval.
The data is processed on the legal basis of Art. 6 Section 1 lit f
and our website. As the privacy of our visitors is very important to us,
etracker anonymizes the IP address as early as possible and converts
login or device IDs into a unique key with which, however,no connection
to any specific person can be made with. etracker does not use it for
any other purpose, combine it with other data or pass it on to third
parties.
You can object to the outlined data processing at any time provided it
is related to your person. Your objection has no detrimental
consequences for you.
Further information on data protection with etracker can be found here.
Helaba maintains social media accounts on Twitter, instagram, Xing,
LinkedIn and YouTube. The relevant pages can be accessed via a link on
our website. Helaba does not use any social media plug-ins for this
purpose so that any personal data can only be exchanged with the
respective social media platforms after clicking on the link and with
your explicit consent.
When you click on a link, you will leave Helaba's website. Pages within
the respective social media channel are operated exclusively by the
respective platform. Helaba has no knowledge of or influence over the
data collected there. This information can be found in the privacy
policies of the platform operators:
If you follow the link, the most basic information that the operator
receives is the fact that you have accessed the page. If you are already
registered with the social media platform, your session may be logged
under your social media account. Even if you have not yet registered
with a platform, the possibility that your IP address may be collected
or stored cannot be ruled out.
Processing of personal data on the basis of the EU General Data Protection Regulation
The following link provides our customers and others information about
the processing of personal data by us and about their rights in respect
of data protection GDPR.
Do you have any questions about data protection or about the collection,
processing and/or use of your personal data? We invite you to contact us
at any time. Simply send an email to dtnschtzhlbd.
Data protection officer
Kaiserleistraße 29-35
63067 Offenbach
T +49 69/91 32-01
E-Mail: dtnschtzhlbd
Your choice
Some are necessary to help our website work properly, and can't be
switched off. And some are optional, but support us in order to improve
your experience during your visit.
Are you happy to accept cookies to improve our website?
necessary cookies
statistic cookies
select all
confirm selection
You can find out more in our data policy.
