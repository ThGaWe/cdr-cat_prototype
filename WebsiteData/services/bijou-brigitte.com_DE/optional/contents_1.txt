Filigrane Ketten - Choker - Colliers - Ketten-Sets - Goldfarbene Ketten
Ohrhänger - Creolen - Clips - Earcuffs - Ohr-Sets - Vintage Ohrringe -
Ringe mit Steinen - Statement Ringe - Antragsring - 925er Silber -
Uhren - Silberfarbene Uhren - Roségoldfarbene Uhren - Armband Uhr -
Damen Uhren - Neue Uhren - Analoge Uhren - Uhren günstig - Uhren kaufen
aus Schmuck - Accessoires - Neu - Taschen Taschen - Umhängetaschen -
Damen Sonnenbrillen - Große Sonnenbrillen - Runde Sonnenbrillen -
Pilotenbrille - Günstige Sonnenbrillen - Sonnenbrillen Trends - Vintage
Silber Ringe - Silber Armbänder - Silber Ohrringe - Silber Ketten -
LOOKS LOOKS - Logomania - Mystic Secrets - High on Fashion - Bunch of
erhöhen, der Direktwerbung dienen oder die Interaktion mit anderen
schließen - [](/) Deutschland DE [](/fr/) Frankreich | France FR
Logo]](https://www.paypal.com/de/cgi-bin/webscr?cmd=xpt/cps/popup/OLCWhatIsPayPal-outside
Newsletter - Filialfinder - [](/) Deutschland DE [](/fr/) Frankreich |
France FR [](/nl/) Niederlande | Nederland NL [](/it/) Italien | Italia
IT [](/es/) Spanien | España ES AGB === Hier finden Sie die wichtigsten
Informationen auf einen Blick: **Inhaltsverzeichnis der AGB** **1.  
Online-Shop](#Gutscheinkarte%20)** **6.  ** **[Warenangebot und
Bestellung ](#Warenangebot)7.  ** **[Speicherung des Vertragstextes
Versandkosten ](#Lieferung)10.  ** **[Gewährleistung und
Haftungsbeschränkung ](#Gewarhrleistung)11.  ** **[Zahlung
Kommunikation](#Elektronische)[ ](#Umwelt)15.   [IP-Rechte
kostenfreie Programm Adobe Reader (unter www.adobe.de) oder
vergleichbare Programme, die das PDF-Format beherrschen. **WICHTIGER
HINWEIS: Bitte beachten Sie, dass wir aus aktuellem Anlass (Maßnahmen
gegen die Ausbreitung des Coronavirus) vorübergehend keine Click &
Collect Bestellungen bearbeiten und versenden können.** **1.
Geltungsbereich und Vertragspartner** Für alle unsere Lieferungen gelten
ausschließlich die nachfolgenden Verkaufsbedingungen. Ihr
Vertragspartner für alle Bestellungen ist die **Bijou Brigitte modische
Accessoires AG** (ladungsfähige Anschrift siehe Punkt „18.
Anschrift/Impressum“ der AGB). **2. Liefergebiet **Die Auslieferung der
bestellten Ware ist nur an eine postalische Adresse oder in eine
click&collect Filiale (Filialabholung) im Gebiet der Bundesrepublik
Deutschland möglich.  **3. Vertragsschluss **Der Vertragsschluss im
Online-Shop erfolgt ausschließlich in deutscher Sprache. Vor Absenden
Ihrer Bestellung erhalten Sie die Möglichkeit, sämtliche Angaben (z. B.
Name, Anschrift, Zahlungsart und bestellte Artikel) nochmals zu
Brigitte Online-Shop unter Angabe der dort geforderten Informationen
durchlaufen haben und im letzten Schritt den Button "Kaufen" anklicken,
geben Sie eine verbindliche Bestellung beim Bijou Brigitte Online-Shop
ab.  Nachdem Sie Ihre Bestellung abgeschickt haben, senden wir Ihnen
eine E-Mail, die den Empfang Ihrer Bestellung bei uns bestätigt und
deren Einzelheiten aufführt (Bestellbestätigung). Diese
Bestellbestätigung stellt keine Annahme Ihres Angebotes dar, sondern
soll Sie nur darüber informieren, dass Ihre Bestellung bei uns
eingegangen ist. Wir behalten uns vor, die Annahme einer Bestellung auf
haushaltsübliche Mengen zu beschränken. Etwaige Ablehnung der Bestellung
wird innerhalb von maximal 3 Tagen erfolgen. Die ggf. bereits
entrichteten Zahlungen werden in diesem Fall unverzüglich erstattet. Der
Kaufvertrag kommt erst durch die Zusendung der Versandbestätigung per
E-Mail (innerhalb von 3 Tagen nach Erhalt Ihrer Bestellung), spätestens
jedoch mit der Zusendung der von Ihnen bestellten Ware zustande. Erfolgt
der Eingang der Zahlung per Vorkasse nicht innerhalb von 14 Tagen nach
Bestelldatum, wird die Bestellung storniert. Spätestens bis zur
Lieferung der Ware erhalten Sie von uns alle Kundeninformationen, die
Sie für Ihre Unterlagen aufbewahren sollten. **4. Filialabholung
postalische Adresse innerhalb Deutschlands, haben Sie auch die
Möglichkeit, einen oder mehrere Artikel im Online-Shop zu reservieren
und kostenfrei in eine teilnehmende Wunschfiliale innerhalb Deutschlands
zur Ansicht liefern zu lassen. Im Bestellprozedere gehen Sie wie folgt
vor: Sofern Sie eine Rechnungsadresse innerhalb Deutschlands angegeben
haben, wählen Sie bei den Zahlungsarten die Option „Bezahlung bei
Filialabholung“. Sofern Sie eine Rechnungsadresse außerhalb Deutschlands
angegeben haben, deaktivieren Sie das Häkchen bei dem sich unter dem
Eingabefeld befindlichen Satz „Die Lieferadresse weicht von der
Rechnungsadresse ab“. Im nächsten Schritt können Sie dann die „Bezahlung
bei Filialabholung“ bestätigen.  Die Anzahl der online reservierten
Artikel ist auf fünfzehn (15) Stück begrenzt. Nachdem Sie Ihre
Reservierung online abgeschickt haben, senden wir Ihnen eine E-Mail, die
den Empfang Ihrer Online-Reservierung bei uns bestätigt und deren
Einzelheiten aufführt (Reservierungsbestätigung). Diese Online-
Reservierung stellt noch keine zahlungspflichtige Bestellung dar und
etwaiger Kaufvertrag kommt erst nach Ihrer Kaufentscheidung in der
Filiale zustande. Die Bezahlung erfolgt mithin erst in der Filiale. Die
Lieferung in die Wunschfiliale wird unverzüglich nach Erhalt der
Online-Reservierung veranlasst und beträgt regelmäßig zwei (2) bis sechs
werden wir Sie jeweils per E-Mail benachrichtigen. Ab dem Tag der
Anlieferung in der Filiale haben Sie die Möglichkeit, die online
reservierten Artikel innerhalb von zwei (2) Wochen anzusehen und ggf.
kostenpflichtig dort zu erwerben. Eine Abholpflicht besteht nicht. Bitte
berufen Sie sich bei der Abholung der Ware in der Filiale auf die
Reservierungs-Nummer, die wir Ihnen in der Reservierungsbestätigung
mitteilen werden. Online-Gutscheine/ Online Rabattaktionen und
Click&Collect: Im Reservierungsvorgang können Sie Ihre
Online-Gutscheine/ Online-Rabattaktionen  eingeben. Bitte beachten Sie,
dass pro Reservierungsvorgang nur 1 Gutschein oder 1 Rabattaktion
eingegeben werden kann. Der Online-Gutschein/ die Rabattaktion wird
anschließend bei einem Kauf in der Filiale entsprechend den jeweiligen
Bedingungen (insbesondere: Mindesteinkaufswert) berücksichtigt und
eingelöst. Bitte beachten Sie, dass die Kombination mit anderen
Werbegutscheinen/ Rabattaktionen nicht möglich ist. Eine Barauszahlung
von Online-Gutschein/ Online-Rabatten ist ausgeschlossen. **5.
Gutscheine im Online-Shop **Die Bijou Brigitte-Gutscheinkarte und/oder
die Bijou Brigitte-Gutscheincodes werden als Zahlungsmittel für Bijou
Brigitte-Artikel verwendet und sind somit wie Bargeld zu behandeln. Das
vorhandene Guthaben wird nicht verzinst. Sie können die Gutscheinkarte
und/oder die Gutscheincodes selbst benutzen oder verschenken. Die
Gutscheinkarte und/oder die Gutscheincodes können in unserem deutschen
Online-Shop unter
werden. Der Mindestwert der online erworbenen Gutscheinkarte und/oder
Gutscheincodes beträgt EUR 25,00, der Höchstwert ist auf EUR 300,00
begrenzt. Für den Versand der Gutscheinkarte müssen Sie eine korrekte
Adresse angeben. Wenn die von Ihnen angegebene Adresse nicht bestätigt
werden kann, behalten wir uns das Recht vor, den Versand der
Gutscheinkarte zurückzustellen. Es gilt das Widerrufsrecht gemäß Ziff.
beim Kauf in allen Bijou Brigitte- Filialen in Deutschland und in
unserem Online-Shop unter www.bijou-brigitte.com eingelöst werden. Im
Rahmen der Online-Bestellung kann höchstens eine Gutscheinkarte oder ein
Gutscheincodes verwendet werden. Es ist keine Kombination mit
Rabattcodes möglich. Zum Einlösen der Gutscheinkarte im Online-Shop
benötigen Sie die Gutscheinnummer von der Rückseite Ihrer Gutscheinkarte
sowie die Prüfnummer, die Sie frei rubbeln können. Die Gutscheinnummer
inklusive Prüfnummer geben Sie dann im Checkout in das Rabattcode-Feld
ein und die Gutscheinkarte wird eingelöst.   Zum Einlösen der
Gutscheincodes im deutschen Online-Shop geben Sie den Code in das
Rabattcode-Feld vor dem Abschluss der Bestellung ein.  Das zum Zeitpunkt
des Einlösens vorhandene Guthaben der Gutscheinkarte bzw. des
Gutscheincodes kann vollständig oder teilweise für Ihre aktuelle
Bestellung verwendet werden und wird automatisch vom Auftragswert
abgezogen. Falls eine Restsumme übrigbleibt, die nicht durch den Wert
der Gutscheinkarte/des Gutscheincodes gedeckt ist, können Sie diese mit
einem Zahlungsmittel Ihrer Wahl begleichen. Ein eventuelles Restguthaben
verbleibt und kann bis zum Ablauf ihrer Gültigkeit verwendet werden. Das
Guthaben der Gutscheinkarte/ des Gutscheincodes  kann nicht gegen
Bargeld eingetauscht werden. Ein Restbetrag wird nicht in Bargeld
erstattet. Die Gutscheinkarte/ der Gutscheincode kann nicht zum Kauf von
weiteren Gutscheinkarten/Gutscheincodes verwendet werden. Wenn Sie die
Retoure in den Filialen für Artikel nutzten, die Sie vollständig mit
unserer Gutscheinkarte/ unserem Gutscheincode bezahlt haben, erhalten
Sie den Retourewert in Form einer neuen Gutscheinkarte zurück. Wenn Sie
beim Kauf zusätzlich zur Gutscheinkarte ein weiteres Zahlungsmittel
eingesetzt haben, erfolgt die Rückzahlung in der Filiale vorrangig in
bar oder wenn gewünscht ebenfalls in Form einer Gutscheinkarte. Die
Gutscheinkarten und/oder die Gutscheincodes sind drei Jahre ab dem
Kaufdatum gültig. Danach können die Gutscheinkarten/Gutscheincodes nicht
mehr als Zahlungsmittel bei uns verwendet werden und ein etwaiger
Restguthaben verfällt. Bei Verlust, Diebstahl oder Beschädigung werden
die Gutscheinkarte bzw. der Gutscheincode und das darauf vorhandene
Guthaben nicht von uns ersetzt. Wir sind berechtigt, schuldbefreiend an
den jeweiligen Inhaber der Gutscheinkarte/des Gutscheincodes zu leisten.
Wir haften nicht für Verluste, die Ihnen durch unberechtigte Einkäufe
unter Verwendung Ihrer Gutscheinkarte/Ihres Gutscheincodes entstehen.
unserem Online-Shop: 1) Artikelauswahl und Warenkorb: Wenn Sie den
gewünschten Artikel gefunden haben, können Sie diesen unverbindlich
durch Anklicken des Buttons "In den Warenkorb" in den Warenkorb legen.
Den Inhalt des Warenkorbs können Sie jederzeit durch Anklicken des
Buttons "Warenkorb" unverbindlich ansehen. Die Artikel können Sie
jederzeit durch Anklicken des Buttons "X" wieder aus dem Warenkorb
entfernen. Sie können hier auch die Bestellmengen, Versand- und
Zahlungsart auswählen. Wenn Sie die Produkte im Warenkorb kaufen wollen,
klicken Sie den Button "Zur Kasse". 2) Adresse:  Wenn Sie bereits als
Kunde angemeldet sind, geben Sie Ihre registrierte E-Mail Adresse und
ihr Passwort ein und klicken Sie dann auf den Button „Anmelden“. Falls
Sie noch nicht registriert sind, können Sie sich entweder neu
registrieren oder die Bestellung als Gast aufgeben. Bitte geben Sie dann
Ihre Rechnungs- und Lieferadresse ein. Die Pflichtangaben sind mit einem
Eingabe Ihrer Daten und Auswahl der Zahlungsart gelangen Sie über den
Button "Weiter" zur Bestellseite, auf der Sie Ihre Eingaben nochmals
bestellen" schließen Sie den Bestellvorgang ab. Der Vorgang lässt sich
jederzeit durch Schließen des Browser-Fensters abbrechen. 3)
Bestellen/Online reservieren (Click&Collect):
Bestellung/Online-Reservierung prüfen und mit dem Button „Kaufen“/"In
Filiale liefern" bestätigen. Bei einer Bestellung/Online-Reservierung
muss kein Mindestbestellwert erreicht werden. **7. Speicherung des
Vertragstextes** Sie können Ihre Bestell- oder Reservierungsdaten, die
auf der letzten Seite des Bestellprozesses im Online-Shop
zusammengefasst werden über die üblichen Speicherfunktionen Ihres
Browsers speichern. Die aktuellen AGB können Sie auch im PDF-Format
herunterladen und  zu Archivierungszwecken speichern oder ausdrucken. Im
Ihrer Bestellung/Online-Reservierung an die von Ihnen angegebene
E-Mail-Adresse. Sofern Sie diese in gedruckter Form wünschen, wenden Sie
sich bitte per E-Mail/Fax/Telefon an uns. Wenn Sie sich als Kunde oder
BB-Club-Mitglied registriert haben, können Sie den Status Ihrer
Bestellung/Online-Reservierung jederzeit unter dem Button "Mein Konto"
einsehen. **8. Preise **Alle Preisangaben sind Endverbraucherpreise,
beinhalten die jeweils geltende gesetzliche Mehrwertsteuer und verstehen
sich zuzüglich Versandkosten. Es gilt der jeweilige ausgewiesene Preis
zum Zeitpunkt der Bestellung/Online-Reservierung. Die Preise gelten
ausschließlich für den deutschen Online-Shop. Preise in den Filialen,
Katalogen und in den länderspezifischen Online-Shops können variieren.
Verfügbarkeit. Ist ein Artikel ausverkauft oder zurzeit nicht lieferbar,
ist eine Bestellung/Online-Reservierung des Artikels nicht möglich.
Etwaige Schadensersatzansprüche aufgrund der Nichtverfügbarkeit der
Artikel sind ausgeschlossen. 2. Ein gewerblicher Wiederverkauf ist
grundsätzlich untersagt. 3. Die Lieferzeit ist abhängig von der
ausgewählten Zahlungsart. Entscheiden Sie sich für die Zahlung per
Vorkasse, erfolgt der Versand der Ware unverzüglich nach Gutschrift bzw.
Deckungszusage durch das Kreditinstitut. Wir möchten darauf hinweisen,
dass die Überweisung durch das Kreditinstitut 2-3 Werktage in Anspruch
nehmen kann. Die Ware wird erst nach Zahlungseingang auf unserem Konto
versendet. Bei der Bezahlung per Kreditkarte (MasterCard/VISA), PayPal,
Sofortüberweisung, Giropay oder per Nachnahme erfolgt der Versand der
Ware innerhalb von 2-3 Werktagen nach Erhalt Ihrer Bestellung. Kann die
Lieferzeit nicht eingehalten werden, informieren wir Sie per E-Mail. Im
Falle einer Online- Reservierung erfolgt der Versand der Ware umgehend
nach Erhalt der Reservierung im Zuge der automatischen Filialbelieferung
und beträgt regelmäßig 5 – 7 Werktage. 4. Versandkosten: Ab einem
Bestellwert von 25,- Euro liefern wir versandkostenfrei. Bei einer
Bestellung unter 25,- Euro berechnen wir lediglich eine
Versandkostenpauschale in Höhe von 3,95 Euro. Wenn Sie Ihre Produkte in
den Warenkorb gelegt haben, informieren wir Sie bei einem Wert unter
sparen. Bei Auswahl der Zahlungsart Nachnahme fallen zusätzlich Gebühren
in Höhe von 4,- Euro an. Dabei handelt es sich um eine Nachnahmegebühr
von 2,- Euro, sowie die Kosten für die Einziehung des
bei Erhalt der bestellten Ware den Rechnungsbetrag an Ihren Postboten.
Die Ware wird umgehend nach dem Eingang Ihrer Bestellung versendet.
Im Falle einer Online- Reservierung entstehen für Sie keine
Versandkosten, auch wenn Sie die reservierte Ware nicht abholen oder
sich in der Filiale letztlich gegen den Erwerb der Ware entscheiden
sollten. Sollte aus technischen oder logistischen Gründen der Versand in
mehreren Etappen erfolgen, berechnen wir nur eine einmalige
Versandkostenbeteiligung. **10. Gewährleistung und Haftungsbeschränkung
Ware beträgt zwei Jahre ab deren Erhalt. Sollten für Produkte besondere
Garantien gelten, bleiben Ihre gesetzlichen Mängelansprüche davon
unbeschadet bestehen. Der Ansprechpartner für die jeweilige Garantie
wird in den Garantiebestimmungen genannt, die dem bestellten Produkt
beiliegen. Haben Sie diese Bestimmungen nicht zur Hand oder handelt es
sich nicht um einen technischen Artikel, wenden Sie sich bitte an
unseren Kundenservice. Sollten gelieferte Artikel offensichtliche
Material-, Herstellungsfehler oder Transportschäden aufweisen, so melden
Sie dies bitte sofort gegenüber uns oder dem Transporteur, der die
Artikel anliefert. Eine Verpflichtung des Kunden hierzu besteht nicht
und ist auch nicht Voraussetzung für die Geltendmachung Ihrer Ansprüche.
Allerdings können wir ansonsten keine Ansprüche gegen den Transporteur
geltend machen. Die Einhaltung der vorstehenden Regelung berührt Ihre
gesetzlichen Ansprüche nicht, soweit Sie zu privaten Zwecken als
Verbraucher bestellt haben. Soweit ein gewährleistungspflichtiger Mangel
vorliegt, richten sich Ihre Gewährleistungsrechte nach den gesetzlichen
Vorschriften. In diesen Fällen können Sie für die Rücksendung bei
unserem Kundenservice jederzeit ein Retouren-Etikett anfordern. Wir sind
berechtigt, die Art der von Ihnen gewählten Nacherfüllung zu verweigern,
wenn sie nur mit unverhältnismäßigen Kosten möglich ist und die andere
Art der Nacherfüllung ohne erhebliche Nachteile für Sie bleibt. Im Falle
einer Online- Reservierung richten Sie bitte Ihre Ansprüche direkt an
die Filiale, in der Sie die online reservierten Artikel gekauft haben. 
Angaben, Zeichnungen, Abbildungen, technische Daten, Gewichts-, Maß- und
Leistungsbeschreibungen, die in Prospekten, Katalogen, Rundschreiben,
Newslettern, Anzeigen oder Preislisten enthalten sind, haben rein
informativen Charakter. Wir übernehmen keine Gewähr für die Richtigkeit
dieser Angaben. Hinsichtlich der Art und des Umfangs der Lieferung sind
allein die in der Auftragsbestätigung enthaltenen Angaben
ausschlaggebend. In den Fällen einer zwingenden gesetzlichen Haftung
Falle der schuldhaften Verletzung des Lebens, des Körpers oder der
Gesundheit haften wir uneingeschränkt. Im Falle einer einfachen
fahrlässigen Verletzung von wesentlichen Vertragspflichten haften wir
der Höhe nach begrenzt nur für den vertragstypischen, vorhersehbaren
Schaden (keine indirekte Schäden, Image-Schäden, entgangener Gewinn und
Schäden aufgrund höherer Gewalt). Wesentliche Vertragspflichten sind
solche, deren Erfüllung die ordnungsgemäße Durchführung des Vertrags
erst ermöglicht, deren Verletzung die Erreichung des Vertragszwecks
gefährdet und auf deren Einhaltung Sie regelmäßig vertrauen, sog.
wesentlicher Pflichten haften wir nicht. Diese Haftungsbeschränkung gilt
auch zugunsten unserer gesetzlichen Vertreter und Erfüllungsgehilfen,
wenn Ansprüche direkt gegen diese geltend gemacht werden. Im Übrigen
sind Sie verpflichtet, angemessene Maßnahmen zur Abwehr und Minderung
des Schadens zu treffen. **11. Zahlung **Im Bijou Brigitte Online-Shop
bieten wir Ihnen folgende Zahlungsarten an:
Wir behalten uns das Recht vor, im Einzelfall bestimmte Zahlungsarten
auszuschließen. Alle angebotenen Zahlungsarten sind durch das
SSL-Sicherheitssystem (Secure Socket Layer) in Verbindung mit einer
einverstanden, dass Sie Rechnungen und Gutschriften ausschließlich in
elektronischer Form erhalten. Vorkasse/Überweisung Entscheiden Sie sich
für die Zahlung per Vorkasse erfolgt der Versand der Ware unverzüglich
nach Gutschrift durch das Kreditinstitut. Die Überweisung durch das
Kreditinstitut kann 2-3 Werktage in Anspruch nehmen. Wir haften nicht
für Verspätungen, die dem ausführenden Kreditinstitut zuzurechnen sind.
Oder durch sonstige Probleme mit der Abwicklung der Überweisung
verursacht wurden. Die Ware wird erst nach Zahlungseingang auf unserem
Konto versendet. Etwaige Kosten der Überweisung tragen Sie. Nach
abgeschlossener Bestellung erhalten Sie eine Bestellbestätigung per
E-Mail, in der alle von Ihnen eingegebenen Bestelldaten vermerkt sind.
Bitte überweisen Sie bei der Bestellung per Vorkasse den in der
Bestellbestätigung genannten Betrag auf das angegebene Konto. Bitte
unbedingt als Verwendungszweck das Kürzel VK sowie Ihre Bestellnummer
angeben. Erfolgt der Eingang der Zahlung per Vorkasse nicht innerhalb
von 14 Tagen nach Bestelldatum, wird die Bestellung storniert. **Unsere
Bankverbindung:** Bijou Brigitte AG Postbank Hannover IBAN : DE90 2501
stärken, unterstützt der Bijou Brigitte Online-Shop das sogenannte 3D
Secure-Verfahren. Dabei handelt es sich um ein von MasterCard
Sicherheitsverfahren, das Ihre Kreditkartendaten durch ein eigenes
Passwort schützt. Um das Verfahren nutzen zu können, müssen Sie sich bei
Ihrer kartenausgebenden Bank oder Sparkasse registrieren. Nachdem Sie am
Ende des Bestellvorganges den Button „Kaufen“ gedrückt haben, können Sie
den Bezahlprozess starten und Ihre Kreditkartendaten in das bei uns
eingebettete und SSL-verschlüsselte Formular Ihres Kreditinstituts
eingeben. Durch die Eingabe Ihres persönlichen Secure Codes bzw. Ihres
Passwortes bestätigen Sie die Transaktion. Die Belastung des
Kreditkartenkontos erfolgt unverzüglich nach erfolgreich durchgeführter
Zahlung und die Ware wird umgehend versendet. PayPal Mit dem kostenlosen
Online-Zahlungsservice PayPal können Sie sicher, einfach und schnell
bezahlen. Hierfür müssen Sie sich vorab unter
eröffnen. Sofortüberweisung/Giropay Sofortüberweisung ist eine neue
innovative Zahlungsart mit TÜV-Zertifikat und TÜV-geprüfter
Transaktionssicherheit. Direkt nachdem Sie den Button „Bestellung
abschließen“ gedrückt haben, wird der Überweisungsprozess gestartet und
die Überweisungsmaske geladen. In einem bereits voraus-gefüllten
Kontonummer und Ihren Namen eingeben und anschließend wie gewohnt mit
Ihrer PIN und TAN die Transaktion bestätigen. Der Kaufbetrag wird sofort
an das Bankkonto von Bijou Brigitte überwiesen, sodass der Versand der
bestellten Ware umgehend durchgeführt werden kann. Weitere Informationen
zu der Zahlung per Sofortüberweisung erhalten Sie unter
Nachnahme (nur bis zu einem Bestellwert von 100€ möglich) Bitte beachten
Sie, dass bei der Zahlung per Nachnahme zusätzliche Gebühren in Höhe von
Euro, sowie die Kosten für die Einziehung des Übermittlungsentgeltes des
Zustelldienstes von 2,- Euro. Bei der Zahlung per Nachnahme bezahlen Sie
bei Erhalt der bestellten Ware den Rechnungsbetrag an Ihren Postboten.
Die Ware wird umgehend nach dem Eingang Ihrer Bestellung versendet.
Amazon Payments Mit „Zahlung mit Amazon Payments“ können Sie die in
Ihrem Amazon-Konto hinterlegten Zahlungs-, Rechnungs- und
Lieferinformationen nutzen, um schnell und sicher einzukaufen. Zudem
genießen Sie den Käuferschutz, den Amazon mit der A-bis-z Garantie
bietet. Registrieren Sie sich bei „Login und Bezahlen mit Amazon“, indem
Sie sich mit der E-Mail-Adresse und dem Passwort Ihres bereits
vorhandenen Amazon-Kontos einloggen. Sonstiges Die Bezahlung durch
Senden von Bargeld oder Schecks ist leider nicht möglich. Wir schließen
eine Haftung bei Verlust aus. Bis zur vollständigen Bezahlung bleibt die
Ware Eigentum der Bijou Brigitte modische Accessoires AG. Pro Bestellung
wird grundsätzlich nur 1 Online-Gutschein oder 1 Online-Rabattaktion
akzeptiert. Die Barauszahlung von Online-Gutscheinen/ Online-Rabatten
ist ausgeschlossen. **12. Rücksendekostenvereinbarung **Machen Sie von
Ihrem Widerrufsrecht auf dem Postwege Gebrauch, haben Sie die
unmittelbaren Kosten der Rücksendung zu tragen. Dies gilt auch, wenn Sie
den Vertrag nur teilweise widerrufen. Alternativ können Sie die Ware
innerhalb der Widerrufsfrist in einer beliebigen Bijou-Brigitte-Filiale
kostenfrei zurückgeben. Die Entgegennahme der zurückzugebenden Ware kann
in den Bijou-Brigitte-Filialen verweigert werden, wenn deren Gesamtwert
EUR 200,00 übersteigt. **13. Widerrufsrecht Das folgende Widerrufsrecht
besteht nur für Verbraucher.** Verlängertes Rückgaberecht bis zum
Angabe von Gründen diesen Vertrag zu widerrufen. Die Widerrufsfrist
beträgt vierzehn Tage ab dem Tag, an dem Sie oder ein von Ihnen
benannter Dritter, der nicht der Beförderer ist, die Waren in Besitz
genommen haben bzw. hat. Im Falle einer Online-Reservierung steht Ihnen
kein Widerrufsrecht zu. Etwaiger Umtausch von online reservierten
Artikeln erfolgt zu Bedingungen des Kaufs in den Filialen.  Um Ihr
Widerrufsrecht auszuüben, müssen Sie uns mittels einer eindeutigen
Erklärung (z.B. ein mit der Post versandter Brief, Telefax oder E-Mail)
Widerruf ist zu richten an: Bijou Brigitte modische Accessoires AG
Online-Shop Poppenbütteler Bogen 1 22399 Hamburg, Deutschland Tel: 040 /
Fax: +49 (0)40 / 602 64 09 E-Mail:
entsprechen den Kosten für ein Gespräch ins deutsche Festnetz. Die
Mobilfunkkosten können ebenfalls je nach Tarif und Mobilfunkanbieter
beigefügte
Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über
die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.
Sie haben darüber hinaus die Möglichkeit, den Widerruf innerhalb der
Widerrufsfrist zeitgleich mit der Rückgabe der Ware in einer beliebigen
Bijou Brigitte-Filiale zu erklären. Die Lieferkosten werden in diesem
Fall nicht erstattet. **Folgen des Widerrufs** Wenn Sie diesen Vertrag
haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen
Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung
als die von uns angebotene, günstigste Standardlieferung gewählt haben),
unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag
zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses
Vertrags bei uns eingegangen ist. Wenn Sie den Vertrag nur teilweise
widerrufen, werden wir Ihnen die Lieferkosten aufgrund ihrer
Pauschalität nicht erstatten. Für die Rückzahlung verwenden wir dasselbe
Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt
haben, es sei denn, mit Ihnen wurde ausdrücklich etwas Anderes
vereinbart; Wenn Sie per Vorkasse, Nachnahme, Sofortüberweisung oder
Giropay bezahlt haben, benötigen wir zusätzlich Ihre vollständige
Kontoverbindung, um Ihnen den Betrag zu überweisen. Wenn Sie per
Kreditkarte oder PayPal bezahlt haben, findet eine Rückbuchung direkt
auf Ihr Konto statt; in keinem Fall werden Ihnen wegen dieser
Rückzahlung Entgelte berechnet. Bitte beachten Sie bei einer Rückgabe in
der Filiale den ausgefüllten Retoure-Schein sowie die Rechnung als
Zahlungsnachweis vorzuzeigen. Im Falle einer Rückgabe in einer
beliebigen Bijou Brigitte-Filiale erfolgt die Rückzahlung in
Bargeld. Übersteigt der Gesamtwert der zurückzugebenden Ware EUR 200,00,
bleibt es im Ermessen der jeweiligen Filiale, die Ware entgegenzunehmen
und entsprechende Rückzahlung  in Bargeld zu leisten. Für den Fall, dass
die Filiale die Ware nicht entgegennimmt, muss die Rücksendung der Ware
auf Ihre Kosten auf dem Postweg erfolgen und die Rückzahlung erfolgt
eingesetzt haben. Wir können die Rückzahlung verweigern, bis wir die
Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht
haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der
frühere Zeitpunkt ist. Sie haben die Waren unverzüglich und in jedem
Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über
den Widerruf dieses Vertrags unterrichten, an uns zurückzusenden oder zu
richten. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist
von vierzehn Tagen absenden. Sie tragen die unmittelbaren Kosten der
Rücksendung der Waren. Alternativ können Sie die Ware unter Verwendung
des Retoure-Scheins in einer beliebigen Bijou Brigitte-Filiale
kostenfrei zurückgeben. Die Ware gilt dann mit der fristgerechten
vorzeitig bei Verträgen zur Lieferung versiegelter Waren, die aus
Gründen des Gesundheitsschutzes oder der Hygiene nicht zur Rückgabe
geeignet sind, wenn ihre Versiegelung nach der Lieferung entfernt
wurde.  Sie müssen für einen etwaigen Wertverlust der Waren nur
aufkommen, wenn dieser Wertverlust auf eine offensichtliche Beschädigung
oder einen zur Prüfung der Beschaffenheit, Eigenschaften und
Funktionsweise der Waren nicht notwendigen Umgang mit ihnen
zurückzuführen ist. Ende der Widerrufsbelehrung [Widerrufsformular als
PDF](https://www.bijou-brigitte.com/media/pdf/4d/3f/53/Widerrufsformular_Online-Shop_DE.pdf
Sie sind damit einverstanden, dass die vertragsbezogene Kommunikation –
insbesondere die Zustellung der Rechnung und Widerrufsinformationen – in
elektronischer Form erfolgen kann. **15. IP-Rechte **Alle Rechte
geistigen Eigentums, z. B. Markenzeichen und Copyrights auf unserem
Online-Shop verbleiben bei Bijou Brigitte modische Accessoires AG, ihren
Niederlassungen oder Lizenznehmern. Die Verwendung unserer Seite oder
der Inhalte des Online-Shops, einschließlich das Kopieren und Speichern
von Inhalten in Teilen oder vollständig ist ohne unsere Erlaubnis nicht
gestattet. Dies gilt nicht, sofern die Verwendung zu persönlichen, nicht
kommerziellen Zwecken erfolgt. **16. Gerichtsstand und anwendbares Recht
Bundesrepublik Deutschland Anwendung. Bei einem beidseitigen Handelskauf
wird als ausschließlicher Gerichtsstand Hamburg und die Anwendung des
Rechts der Bundesrepublik Deutschland unter Ausschluss des UN-Kaufrechts
vereinbart.  **17. Anschrift / Impressum **Adresse: Bijou Brigitte
modische Accessoires AG Online-Shop Poppenbütteler Bogen 1 22399
Hamburg, Deutschland Sitz der Gesellschaft Hamburg Amtsgericht Hamburg
Gödecke (Vorstandsmitglied) Vorsitzender des Aufsichtsrates: Dr.
Friedhelm Steinberg Bei Fragen wenden Sie sich bitte an unseren
Online-Shop-Kundenservice: E-Mail:
Telefon: 040 / 60 609 3434 \* (Mo. - Do. 7:30 Uhr –16:30 Uhr; Fr. 7:30
Uhr –12:00 Uhr) \* Die Kosten hängen von Ihrem
Telekommunikationsdienstanbieter ab und entsprechen den Kosten für ein
Gespräch ins deutsche Festnetz. Die Mobilfunkkosten können ebenfalls je
nach Tarif und Mobilfunkanbieter variieren.  Fax: 040 / 602 64 09
Schriftlich: Bijou Brigitte modische Accessoires AG, Online-Shop,
Poppenbütteler Bogen 1, 22399 Hamburg **18. EHI-Zertifizierung **Unser
Online-Shop trägt das Gütesiegel EHI Geprüfter Online-Shop. Um dieses
Siegel zu erhalten, werden wir regelmäßig von der EHI Retail Institute
GmbH auf die Einhaltung der Kriterien des EHI-Verhaltenskodex überprüft.
Sie können den Verhaltenskodex unter diesem Link einsehen: **19.
Rechtsstreitigkeiten **Der Link zu der Online-Plattform der
EU-Kommission zur außergerichtlichen Online-Streitbeilegung (sog.
OS-Plattform) lautet: Diese Plattform gibt Händlern und Kunden die
Möglichkeit, Streitigkeiten außergerichtlich zu klären. Zur Teilnahme an
einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle
sind wir nicht verpflichtet und nicht bereit. **20. Gültigkeit **Wir
behalten uns das Recht vor, diese Verkaufsbedingungen jederzeit zu
Bestellung im Online-Shop geltenden Verkaufsbedingungen.  Stand
Frankreich | France FR [](/nl/) Niederlande | Nederland NL [](/it/)
Italien | Italia IT [](/es/) Spanien | España ES Aktivieren Sie die
Beschwerdeverfahren
Versandkosten. Swarovski ® ist eine eingetragene Handelsmarke der
Swarovski AG. ©2021 Bijou Brigitte modische Accessoires AG - Online-Shop
erhalten Sie eine E-Mail mit einem Bestätigungslink. Nach erfolgreicher
vollendet und bin berechtigt mich zum Newsletter zu registrieren.
widerrufen. Absenden Datenschutz
verarbeitet werden. Einige davon werden zwingend benötigt, während
Verarbeitung in den USA und der Speicherdauer finden Sie unter „Weitere
Brigitte modische Accessoires AG
Dieser Google Cookie speichert Informationen über Nutzereinstellungen
notwenigen) gesetzt werden dürfen Speicherdauer: Session Name: Session
AG [(Datenschutzbestimmungen)](/datenschutzerklaerung) Funktionale
Ireland Ltd., Gordon House, Barrow Street, Dublin 4, Ireland, Fax: +353
bereitzustellen, Berichte zur Kampagnenleistung zu verbessern oder um zu
Ltd., Gordon House, Barrow Street, Dublin 4, Ireland, Fax: +353 (1)
Ltd., Gordon House, Barrow Street, Dublin 4, Ireland und Google, LLC
Funktion „Dynamic Remarketing“ verwendet. Hierbei handelt es sich um ein
Gordon House, Barrow Street, Dublin 4, Ireland und Google, LLC 1600
attraktiven Angebote aufmerksam zu machen. Wir können in Relation zu den
bestimmte Parameter zur Reichweitenmessung – z.B. Einblendung der
House, Barrow Street, Dublin 4, Ireland und Google, LLC 1600
Amphitheatre Parkway Mountain View, CA 94043, USA Wir verwenden den
eine direkte Verbindung mit dem Server von Facebook auf. Durch die
Speicherdauer: bis zu 24 Monate. Cookie-Informationen: fr / \_fbp /
automatisch geladen werden und eine Nachverfolgung des Nutzerverhaltens
eine direkte Verbindung mit dem Server von Facebook auf. Durch die
Speicherdauer: bis zu 24 Monate. Cookie-Informationen: fr / \_fbp
Budapester Straße 45, 20359 Hamburg. Funktionsweise: Die
und ermittelt, welche Inhalte schließlich zum Kauf unserer Produkte
