dass Inhalte unseres Shops nicht fehlerfrei angezeigt werden können.
Eine Bestellung ist unter Umständen nicht möglich. Bitte bringen Sie
Ihren Browser auf eine aktuelle Version, oder nutzen sie einen anderen.
Sollten Sie Hilfe benötigen oder eine Frage haben, hilft Ihnen unser
Kundenservice unter 0961 - 400 9595 gerne weiter. Menü [WITT WEIDEN]
Stichwort / Artikelnummer Kundenkonto Bestellschein Warenkorb
Tuniken](/blusen)[Homewear](/damen-freizeitmode)[Hosen](/damenhosen)[Jacken](/jacken-damen)[Jeans](/damen-jeanshosen)[Kleider](/kleider)[Mäntel](/maentel-damen)[Pullover](/damen-pullover)[Röcke](/roecke)[Shirts](/damen-shirts)[Shirtjacken](/damen-shirtjacken)[Strickmode](/damen-strickmode)[Tops](/tops)[Westen](/damenwesten)[Bademäntel](/damen-bademaentel)[BHs](/bhs)[Bodies](/damen-einteiler)[Homewear](/damen-homewear)[Nachtwäsche](/damen-nachtwaesche)[Socken
Qualität mit Herz - seit mehr als 110 Jahren
Unternehmens beginnt vor über 110 Jahren in einem kleinen Laden im
oberpfälzischen Reuth b. Erbendorf. Von dort versandte der gelernte
Zimmermann Josef Witt seine ersten Textilwaren, bevor er 1913 ins
benachbarte Weiden zog. Aus dem Ein-Mann-Betrieb des Jahres 1907 ist
heute ein international tätiges Unternehmen mit rund 3.500 Mitarbeitern
geworden. Die Witt-Gruppe ist mit zehn Marken in 10 Ländern aktiv und
einer der führenden europäischen Mode-Versandhändler auf dem wachsenden
Zukunftsmarkt „50 plus“. [ueber uns 1] [ueber uns 2] Zufriedene Kunden -
Tag für Tag -------------------------------- - Bei WITT WEIDEN arbeiten
aktuell rund 3.500 Menschen vom Einkauf der Ware bis zum Versand, davon
sind ca. 75% Frauen. - In Spitzenzeiten werden täglich rund 120.000
Sendungen verschickt. - Wir versenden jährlich Kataloge in einer
dreistelligen Millionen Auflage an rund 21,1 Millionen Kunden weltweit
informieren, setzen wir primär auf unsere Kernkompetenz Katalog und
nutzen natürlich verstärkt die Möglichkeiten, die uns das Internet
bietet. Darüber hinaus bauen wir in Deutschland den stationären
Einzelhandel als unser „Gesicht im Markt“ stetig aus. Dabei verfolgen
wir immer das Ziel, den Einkauf für unsere Kunden über alle Kanäle
hinweg noch schneller und einfacher zu machen. Philosophie unserer Firma
bescheidenen Anfänge und doch ist unser Unternehmen bis heute dem
Erfolgsrezept des Firmengründers treu geblieben: Wie Josef Witt vor über
ziehen sich wie ein roter Faden durch die Geschichte WITT WEIDEN, die
aufs Engste mit der Geschichte des Versandhandels in Deutschland
verwoben ist. Der Schwerpunkt des Sortiments ist tragbare Mode und
Wäsche für die Frau ab 50, ergänzt um Herrenbekleidung und Schuhe. Dabei
zeichnen sich die Produkte durch solide Verarbeitung,
die perfekte Passform und hohen Tragekomfort bieten. [ueber uns 3]
wieder Kunden der Josef Witt GmbH zu ihrer Zufriedenheit mit dem
Kundenservice von WITT WEIDEN befragt worden. In der repräsentativen
Studie wurde u.a. die Zufriedenheit der 1.825 Befragten mit der
Kundenbetreuung von WITT WEIDEN in den Ländern Deutschland, Österreich
und der Schweiz erhoben. Ökologie & Nachhaltigkeit von Anfang an
Qualitätssiegel zeichnen die Kleidung im WITT WEIDEN Sortiment aus, die
während des gesamten Produktionsprozesses zuverlässig gemäß ökologischer
und sozialverträglicher Standards kontrolliert wird. An diesen
können. Hier erhalten Sie mehr Informationen zum Thema Ökologie &
Nachhaltigkeit bei WITT WEIDEN. [ueber uns 5] [ueber uns 6] Qualität bei
WITT WEIDEN ------------------------ Mehr als 110 Jahre Erfahrung in der
Welt der Mode ermöglichen es uns, die bewährt hohe WITT WEIDEN-Qualität
und Kunden stets langlebige und hochwertige Produkte zu bieten, legen
wir größten Wert auf die Auswahl geeigneter Rohstoffe und deren
erstklassiger Verarbeitung in vertrauenswürdigen Produktionsstätten.
Hier erhalten Sie mehr Informationen über Qualität bei WITT WEIDEN.
Schnäppchen - GRATIS Versand Ihre E-Mail-Adresse Anmelden Hinweis zum
Datenschutz nach oben Unser Newsletter Jetzt anmelden und GRATIS-Versand
Image] Chat WITT WEIDEN Über uns Nachhaltigkeit Unsere Filialen WITT
WEIDEN App Qualität bei WITT WEIDEN Service Lieferung & Rückgabe
Bezahlung Häufige Fragen Kataloganforderung Platz schaffen mit Herz
Beratung Größentabelle Damen Größentabelle Herren Größentabelle Schuhe
Individuelle Modeberatung BH-Beratung Figurformer: BODY FIT Kontakt
Kontaktadressen Facebook Bestellservice Rund um die Uhr erreichbar 01805
max. 0,42 Euro/Minute E-Mail: kundenservice@witt-weiden.de
Bestellservice Rund um die Uhr erreichbar 01805 - 21 21 00 *
Euro/Minute E-Mail: kundenservice@witt-weiden.de WITT WEIDEN Über
unsNachhaltigkeitUnsere FilialenWITT WEIDEN AppQualität bei WITT WEIDEN
FragenKataloganforderungPlatz schaffen mit Herz Beratung Größentabelle
KontaktadressenFacebook © 2021 WITT WEIDEN AGB Datenschutz Impressum
