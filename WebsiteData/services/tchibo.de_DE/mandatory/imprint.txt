Verantwortlich
Tchibo GmbH
Tel.: +49 (0) 40 63 87 - 0
www.tchibo.de
KONTAKT TCHIBO BESTELLSERVICE
E-Mail: service@tchibo.de
Kontaktformular
Geschäftsführung: Thomas Linemayr (Vorsitzender), Ulf Brettschneider,
Dr. Jens Köppen Vorsitzender des Aufsichtsrats: Michael Herz
Amtsgericht Hamburg, HRB 43618
Umsatzsteuer-Identifikationsnummer: DE 811164447
Die EU-Kommission stellt eine Plattform für außergerichtliche
Streitschlichtung bereit. Verbrauchern gibt dies die Möglichkeit,
Streitigkeiten im Zusammenhang mit Ihrer Online-Bestellung zunächst ohne
die Einschaltung eines Gerichts zu klären. Die
Streitbeilegungs-Plattform ist unter dem externen Link
http://ec.europa.eu/consumers/odr/ erreichbar.
In diesem Zusammenhang sind wir verpflichtet, Sie auf unsere
E-Mailadresse service@tchibo.de hinzuweisen.
Wir sind immer bemüht, eventuelle Meinungsverschiedenheiten aus unserem
Vertrag einvernehmlich beizulegen. Darüber hinaus haben wir uns
entschieden, nicht an einem Schlichtungsverfahren teilzunehmen. Hierzu
sind wir auch nicht verpflichtet.
Haftungs-Ausschluss
Anbieter. Die Tchibo GmbH übernimmt keine Gewähr oder Haftung für
eventuell rechtswidrige Inhalte oder sonstige Rechtsverletzungen auf
Dieses Impressum gilt auch für unseren Unternehmensauftritt bei
Facebook, Instagram, Kununu, LinkedIn, Pinterest, Twitter, Youtube und
Einfache & sichere Zahlung
Lieferung & Retoure
Filialabholung
Kundenservice
TchiboCard
Weitere Services
Tel.: 0800 - 300 01 11 (kostenfrei)
TreueBohnen sammeln und exklusive Vorteile sichern.
Sie finden uns auch auf
Deutschland Schweiz (deutsch) Suisse (français) Polska Slovensko Česká
republika Magyarország Österreich Türkiye
Unsere beliebtesten Kategorien
Alle Preise verstehen sich inkl. gesetzlicher Mehrwertsteuer und zzgl.
Versandkosten .
