BusinessEdition - Das neue VR OnlineBanking für Firmenkunden und Vereine
Menü ---- - Frankfurter Volksbank - Baufinanzierung -
Vermögensmanagement - Privatkunden - Geschäftskunden - FVB-Online - Wir
Berichte - Tochtergesellschaften - Engagement - Unternehmensleitbild -
Ansprechpartner - Was wir anders machen - Karriere - Aktuelles -
Filialen - FVB-FutureForum - Jugendwebsite "move" - Presse 1. Wir für
Sie 2. Profil 3. **Übersicht** Herzlich Willkommen bei der Frankfurter
Volksbank ================================================= In der über
geändert. Eines aber ist immer gleich geblieben: Der Kunde steht für uns
möchten, dass Sie uns näher kennenlernen. Am besten besuchen Sie uns
dazu natürlich persönlich in einer unserer Filialen. Für einen ersten
Eindruck können Sie sich aber auch hier informieren. Zum Beispiel, was
uns ausmacht und was uns auszeichnet. Hier erfahren Sie auch, wie wir
uns engagieren, wie Sie bei uns Karriere machen und warum es sich lohnt,
bei uns eine Ausbildung zu starten. Immer anders und doch gleich
Tradition: Genauer gesagt wurden wir vor über 155 Jahren gegründet. Ein
wesentlicher Teil dieser Tradition ist es, immer etwas anders zu denken
und zu sein als andere Banken. Wir glauben zum Beispiel, dass Menschen,
die über Geld reden, einander persönlich kennen sollten. Aus diesem
Grund sind wir im Raum Frankfurt /Rhein-Main mit über 150 Standorte
vertreten – [bestimmt auch in Ihrer
Nähe](https://www.frankfurter-volksbank.de/wir-fuer-sie/filialen.html).
mit Frankfurt/Rhein-Main verbunden – und damit mit der Region, in der
wir leben und arbeiten. Zudem betrachten wir uns als Bank für den
Mittelstand, die aber auch globale Herausforderungen meistert. ###
Solides Wirtschaften nach dem genossenschaftlichen Prinzip Nicht zuletzt
wirtschaften wir aus unserer Tradition heraus solide und nach dem
genossenschaftlichen Prinzip. Das gilt auch und insbesondere in Zeiten
der Globalisierung. Dabei sind wir immer geleitet von dem Bewusstsein,
dass wir unseren Mitgliedern gehören und diesen verpflichtet sind. ###
Wir wählen nur Anlagen, die wir auch verstehen Und dabei wollen wir auch
Sie verstehen: Wir legen viel Wert auf eine persönliche Beratung, die
Ihre individuellen Ansprüche und Bedürfnisse berücksichtigt. Was auch
immer gerade Ihr Leben bewegt: Wir empfehlen Ihnen die passende
Finanzlösung dazu. Für unsere Beratung wurden wir übrigens bereits
mehrfach von unabhängigen Institutionen ausgezeichnet. ### Beratung, die
uns auszeichnet Seit unserer Gründung haben wir uns im Kundeninteresse
mit anderen Volks- und Raiffeisenbanken aus unserer Region und unserem
gemeinsamen Geschäftsgebiet zusammengeschlossen. Und wir sind inzwischen
die mitgliederstärkste Volksbank Deutschlands. Dabei wollten wir aber
nie eine besonders große Bank sein – sondern lieber eine besonders gute.
Das erreichen wir vor allem durch zufriedene Mitarbeiter, die Freude an
ihrer Arbeit haben, die das Geschäft unserer Kunden kennen und diese
Erfolgs. Doch die Zahlen allein zeigen nicht immer alles. Wir leisten
für unsere Region mehr als Zahlen ausdrücken können.   \* inklusive
Sonderbedingungen](https://www.frankfurter-volksbank.de/service/agb.html)
Nachricht Wir freuen uns auf Ihre Nachricht ### Service - Karte sperren
Wertpapierdepot - MeinVermögen - Sofortkredit - Dispokredit ###
Baufinanzierung - Tipps zur Finanzierung - KfW-Förderung - Bausparen -
Wohn-Riester - Immobiliensuche - Immobilien GmbH ### Über uns - Profil -
Kontakt - [[]](https://www.facebook.com/frankfurtervolksbank/) -
Wir freuen uns auf Ihren Anruf 069 2172-0 Wir rufen Sie gerne zurück
Ihre Nachricht an uns Termin vereinbaren Jetzt Info-Chat starten Ihr
Kontakt per WhatsApp zu uns promo kontakt Filialen (1)
