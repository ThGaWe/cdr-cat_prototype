Bike Accessories
Sale %
1.  Privacy Policy
PRIVACY POLICY
The following privacy policy contains information on the nature and
extent to which your personal data is processed while using our online
shop. The EU General Data Protection Regulation (GDPR) serves as the
main legal foundation for this privacy policy.
Collection, Processing, and Use of your Data
You can visit our website without providing personal information. We
only save access data with no personal connection, such as the name of
your internet service provider, the page from which you visited us, or
the name of the requested data file. This data is processed exclusively
for the purpose of improving our services and cannot be traced back to
you.
Personal data, such as your first/last name, address, birthdate,
telephone number, and email address, is only collected when you provide
such data voluntarily when placing an order, opening a customer account
or registering for our newsletter. Your data will only be used without
your express permission for processing your order and for distributing
our catalogues/mailings. When you register for our newsletter, we will
use your email address for advertising purposes until you unsubscribe
from the newsletter.
We will store your data only as necessary for the purposes of the
transaction and as permitted under data protection regulations (EU
GDPR).
Transfer of Personal Data
We process your personal data for order processing purposes and transfer
the information required for this purpose to other service providers.
So that your shipment of your order can be tracked, we use the services
of PAQATO GmbH, Johann-Krane-Weg 6, 48149 Münster (“Paqato”). Paqato
sends shipping notifications and status updates to the shipment in our
name. In accordance with Art. 6 section 1 f) of the EU GDPR, after we
ship a package we pass on customer data (mailing address, first and last
name and address) and the tracking number to Paqato in accordance with
our justified interest in effective and informative customer
communication as well as the customer’s interest in transparent and
reliable shipping processes. This data will not be passed on to
third-parties by Paqato and will be used exclusively for the purposes
named above. Paqato’s data protection policies can be read here:
https://www.paqato.com/en/datenschutzerklaerung/.
If you are a resident in Germany and request a purchase via invoice,
direct debit or financing, we will request a credit check on the basis
of statistical methods from infoscore Consumer Data GmbH, Rheinstr. 99,
76532 Baden-Baden, Germany, and if necessary from Creditreform
Boniversum GmbH, Hellersbergstraße 11, 41460 Neuss, Germany
legitimate interests. We transfer all personal data required for the
purpose of this creditworthiness check to the Creditreform Boniversum
GmbH, 41460 Neuss, Germany, and use the information we receive
concerning the statistical likelihood of non-payment as the basis for
deciding whether to enter a contractual relationship. The credit
information may contain score values which are calculated on the basis
of approved statistical methods and in the calculation of which address
data etc. is also involved. For detailed information about the way
infoscore Consumer Data GmbH processes data according to Art. 14 of the
General Data Protection Regulation see here:
https://finance.arvato.com/icdinfoblatt, for Creditreform Boniversum
GmbH see: https://www.boniversum.de/eu-dsgvo/ .
Your legitimate interests with regard to data protection are taken into
account at all times in accordance with legal provisions. If your data
is processed by our service partners who help us provide a high level of
customer service and delivery, the scope of the data transmitted for
this purpose shall be restricted to the necessary minimum. In case we
provide your data to contractual partners or cooperation partners
regards to order processing, prize draws or partner offers, we will
inform you accordingly. Our contractual partners and cooperation
partners have been carefully chosen and have committed themselves to
confidentiality in accordance with the legal provisions of Art. 28 of
the EU GDPR, as well as to compliance with our own data protection
standards. In particular, our contractual partners and cooperation
partners are not permitted to pass the data of our customers on to third
parties for advertising purposes. Our contractual partners and
cooperation partners may only use the data provided to fulfil their
function to process your order.
Fraud prevention and Abuse Detection Measures
In order to secure the ordering process against fraudulent and/or
abusive behaviour, we automatically check during the ordering process
whether there are any anomalies in the specific order for the contract.
For this reason, the 1) data for the execution of the contract (e.g.
object of purchase, name, postal address, email address, delivery
address, payment method and bank information) and 2) usage data of the
and scope of the visited websites as well as click paths) together with
a cookie and/or a visitor ID, each of which may contain anonymous data
about the end devices used when visiting the websites (e.g. the screen
resolution or the operating system version) and which has some
probability of being recognised via the end devices on future visits,
are processed by this ROSE BIKES online shop with the purpose of
enabling your user account to be used in the future. The ROSE BIKES
online shop processes this data for the purpose of managing your user
account, the websites that you visit and the services that you use on
the takeover of user accounts, the automated creation of fake user
accounts by bots, the use of stolen identities or payment data or
incorrect ratings for services), for product optimisation and further
development, or against misuse (e.g. through attacks on the IT
infrastructure, "man-in-the-middle" attacks, brute force attacks or the
use of malware) on the basis of legitimate interest pursuant to Art. 6
Section 1 f) of the EU GDPR in conjunction with Recital 47. The ROSE
BIKES online shop also transmits the previously named data to the Device
Transaction Pool (DTP) and stores it there. The purpose of the DTP is to
protect the member companies participating in the DTP from abuse and
from bad debts due to fraud, which can occur while providing commercial,
remunerated telecommunications services or telemedia services to
contract partners who are unwilling or unable to pay, especially due to
fraud. In the case of a request from a member company to the DTP, only
the results of the suspicion check on the request are transmitted to
this member company. Positive data can also be used, meaning, for
example, that an end device used to make frequent and punctual payments
can be rated positively. Results data for individual member companies,
beyond the specific case of an particular use, are not stored. The DTP
is operated by infoscore Profile Tracking GmbH (IPT), Kaistraße 7, 40221
Düsseldorf, Germany as the data processing company of the member
company. The data is automatically deleted after five months. The ROSE
BIKES online shop has contracted infoscore Tracking Solutions GmbH,
Kaistraße 7, 40221 Düsseldorf, Germany with conducting the fraud
prevention and abuse detection measures in accordance with Art. 28 of
the EU GDPR. Recipients of the data are exclusively contractual partners
of the ROSE BIKES online shop. In this case, the recipients are
infoscore Tracking Solutions GmbH, Kaistraße 7, 40221 Düsseldorf,
Germany; infoscore Profile Tracking GmbH, Kaistraße 7, 40221 Düsseldorf,
Germany; infoscore Tracking Technology GmbH, Kaistraße 7, 40221
Düsseldorf, Germany; as well as data centre service providers that are
tasked with storing the data. If fraud or misuse is suspected, a ROSE
BIKES employee examines the results and the evidence on which they are
based. If a contract is declined, this will be communicated to you and
also, if requested, the principal reasons for this decision. You then
have the opportunity to make your case by contacting [email protected],
whereupon a ROSE BIKES employee will reexamine the decision.
Your personal data is encoded during the online data transfer using SSL,
SHA-256 with RSA encryption, RSA with 2048 bit exchange by Symantec
Corporation. Our website and other systems are protected by technical
and organisational measures to prevent loss, damage, access,
modification or distribution of your data by unauthorised persons. Your
customer account can only be accessed with your personal password. You
should always treat your access data as confidential and close the
browser window as soon as you have finished communicating with us,
particularly if you share your computer with other persons. Please use
your access data for one customer account only and do not use the same
password multiple times.
We use cookies on various pages to make your visit to our website more
enjoyable and to facilitate the use of certain functions. These cookies
are small text files that are deposited on your computer. Most of the
cookies we use are deleted from your hard disk after the end of the
browser session (session cookies). Other cookies remain on your computer
and enable us to recognise your computer the next time you visit our
website (permanent cookies). Our affiliated companies are not permitted
to collect, process or use personal data via cookies through our
website. You can change the settings of your browser so that you are
informed about cookie settings and can individually choose whether or
not to accept cookies for specific circumstances or generally not accept
them. If cookies are not accepted it can lead to restrictions in the
Right of Access
According to the EU GDPR, you have the right to request information on
your personal data free of charge. In addition, you have the right to
rectification, restriction of processing or erasure of such data. ■
Right of access acc. Art. 15 EU GDPR ■ Right to rectification of
inaccurate personal data and completion acc. Art. 16 EU GDPR ■ Right to
erasure acc. Art. 17 EU GDPR ■ Right to restriction of processing acc.
Art. 18 EU GDPR ■ Right to data portability pursuant to Art. 20 EU GDPR
Email Newsletter or WOWDEALS Newsletter
With our free newsletter you can stay-up-to-date on our latest offers,
customer events, prize draws and trade shows.
We use the ‘double opt-in’ process for newsletter subscription, which
means that we will only send you a newsletter, if you click the
confirmation link in the confirmation email to confirm your
subscription. Please note that we need your email address for
subscription. You will only receive a personalised newsletter, if you
share your personal data when registering as a new customer. We only ask
for your title and name to personally address you in the newsletter. You
can unsubscribe at any time. You can find a respective unsubscribe link
in every newsletter email. Alternatively, you can get in touch with us
using the provided contact data.
By confirming your newsletter subscription, you also agree to the
analysis of your newsletter usage behaviour. For the analysis of such
usage data, our emails have an embedded tracking pixel to track open
rates. A tracking pixel is an image file measuring one pixel by one
pixel and creating a link to our website to allow us to analyse data on
the usage of the newsletter. Therefore, we use the data entered for
newsletter subscription as well as the tracking pixels that are assigned
to your email address and linked to a unique ID. This data is combined
with data about your use of our website. To make sure the newsletter is
displayed correctly, we collect information about the type of device you
use to open the newsletter. Based on the links you click and the open
rates of the newsletter emails, we can determine which topics you are
interested in. The data collected serves to create personal user
profiles. In this way, we try to continuously improve our newsletter and
to provide you with more individual topics about ROSE Bikes.
The information collected is stored by the newsletter provider Inxmail
server in Germany. The tracking mechanism is not supported when images
are disabled in your emails by default. However in this case, the
newsletter is not displayed correctly and you may not be able to use all
functions. If you manually click on ‘display images’, tracking is
supported, unless you have objected to the analysis of usage data. The
legal ground for processing personal data is your consent in accordance
with Art. 6 Section 1 a) of the EU GDPR. You may object to the analysis
of your newsletter usage data in writing at any time. Simply use the
contact information provided.
For our newsletter, we use software from epoq internet services GmbH, Am
from epoq, we are able to offer you targeted and individual product
recommendations within the scope of our newsletter. The product
recommendations are displayed on the basis of an analysis of previous
and current click and purchase behaviour. Provided the information
collected is personally identifiable, it can be processed in accordance
with Art. 6 Section 1 f) of the EU GDPR based on our justified interest
in displaying personalized advertising and conducting market analysis.
You can object to this advertisement at any time in the newsletter by
clicking on the opt-out link or by sending us a message. As a result of
the opt-out, individual product recommendations will no longer be
displayed.
Advertising Mail
Our customers automatically receive our customer magazine and further
offer mailings by post. You may unsubscribe from receiving advertising
mail or magazines by phone or in writing. The legal ground for
processing personal data is your consent in accordance with Art. 6
Section 1 f) of the EU GDPR.
To unsubscribe (from receiving newsletters, magazines or offer mailings)
you can also contact: ROSE Bikes GmbH, 46393 Bocholt or
Revocation Of Consent
It is likely that you gave the following consent when you opened your
customer account. We want to draw your attention to the fact that you
may withdraw your consent at any time.
regular basis. My email address will not be passed on to other
companies. I may withdraw this consent to my email address being used
for advertising purposes at any time, e.g. via email to
Product Reviews / Comments
Once you’ve submitted your order, you will receive an email asking you
to write a product review (please note that only reviews related to the
product can be published). You will receive this email regardless of
whether you have subscribed to our newsletter or not. Please note that
these emails comply strictly with the legal regulations of the
Protection Against Unfair Competition Act (UC). We will use the provided
email address to promote own products you’ve already bought from us. By
writing a review or leaving a comment, the data is transferred back to
us and stored. We may use your email address to assign a product review,
contact you for verification or to react to complaints.
The product review is published with your first name and the first
letter of your last name. By submitting a review and/or a comment you
grant ROSE BIKES GmbH a non-exclusive, royalty-free, perpetual and
irrevocable right to use, reproduce, modify, adapt, translate,
distribute, publish, create derivative works from and publicly display
such review and/or comment on- or offline. This means, for example, that
ROSE BIKES may publish the comment and use the review for
advertising/marketing purposes. Please note that all product reviews are
analysed to make sure we can offer you new and even better products.
CONTACT PERSON FOR DATA SECURITY
If you have questions concerning the collection, processing or use of
your personal data, or if you want to provide information, correct,
block, delete data or revoke consent, please contact our data protection
officer: Anke Bösing, ROSE Bikes GmbH, Schersweide 4, 46395 Bocholt,
phone: 02871-275533, email: [email protected].
RESPONSIBLE FOR PROCESSING
ROSE Bikes GmbH
Schersweide 4
46395 Bocholt
Deutschland
Management: Erwin Rose, Stefanie Rose, Thorsten Heckrath-Rose, Marcus
Diekmann.
Right Of Appeal to the Competent Authority
You have the right to appeal to the supervisory authority/authorities;
e.g. to the
North Rhine-Westphalia Commissioner for Data Protection and Freedom of
Information
PO Box 20 04 44
40102 Düsseldorf Germany
Phone: 0211/38424-0
Fax: 0211/38424-10
Email: [email protected]
Terms and Conditions
For €200 minimum purchase value without return. More details:
rosebikes.com/privacy-policy
Your email address
Subscribe Subscribed
PAYMENT OPTIONS
More information
DISPATCH TYPES
Legal
