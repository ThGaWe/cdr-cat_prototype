Information on delivery times
LEGAL NOTICE ROSE BIKES GMBH
ROSE Bikes GmbH
Schersweide 4
Germany
Phone: +49 2871  2755-55
Email: [email protected]
Our e-mail addresses and telephone numbers
Customer Service Centre opening hours:
Mon.-Fri.: 9.00am - 1.00pm
View the full "Contact Us" page here
Registered office: Bocholt 
Commercial Register: HRB 8315, District Court Coesfeld 
Management: Erwin Rose, Stefanie Rose, Thorsten Heckrath-Rose, Marcus
Diekmann
VAT No.: DE 811 440 544 
WEEE-Reg.-Nr. :
DE 26905420, NL WEEENL0592, BE 454237, DK DE811440544, FR M2976, GB
WEE/BU3548YD, AT GLN 9008391794660, SE 556552-7487
Organic Production: DE-ÖKO-039
Concept and design: ROSE Bikes GmbH
Arbitration
Online dispute resolution according to the Regulation on consumer ODR
Art. 14 Section 1: The European commission offers a platform for online
dispute resolution (OS), see https://ec.europa.eu/consumers/odr.
We always endeavour to amicably resolve any disagreements arising from
our contract. Furthermore, we have decided to not take part in any
arbitration proceedings, as we are not obliged to do so. Our email
address is: [email protected]
LIABILITY NOTE
This website contains external links (hyperlinks) to third-party
websites. The submitting third parties are solely responsible for the
content of these websites. Before creating links, ROSE Bikes checks the
contents of third-party websites for any infringement. Yet, ROSE Bikes
has no influence on the contents of the linked pages and expressly
accepts no liability for the contents of those websites. ROSE Bikes will
check the contents of the linked pages regularly and remove all links to
websites containing any infringement immediately.
SUBSCRIBE TO OUR NEWSLETTER
For €200 minimum purchase value without return. More details:
rosebikes.com/privacy-policy
Your email address
Subscribe Subscribed
PAYMENT OPTIONS
More information
DISPATCH TYPES
Legal
