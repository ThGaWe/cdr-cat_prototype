und Bestellungen
Registrieren
Was suchen Sie?
Wählen Sie einen Markt
und Bestellungen
Registrieren
Was suchen Sie?
IMPRESSUM
Verantwortlich für den Betrieb von www.mediamarkt.de und von Media Markt
MMS E-Commerce GmbH
Eingetragen im Handelsregister des Amtsgerichts Ingolstadt unter HRB
USt ID: DE233830101
RETOURNIEREN SIE BITTE KEINE WAREN AN DIESE ADRESSE.
Informationen zur Retoure finden Sie unter diesem Link:
GERNE KÖNNEN SIE SICH MIT UNS IN VERBINDUNG SETZEN:
Mo.−Sa.: 08:00−21:00 Uhr
So./Feiertag: 09:00−18:00 Uhr
Telefon: 0221/22243-333 (Kosten zum Festnetztarif)
E-Mail: [email protected]
Zum Kontaktformular
Jugendschutzbeauftragter ist Herr Joo Straßburger. Für Fragen zum
Jugendschutz kontaktieren Sie uns gern per Mail.
Informationen zur Online-Streitbeilegung: Die EU-Kommission hat eine
Internetplattform zur Online-Beilegung von Streitigkeiten geschaffen.
Die Plattform dient als Anlaufstelle zur außergerichtlichen Beilegung
von Streitigkeiten betreffend vertragliche Verpflichtungen, die aus
Online-Kaufverträgen erwachsen. Der Kunde kann die Plattform unter dem
folgenden Link erreichen: https://ec.europa.eu/consumers/odr
Wir sind weder bereit noch verpflichtet, an Streitbeilegungsverfahren
vor Verbraucherschlichtungsstellen teilzunehmen.
Verantwortlich für den Betrieb des Marktplatzes bzw. Marketplace auf
www.mediamarkt.de:
MediaMarktSaturn Plattform Services GmbH
Luise-Ullrich-Str.4
Bitte an diese Adresse keine Waren retournieren. Informationen zur
Retoure finden Sie hier.
Die MediaMarktSaturn Plattform Services GmbH vertreibt keine Produkte
und ist im Rahmen von Verträgen, die über den Marktplatz geschlossen
werden, nicht Vertragspartner. Vertragspartner eines über den Marktplatz
verkauften Produkts ist der jeweilige Verkäufer. Wenden Sie sich bitte
an den jeweiligen Verkäufer, wenn Sie Fragen zu einem Produkt haben, mit
diesem unzufrieden sind oder ein sonstiges Problem mit einem
Marktplatz-Produkt haben.
Geschäftsführer: Dr. Oliver Niggemeier, Marius Lückemeyer, Thomas Nugel
Eingetragen im Handelsregister des Amtsgerichts München unter HRB 256088
USt ID: DE329404887
E-Mail: [email protected]
Kontaktformular
Erlaubnis nach § 34c Absatz 1 Satz 1 Nummer 2 Gewerbeordnung
Verträgen im Sinne des § 34i Absatz 1 Satz 1 Gewerbeordnung, oder
Nachweis der Gelegenheit zum Abschluss solcher Verträge)
Aufsichtsbehörde: Industrie- und Handelskammer für München und
Oberbayern, Max-Joseph-Straße 2, 80333 München
Inhaltlich Verantwortlich: Dr. Oliver Niggemeier, Luise-Ullrich-Str. 4,
Jugendschutzbeauftragter ist Herr Joo Straßburger. Für Fragen zum
Jugendschutz kontaktieren Sie uns gern per Mail.
Informationen zur Online-Streitbeilegung: Die EU-Kommission hat eine
Internetplattform zur Online-Beilegung von Streitigkeiten geschaffen.
Die Plattform dient als Anlaufstelle zur außergerichtlichen Beilegung
von Streitigkeiten betreffend vertragliche Verpflichtungen, die aus
Online-Kaufverträgen erwachsen. Der Kunde kann die Plattform unter dem
folgenden Link erreichen: https://ec.europa.eu/consumers/odr
Wir sind weder bereit noch verpflichtet, an Streitbeilegungsverfahren
vor Verbraucherschlichtungsstellen teilzunehmen.
Unsere Versandpartner
Einfach bezahlen ****
finden Sie hier
Schreiben Sie uns
Kontaktformular
Aktuell beliebt
Alle Preise in Euro und inkl. der gesetzlichen Mehrwertsteuer, zzgl.
Versandkosten. Verkauf nur an private Endkunden. Abgabe nur in
haushaltsüblichen Mengen. Änderungen und Irrtümer vorbehalten.
Abbildungen ähnlich, alle Angebote ohne Dekoration. Angebot gültig auf
mediamarkt.de, nur solange der Vorrat reicht. Liefergebiet: Deutschland.
Produkte. Wir liefern Ihnen Ihre Bestellung ab einem Warenkorbwert von
Speditionslieferung und FSK18 Artikel). Die Versandkosten werden
automatisch im Warenkorb (ab einem Wert von 59 €, berücksichtigt werden
nur direkt von MediaMarkt angebotene Produkte) abgezogen. Großgeräte per
Speditionslieferung sind Haushaltsgroßgeräte und TVs ab 42 Zoll. Die
Versandkosten betragen 29,90€.
Mindestrate € 10.-, Laufzeit 6 bis 10 Monate. Erst- und Schlussrate kann
abweichen. Bonität vorausgesetzt. Bei der BNP Paribas erfolgt die
Finanzierung über einen Kreditrahmen mit Mastercard®. Für diesen gilt
ergänzend: Nettodarlehensbetrag bonitätsabhängig bis € 10.000.-.
Vertragslaufzeit auf unbestimmte Zeit. Gebundener Sollzinssatz von 0 %
gilt nur für Verfügungen beim kreditvermittelnden Händler, für diesen
Einkauf zeitlich befristet für die oben angegebene Zeit ab
Vertragsschluss. Danach und für alle weiteren Verfügungen beträgt der
veränderliche Sollzinssatz (jährlich) 14,84 % (15,90 % effektiver
Jahreszinssatz). Höhe und Anzahl der monatlichen Raten können sich
verändern, wenn weitere Verfügungen über den Kreditrahmen vorgenommen
werden. Vermittlung erfolgt ausschließlich für unsere
Finanzierungspartner: Santander Consumer Bank AG, Santander-Platz 1,
Schwanthalerstr. 31, 80336 München • Hanseatic Bank GmbH & Co KG,
Bramfelder Chaussee 101, 22177 Hamburg. Finanzierungspartner
marktabhängig. Der Partner für den Onlineshop der MMS E-Commerce GmbH
ist BNP Paribas S.A. Niederlassung Deutschland, Standort München:
Schwanthalerstr. 31, 80336 München
jeweils weitere 12 Monate, wenn nicht mit einer Frist von 3 Monaten zum
Ende der Mindestvertragslaufzeit bzw. eines jeden Verlängerungszeitraums
gekündigt wird. Ab dem 36. Monat ist der Vertrag monatlich kündbar.
Einmalige Beitragsrückerstattung für Schadensfreiheit nach einer
Laufzeit von 36 Monaten ab Vertragsbeginn – max. € 40.-, min. € 10.-.
Neben der monatlichen Zahlungsvariante besteht auch die Möglichkeit
einer Einmalzahlung für 36 Monate.
mediamarkt.de für entweder den Online- oder den Club-Newsletter erhalten
Sie einen Willkommens-Coupon über 10 €. Nicht bei Newsletter Anmeldung
nach Anklicken der Bestätigungs-Mail zum Newsletter (Double-Opt-In) und
sofern bisher noch kein Newsletter-Versand  an die angegebene
Mailadresse erfolgte (auch, wenn zwischenzeitlich eine Abmeldung vom
Newsletter erfolgte). Der Coupon-Code ist ab Zustellung 2 Wochen lang
ausschließlich in unserem Onlineshop unter mediamarkt.de (nicht
einlösbar bei Drittanbietern auf mediamarkt.de sowie in stationären
Media Märkten vor Ort) einlösbar. Der Coupon Code ist einlösbar ab einem
Bestellwert von 100 €. Der Coupon-Code ist nicht einlösbar für: Verträge
mit Drittanbietern, sämtliche Download-/Content-/GamingCards,
Gutscheinkarten oder –boxen, Prepaid-Aufladekarten/-Services,
E-Books/Bücher, digitale Guthabenkarten, Zusatzgarantien,
Lieferservices, Reparaturdienstleistungen, Fotoservices, Lebensmittel,
Tchibo Cafissimo Produkte, IQOS, E-Zigaretten, Heets, Liquids und
Versandkosten. Coupon nicht kombinierbar mit anderen Aktionen und bei
Online-Käufen nicht mit anderen Coupons.
Produktkategorie in einem der teilnehmenden Märkte, (Online-Bestellungen
mit Marktabholung ausgeschlossen) erhalten Sie einen Direktabzug auf den
jeweils ausgewiesenen Verkaufspreis wie folgt: 5 % auf Artikel der Marke
Drucker, Drohnen, E Bikes. 10 % auf PC, Notebook, Computer-Bildschirme,
TV-Receiver, Bluray- DVD & Videoplayer, Hifi-Komponenten, Multiroom - &
Kopfhörer, Foto- &Videokamera, Beamer , Beamer Leinwände, Klimageräte,
Luftreiniger, Ventilatoren, Smart Home, Hifi Lautsprecher, E Scooter,
und Öfen, Staubsauger, Kaffee Vollautomaten, Toaster, Wasserkocher,
Rasierer, Epilierer, elektr. Zahnbürsten, Festnetz Telefone,
Tablets und Wearables der Marke Samsung und auf Produkte aus dem Bereich
Zubehör (für z.B. Tablet, Fernseher, SAT, Konsolen, Mobilfunk,
Computer). 30% auf CD, DVD, Bluray, Vinyl-Schallplatten. Aktion nicht
gültig für Produkte der Hersteller Apple und Dyson. Ausgenommen sind:
Bücher, Vorbesteller-Artikel, jede Art von
Download-/Content-/Gaming-Cards, digitale Guthabenkarten, Tchibo
Caffissimo und Nespresso Kapseln. Aktion nur in den teilnehmenden
Märkten vor Ort gültig und nicht im Onlineshop verfügbar. Der Abzug
erfolgt direkt an der Kasse. Nur solange der Vorrat reicht. Abgabe nur
in haushaltüblichen Mengen. Aktion nur gültig für private
Endverbraucher. Keine Barauszahlung. Nicht kombinierbar mit anderen
Aktionen.  Alle Informationen zu den teilnehmenden Märkten sowie zur
Aktionsdauer in den Märkten erhalten Sie hier über die Marktsuche.
Mit dem Kauf bei einem Marktplatz Verkäufer akzeptieren Sie dessen AGB.
Weitere Informationen zum Verkäufer erhalten Sie, indem Sie auf dessen
Verkäufernamen klicken.
