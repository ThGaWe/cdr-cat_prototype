Newsroom - Jobs Jobs - Partner Partner OTTO Logo Menü öffnen/ schließen
Historie - Unser Campus - Auf einen Blick - Was uns bewegt Was uns
bewegt {{item.title}} Secondary Open {{item.title}} Secondary Close -
Wer wir sind {{item.title}} Secondary Open {{item.title}} Secondary
Unser Campus - Auf einen Blick - Was uns bewegt Was uns bewegt
Nachhaltigkeit ============== Seit über 30 Jahren ist nachhaltiges
Wirtschaften als Unternehmensziel in unserer Unternehmenskultur
verankert
Wir alle sorgen uns um unsere Welt: Klimawandel, Plastik-Verschmutzung,
schlechte Arbeitsbedingungen – es gibt vieles, was wir anpacken müssen.
Als Unternehmen zeigen wir Verantwortung und möchten einen nachhaltigen
Beitrag leisten 1. Home 2. Was uns bewegt Unser Ziel ist es, entlang der
Wertschöpfungskette die negativen Auswirkungen auf Mensch und Natur
wirksam und effizient zu verringern. Um dies zu erreichen, haben wir
eine Corporate Responsibility (CR)-Strategie entwickelt, die für alle
Konzernunternehmen der Otto Group gilt. Die tiefe Verwurzelung unseres
Selbstverständnisses als verantwortlich handelnde Unternehmensgruppe
ermöglicht uns eine nachhaltige Ausrichtung. Natürlich wollen unsere
Konsument\*innen wissen, unter welchen Bedingungen zum Beispiel ihre
Kleidung und ihre Möbel hergestellt werden, aus welchen Materialien sie
bestehen und wie diese erzeugt werden. Transparente Information zum
Nachhaltigkeitsengagement ist OTTO sehr wichtig. Nachhaltige Baumwolle 
expand Nachhaltige Baumwolle ist für OTTO, als Deutschlands größtem
Onlinehändler für Fashion und Lifestyle, eine der zentralen Ressourcen.
Auch deshalb ist eine nachhaltige Erwirtschaftung wichtig für uns und
wir wollen den Einsatz nachhaltiger Baumwolle weiter ausbauen. Dabei
fokussieren wir uns hauptsächlich auf Baumwolle der
Africa. ### Unser Nachhaltigkeitsziel bis Ende 2020 Schon heute stammen
Kleinbäuer\*innen der CmiA-Initiative. Und wir befinden uns auf der
Zielgeraden – bis Ende 2020 sollen es 100 Prozent sein. ### Cotton made
in Africa (CmiA) Die Initiative hat das Ziel, die Arbeits- und
Lebensbedingungen der Baumwollbauer\*innen in Subsahara Afrika
nachhaltig zu verbessern und zudem einen relevanten Beitrag zum
Umweltschutz zu leisten. Die CmiA-Initiative fördert ausschließlich
afrikanische Kleinbäuer\*innen, um deren Arbeits- und Lebensbedingungen
nachhaltig zu verbessern. Kinder- und Zwangsarbeit sind strengstens
verboten, es gibt feste Arbeitszeiten und ein angemessenes Gehalt –
unabhängig vom Geschlecht. Zudem gibt es regelmäßige Schulungen, in
denen die Bäuer\*innen lernen, wie sie ihre Baumwolle umweltschonend und
ertragsfördernd anbauen können. Ein aktives Mitspracherecht gilt dabei
zu jeder Zeit. Das
von Cotton made in Africa beruht auf dem Prinzip „Hilfe zur
Selbsthilfe”. In den Anbauländern werden Projekte zur Stärkung der
Frauenrechte, Bildungsprojekte oder Hygieneprogramme umgesetzt. ### Die
Umwelt freut sich Konventionell angebaute Baumwolle ist nicht nur wegen
des hohen Wasserverbrauchs problematisch für die Umwelt, sondern auch
für die Kleinbäuer\*innen durch den Einsatz von Chemikalien. Daher setzt
sich die CmiA-Initiative für umweltschonende Agrarmethoden ohne
künstliche Bewässerung und Mono-Kulturen ein. Das spart 20 Prozent
Wasser und erzeugt 40 Prozent weniger Treibhausgase. Außerdem gibt es
strenge Regeln, welche Pestizide genutzt werden dürfen. Gentechnisch
verändertes Saatgut ist sogar komplett verboten. Cotton Made in Africa
FSC©-Holz  expand FSC® ist eine globale Gemeinschaft, die einen Standard
für eine verantwortungsvolle Waldbewirtschaftung setzt. Weltweit gelten
zehn einheitliche Grundsätze und 56 Kriterien, die eingehalten werden
garantiert OTTO, dass alle verwendeten Hölzer aus verantwortungsvoll
bewirtschafteten Wäldern stammen. Unser Ziel bis 2025: 100 Prozent FSC©
bei unserem gesamten Holzmöbelsortiment. Gerade beim Rohstoff Holz
Seit der Gründung des FSC®-Systems 1993 hat OTTO begonnen,
FSC®-zertifizierte Produkte ins Sortiment aufzunehmen und das Angebot
darauf umzustellen. Neben dem Einsatz von FSC®-zertifiziertem Holz
betrachten wir außerdem noch andere ökologische Gesichtspunkte, wie
beispielsweise die teilweise Abschaffung und Verkleinerung der
Katalogformate oder den Einsatz von Recyclingpapier. Auch für unsere
Kartons verwenden wir ausschließlich FSC®-zertifizierte Materialien.
Klimaschutz  expand Nicht erst seit fridays4future ist klar: Der
Klimawandel ist da – und wir müssen etwas dagegen tun. Deshalb ist es
unser Ziel, bis 2030 klimaneutral zu werden und uns gemeinsam mit dir
weiterhin für den Klimaschutz einzusetzen. Seit vielen Jahren setzen wir
uns für das Klima ein – schon 2006 haben wir das Einsparen von CO2 zum
Unternehmensziel erklärt. Mit Erfolg: 2020 konnten wir unseren
umsatzbereinigten CO2-Ausstoß im Vergleich zum Jahr 2006 mehr als
Logistik. Unser Ansatz ist seit Jahren, CO2-Emissionen durch verbesserte
Energieeffizienz in Gebäuden und Optimierungen in der
Beschaffungslogistik sowie in der Zustellung zu senken. ### So packen
wir das Klima an Statt mit dem Flugzeug kommen unsere Lizenz- und
Eigenwaren zunehmend auf dem Seeweg, Schiene oder Straße. So haben wir
den Luftfrachtanteil im Vergleich zu 2012 mehr als halbiert – und noch
besser: Im Vergleich zum Basisjahr 2006 konnten wir in der
Beschaffungslogistik umsatzbereinigt 68 Prozent CO2 einsparen. ### Und
wir sind noch nicht am Ziel In Sachen Klimaschutz wollen wir immer
besser werden und noch mehr tun: - Bis 2030 soll OTTO klimaneutral
werden, das heißt die Emissionen werden vor allem durch Reduktion und
Vermeidung sowie den Einsatz erneuerbarer Energien und erst im zweiten
Schritt durch Kompensation von CO2 auf null gesenkt - Durch
vorausschauende Einkaufsplanung möchten wir den Luftfrachtanteil immer
weiter reduzieren und auf den Seeweg oder die Bahn verlagern - Auch am
Standort Hamburg werden wir den CO2-Ausstoß weiter senken: z. B. durch
den Ausbau erneuerbarer Energien bei den OTTO-Gebäuden und die Nutzung
von 100 Prozent Ökostrom bis 2025 - Unser Partner Hermes steigt
zunehmend auf klimafreundlichere Transportwege und Elektromobilität um -
Dienstfahrten oder -reisen haben wir bereits reduziert und nehmen
stattdessen virtuelle Termine wahr. Wenn ein persönliches Treffen
unbedingt notwendig ist, dann setzen wir auf klimaschonende Fahrzeuge
und auf die Bahn statt aufs Flugzeug - In Zeiten von Digitalisierung
werden wir verstärkt auf Grünstrom bei ausgelagerten
Cloud-Dienstleistungen und Rechenzentren achten - Nur wenn sich
CO2-Emissionen absolut nicht vermeiden lassen, werden wir diese mit
Klimaschutzprojekten nach höchstem Standard kompensieren Der
Zukunftswald auf Amrum Baumpat\*innenschaft-Aktionserfolg  expand
Wiedervernässung im Moor: Waldmoos oder Torfmoos? Wälder sind nicht nur
ein wichtiger Erholungsraum für uns Menschen: Sie dienen auch als
Wasserspeicher und können bis zu 20 Tonnen CO2 pro Jahr und Hektar aus
der Atmosphäre ziehen. Neben unseren Mooren sind sie damit der
wichtigste Treibhausgas-Speicher. Der tropische Regenwald beispielsweise
Menschen in zehn Jahren produzieren. Doch durch Abholzung, Klimawandel
und Umweltzerstörung verlieren Waldgebiete diese wichtige Fähigkeit. Und
das gilt auch bei uns in Deutschland. Deshalb setzen wir nicht nur auf
FSC®-zertifizierte Möbel, sondern haben ganz konkret mit der Aktion
Inselwaldes beigetragen. Der Zukunftswald auf Amrum 2001 hatte die
Naturschutzorganisation Bergwaldprojekt e. V. begonnen, den Inselwald
auf Amrum wieder aufzuforsten: Er war zwei Jahre zuvor vom Orkan
Nadelholzbäumen konnte stabilisiert und nach und nach ökologisch
aufgewertet werden. Seit 2013 haben wir die Arbeit vom Bergwaldprojekt
auf Amrum aktiv mit der Aktion „Werde Baumpate" unterstützt: Mit jedem
Verkauf eines unserer FSC®-Möbel innerhalb der verschiedenen
haben wir unser Ziel erreicht und über 60.000 Baumpat\*innen gewonnen
und damit 60.000 Bäume gepflanzt. Arbeitsbedingungen  expand Das
Wohlergehen der Menschen liegt uns am Herzen. Daher nehmen wir unsere
soziale Verantwortung sehr ernst – sie reicht von Europa bis nach Afrika
und Asien. Da sich die Arbeitsbedingungen von Land zu Land sehr
unterscheiden, machen wir uns als Teil der Otto Group stark für eine
weltweit einheitliche und sozialverträgliche Produktion und
menschenwürdige Arbeitsbedingungen. ### Ein Verhaltenskodex für alle
Schon 1996 haben wir unseren ersten [Code of
Conduct](https://www.ottogroup.com/de/verantwortung/Dokumente/Code-of-Conduct.php)
amfori übernommen – denn gemeinsam sind wir stärker. Mittlerweile ist er
ein fester Bestandteil der „Supplier Declaration on Sustainability”. Mit
diesem Verhaltenskodex fordern wir ein, dass in den Produktionen von
Risikoländern alle Beteiligten die strengen Regelungen zu sozialen
Standards und menschenwürdigen Arbeitsbedingungen einhalten. Hierunter
fallen u.a. das Verbot von Kinder- und Zwangsarbeit, eindeutige
Regelungen zum Arbeitsschutz und gegen Diskriminierung. Unsere
nachhaltigen Mindestanforderungen haben wir darüber hinaus in der
sogenannten „Supplier Declaration on Sustainability” erweitert. Diese
umfasst neben dem amfori BSCI Code of Conduct unter anderem auch
wesentliche Anforderungen, die dem Anspruch für ethisch korrektes
Handeln entsprechen. Das sind Themen wie Arten- und Tierschutz,
chemische Produktanforderungen und nachhaltige Materialien. Zum Beispiel
ist bei uns der Verkauf von Echtpelz und der Einsatz von Sandblasting
verboten. ### Unsere Lieferanten machen mit Alle unsere Lieferanten
müssen die „Supplier Declaration on Sustainability” verbindend
unterschreiben. Für unsere Eigenmarken verpflichten wir sie darüber
hinaus zur Transparenz: Sie müssen genau angeben, mit welchen
Endfertigungsfabriken sie zusammenarbeiten. Dabei prüfen wir, ob die
Produktionsstätten unseren Standards entsprechen. Als Marktplatz bieten
wir jedoch auch Artikel anderer Markenhersteller an. Für diese Produkte
liegt die Einhaltung der nachhaltigen und sozialen Standards bei den
Herstellern selbst. Sie müssen uns jedoch im Vorwege bestätigen, dass
sie unsere Werte zur Einhaltung von Menschen- und Umweltrechten teilen.
Ein großes Anliegen ist uns zudem die Einhaltung des ACCORD-Abkommens
und des „RMG Sustainability Council” (RSC): Es regelt den Gebäude- und
Brandschutz in Textilfabriken in Bangladesch, denn eine Katastrophe wie
der Einsturz des Rana-Plaza-Gebäudes in 2013 darf sich nicht
Arbeitsbedingungen dauerhaft zu optimieren und langfristig zu
verbessern. Ziel ist es, 100 Prozent unserer Lieferanten in das
Sozialprogramm zu integrieren. Sollten die Fabriken auch nach intensiven
Bemühungen kein gültiges Audit vorweisen können, müssen wir die
Geschäftsbeziehung beenden. Denn für bestehende und neue
Geschäftsbeziehungen ist ein gültiges Audit die Voraussetzung. Um
langfristig eine größtmögliche Verbesserung der Arbeitsbedingungen zu
erzielen, engagieren wir uns zusätzlich in unterschiedlichen
Partnerschaften und Kooperationen wie beispielsweise das Bündnis für
nachhaltige Textilien. Wir glauben durch die Bündelung von Kräften und
Know-how können wir mehr erreichen als allein. Verpackung  expand
Plastik versus Papier: Ist es wirklich so einfach? Bei OTTO kommen
Versandtaschen zum Einsatz, die zu 80 Prozent aus recyceltem Kunststoff
bestehen und anschließend wieder dem Recyclingkreislauf zugeführt werden
können. Plastik kommt den meisten Menschen nicht in die Tüte – aber ob
Papier immer die ökologisch sinnvollere Maßnahme ist? Wir wollten es
genau wissen und haben in einer unabhängigen Studie herausgefunden, dass
recyceltes Plastik (noch) die ökologisch sinnvollere Variante darstellt.
Daher versenden wir Artikel, die für den Karton zu klein sind, mit
Versandtaschen, die aus 80 Prozent recyceltem Kunststoff bestehen und
enger gestapelt werden und so für mehr Auslastung im Transporter sorgen,
was wiederum CO2 einspart. Wir prüfen weiterhin regelmäßig den Markt auf
der Suche nach ökologischen Alternativen, die den Einsatz fossiler
Rohstoffe vermeiden. ### Richtig gut gebeutelt Kreislauf des recycelten,
fast durchsichtigen Polybeutels Zusammen mit dem Start-up Cadel Deinking
ist es unser Ziel, auch hier eine umweltfreundliche Alternative zu
entwickeln. So sollen alte Kunststoffbeutel, auch Polybeutel genannt,
recycelt werden, um daraus nach einer Retoure wieder neue herzustellen.
In unseren Retouren-Betrieben haben wir die Beutel derzeit schon im
Test. Langfristig sollen bei uns alle verwendeten Beutel aus 100 Prozent
recyceltem Material bestehen. ### Plastik, das die Umwelt aufräumt
Derzeit testen wir in Kooperation mit dem Unternehmen WILDPLASTIC
Versandtüten, die aus gesammeltem Kunststoff hergestellt werden. Die
Idee: WILDPLASTIC sammelt in Kooperation mit verschiedenen
gemeinnützigen Sammelorganisationen weggeworfenes Plastik aus der Natur,
um es im Anschluss zu säubern und zu Granulat zu verarbeiten. Aus diesem
Granulat werden dann die
hergestellt. Und noch mehr passiert durch das Aufräumen: Sammler\*innen
bekommen einen fairen und regelmäßigen Lohn für ihre Arbeit. Bewährt
sich der Test, prüfen wir, inwiefern wir die neuen Versandtüten
flächendeckend einsetzen können. Zudem testen wir zurzeit gemeinsam mit
Tchibo und dem Avocadostore die Mehrwegverpackung von RePack im Rahmen
Bildung und Forschung gefördert wird. Das Pilotprojekt will den Einsatz
von Mehrwegverpackungen im Online-Handel erproben: Die Verpackungen
können bis zu 20-mal wiederverwendet werden und bis zu 96 Prozent des
Verpackungsmülls einsparen OTTO verwendet bei größeren Bestellungen
Papierkartons. Wir vermeiden den Einsatz von Papier an vielen Stellen,
beispielsweise haben wir das Füllmaterial auf ein Minimum vermindert.
Der wesentliche Aspekt ist das Recycling: Unsere Kartons bestehen aus
recyceltem Kunststoff. OTTO products  expand Immer mehr, immer
unserer neuen Eigenmarke setzen wir auf bewusstes Shoppen, den Einsatz
von nachhaltigen Rohstoffen und den Schutz der Natur. Heute sind bereits
viele unserer OTTO products zu 100 Prozent aus umweltfreundlichen
Rohstoffen und in Zukunft werden es mehr! Wir haben den Anspruch, unser
Sortiment stetig weiterzuentwickeln und noch nachhaltiger zu werden. ###
produziert in Indien – die Wertschöpfungskette eines Produktes ist oft
komplex und zieht sich über mehrere Kontinente, sodass man am Ende kaum
noch durchblickt. Wir glauben jedoch, ein transparentes
Shopping-Erlebnis ist der erste Schritt in eine nachhaltigere Zukunft.
sind dabei sehr hoch: Nur Artikel, die sich durch besonders
umweltfreundliche Rohstoffe oder Produktionsweisen auszeichnen, bekommen
unsere Kennzeichnung. Wir haben einen hohen Qualitätsanspruch und setzen
unter anderem auf GOTS-zertifizierte Bio-Baumwolle, recycelte Fasern aus
Meeresplastik oder Holz aus nachhaltigem und kontrolliertem Anbau. ###
FSC®-zertifiziertes Holz in Frage. Und für unsere Polsterfüllungen
setzen wir auf Federn, die mit dem Responsible Down Standard
ausgezeichnet wurden. Große Möbel werden extra für dich produziert, um
eine Überproduktion und somit Lagerkapazitäten zu vermeiden. So kann
jedes Möbelstück mit Ruhe und Sorgfalt hergestellt werden. Das dauert
manchmal vielleicht etwas länger, steigert die Vorfreude aber umso mehr.
geachtet, modische und gut kombinierbare Styles zu entwerfen – so steht
deiner minimalistischen Garderobe nichts mehr im Wege. Und das Beste:
Unsere Produkte bestehen zu mindestens 70 Prozent aus nachhaltigen
Rohstoffen. Bei vielen unserer Styles ist der Anteil sogar schon
deutlich höher. Du fragst dich, aus welchen Rohstoffen sie genau
bestehen? Bio-Baumwolle, recycelte Fasern und besonders hautfreundliche
Stoffe wie LENZING™ ECOVERO™ finden ihren Platz in unserer Kollektion
nachhaltigen Heimtextilien: Von Handtüchern über Bettwäsche bis Gardinen
Kund\*innen, die bei OTTO bestellt haben, können mit dem OTTO-Karton
etwas Gutes tun: Sie können diesen – oder auch jeden anderen Karton –
verschicken. Wir haben schon 2014 die Initiative „Platz schaffen mit
Herz“ ins Leben gerufen: Zusammen können wir so verantwortungsbewusst
mit Textilien umgehen und zugleich Gutes tun – per Voting entscheidest
du mit, welche gemeinnützigen Organisationen wir unterstützen. So
schaffen es die alten Lieblingsstücke zu einem\*einer neuen
Besitzer\*in, der\*die sie noch gebrauchen kann oder bekommen ein
zweites Leben als
Tonnen Kleidung in Altkleidercontainern oder bei Sammlungen abgegeben,
ungefähr 62.000 LKW-Ladungen voll. Damit sind wir in Deutschland
weltweit führend bei der Textilsammlung. Ständig neue Trends und häufige
Kollektionswechsel führen dazu, dass die einzelnen Stücke immer kürzer
getragen werden. Unser Ziel: Kleidungsstücke so lange wie möglich im
Kreislauf zu halten. 'Circularity' oder 'Kreislaufwirtschaft' sind die
Fachbegriffe: Dabei geht es darum, den bereits existierenden Produkten
ein neues Leben zu schenken, z. B. indem sie gespendet, von jemand
anderem weitergetragen oder recycelt werden. Schon vor zehn Jahren haben
wir unseren Kund\*innen angeboten, aussortierte Textilien fachgerecht
bei uns abzugeben und zurück in den Kreislauf zu bringen. Mehr zum
Projekt gibt es auf der [Website](https://www.platzschaffenmitherz.de/)
von „Platz schaffen mit Herz” Platz schaffen mit Herz Ihr wollt mehr
Infos zum Thema Nachhaltigkeit? Diese findet ihr auf
Nachhaltigkeits-Blog von OTTO. Dort beleuchten wir verschiedene Themen
rund um Nachhaltigkeit. Schaut doch einmal vorbei. Diesen Artikel
teilen: - Facebook - Twitter - Xing - Linkedin - WhatsApp - Link
kopieren Zur Startseite © Otto (GmbH & Co KG), 22179 Hamburg Folge uns:
linkedin xing twitter instagram facebook rss - Datenschutz - Impressum -
OTTO-Shop Zum OTTO-Shop OTTO verwendet zum Teil solche Cookies auf
ermöglichen. [Mehr](https://www.otto-newsroom.de/de/datenschutz)
Informationen zur
Einwilligung.[](https://www.otto.de/unternehmen/de/datenschutz) Ich
