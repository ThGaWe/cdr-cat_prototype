Schneller shoppen in der App
COOKIES ERLAUBEN?
OTTO und sieben Partner brauchen deine Zustimmung (Klick auf „OK”) bei
vereinzelten Datennutzungen, um Informationen auf einem Gerät zu
speichern und/oder abzurufen (IP-Adresse, Nutzer-ID,
Browser-Informationen). Die Datennutzung erfolgt für personalisierte
Erkenntnisse über Zielgruppen und Produktentwicklungen zu gewinnen. Mehr
Infos zur Einwilligung und zu Einstellungsmöglichkeiten gibt’s jederzeit
hier. Mit Klick auf den Link "Cookies ablehnen" kannst du deine
Einwilligung jederzeit ablehnen.
DATENNUTZUNGEN
OTTO arbeitet mit Partnern zusammen, die von Ihrem Endgerät abgerufene
Daten (Trackingdaten) auch zu eigenen Zwecken (z.B. Profilbildungen) /
zu Zwecken Dritter verarbeiten. Vor diesem Hintergrund erfordert nicht
nur die Erhebung der Trackingdaten, sondern auch deren
Weiterverarbeitung durch diese Anbieter einer Einwilligung. Die
Trackingdaten werden erst dann erhoben, wenn Sie auf den in dem Banner
auf otto.de wiedergebenden Button „OK” anklicken. Bei den Partnern
handelt es sich um die folgenden Unternehmen:
Microsoft Ireland Operations Limited, OS Data Solutions GmbH & Co. KG,
Otto Group Media GmbH, Ströer SSP GmbH.
Weitere Informationen zu den Datenverarbeitungen durch diese Partner
finden Sie in der Datenschutzerklärung auf otto.de. Die Informationen
sind außerdem über einen Link in dem Banner abrufbar.
OK
ALLGEMEINE GESCHÄFTSBEDINGUNGEN
1. ZUSTANDEKOMMEN DES VERTRAGES: Die Darstellung der Produkte auf
www.otto.de stellt kein rechtlich bindendes Angebot, sondern einen
unverbindlichen Online-Katalog dar. Durch Anklicken des Buttons „Jetzt
zum genannten Preis bestellen“ geben Sie eine verbindliche Bestellung
der im Warenkorb enthaltenen Waren ab. Die Bestätigung des Eingangs der
Bestellung folgt unmittelbar nach dem Absenden der Bestellung und stellt
noch keine Vertragsannahme dar. Der Kaufvertrag kommt erst durch
ausdrückliche Erklärung oder den Versand der Ware zustande. Wenn Sie per
Kreditkarte, Vorkasse, PayPal oder Paydirekt bezahlt haben, kommt der
Kaufvertrag bereits mit erfolgreichem Abschluss des Bestellvorgangs
zustande. Der Kaufvertrag/ die Kaufverträge kommt/ kommen zustande
zwischen Ihnen und dem Verkäufer der von Ihnen bestellten Artikel; der
jeweilige Verkäufer eines Artikels wird an verschiedenen Stellen des
Warenkorb und vor Abschluss der Bestellung ("Verkäufer: Firma X").
Verkäufer eines Artikels sind entweder wir (Otto (GmbH & Co KG)) oder
einer unserer Partner. Unsere Partner verkaufen auf www.otto.de Produkte
im eigenen Namen und auf eigene Rechnung. Der Verkauf von Produkten über
www.otto.de erfolgt grundsätzlich nur an Verbraucher im Sinne des § 13
BGB.
2. WIDERRUFSBELEHRUNG
WIDERRUFSRECHT
Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen den
Vertrag mit uns oder einem anderen Verkäufer auf www.otto.de zu
widerrufen.
Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag an dem Sie oder ein
von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte
Ware in Besitz genommen haben bzw. hat.
Um Ihr Widerrufsrecht auszuüben, können Sie – gleich ob Sie einen
Vertrag mit uns oder einem anderen Verkäufer auf www.otto.de haben – uns
Erklärung (z.B. ein mit der Post versandter Brief, Telefax oder E-Mail)
können dafür das beigefügte Muster-Widerrufsformular verwenden, das
jedoch nicht vorgeschrieben ist. Einen Widerruf können Sie auch dadurch
erklären, dass Sie Ihre Rücksendung unter Mein Konto anmelden.
Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung
absenden.
FOLGEN DES WIDERRUFS
Wenn Sie den Vertrag widerrufen, werden Ihnen alle geleisteten Zahlungen
einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten,
die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die
angebotene, günstigste Standardlieferung gewählt haben), unverzüglich
und spätestens binnen vierzehn Tagen ab dem Tag zurückgezahlt, an dem
die Mitteilung über Ihren Widerruf des Vertrags eingegangen ist. Für
diese Rückzahlung wird dasselbe Zahlungsmittel verwendet, das Sie bei
der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen
wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen
wegen dieser Rückzahlung Entgelte berechnet. Die Rückzahlung kann
verweigert werden, bis die Waren wieder zurückerhalten wurden oder bis
Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben,
je nachdem, welches der frühere Zeitpunkt ist.
Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen
vierzehn Tagen ab dem Tag, an dem Sie über den Widerruf des Vertrags
informiert haben, an den jeweiligen Verkäufer zurückzusenden.
Einzelheiten und Hinweise für eine schnelle und unkomplizierte
Rücksendung der Waren finden Sie unter Mein Konto. Hier können Sie für
viele Artikel Ihr Rücksendeetikett abrufen und im Falle von
Speditionssendungen die Kontaktdaten des Spediteurs einsehen. Die Frist
ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen
absenden.
Sie tragen keine Kosten für die Rücksendung der Ware.
Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn
dieser Wertverlust auf einen zur Prüfung der Beschaffenheit,
Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit
ihnen zurückzuführen ist.
3. Ausschluss/Erlöschen des Widerrufsrechts
Verträgen zur Lieferung von Waren, die nicht vorgefertigt sind und für
deren Herstellung eine individuelle Auswahl oder Bestimmung durch den
Verbraucher maßgeblich ist oder die eindeutig auf die persönlichen
Bedürfnisse des Verbrauchers zugeschnitten sind und es erlischt
vorzeitig bei Verträgen zur Lieferung versiegelter Waren, die aus
Gründen des Gesundheitsschutzes oder der Hygiene nicht zur Rückgabe
geeignet sind, wenn ihre Versiegelung nach der Lieferung entfernt wurde.
4. FREIWILLIGE RÜCKGABEGARANTIE: Für alle von uns, der Otto (GmbH & Co
KG), verkauften Artikel gewähren wir Ihnen neben dem gesetzlichen
Widerrufsrecht eine freiwillige Rückgabegarantie von insgesamt 30 Tagen
ab Warenerhalt. Sie können sich auch nach Ablauf der 14-tägigen
Widerrufsfrist vom Vertrag lösen, indem Sie die Ware innerhalb von 30
Tagen nach deren Erhalt (Fristbeginn am Tag nach Warenerhalt) an uns
zurücksenden, sofern die Ware vollständig und originalverpackt ist und
sich in ungebrauchtem und unbeschädigtem Zustand befindet und es sich
nicht um Ware handelt, die speziell nach Kundenspezifikation angefertigt
wird oder eindeutig auf die persönlichen Bedürfnisse zugeschnitten ist.
Die rechtzeitige Absendung reicht zur Fristwahrung aus. Die freiwillige
Rückgabegarantie gilt nur in Deutschland.
Die Ware ist zurückzusenden an: OTTO (GMBH & CO KG), 20088 HAMBURG
DIE VERTRAGLICH EINGERÄUMTE FREIWILLIGE RÜCKGABEGARANTIE LÄSST IHRE
GESETZLICHEN RECHTE UND ANSPRÜCHE UNBERÜHRT. INSBESONDERE IHR
GESETZLICHES WIDERRUFSRECHT UND IHRE GESETZLICHEN GEWÄHRLEISTUNGSRECHTE
BLEIBEN IHNEN UNEINGESCHRÄNKT ERHALTEN. BITTE BEACHTEN SIE, DASS DIESE
FREIWILLIGE RÜCKGABEGARANTIE NICHT FÜR EINKÄUFE BEI UNSEREN PARTNERN AUF
WWW.OTTO.DE GILT.
5. Lieferungen sind grundsätzlich nur innerhalb Deutschlands möglich.
Von uns verkaufte Artikel werden überwiegend durch Hermes oder DHL
angeliefert. Unsere Partner verwenden unter Umständen andere
Transportunternehmen. Die Abgabe von Artikeln erfolgt nur in
haushaltsüblichen Mengen.
6. SOLLTEN GELIEFERTE ARTIKEL OFFENSICHTLICHE MATERIAL- ODER
HERSTELLUNGSFEHLER AUFWEISEN, WOZU AUCH TRANSPORTSCHÄDEN ZÄHLEN, SO
REKLAMIEREN SIE BITTE SOLCHE FEHLER SOFORT GEGENÜBER UNS ODER DEM
MITARBEITER DES VERSANDDIENSTLEISTERS, DER DIE ARTIKEL ANLIEFERT. DIE
VERSÄUMUNG DIESER RÜGE HAT ALLERDINGS FÜR IHRE GESETZLICHEN ANSPRÜCHE
KEINE KONSEQUENZEN. FÜR ALLE WÄHREND DER GESETZLICHEN
GEWÄHRLEISTUNGSFRIST AUFTRETENDEN MÄNGEL DER KAUFSACHE GELTEN NACH IHRER
WAHL DIE GESETZLICHEN ANSPRÜCHE AUF NACHERFÜLLUNG, AUF
MANGELBESEITIGUNG/NEULIEFERUNG SOWIE – BEI VORLIEGEN DER GESETZLICHEN
VORAUSSETZUNGEN – DIE WEITERGEHENDEN ANSPRÜCHE AUF MINDERUNG ODER
RÜCKTRITT SOWIE DANEBEN AUF SCHADENSERSATZ, EINSCHLIESSLICH DES ERSATZES
DES SCHADENS STATT DER ERFÜLLUNG SOWIE DES ERSATZES IHRER VERGEBLICHEN
AUFWENDUNGEN. SOWEIT IHNEN EINE VERKÄUFERGARANTIE GEWÄHRT WURDE, ERGEBEN
SICH DIE EINZELHEITEN AUS DEN GARANTIEBEDINGUNGEN, DIE DEM JEWEILS
GELIEFERTEN ARTIKEL BEIGEFÜGT SIND. GARANTIEANSPRÜCHE BESTEHEN
UNBESCHADET DER GESETZLICHEN ANSPRÜCHE/RECHTE.
7. ANSPRECHPARTNER: Im Falle von Rückfragen oder Problemen im
Zusammenhang mit einer Lieferung oder einem erworbenen Artikel, gleich
ob dieser durch uns oder einen unserer Partner verkauft wurde, können
Sie sich zunächst an uns wenden. Soweit eine Klärung durch uns nicht
möglich ist, werden wir einen Kontakt zum entsprechenden Partner
herstellen.
8. GÜLTIGKEIT DER PREISE: Die auf www.otto.de genannten Preise sind
Euro-Preise (einschl. der gesetzlichen Mehrwertsteuer).
9. LIEFERUNG UND VERSANDKOSTEN: Bei einer Bestellung von Artikeln
verschiedener Verkäufer erhalten Sie grundsätzlich mehrere Lieferungen.
Die Versandart (Paket oder Spedition) und die Lieferbedingungen (bei
Speditionsversand) sind Teil der Artikelinformationen auf der
Artikeldetailseite. Die jeweiligen Versandkosten Ihrer Bestellung sind
im Warenkorb und in der Übersicht vor Anklicken des Buttons „Jetzt zum
10. ZAHLUNG: Sie können bei allen Verkäufern grundsätzlich per Vorkasse,
Kreditkarte, Lastschrift, auf Rechnung oder per Ratenzahlung bezahlen.
Für Käufe, bei denen ausschließlich wir (Otto (GmbH & Co KG)) Verkäufer
sind, können Sie zusätzlich per PayPal und paydirekt zahlen. Die
Möglichkeit zum Skontoabzug besteht nicht.
Um Ihnen auf www.otto.de ein einheitliches, attraktives Angebot von
Zahlungsdienstleistern zusammen. Bei Käufen bei einem unserer Partner,
bei dem die Zahlungsart Vorkasse, Lastschrift, Rechnung und Ratenzahlung
ausgewählt wurden („Ratepay-Zahlungsarten“) arbeiten wir mit der Ratepay
GmbH, Franklinstr. 28-29, 10587 Berlin ("Ratepay") zusammen. Kommt bei
Nutzung einer Ratepay-Zahlungsart ein wirksamer Kaufvertrag zwischen
Ihnen und einem Partner zustande, wird die gegen Sie bestehende
Zahlungsforderung des Partners an Ratepay abgetreten. Wenn Sie eine der
hier angebotenen Ratepay-Zahlungsarten wählen, willigen Sie im Rahmen
Ihrer Bestellung in die Weitergabe Ihrer persönlichen Daten und die der
Bestellung, zum Zwecke der Identitäts- und Bonitätsprüfung, sowie der
Vertragsabwicklung, an Ratepay ein. Alle Einzelheiten finden Sie in den
Ratepay-Zahlungsbedingungen und den Ratepay-Datenschutzbestimmungen, die
Teil dieser Allgemeinen Geschäftsbedingungen sind und immer dann
Anwendung finden, wenn Sie sich für eine Ratepay-Zahlungsart
entscheiden. Der Einzug von Lastschriften erfolgt durch den
Zahlungsdienstleister Hanseatic Bank GmbH & Co KG, Bramfelder Chaussee
101, 22177 Hamburg.
Wir behalten uns vor, Ihnen für die erbetene Lieferung nur bestimmte
Zahlungsarten anzubieten, beispielweise zur Absicherung unseres
Kreditrisikos nur solche entsprechend der jeweiligen Bonität.
In Einzelfällen behalten wir uns vor, die Ware erst nach einer Anzahlung
auszuliefern. Nach Eingang der Bestellung werden wir dies mit dem Kunden
abstimmen.
Wenn Sie sich im Hinblick auf einen Kauf bei uns (Otto (GmbH & Co KG))
im Verzug befinden, behalten wir uns vor, für jede Mahnung als Ersatz
für den entstandenen Schaden pauschal 1,25 € als Verzugsschaden zu
berechnen; Ihnen steht jeweils der Nachweis frei, dass uns kein oder nur
ein geringerer Schaden entstanden ist. Im Übrigen können wir Ihnen
gesetzliche Verzugszinsen in Höhe von fünf Prozentpunkten über dem
Basiszinssatz berechnen.
Weitere Informationen zur Zahlung finden Sie auf: www.otto.de/bezahlung
11. EIGENTUMSVORBEHALT: Bis zur vollständigen Bezahlung bleibt die Ware
Eigentum des jeweiligen Verkäufers.
12. ELEKTRONISCHE KOMMUNIKATION: Sie stimmen zu, dass die
vertragsbezogene Kommunikation in elektronischer Form erfolgen kann.
13. ALTERNATIVE STREITBEILEGUNG: Die EU-Kommission bietet die
Möglichkeit zur Online-Streitbeilegung auf einer von ihr betriebenen
Online-Plattform. Diese Plattform ist über den externen Link
http://ec.europa.eu/consumers/odr/ zu erreichen. Wir sind nicht
verpflichtet und nicht gewillt, an Streitbeilegungsverfahren vor einer
Verbraucherschlichtungsstelle teilzunehmen. Das gleiche gilt auch für
unsere Partner, es sei denn etwas Abweichendes ergibt sich aus dem
Impressum auf der Verkäuferdetailseite des entsprechenden Partners.
14.VERTRAGSSPRACHE/SPEICHERUNG DES BESTELLTEXTES: Der Vertragsschluss
erfolgt in deutscher Sprache. Der Bestelltext wird bei uns nicht
gespeichert und kann nach Abschluss des Bestellvorgangs nicht mehr
abgerufen werden. Sie können Ihre Bestelldaten aber unmittelbar nach dem
Absenden der Bestellung ausdrucken.
15. ANWENDBARES RECHT: Es gilt das Recht der Bundesrepublik Deutschland
unter Ausschluss des UN-Kaufrechts. Wenn Sie Verbraucher mit Wohnsitz
außerhalb der Bundesrepublik Deutschland sind, kann ggf. auch das Recht
kommen, wenn es sich um zwingende Bestimmungen handelt.
OTTO (GMBH & CO KG), WERNER-OTTO-STRASSE 1-7, 22179 HAMBURG,
DEUTSCHLAND, AG HAMBURG HRA 62024, PERSÖNLICH HAFTEND:
VERWALTUNGSGESELLSCHAFT OTTO MBH, WERNER-OTTO-STRASSE 1-7, 22179
HAMBURG, DEUTSCHLAND, AG HAMBURG HRB 13762, VERTRETEN DURCH: ALEXANDER
BIRKEN (VORSITZENDER), SEBASTIAN KLAUKE, DR. MARCUS ACKERMANN, PETRA
SCHARNER-WOLFF, KAY SCHIEBUR, SERGIO BUCHER; AUFSICHTSRAT: PROF. DR.
MICHAEL OTTO (VORSITZENDER).
MUSTER-WIDERRUFSFORMULAR
Formular aus und senden
Sie es zurück.)
Service@otto.de
Vertrag über den Kauf
der folgenden Waren (*)/ die Erbringung der folgenden Dienstleistung (*)
Preisangaben inkl. gesetzl. MwSt. und zzgl. Service- und Versandkosten
