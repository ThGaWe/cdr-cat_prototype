Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten
sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und
entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser
Datenschutzerklärung.
Wenn Sie diese Website benutzen, werden verschiedene personenbezogene
Daten erhoben. Personenbezogene Daten sind Daten, mit denen Sie
persönlich identifiziert werden können. Die vorliegende
Datenschutzerklärung erläutert, welche Daten wir erheben und wofür wir
sie nutzen. Sie erläutert auch, wie und zu welchem Zweck das geschieht.
Wir weisen darauf hin, dass die Datenübertragung im Internet (z. B. bei
der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein
lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht
möglich.
Hinweis zur verantwortlichen Stelle
ist:
dr. dresing & pehl GmbH
Deutschhausstrasse 22a
35037 Marburg
E-Mail: info@audiotranskription.de
Verantwortliche Stelle ist die natürliche oder juristische Person, die
allein oder gemeinsam mit anderen über die Zwecke und Mittel der
Verarbeitung von personenbezogenen Daten (z. B. Namen, E-Mail-Adressen
o. Ä.) entscheidet.
Soweit innerhalb dieser Datenschutzerklärung keine speziellere
Speicherdauer genannt wurde, verbleiben Ihre personenbezogenen Daten bei
berechtigtes Löschersuchen geltend machen oder eine Einwilligung zur
Datenverarbeitung widerrufen, werden Ihre Daten gelöscht, sofern wir
keine anderen rechtlich zulässigen Gründe für die Speicherung Ihrer
personenbezogenen Daten haben (z.B. steuer- oder handelsrechtliche
Aufbewahrungsfristen); im letztgenannten Fall erfolgt die Löschung nach
Fortfall dieser Gründe.
Hinweis zur Datenweitergabe in die USA
den USA eingebunden. Wenn diese Tools aktiv sind, können Ihre
personenbezogenen Daten an die US-Server der jeweiligen Unternehmen
weitergegeben werden. Wir weisen darauf hin, dass die USA kein sicherer
Drittstaat im Sinne des EU-Datenschutzrechts sind. US-Unternehmen sind
dazu verpflichtet, personenbezogene Daten an Sicherheitsbehörden
herauszugeben, ohne dass Sie als Betroffener hiergegen gerichtlich
vorgehen könnten. Es kann daher nicht ausgeschlossen werden, dass
US-Behörden (z.B. Geheimdienste) Ihre auf US-Servern befindlichen Daten
zu Überwachungszwecken verarbeiten, auswerten und dauerhaft speichern.
Wir haben auf diese Verarbeitungstätigkeiten keinen Einfluss.
SSL- bzw. TLS-Verschlüsselung
Diese Seite nutzt aus Sicherheitsgründen und zum Schutz der Übertragung
vertraulicher Inhalte, wie zum Beispiel Bestellungen oder Anfragen, die
Sie an uns als Seitenbetreiber senden, eine SSL- bzw.
TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran,
dass die Adresszeile des Browsers von „http://“ auf „https://“ wechselt
und an dem Schloss-Symbol in Ihrer Browserzeile.
Wenn die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten,
die Sie an uns übermitteln, nicht von Dritten mitgelesen werden.
Besteht nach dem Abschluss eines kostenpflichtigen Vertrags eine
Verpflichtung, uns Ihre Zahlungsdaten (z. B. Kontonummer bei
Einzugsermächtigung) zu übermitteln, werden diese Daten zur
Zahlungsabwicklung benötigt.
Der Zahlungsverkehr über die gängigen Zahlungsmittel (Visa/MasterCard,
Lastschriftverfahren) erfolgt ausschließlich über eine verschlüsselte
SSL- bzw. TLS-Verbindung. Eine verschlüsselte Verbindung erkennen Sie
daran, dass die Adresszeile des Browsers von „http://“ auf „https://“
wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.
Bei verschlüsselter Kommunikation können Ihre Zahlungsdaten, die Sie an
uns übermitteln, nicht von Dritten mitgelesen werden.
Auskunft, Löschung und Berichtigung
Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit
das Recht auf unentgeltliche Auskunft über Ihre gespeicherten
Datenverarbeitung und ggf. ein Recht auf Berichtigung oder Löschung
dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene
Daten können Sie sich jederzeit an uns wenden.
kleine Textdateien und richten auf Ihrem Endgerät keinen Schaden an. Sie
werden entweder vorübergehend für die Dauer einer Sitzung
gespeichert. Session-Cookies werden nach Ende Ihres Besuchs automatisch
gelöscht. Permanente Cookies bleiben auf Ihrem Endgerät gespeichert, bis
Sie diese selbst löschen oder eine automatische Löschung durch Ihren
Webbrowser erfolgt.
gespeichert werden, wenn Sie unsere Seite betreten
bestimmter Dienstleistungen des Drittunternehmens (z.B. Cookies zur
Abwicklung von Zahlungsdienstleistungen).
würden (z.B. die Warenkorbfunktion oder die Anzeige von Videos). Andere
Cookies, die zur Durchführung des elektronischen Kommunikationsvorgangs
Messung des Webpublikums) erforderlich sind, werden auf Grundlage von
Art. 6 Abs. 1 lit. f DSGVO gespeichert, sofern keine andere
Rechtsgrundlage angegeben wird. Der Websitebetreiber hat ein
berechtigtes Interesse an der Speicherung von Cookies zur technisch
fehlerfreien und optimierten Bereitstellung seiner Dienste. Sofern eine
Einwilligung zur Speicherung von Cookies abgefragt wurde, erfolgt die
Speicherung der betreffenden Cookies ausschließlich auf Grundlage dieser
Einwilligung (Art. 6 Abs. 1 lit. a DSGVO); die Einwilligung ist
jederzeit widerrufbar.
Sie können Ihren Browser so einstellen, dass Sie über das Setzen von
Cookies informiert werden und Cookies nur im Einzelfall erlauben, die
Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie
das automatische Löschen der Cookies beim Schließen des Browsers
aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität
Soweit Cookies von Drittunternehmen oder zu Analysezwecken eingesetzt
werden, werden wir Sie hierüber im Rahmen dieser Datenschutzerklärung
gesondert informieren und ggf. eine Einwilligung abfragen.
Diese Website nutzt die Cookie-Consent-Technologie von Usercentrics, um
Ihre Einwilligung zur Speicherung bestimmter Cookies auf Ihrem Endgerät
oder zum Einsatz bestimmter Technologien einzuholen und diese
datenschutzkonform zu dokumentieren. Anbieter dieser Technologie ist die
https://usercentrics.com/de/ (im Folgenden „Usercentrics“).
an Usercentrics übertragen:
Des Weiteren speichert Usercentrics ein Cookie in Ihrem Browser, um
Ihnen die erteilten Einwilligungen bzw. deren Widerruf zuordnen zu
können. Die so erfassten Daten werden gespeichert, bis Sie uns zur
Löschung auffordern, das Usercentrics-Cookie selbst löschen oder der
Zweck für die Datenspeicherung entfällt. Zwingende gesetzliche
Aufbewahrungspflichten bleiben unberührt.
Der Einsatz von Usercentrics erfolgt, um die gesetzlich vorgeschriebenen
Einwilligungen für den Einsatz bestimmter Technologien einzuholen.
Rechtsgrundlage hierfür ist Art. 6 Abs. 1 S. 1 lit. c DSGVO.
Vertrag über Auftragsverarbeitung
Wir haben einen Vertrag über Auftragsverarbeitung mit Usercentrics
geschlossen. Hierbei handelt es sich um einen datenschutzrechtlich
vorgeschriebenen Vertrag, der gewährleistet, dass Usercentrics die
personenbezogenen Daten unserer Websitebesucher nur nach unseren
Weisungen und unter Einhaltung der DSGVO verarbeitet.
Server-Log-Dateien
Der Provider der Seiten erhebt und speichert automatisch Informationen
in so genannten Server-Log-Dateien, die Ihr Browser automatisch an uns
Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht
vorgenommen.
Die Erfassung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit.
f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der
hierzu müssen die Server-Log-Files erfasst werden.
Analyse-Tools und Werbung
Wir setzen den Google Tag Manager ein. Anbieter ist die Google Ireland
Limited, Gordon House, Barrow Street, Dublin 4, Irland.
Der Google Tag Manager ist ein Tool, mit dessen Hilfe wir Tracking- oder
können. Der Google Tag Manager selbst erstellt keine Nutzerprofile,
speichert keine Cookies und nimmt keine eigenständigen Analysen vor. Er
dient lediglich der Verwaltung und Ausspielung der über ihn
eingebundenen Tools. Der Google Tag Manager erfasst jedoch Ihre
IP-Adresse, die auch an das Mutterunternehmen von Google in die
Vereinigten Staaten übertragen werden kann.
Der Einsatz des Google Tag Managers erfolgt auf Grundlage von Art. 6
Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse
an einer schnellen und unkomplizierten Einbindung und Verwaltung
Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschließlich
auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist
jederzeit widerrufbar.
Street, Dublin 4, Irland.
Google Analytics ermöglicht es dem Websitebetreiber, das Verhalten der
verschiedene Nutzungsdaten, wie z.B. Seitenaufrufe, Verweildauer,
verwendete Betriebssysteme und Herkunft des Nutzers. Diese Daten werden
von Google ggf. in einem Profil zusammengefasst, das dem jeweiligen
Nutzer bzw. dessen Endgerät zugeordnet ist.
Cookies oder Device-Fingerprinting). Die von Google erfassten
einen Server von Google in den USA übertragen und dort gespeichert.
Die Nutzung dieses Analyse-Tools erfolgt auf Grundlage von Art. 6 Abs. 1
lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der
Werbung zu optimieren. Sofern eine entsprechende Einwilligung abgefragt
wurde (z. B. eine Einwilligung zur Speicherung von Cookies), erfolgt die
Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a
DSGVO; die Einwilligung ist jederzeit widerrufbar.
der EU-Kommission gestützt. Details finden Sie hier:
https://privacy.google.com/businesses/controllerterms/mccs/.
IP Anonymisierung
Wir haben auf dieser Website die Funktion IP-Anonymisierung aktiviert.
Dadurch wird Ihre IP-Adresse von Google innerhalb von Mitgliedstaaten
der Europäischen Union oder in anderen Vertragsstaaten des Abkommens
gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server
von Google in den USA übertragen und dort gekürzt. Im Auftrag des
Websitenutzung und der Internetnutzung verbundene Dienstleistungen
gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google
Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit
anderen Daten von Google zusammengeführt.
Browser Plugin
Sie können die Erfassung und Verarbeitung Ihrer Daten durch Google
verhindern, indem Sie das unter dem folgenden Link verfügbare
Browser-Plugin herunterladen und installieren:
https://tools.google.com/dlpage/gaoptout?hl=de.
finden Sie in der Datenschutzerklärung von Google:
https://support.google.com/analytics/answer/6004245?hl=de.
Auftragsverarbeitung
Wir haben mit Google einen Vertrag zur Auftragsverarbeitung
abgeschlossen und setzen die strengen Vorgaben der deutschen
Datenschutzbehörden bei der Nutzung von Google Analytics vollständig um.
Demografische Merkmale bei Google Analytics
erstellt werden, die Aussagen zu Alter, Geschlecht und Interessen der
Seitenbesucher enthalten. Diese Daten stammen aus interessenbezogener
Werbung von Google sowie aus Besucherdaten von Drittanbietern. Diese
Daten können keiner bestimmten Person zugeordnet werden. Sie können
diese Funktion jederzeit über die Anzeigeneinstellungen in Ihrem
Google-Konto deaktivieren oder die Erfassung Ihrer Daten durch Google
Analytics wie im Punkt „Widerspruch gegen Datenerfassung“ dargestellt
generell untersagen.
Online-Marketing-Kampagnen analysieren. Hierbei werden Informationen,
wie zum Beispiel die getätigten Bestellungen, durchschnittliche
Bestellwerte, Versandkosten und die Zeit von der Ansicht bis zum Kauf
eines Produktes erfasst. Diese Daten können von Google unter einer
Transaktions-ID zusammengefasst werden, die dem jeweiligen Nutzer bzw.
dessen Gerät zugeordnet ist.
Bei Google gespeicherte Daten auf Nutzer- und Ereignisebene, die mit
Cookies, Nutzerkennungen (z. B. User ID) oder Werbe-IDs (z. B.
DoubleClick-Cookies, Android-Werbe-ID) verknüpft sind, werden nach 14
Monaten anonymisiert bzw. gelöscht. Details hierzu ersehen Sie unter
https://support.google.com/analytics/answer/7667196?hl=de
House, Barrow Street, Dublin 4, Irland.
Google Ads ermöglicht es uns Werbeanzeigen in der Google-Suchmaschine
oder auf Drittwebseiten auszuspielen, wenn der Nutzer bestimmte
Suchbegriffe bei Google eingibt (Keyword-Targeting). Ferner können
Nutzerdaten (z.B. Standortdaten und Interessen) ausgespielt werden
quantitativ auswerten, indem wir beispielsweise analysieren, welche
Suchbegriffe zur Ausspielung unserer Werbeanzeigen geführt haben und wie
viele Anzeigen zu entsprechenden Klicks geführt haben.
Die Nutzung von Google Ads erfolgt auf Grundlage von Art. 6 Abs. 1 lit.
f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer
möglichst effektiven Vermarktung seiner Dienstleistung Produkte.
der EU-Kommission gestützt. Details finden Sie hier:
https://policies.google.com/privacy/frameworks und
https://privacy.google.com/businesses/controllerterms/mccs/.
6. Newsletter
Newsletter­daten
benötigen wir von Ihnen eine E-Mail-Adresse sowie Informationen, welche
uns die Überprüfung gestatten, dass Sie der Inhaber der angegebenen
E-Mail-Adresse sind und mit dem Empfang des Newsletters einverstanden
sind. Weitere Daten werden nicht bzw. nur auf freiwilliger Basis
erhoben. Diese Daten verwenden wir ausschließlich für den Versand der
angeforderten Informationen und geben diese nicht an Dritte weiter.
Die Verarbeitung der in das Newsletteranmeldeformular eingegebenen Daten
erfolgt ausschließlich auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1
lit. a DSGVO). Die erteilte Einwilligung zur Speicherung der Daten, der
E-Mail-Adresse sowie deren Nutzung zum Versand des Newsletters können
Sie jederzeit widerrufen, etwa über den „Austragen“-Link im Newsletter.
Die Rechtmäßigkeit der bereits erfolgten Datenverarbeitungsvorgänge
bleibt vom Widerruf unberührt.
Die von Ihnen zum Zwecke des Newsletter-Bezugs bei uns hinterlegten
Daten werden von uns bis zu Ihrer Austragung aus dem Newsletter bei uns
bzw. dem Newsletterdiensteanbieter gespeichert und nach der Abbestellung
des Newsletters oder nach Zweckfortfall aus der Newsletterverteilerliste
gelöscht. Wir behalten uns vor, E-Mail-Adressen aus unserem
Newsletterverteiler nach eigenem Ermessen im Rahmen unseres berechtigten
Interesses nach Art. 6 Abs. 1 lit. f DSGVO zu löschen oder zu sperren.
Nach Ihrer Austragung aus der Newsletterverteilerliste wird Ihre
E-Mail-Adresse bei uns bzw. dem Newsletterdiensteanbieter ggf. in einer
Blacklist gespeichert, um künftige Mailings zu verhindern. Die Daten aus
der Blacklist werden nur für diesen Zweck verwendet und nicht mit
anderen Daten zusammengeführt. Dies dient sowohl Ihrem Interesse als
auch unserem Interesse an der Einhaltung der gesetzlichen Vorgaben beim
Versand von Newslettern (berechtigtes Interesse im Sinne des Art. 6 Abs.
1 lit. f DSGVO). Die Speicherung in der Blacklist ist zeitlich nicht
befristet. SIE KÖNNEN DER SPEICHERUNG WIDERSPRECHEN, SOFERN IHRE
INTERESSEN UNSER BERECHTIGTES INTERESSE ÜBERWIEGEN.
CleverReach
Anbieter ist die CleverReach GmbH & Co. KG, Schafjückenweg 2, 26180
kann. Die von Ihnen zwecks Newsletterbezug eingegebenen Daten (z. B.
E-Mail-Adresse) werden auf den Servern von CleverReach in Deutschland
bzw. Irland gespeichert.
des Verhaltens der Newsletterempfänger. Hierbei kann u. a. analysiert
werden, wie viele Empfänger die Newsletternachricht geöffnet haben und
wie oft welcher Link im Newsletter angeklickt wurde. Mit Hilfe des
nach Anklicken des Links im Newsletter eine vorab definierte Aktion (z.
Informationen zur Datenanalyse durch CleverReach-Newsletter erhalten Sie
unter:
https://www.cleverreach.com/de/funktionen/reporting-und-tracking/.
Die Datenverarbeitung erfolgt auf Grundlage Ihrer Einwilligung (Art. 6
Abs. 1 lit. a DSGVO). Sie können diese Einwilligung jederzeit
widerrufen, indem Sie den Newsletter abbestellen. Die Rechtmäßigkeit der
bereits erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf
unberührt.
Wenn Sie keine Analyse durch CleverReach wollen, müssen Sie den
Newsletter abbestellen. Hierfür stellen wir in jeder Newsletternachricht
einen entsprechenden Link zur Verfügung.
Die von Ihnen zum Zwecke des Newsletter-Bezugs bei uns hinterlegten
Daten werden von uns bis zu Ihrer Austragung aus dem Newsletter bei uns
bzw. dem Newsletterdiensteanbieter gespeichert und nach der Abbestellung
des Newsletters aus der Newsletterverteilerliste gelöscht. Daten, die zu
anderen Zwecken bei uns gespeichert wurden, bleiben hiervon unberührt.
Nach Ihrer Austragung aus der Newsletterverteilerliste wird Ihre
E-Mail-Adresse bei uns bzw. dem Newsletterdiensteanbieter ggf. in einer
Blacklist gespeichert, um künftige Mailings zu verhindern. Die Daten aus
der Blacklist werden nur für diesen Zweck verwendet und nicht mit
anderen Daten zusammengeführt. Dies dient sowohl Ihrem Interesse als
auch unserem Interesse an der Einhaltung der gesetzlichen Vorgaben beim
Versand von Newslettern (berechtigtes Interesse im Sinne des Art. 6 Abs.
1 lit. f DSGVO). Die Speicherung in der Blacklist ist zeitlich nicht
befristet. SIE KÖNNEN DER SPEICHERUNG WIDERSPRECHEN, SOFERN IHRE
INTERESSEN UNSER BERECHTIGTES INTERESSE ÜBERWIEGEN.
Näheres entnehmen Sie den Datenschutzbestimmungen von CleverReach unter:
https://www.cleverreach.com/de/datenschutz/.
Abschluss eines Vertrags über Auftragsverarbeitung
Wir haben mit dem Anbieter von CleverReach einen Vertrag zur
Auftragsverarbeitung abgeschlossen und setzen die strengen Vorgaben der
deutschen Datenschutzbehörden bei der Nutzung von CleverReach
vollständig um.
7. Plugins und Tools
Dublin 4, Irland.
Wir nutzen YouTube im erweiterten Datenschutzmodus. Dieser Modus bewirkt
laut YouTube, dass YouTube keine Informationen über die Besucher auf
dieser Website speichert, bevor diese sich das Video ansehen. Die
Weitergabe von Daten an YouTube-Partner wird durch den erweiterten
Datenschutzmodus hingegen nicht zwingend ausgeschlossen. So stellt
YouTube – unabhängig davon, ob Sie sich ein Video ansehen – eine
Verbindung zum Google DoubleClick-Netzwerk her.
Verbindung zu den Servern von YouTube hergestellt. Dabei wird dem
YouTube-Server mitgeteilt, welche unserer Seiten Sie besucht haben. Wenn
Sie in Ihrem YouTube-Account eingeloggt sind, ermöglichen Sie YouTube,
Ihr Surfverhalten direkt Ihrem persönlichen Profil zuzuordnen. Dies
können Sie verhindern, indem Sie sich aus Ihrem YouTube-Account
ausloggen.
Des Weiteren kann YouTube nach Starten eines Videos verschiedene Cookies
auf Ihrem Endgerät speichern oder vergleichbare
Wiedererkennungstechnologien (z.B. Device-Fingerprinting) einsetzen. Auf
erhalten. Diese Informationen werden u. a. verwendet, um
Videostatistiken zu erfassen, die Anwenderfreundlichkeit zu verbessern
und Betrugsversuchen vorzubeugen.
Gegebenenfalls können nach dem Start eines YouTube-Videos weitere
Datenverarbeitungsvorgänge ausgelöst werden, auf die wir keinen Einfluss
haben.
Die Nutzung von YouTube erfolgt im Interesse einer ansprechenden
Darstellung unserer Online-Angebote. Dies stellt ein berechtigtes
Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar. Sofern eine
entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung
ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die
Einwilligung ist jederzeit widerrufbar.
Weitere Informationen über Datenschutz bei YouTube finden Sie in deren
Datenschutzerklärung unter: https://policies.google.com/privacy?hl=de.
Vimeo ohne Tracking (Do-Not-Track)
Diese Website nutzt Plugins des Videoportals Vimeo. Anbieter ist die
Vimeo Inc., 555 West 18th Street, New York, New York 10011, USA.
Wenn Sie eine unserer mit Vimeo-Videos ausgestatteten Seiten besuchen,
wird eine Verbindung zu den Servern von Vimeo hergestellt. Dabei wird
dem Vimeo-Server mitgeteilt, welche unserer Seiten Sie besucht haben.
Zudem erlangt Vimeo Ihre IP-Adresse. Wir haben Vimeo jedoch so
eingestellt, dass Vimeo Ihre Nutzeraktivitäten nicht nachverfolgen und
keine Cookies setzen wird.
Die Nutzung von Vimeo erfolgt im Interesse einer ansprechenden
Darstellung unserer Online-Angebote. Dies stellt ein berechtigtes
Interesse im Sinne des Art. 6 Abs. 1 lit. f DSGVO dar. Sofern eine
entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung
ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die
Einwilligung ist jederzeit widerrufbar.
der EU-Kommission sowie nach Aussage von Vimeo auf „berechtigte
Geschäftsinteressen“ gestützt. Details finden Sie hier:
https://vimeo.com/privacy.
Weitere Informationen zum Umgang mit Nutzerdaten finden Sie in der
Datenschutzerklärung von Vimeo unter: https://vimeo.com/privacy.
Audio- und Videokonferenzen
Datenverarbeitung
Für die Kommunikation mit unseren Kunden setzen wir unter anderen
Online-Konferenz-Tools ein. Die im Einzelnen von uns genutzten Tools
sind unten aufgelistet. Wenn Sie mit uns per Video- oder Audiokonferenz
via Internet kommunizieren, werden Ihre personenbezogenen Daten von uns
und dem Anbieter des jeweiligen Konferenz-Tools erfasst und verarbeitet.
Die Konferenz-Tools erfassen dabei alle Daten, die Sie zur Nutzung der
Tools bereitstellen/einsetzen (E-Mail-Adresse und/oder Ihre
Telefonnummer). Ferner verarbeiten die Konferenz-Tools die Dauer der
Konferenz, Beginn und Ende (Zeit) der Teilnahme an der Konferenz, Anzahl
der Teilnehmer und sonstige „Kontextinformationen“ im Zusammenhang mit
dem Kommunikationsvorgang (Metadaten).
Des Weiteren verarbeitet der Anbieter des Tools alle technischen Daten,
die zur Abwicklung der Online-Kommunikation erforderlich sind. Dies
Betriebssystemtyp und -version, Client-Version, Kameratyp, Mikrofon oder
Lautsprecher sowie die Art der Verbindung.
Sofern innerhalb des Tools Inhalte ausgetauscht, hochgeladen oder in
sonstiger Weise bereitgestellt werden, werden diese ebenfalls auf den
Servern der Tool-Anbieter gespeichert. Zu solchen Inhalten zählen
insbesondere Cloud-Aufzeichnungen, Chat-/ Sofortnachrichten, Voicemails
hochgeladene Fotos und Videos, Dateien, Whiteboards und andere
Informationen, die während der Nutzung des Dienstes geteilt werden.
Bitte beachten Sie, dass wir nicht vollumfänglich Einfluss auf die
Datenverarbeitungsvorgänge der verwendeten Tools haben. Unsere
Möglichkeiten richten sich maßgeblich nach der Unternehmenspolitik des
jeweiligen Anbieters. Weitere Hinweise zur Datenverarbeitung durch die
Konferenztools entnehmen Sie den Datenschutzerklärungen der jeweils
eingesetzten Tools, die wir unter diesem Text aufgeführt haben.
Die Konferenz-Tools werden genutzt, um mit angehenden oder bestehenden
Vertragspartnern zu kommunizieren oder bestimmte Leistungen gegenüber
unseren Kunden anzubieten (Art. 6 Abs. 1 S. 1 lit. b DSGVO). Des
Weiteren dient der Einsatz der Tools der allgemeinen Vereinfachung und
Beschleunigung der Kommunikation mit uns bzw. unserem Unternehmen
eine Einwilligung abgefragt wurde, erfolgt der Einsatz der betreffenden
Tools auf Grundlage dieser Einwilligung; die Einwilligung ist jederzeit
Die unmittelbar von uns über die Video- und Konferenz-Tools erfassten
Daten werden von unseren Systemen gelöscht, sobald Sie uns zur Löschung
auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck
für die Datenspeicherung entfällt. Gespeicherte Cookies verbleiben auf
Ihrem Endgerät, bis Sie sie löschen. Zwingende gesetzliche
Aufbewahrungsfristen bleiben unberührt.
Auf die Speicherdauer Ihrer Daten, die von den Betreibern der
Konferenz-Tools zu eigenen Zwecken gespeichert werden, haben wir keinen
Einfluss. Für Einzelheiten dazu informieren Sie sich bitte direkt bei
den Betreibern der Konferenz-Tools.
Eingesetzte Konferenz-Tools
Wir setzen folgende Konferenz-Tools ein:
GoToMeeting
Wir nutzen GoToMeeting. Anbieter ist die LogMeIn, Inc., 320 Summer
Street Boston, MA 02210, USA. Details zur Datenverarbeitung entnehmen
Sie der Datenschutzerklärung von GoToMeeting:
https://www.logmeininc.com/de/legal/privacy.
Abschluss eines Vertrags über Auftragsverarbeitung
Wir haben mit dem Anbieter von GoToMeeting einen Vertrag zur
Auftragsverarbeitung abgeschlossen und setzen die strengen Vorgaben der
deutschen Datenschutzbehörden bei der Nutzung von GoToMeeting
vollständig um.
LIZENZPRÜFUNG DURCH DIE SOFTWARES F4ANALYSE UND F4TRANSKRIPT
Die Softwares f4transkript und f4analyse prüfen beim Start die
Legitimatität der Aktivierung. Hierzu wird ein anonymer Hash-Wert samt
Lizenznummer an unseren Aktivierungsserver übermittelt.
DATENERHEBUNG -VERWENDUNG UND -LÖSCHUNG IM RAHMEN EINER AUTOMATISIERTEN ERSTELLUNG VON TRANSKRIPTEN “SPRACHERKENNUNG”
11.1 Registrierung
Wenn Sie sich bei uns als nutzende Person (Kunde/Kundin) registrieren,
erheben wir von Ihnen die folgenden Pflichtangaben (nachfolgend:
Die Registrierung ist notwendig, da auf diesem Wege zugleich der
Rahmenvertrag über die Erbringung der Transkriptionsleistung und ein
Vertrag über die Auftragsverarbeitung geschlossen wird. Zugleich dient
die Registrierung dem Zwecke der Zuordnung von Auftragsdaten und der
Abrechnung des Transkriptionsdienstes. Schließlich wird Ihnen im Rahmen
der Registrierung – sofern dies aufgrund Ihrer Tätigkeit erforderlich
ist – eine zusätzliche Vereinbarung zur Geheimhaltung nach Maßgabe von §
203 StGB bereitgestellt.
Die Verarbeitung Ihrer personenbezogenen Daten im Rahmen der
Registrierung und erfolgt auf Basis von Art. 6 Abs. 1 S. 1 lit. b DSGVO.
11.2 Vertragsabwicklung / Transkription
AUFGRUND DER AUTOMATISIERTEN TRANSKRIPTION ERHALTEN WIR GRDS. KEINE
KENNTNIS VON DEM INHALT DER VON IHNEN BEREITGESTELLTEN AUDIODATEN. DIESE
WERDEN GRUNDSÄTZLICH FÜR UNS UNZUGÄNGLICH, VERSCHLÜSSELT GESPEICHERT.
ALLERDINGS IST ES IM RAHMEN DES TRANSKRIPTIONSVORGANGS UND DES HOSTINGS
ZUMINDEST TECHNISCH NICHT AUSGESCHLOSSEN, DASS EIN ZUGRIFF ERFOLGT.
SELBST WENN ABER EIN ZUGRIFF ERFOLGEN SOLLTE, HABEN WIR REGELMÄSSIG
KEINE MÖGLICHKEIT DIE AUDIODATEN MIT EINER BESTIMMTEN PERSON IN
ZUSAMMENHANG ZU BRINGEN. DIES WÄRE I.D.R. NUR DANN DER FALL, WENN Z.B.
IM RAHMEN EINES INTERVIEWS DIE BETEILIGTEN PERSONEN NAMENTLICH GENANNT
WERDEN.
Da aus diesem Grund eine Identifikation einer natürlichen Person und
damit einer Verarbeitung personenbezogener Daten nicht ausgeschlossen
ist, schließen wir mit Ihnen einen Vertrag über die Auftragsverarbeitung
und konkretisieren damit die Verpflichtungen der Parteien zum
Datenschutz, die sich aus dem Hauptvertrag ergeben. Die Verarbeitung der
von Ihnen bereitgestellten Audiodaten und der entsprechenden Textdateien
Vertrags über die Auftragsverarbeitung.
Die Verarbeitung der sonstigen Daten zur Vertragsabwicklung wie z.B.
Datum und Anzahl der Aufträge, Dateigröße, Datum des Uploads und des
Downloads (nachfolgend einheitlich: „ERGÄNZENDE INFORMATIONEN ZU DEN
AUFTRAGSDATEN“) erfolgt auf Basis von Art. 6 Abs. 1 S. 1 lit. b DSGVO.
11.3 Dauer der Datenspeicherung und Datenlöschung
dauerhaft auf dem Spracherkennungsserver gespeichert. Die Löschung der
Vertragsdaten erfolgt bei der Löschung des Accounts, sofern der Löschung
keine vertraglichen und/oder gesetzlichen Aufbewahrungsfristen
entgegenstehen.
durch Sie oder bis zum Verstreichen der vertraglich vereinbarten
Löschfrist gespeichert und sodann automatisch gelöscht.
die Abwicklung und Abrechnung der Einzelaufträge zu ermöglichen und
diese zu dokumentieren. Diese Daten werden für die Nachvollziehbarkeit
für Sie und die Dokumentation möglicher Ansprüche so lange gespeichert,
wie der Account aktiv ist. Mit der Löschung des Accounts werden diese
Daten gelöscht.
Wir geben Ihre personenbezogenen Daten nur an Dritte oder sonstige
Empfänger weiter, wenn dies zur Leistungserbringung erforderlich ist,
Sie Ihre Einwilligung erteilt haben, eine gesetzliche Verpflichtung
besteht oder die Datenweitergabe aufgrund einer sonstigen gesetzlichen
Grundlage zulässig ist. Eine Datenweitergabe erfolgt beispielsweise an
den technischen Dienstleister bzw. den Hosting Anbieter oder – im Falle
eines Unternehmenstransaktion – an Interessenten/Käufer etc. Soweit
erforderlich, haben wir mit den Empfängern Ihrer Daten Vereinbarungen
11.5 Kontaktmöglichkeiten und Ihre Rechte
Nehmen Sie mit uns z.B. über ein Kontaktformular Verbindung auf, werden
Ihre Angaben gespeichert, damit auf diese zur Bearbeitung Ihrer Anfrage
zurückgegriffen werden kann.
Die Rechtsgrundlage für die Verarbeitung Ihrer Daten richtet sich grds.
nach Art. 6 Abs. 1 S. 1 lit. f DSGVO. Unser berechtigtes Interesse
besteht dann in der Beantwortung Ihrer Anfrage. Im Falle der
Durchführung vorvertraglicher Maßnahmen ist die Rechtsgrundlage Art. 6
Abs. 1 S. 1 lit. b DSGVO.
Als betroffene Person haben Sie hinsichtlich der VERTRAGSDATEN und der
ERGÄNZENDEN INFORMATIONEN ZU DEN AUFTRAGSDATEN die unter Ziff. 7 in
dieser Datenschutzerklärung dargestellten Rechte. Im Übrigen wenden Sie
sich im Hinblick auf die AUFTRAGSDATEN bitte an den jeweiligen
Verantwortlichen.
FÜR SIE ERREICHBAR
Mo – Fr 9.00 – 15.00 Uhr
info@audiotranskription.de
audiotranskription
dr.dresing & pehl GmbH
Deutschhausstrasse 22a
Software kaufen
Hardware kaufen
Themen
Rechtliches
Aktuelles
Ihr Warenkorb ist leerZurück zum Shop
