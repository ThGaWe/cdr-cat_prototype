IMPRESSUM
IMPRESSUM
DR. DRESING & PEHL GMBH
Deutschhausstrasse 22a
Deutschland
Telefon: 49 6421 – 590 979 0
Geschäftsführer: Dr. Thorsten Dresing, Thorsten Pehl
Umsatzsteuer-Identifikationsnummer: DE 252 446 309
Registrierungsnummer der EAR: WEEE-Reg.-Nr. DE 36759830.
Informationen zur Erfüllung der quantitativen Zielvorgaben zu Altgeräten
nach §§ 10 Abs. 3, 22 Abs. 1 ElektroG finden Sie auf der Seite des
Bundesumweltministeriums.
Verantwortlich gemäß § 18 MStV:
Thorsten Pehl, Thorsten Dresing
Die Europäische Kommission stellt eine Plattform zur
Online-Streitbeilegung (OS) bereit, die Sie hier finden
https://ec.europa.eu/consumers/odr/. Zur Teilnahme an einem
Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle sind
wir nicht verpflichtet und nicht bereit.
Alternative Streitbeilegung gemäß Art. 14 Abs. 1 ODR-VO und § 36 VSBG:
Die Europäische Kommission stellt eine Plattform zur
Online-Streitbeilegung (OS) bereit, die du unter
https://ec.europa.eu/consumers/odr findest. Zur Teilnahme an einem
Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle sind
wir nicht verpflichtet und nicht bereit.
FÜR SIE ERREICHBAR
Mo – Fr 9.00 – 15.00 Uhr
info@audiotranskription.de
audiotranskription
dr.dresing & pehl GmbH
Deutschhausstrasse 22a
Software kaufen
Hardware kaufen
Themen
Rechtliches
Aktuelles
Ihr Warenkorb ist leerZurück zum Shop
