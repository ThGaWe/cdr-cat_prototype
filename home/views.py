import os
import math
from django.http import HttpResponseRedirect, JsonResponse

from home.imports import *

import home.ruleset.access as rulesAccess
import home.ruleset.awareness as rulesAwareness
import home.ruleset.transparency as rulesTransparency
import home.ruleset.financial as rulesFinancial
import home.ruleset.security as rulesSecurity
import home.ruleset.privacy as rulesPrivacy
import home.ruleset.compensation as rulesCompensation
import home.ruleset.participation as rulesParticipation

import home.analyze_ruleset.access as analyzeAccess
import home.analyze_ruleset.awareness as analyzeAwareness
import home.analyze_ruleset.transparency as analyzeTransparency
import home.analyze_ruleset.financial as analyzeFinancial
import home.analyze_ruleset.security as analyzeSecurity
import home.analyze_ruleset.privacy as analyzePrivacy
import home.analyze_ruleset.compensation as analyzeCompensation
import home.analyze_ruleset.participation as analyzeParticipation


def index(request):
    """
    Subpage to index (first site appearing after someone opens the website)
    :param request: url request to get subpage /
    :return: rendering the subpage based on index.html
    """
    return render(request, 'index.html')


def analyzing(request):
    """
    Main function to execute all submodules
    :param request: URL request to filter transfered URL parameters
    :return: JSON response containing all results, or directly render results with analyzing.html
    """
    # Extract URL parameters
    if '?' in request.get_full_path():
        url_params = request.get_full_path().split('?')[-1]
        params = url_params.split('&')
    else:
        params = []
    param_dict = {
        'cos': [],
        'comps': [],
        'docs': []
    }

    # Add parameters into the param_dict
    if len(params) > 0:
        for param in params:
            name = param.split('=')[0]
            param_list = param.split('=')[1].split(';')

            if param_list == ['']:
                param_dict[name] = []
            else:
                param_dict[name] = param_list

    # Return the parameters to the program to work with. You can change the input by manipulating the URL
    # Select if you want to execute only over company or service documents. Leave empty if you want to execute both
    select_companies_or_services = param_dict['cos']
    # Select which companies to execute. Leave empty if you want to execute over the full dataset
    select_companies = param_dict['comps']   # 'edding', 'telekom', 'ottogroup', 'medion', 'tom-tailor', 'aboutyou'  /  'trivago'
    # Select which documents to execute. You can add specific document names, mandatory, optional or leave empty for all documents of the company
    select_documents = param_dict['docs']
    # Add the name of new rules in the right dimension to include them in execution
    select_rules = {
        'access': [
            'get_language_check',
        ],
        'awareness': [
            'get_eco_friendly',
            'get_energy_efficient',
        ],
        'transparency': [
            'get_average_num_char',
            'get_average_sent_len',
            'get_average_comma_count',
            'get_lix',
            'get_token_ratio',
            'get_verb_ratio',
            'get_noun_ratio',
            'get_verb_noun_ratio',
        ],
        'financial': [],
        'security': [
            'get_certificates',
        ],
        'privacy': [
            # 'privacy_test',
            'get_saved_data',
        ],
        'compensation': [
            'get_contact_formular',
            'get_contact_email',
            'get_contact_phone',
            'get_contact_privacy',
        ],
        'participation': [
            'get_survey',
            'get_newsletter',
        ]
    }

    # Create a new doc if the configuration changed or load the saved doc from disk if not
    if helperFunction.new_doc('WebsiteData/doc_config.json', select_companies_or_services, select_companies, select_documents):
        doc = get_doc(select_companies_or_services, select_companies, select_documents)
        helperFunction.save_doc('WebsiteData/doc.pkl', doc)
    else:
        doc = helperFunction.open_doc('WebsiteData/doc.pkl')

    # Execute all rules in select_rules for all selected companies and services
    results_companies = execute_rules(select_rules, doc['companies'])
    results_services = execute_rules(select_rules, doc['services'])

    # Select all companies that will be included into analysis
    if len(select_companies) == 0:
        select_companies = helperFunction.get_company_names(list(doc['companies'].keys()), list(doc['services'].keys()))

    # Interpret all results from rule execution
    interpretation = analyze_results(select_companies_or_services, select_companies, select_documents, select_rules, results_companies, results_services)

    # Only relevant if you want to represent the results in analyzing.html
    context = {
        'Companies': results_companies,
        'Services': results_services,
        'Interpretation': get_result_interpretation(select_rules)
    }

    # Returns interpreted results as JSON Response. Comment if other return statement is used
    return JsonResponse(interpretation)
    # Returns uninterpreted results to analyzing.html. Comment if other return statement is used
    # return render(request, 'analyzing.html', context)


def get_doc(sector, companies, documents):
    """
    Loads all documents from the WebsiteData folder, creates Spacy Docs and returns them as a structured dictionary
    :param sector: Contains the URL parameter cos
    :param companies: Contains the URL parameter comps
    :param documents: Contains the URL parameter docs
    :return: Dictionary with all Spacy Docs of all documents of the selected companies
    """
    doc = {
        'companies': {},
        'services': {}
    }

    # Iterate through directories and files inside the WebsiteData folder
    for subdir, dirs, files in os.walk('WebsiteData'):
        if 'mandatory' in subdir or 'optional' in subdir:
            if os.name == 'nt':
                website = subdir.split('\\')
            else:
                website = subdir.split('/')

            # Unique name of a company. Is the same for company and service websites
            company_name = website[2].split(".")[math.floor((len(website[2].split(".")) - 1) / 2)]
            # Defines if the file is from a company or a service website
            company_or_service = website[1]
            # Defines if the file is mandatory or optional
            mandatory_or_optional = website[3]
            # Defines if the file is German or English
            language = website[2][-2:].lower()

            # Add a new company to the dictionary
            if company_name not in doc[company_or_service].keys():
                print("\033[93m" + company_name + "_" + language + " - " + company_or_service + ": " + mandatory_or_optional + " complete" + "\033[0m")

                # Structure of the returning dictionary
                company_doc = {
                    'de': {
                        'mandatory': {
                            'full': Doc(nlp.vocab)
                        },
                        'optional': {
                            'full': Doc(nlp.vocab)
                        }
                    },
                    'en': {
                        'mandatory': {
                            'full': Doc(nlp.vocab)
                        },
                        'optional': {
                            'full': Doc(nlp.vocab)
                        }
                    }
                }
            # Load the dictionary of the already existing company
            else:
                company_doc = doc[company_or_service][company_name]

            # Iterate through all files and reate Spacy Docs from the found files
            if (len(sector) == 0 or company_or_service in sector) and (len(companies) == 0 or company_name in companies):
                for subfile in files:
                    # Only include text files, since those are the relevant ones
                    if '.txt' in subfile:
                        # Defines the path of the file
                        if os.name == 'nt':
                            file_path = (subdir + '\\' + subfile)
                        else:
                            file_path = (subdir + '/' + subfile)
                        # Defines the name of the file
                        file_name = subfile.replace('.txt', '')
                        # Variable to store the text of the file
                        file_text = ''

                        # Create Spacy Docs from the found files
                        if len(documents) == 0 or file_name in documents or mandatory_or_optional in documents:
                            with open(file_path, errors='replace', encoding='utf-8') as curr_file:
                                for line in curr_file:
                                    file_text += line
                                curr_file.close()

                            # Create Spacy Docs from the content of the files
                            spacy_doc = nlp(file_text)

                            # Add Spacy Doc and file path to resulting dictionary
                            company_doc[language][mandatory_or_optional][file_name] = (file_path, spacy_doc)
                            # Create a Spacy Doc with all tokens from all mandatory or optional documents
                            company_doc[language][mandatory_or_optional]['full'] = Doc.from_docs(
                                [company_doc[language][mandatory_or_optional]['full'], spacy_doc])

                doc[company_or_service][company_name] = company_doc

    return doc


def execute_rules(rules, companies):
    """
    Automatically executes all rules in dictionary rules
    :param rules: Dictionary with all rules to execute
    :param companies: The data from company documents to execute the rules with
    :return: Dictionary with results of every rule for every company
    """
    result = {
        'access': {},
        'awareness': {},
        'transparency': {},
        'financial': {},
        'security': {},
        'privacy': {},
        'compensation': {},
        'participation': {}
    }

    for company in companies:
        for rule_category in rules:
            # Get CDR dimension of rule
            rule_function = rule_category[0].upper() + rule_category[1:]
            rule_function = 'rules' + rule_function

            # Execute rule
            for rule in rules[rule_category]:
                curr_rule = getattr(eval(rule_function), rule)
                result_rule = curr_rule(companies[company])

                if rule in result[rule_category].keys():
                    result[rule_category][rule][company] = result_rule
                else:
                    result[rule_category][rule] = {company: result_rule}

    return result


def analyze_results(companies_or_services, companies, documents, rules, results_companies, results_services):
    """
    Automatically executes the analysis part
    :param companies_or_services: Contains the URL parameter cos
    :param companies: Contains the URL parameter comps
    :param documents: Contains the URL parameter docs
    :param rules: Dictionary with all rules to execute
    :param results_companies: Contains the results of the rule execution for all companies
    :param results_services: Contains the results of the rule execution for all services
    :return:
    """
    result = {}

    if len(companies_or_services) == 0:
        companies_or_services = ['companies', 'services']
    if len(documents) == 0:
        documents = 'full'

    companies_or_services = 'Companies/Services: ' + str(companies_or_services)
    documents = 'Documents: ' + str(documents)

    for company in companies:
        result[company] = {}

        for category in rules:
            analyze_class = 'analyze' + category[0].upper() + category[1:]
            analyze_function = 'analyze_' + category + '_results'

            if hasattr(eval(analyze_class), analyze_function):
                curr_func = getattr(eval(analyze_class), analyze_function)
                result[company][category] = curr_func(company, (companies_or_services, documents), category, rules[category], results_companies, results_services, len(companies) == 0)
            else:
                result[company][category] = {}

    return result


def get_result_interpretation(rules):
    """
    A rule I used to test the interpretation. But the functionality moved to the documents of the analyze_results folder
    :param rules: Dictionary with all rules to execute
    :return: Textual interpretation of the rules get_contact_formular, get_lix and saved_data
    """
    result = {}

    for category in rules:
        result[category] = {}
        for rule in rules[category]:
            if category == 'access' and rule == 'get_contact_formular':
                result[category][rule] = {
                    'name': 'Kontaktformular',
                    'type': 'boolean',
                    'values': {
                        'True': 'Auf der Webseite befindet sich ein Kontaktformular',
                        'False': 'Auf der Webseite befindet sich kein Kontaktformular',
                        'None': 'Bitte Datenschutzerklärung hinzufügen'
                    },
                    'description': 'Überprüft ob auf der Webseite ein Kontaktformular vorhanden ist'
                }
            elif category == 'transparency' and rule == 'get_lix':
                result[category][rule] = {
                    'name': 'Lesbarkeitsindex',
                    'type': 'number',
                    'values': {
                        '0': 'Bitte Datenschutzerklärung, Impressum und AGB hinzufügen',
                        '40': 'Vergleichbar mit Kinder- und Jugendliteratur',
                        '50': 'Vergleichbar mit Belletristik',
                        '60': 'Vergleichbar mit Sachliteratur',
                        '9999': 'Vergleichbar mit Fachliteratur'
                    },
                    'description': 'Der Lesbarkeitsindex zeigt, wie gut lesbar ein Text ist'
                }
            elif category == 'privacy' and rule == 'saved_data':
                result[category][rule] = {
                    'name': 'Datenspeicherung',
                    'type': 'tuple',
                    'values': {
                        'text': 'Folgende Daten werden gesammelt: ',
                        'True': 'Werden wieder gelöscht',
                        'False': 'Werden nicht wieder gelöscht',
                        'None': 'Bitte Datenschutzerklärung hinzufügen'
                    },
                    'description': 'Zeigt welche personenbezogenen Daten gesammelt werden und ob diese auch wieder gelöscht werden'
                }
            else:
                result[category][rule] = None

    return result
