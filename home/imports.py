import os

import home.helperFunctions as helperFunction

from django.shortcuts import render
import spacy
from spacy.matcher import Matcher
from spacy.tokens import Doc

nlp = spacy.load("de_core_news_sm")
matcher = Matcher(nlp.vocab)