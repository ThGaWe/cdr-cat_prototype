from home.imports import *


def analyze_transparency_results(company, config, category, rules, results_companies, results_services, save):
    """
    Executes the analysis part for this CDR dimension
    :param company: The company that currently gets analyzed
    :param config: The current configuration
    :param category: The CDR dimension of the current rule
    :param rules: The rules from the current CDR dimension
    :param results_companies: The results of the company website of the current company
    :param results_services: The results of the service website of the current company
    :param save: True if execution runs over full dataset, False otherwise
    :return: Dictionary with all interpreted results of the company of this CDR dimension
    """
    root_path = 'home/analyze_ruleset/results/transparency/'
    result = {
        'score': 0
    }

    # Analyze, interpret and save results
    for rule in rules:
        rule_path = root_path + rule + '.txt'
        new_rule = rule.replace('get', 'analyze')

        if new_rule in globals():
            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, config)

            analyze_rule = eval(new_rule)
            analyze_result = analyze_rule(save, company, results_companies[category][rule], results_services[category][rule])
            result[rule] = analyze_result[1]

            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, result[0])

    # Determine the overall score of this CDR dimension
    amount = 0
    for rule in result:
        if not rule == 'score':
            result['score'] += result[rule]['weight'] * result[rule]['score']
            amount += result[rule]['weight']
    result['score'] = result['score'] / amount

    return result


def analyze_average_num_char(analyze, company, num_char_companies, num_char_services):
    """
    Analyzes and interprets the results of the rule get_average_num_char(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param num_char_companies: Results from company websites
    :param num_char_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in num_char_companies:
        num_char_companies[company] = 0
    if company not in num_char_services:
        num_char_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Wortlänge',
        'description': 'Es wird die durchschnittliche Wortlänge in Datenschutzerklärung, Impressum und AGB ermittelt. Längere Worte können ein Anzeichen für schlechtere Lesbarkeit und geringere Transparenz sein.',
        'result': (num_char_companies[company], num_char_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if num_char_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Datenschutzerklärung, Impressum und AGB konnten nicht gefunden werden.'
        interpretation_result['recommendation'] = 'Bitte Vorhandensein von Datenschutzerklärung, Impressum und AGB prüfen.'
    elif 0 < num_char_companies[company] <= 6:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Unterdurchschnittliche Wortlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sehr gut, kurze Worte deuten auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 6 < num_char_companies[company] <= 7:
        interpretation_result['score'] = 4
        interpretation_result['interpretation'] = 'Leicht unterdurchschnittliche Wortlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Gut, kurze Worte deuten auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 7 < num_char_companies[company] <= 8:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Mittlere durchschnittliche Wortlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sie können die Transparent Ihrer Texte noch weiter verbessern, indem einfachere, kürzere Worte verwendet werden.'
    elif 8 < num_char_companies[company] <= 10:
        interpretation_result['score'] = 2
        interpretation_result['interpretation'] = 'Leicht überdurchschnittliche Wortlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Längere Worte deuten auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie können die Transparent Ihrer Texte verbessern, indem einfachere, kürzere Worte verwendet werden.'
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Stark überdurchschnittliche Wortlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sehr lange Worte deuten auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie können die Transparent Ihrer Texte verbessern, indem einfachere, kürzere Worte verwendet werden.'

    if analyze:
        all_comps = list(num_char_companies.items()) + list(num_char_services.items())
        comp_count = 0

        min_num_char = ('', 9999999999)
        max_num_char = ('', 0)
        average_num_char = 0

        for company, avg in all_comps:
            if avg != 0:
                comp_count += 1

                if avg < min_num_char[1]:
                    if company in num_char_companies and avg == num_char_companies[company]:
                        min_num_char = ('c-' + company, avg)
                    else:
                        min_num_char = ('s-' + company, avg)
                if avg > max_num_char[1]:
                    if company in num_char_companies and avg == num_char_companies[company]:
                        max_num_char = ('c-' + company, avg)
                    else:
                        max_num_char = ('s-' + company, avg)

                average_num_char += avg

        average_num_char = average_num_char/comp_count

        min_num_char = 'Min number of characters: ' + str(min_num_char)
        max_num_char = 'Max number of characters: ' + str(max_num_char)
        average_num_char = 'Average number of characters: ' + str(average_num_char) + '\n'

        analyze_result = (min_num_char, max_num_char, average_num_char, '\n')
    else:
        analyze_result = ()

    return analyze_result, interpretation_result


def analyze_average_sent_len(analyze, company, sent_len_companies, sent_len_services):
    """
    Analyzes and interprets the results of the rule get_average_sent_len(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param sent_len_companies: Results from company websites
    :param sent_len_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in sent_len_companies:
        sent_len_companies[company] = 0
    if company not in sent_len_services:
        sent_len_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Satzlänge',
        'description': 'Es wird die durchschnittliche Anzahl an Worten pro Satz in Datenschutzerklärung, Impressum und AGB ermittelt. Längere Sätze können ein Anzeichen für schlechtere Lesbarkeit und geringere Transparenz sein.',
        'result': (sent_len_companies[company], sent_len_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if sent_len_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Datenschutzerklärung, Impressum und AGB konnten nicht gefunden werden.'
        interpretation_result['recommendation'] = 'Bitte Vorhandensein von Datenschutzerklärung, Impressum und AGB prüfen.'
    elif sent_len_companies[company] < 15:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Unterdurchschnittliche Satzlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sehr gut, kurze Sätze deuten auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 15 <= sent_len_companies[company] <= 20:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Leicht unterdurchschnittliche Satzlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Gut, kurze Sätze deuten auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 20 < sent_len_companies[company] <= 25:
        interpretation_result['score'] = 4
        interpretation_result['interpretation'] = 'Mittlere durchschnittliche Satzlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sie können die Transparenz Ihrer Texte noch weiter verbessern, indem einfachere, kürzere Sätze verwendet werden.'
    elif 25 < sent_len_companies[company] <= 30:
        interpretation_result['score'] = 2
        interpretation_result['interpretation'] = 'Leicht überdurchschnittliche Satzlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Längere Sätze deuten auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie können die Transparenz Ihrer Texte verbessern, indem einfachere, kürzere Sätze verwendet werden.'
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Stark überdurchschnittliche Satzlänge in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sehr lange Sätze deuten auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie können die Transparenz Ihrer Texte verbessern, indem einfachere, kürzere Sätze verwendet werden.'

    if analyze:
        all_comps = list(sent_len_companies.items()) + list(sent_len_services.items())
        comp_count = 0

        min_sent_len = ('', 9999999999)
        max_sent_len = ('', 0)
        average_sent_len = 0

        for company, avg in all_comps:
            if avg != 0:
                comp_count += 1

                if avg < min_sent_len[1]:
                    if company in sent_len_companies and avg == sent_len_companies[company]:
                        min_sent_len = ('c-' + company, avg)
                    else:
                        min_sent_len = ('s-' + company, avg)
                if avg > max_sent_len[1]:
                    if company in sent_len_companies and avg == sent_len_companies[company]:
                        max_sent_len = ('c-' + company, avg)
                    else:
                        max_sent_len = ('s-' + company, avg)

                average_sent_len += avg

        average_sent_len = average_sent_len/comp_count

        min_sent_len = 'Min sentence length: ' + str(min_sent_len)
        max_sent_len = 'Max sentence length: ' + str(max_sent_len)
        average_sent_len = 'Average sentence length: ' + str(average_sent_len) + '\n'

        analyze_result = (min_sent_len, max_sent_len, average_sent_len, '\n')
    else:
        analyze_result = ()

    return analyze_result, interpretation_result


def analyze_average_comma_count(analyze, company, comma_companies, comma_services):
    """
    Analyzes and interprets the results of the rule get_average_comma_count(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param comma_companies: Results from company websites
    :param comma_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in comma_companies:
        comma_companies[company] = 0
    if company not in comma_services:
        comma_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Anzahl Kommas',
        'description': 'Es wird die durchschnittliche Anzahl Kommas innerhalb eines Satzes ermittelt. Viele Kommas deuten auf einen Schachtelsatz hin der schwer zu verstehen ist.',
        'result': (comma_companies[company], comma_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if comma_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Datenschutzerklärung, Impressum und AGB konnten nicht gefunden werden.'
        interpretation_result['recommendation'] = 'Bitte Vorhandensein von Datenschutzerklärung, Impressum und AGB prüfen.'
    elif comma_companies[company] < 0.5:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Unterdurchschnittliche Anzahl an Kommas in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sehr gut, wenige Kommas deuten auf optimale Lesbarkeit und leicht verständliche Sätze hin.'
    elif 0.5 <= comma_companies[company] <= 1:
        interpretation_result['score'] = 4
        interpretation_result['interpretation'] = 'Leicht unterdurchschnittliche Anzahl an Kommas in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Gut, wenige Kommas deuten auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 1 < comma_companies[company] <= 1.5:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Mittlere durchschnittliche Anzahl an Kommas in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sie können die Transparenz Ihrer Texte noch weiter verbessern, indem Schachtelsätze durch mehrere kürzere Sätze ersetzt werden.'
    elif 1.5 < comma_companies[company] <= 2:
        interpretation_result['score'] = 2
        interpretation_result['interpretation'] = 'Leicht überdurchschnittliche Anzahl an Kommas in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Schachtelsätze deuten auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie können die Transparenz Ihrer Texte verbessern, indem Schachtelsätze durch mehrere kürzere Sätze ersetzt werden.'
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Stark überdurchschnittliche Anzahl an Kommas in Datenschutzerklärung, Impressum und AGB.'
        interpretation_result['recommendation'] = 'Sehr oft geschachtelte Sätze deuten auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie können die Transparenz Ihrer Texte verbessern, indem Schachtelsätze durch mehrere kürzere Sätze ersetzt werden.'

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_lix(analyze, company, lix_companies, lix_services):
    """
    Analyzes and interprets the results of the rule get_lix(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param lix_companies: Results from company websites
    :param lix_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in lix_companies:
        lix_companies[company] = 0
    if company not in lix_services:
        lix_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Lesbarkeitsindex',
        'description': 'Es wird auf Basis von Wort- und Satzlängen eine allgemeine Einschätzung der Lesbarkeit von Datenschutzerklärung, Impressum und AGB erstellt (LIX).',
        'result': (lix_companies[company], lix_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if lix_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Datenschutzerklärung, Impressum und AGB konnten nicht gefunden werden.'
        interpretation_result['recommendation'] = 'Bitte Vorhandensein von Datenschutzerklärung, Impressum und AGB prüfen.'
    elif lix_companies[company] <= 40:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Einfache Sprache erkannt.'
        interpretation_result['recommendation'] = 'Sehr gut, einfache Sprache deutet auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 40 < lix_companies[company] <= 50:
        interpretation_result['score'] = 4
        interpretation_result['interpretation'] = 'Eher einfache Sprache erkannt.'
        interpretation_result['recommendation'] = 'Gut, einfache Sprache deutet auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 50 < lix_companies[company] <= 60:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Mittelschwere Sprache erkannt (Vergleichbar mit Sachliteratur).'
        interpretation_result['recommendation'] = 'Sie können die Transparent Ihrer Texte noch weiter verbessern, indem einfachere, kürzere Sprache verwendet wird.'
    elif 60 < lix_companies[company] <= 70:
        interpretation_result['score'] = 2
        interpretation_result['interpretation'] = 'Schwere Sprache erkannt (Vergleichbar mit Fachliteratur).'
        interpretation_result['recommendation'] = 'Schwere Sprache deutet auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie können die Transparent Ihrer Texte verbessern, indem einfachere, kürzere Sprache verwendet wird.'
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Sehr schwere Sprache erkannt'
        interpretation_result['recommendation'] = 'Sehr schwere Sprache deutet auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie sollten die Transparent Ihrer Texte verbessern, indem einfachere, kürzere Sprache verwendet wird.'

    if analyze:
        all_comps = list(lix_companies.items()) + list(lix_services.items())
        comp_count = 0

        min_lix = ('', 9999999999)
        max_lix = ('', 0)
        average_lix = 0

        lix_to_easy = 0  # <40
        lix_to_easy_list = []
        lix_medium = 0  # 41-50
        lix_medium_list = []
        lix_hard = 0  # 51-60
        lix_hard_list = []
        lix_very_hard = 0  # 61-70
        lix_very_hard_list = []
        lix_to_hard = 0  # >70
        lix_to_hard_list = []

        for company, lix in all_comps:
            if not lix == 0:
                comp_count += 1

                if lix < min_lix[1]:
                    if company in lix_companies and lix == lix_companies[company]:
                        min_lix = ('c-' + company, lix)
                    else:
                        min_lix = ('s-' + company, lix)
                if lix > max_lix[1]:
                    if company in lix_companies and lix == lix_companies[company]:
                        max_lix = ('c-' + company, lix)
                    else:
                        max_lix = ('s-' + company, lix)

                average_lix += lix

                if lix <= 40:
                    lix_to_easy += 1
                    if company in lix_companies and lix == lix_companies[company]:
                        lix_to_easy_list.append(('c-' + company, lix))
                    else:
                        lix_to_easy_list.append(('s-' + company, lix))
                elif 40 < lix <= 50:
                    lix_medium += 1
                    if company in lix_companies and lix == lix_companies[company]:
                        lix_medium_list.append(('c-' + company, lix))
                    else:
                        lix_medium_list.append(('s-' + company, lix))
                elif 50 < lix <= 60:
                    lix_hard += 1
                    if company in lix_companies and lix == lix_companies[company]:
                        lix_hard_list.append(('c-' + company, lix))
                    else:
                        lix_hard_list.append(('s-' + company, lix))
                elif 60 < lix <= 70:
                    lix_very_hard += 1
                    if company in lix_companies and lix == lix_companies[company]:
                        lix_very_hard_list.append(('c-' + company, lix))
                    else:
                        lix_very_hard_list.append(('s-' + company, lix))
                elif lix > 70:
                    lix_to_hard += 1
                    if company in lix_companies and lix == lix_companies[company]:
                        lix_to_hard_list.append(('c-' + company, lix))
                    else:
                        lix_to_hard_list.append(('s-' + company, lix))

        average_lix = average_lix/comp_count

        # print('\033[91m' + 'Min LIX: ' + '\033[0m' + str(min_lix))
        # print('\033[91m' + 'Max LIX: ' + '\033[0m' + str(max_lix))
        # print('\033[91m' + 'Average LIX: ' + '\033[0m' + str(average_lix) + '\n')

        # print('\033[91m' + 'To Easy Companies: ' + '\033[0m' + str(lix_to_easy) + ', ' + str(lix_to_easy_list))
        # print('\033[91m' + 'Medium Companies: ' + '\033[0m' + str(lix_medium) + ', ' + str(lix_medium_list))
        # print('\033[91m' + 'Hard Companies: ' + '\033[0m' + str(lix_hard) + ', ' + str(lix_hard_list))
        # print('\033[91m' + 'Very Hard Companies: ' + '\033[0m' + str(lix_very_hard) + ', ' + str(lix_very_hard_list))
        # print('\033[91m' + 'To Hard Companies: ' + '\033[0m' + str(lix_to_hard) + ', ' + str(lix_to_hard_list))

        min_lix = 'Min LIX: ' + str(min_lix)
        max_lix = 'Max LIX: ' + str(max_lix)
        average_lix = 'Average LIX: ' + str(average_lix) + '\n'

        lix_to_easy = 'To Easy Companies: ' + str(lix_to_easy) + ', ' + str(lix_to_easy_list)
        lix_medium = 'Medium Companies: ' + str(lix_medium) + ', ' + str(lix_medium_list)
        lix_hard = 'Hard Companies: ' + str(lix_hard) + ', ' + str(lix_hard_list)
        lix_very_hard = 'Very Hard Companies: ' + str(lix_very_hard) + ', ' + str(lix_very_hard_list)
        lix_to_hard = 'To Hard Companies: ' + str(lix_to_hard) + ', ' + str(lix_to_hard_list)

        analyze_result = (min_lix, max_lix, average_lix, lix_to_easy, lix_medium, lix_hard, lix_very_hard, lix_to_hard, '\n')
    else:
        analyze_result = ()

    return analyze_result, interpretation_result


def analyze_token_ratio(analyze, company, ratio_companies, ratio_services):
    """
    Analyzes and interprets the results of the rule get_token_ratio(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param ratio_companies: Results from company websites
    :param ratio_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in ratio_companies:
        ratio_companies[company] = 0
    if company not in ratio_services:
        ratio_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Vokabular-Schwierigkeit',
        'description': 'Es wird geprüft, wie komplex das verwendete Vokabular in Datenschutzerklärung, Impressum und AGB ist.',
        'result': (ratio_companies[company], ratio_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if ratio_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Datenschutzerklärung, Impressum und AGB konnten nicht gefunden werden.'
        interpretation_result['recommendation'] = 'Bitte Vorhandensein von Datenschutzerklärung, Impressum und AGB prüfen.'
    elif 0 < ratio_companies[company] <= 0.2:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Einfaches Vokabular erkannt.'
        interpretation_result['recommendation'] = 'Sehr gut, einfaches Vokabular deutet auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 0.2 < ratio_companies[company] <= 0.4:
        interpretation_result['score'] = 4
        interpretation_result['interpretation'] = 'Eher einfaches Vokabular erkannt.'
        interpretation_result['recommendation'] = 'Gut, einfaches Vokabular deutet auf optimale Lesbarkeit und leicht verständliche Texte hin.'
    elif 0.4 < ratio_companies[company] <= 0.6:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Mittelschweres Vokabular erkannt (Vergleichbar mit Sachliteratur).'
        interpretation_result['recommendation'] = 'Sie können die Transparent Ihrer Texte noch weiter verbessern, indem einfachere, kürzere Sprache verwendet wird.'
    elif 0.6 < ratio_companies[company] <= 0.8:
        interpretation_result['score'] = 2
        interpretation_result['interpretation'] = 'Schweres Vokabular erkannt (Vergleichbar mit Fachliteratur).'
        interpretation_result['recommendation'] = 'Schweres Vokabular deutet auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie können die Transparent Ihrer Texte verbessern, indem einfachere, kürzere Sprache verwendet wird.'
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Sehr schweres Vokabular erkannt'
        interpretation_result['recommendation'] = 'Sehr schweres Vokabular deutet auf schlechtere Lesbarkeit und geringere Transparenz hin. Sie sollten die Transparent Ihrer Texte verbessern, indem einfachere, kürzere Sprache verwendet wird.'

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_verb_ratio(analyze, company, ratio_companies, ratio_services):
    """
    Analyzes and interprets the results of the rule get_verb_ratio(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param ratio_companies: Results from company websites
    :param ratio_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in ratio_companies:
        ratio_companies[company] = 0
    if company not in ratio_services:
        ratio_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Verbrate',
        'description': 'Es wird die Rate an Verben pro Satz geprüft. "Nominalstil" (wenige Verben pro Satz) deuten auf unverständliche, schlecht lesbare Sprache hin.',
        'result': (ratio_companies[company], ratio_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    # TODO: Check which results do we want?
    if ratio_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Datenschutzerklärung, Impressum und AGB konnten nicht gefunden werden.'
        interpretation_result['recommendation'] = 'Bitte Vorhandensein von Datenschutzerklärung, Impressum und AGB prüfen.'
    elif ratio_companies[company] <= 0.03:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Durchschnittliche Anzahl an Verben pro Satz erkannt.'
        interpretation_result['recommendation'] = 'Gut, viele Verben sind im Allgemeinen ein Anzeichen für gut lesbare Sprache.'
    else:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Überdurschschnittliche Anzahl an Verben pro Satz erkannt.'
        interpretation_result['recommendation'] = 'Sehr gut, viele Verben sind im Allgemeinen ein Anzeichen für gut lesbare Sprache'

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_noun_ratio(analyze, company, ratio_companies, ratio_services):
    """
    Analyzes and interprets the results of the rule get_noun_ratio(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param ratio_companies: Results from company websites
    :param ratio_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in ratio_companies:
        ratio_companies[company] = 0
    if company not in ratio_services:
        ratio_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Nomenrate',
        'description': 'Es wird die Rate an Nomen pro Satz geprüft. "Nominalstil" (viele Nomen pro Satz) deuten auf unverständliche, schlecht lesbare Sprache hin.',
        'result': (ratio_companies[company], ratio_services[company]),
        'weight': 1,
        'score': 1,
        'interpretation': '',
        'recommendation': ''
    }

    # TODO: Check which results do we want?
    if ratio_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Datenschutzerklärung, Impressum und AGB konnten nicht gefunden werden.'
        interpretation_result['recommendation'] = 'Bitte Vorhandensein von Datenschutzerklärung, Impressum und AGB prüfen.'
    elif ratio_companies[company] <= 0.1:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Durchschnittliche Anzahl an Nomen pro Satz erkannt.'
        interpretation_result['recommendation'] = 'Gut, wenige Nomen sind im Allgemeinen ein Anzeichen für gut lesbare Sprache.'
    else:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Unterdurschschnittliche Anzahl an Nomen pro Satz erkannt.'
        interpretation_result['recommendation'] = 'Sehr gut, wenige Nomen sind im Allgemeinen ein Anzeichen für gut lesbare Sprache.'

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_verb_noun_ratio(analyze, company, ratio_companies, ratio_services):
    """
    Analyzes and interprets the results of the rule get_verb_noun_ratio(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param ratio_companies: Results from company websites
    :param ratio_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in ratio_companies:
        ratio_companies[company] = 0
    if company not in ratio_services:
        ratio_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Verhältnis zwischen Nomen und Verben',
        'description': 'Gibt das Verhältnis zwischen Verben und Nomen an um das Ausmaß der Nominalisierung zu bestimmen. Häufige Nominalisierung deutet auf einen schlechter lesbaren Text hin.',
        'result': (ratio_companies[company], ratio_services[company]),
        'weight': 1,
        'score': 1,
        'interpretation': '',
        'recommendation': ''
    }

    # TODO: Check which results do we want?
    if ratio_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Datenschutzerklärung, Impressum und AGB konnten nicht gefunden werden.'
        interpretation_result['recommendation'] = 'Bitte Vorhandensein von Datenschutzerklärung, Impressum und AGB prüfen.'
    elif ratio_companies[company] <= 0.7:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Durchschnittliches Verhältnis zwischen Nomen und Verben erkannt.'
        interpretation_result['recommendation'] = 'Gut, wenige Nomen und viele Verben sind im Allgemeinen ein Anzeichen für gut lesbare Sprache.'
    else:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Unterdurschschnittliches Verhältnis zwischen Nomen und Verben erkannt.'
        interpretation_result['recommendation'] = 'Sehr gut, wenige Nomen und viele Verben sind im Allgemeinen ein Anzeichen für gut lesbare Sprache.'

    analyze_result = ()

    return analyze_result, interpretation_result
