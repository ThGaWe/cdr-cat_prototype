from home.imports import *


def analyze_participation_results(company, config, category, rules, results_companies, results_services, save):
    """
    Executes the analysis part for this CDR dimension
    :param company: The company that currently gets analyzed
    :param config: The current configuration
    :param category: The CDR dimension of the current rule
    :param rules: The rules from the current CDR dimension
    :param results_companies: The results of the company website of the current company
    :param results_services: The results of the service website of the current company
    :param save: True if execution runs over full dataset, False otherwise
    :return: Dictionary with all interpreted results of the company of this CDR dimension
    """
    root_path = 'home/analyze_ruleset/results/participation/'
    result = {
        'score': 0
    }

    # Analyze, interpret and save results
    for rule in rules:
        rule_path = root_path + rule + '.txt'
        new_rule = rule.replace('get', 'analyze')

        if new_rule in globals():
            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, config)

            analyze_rule = eval(new_rule)
            analyze_result = analyze_rule(save, company, results_companies[category][rule], results_services[category][rule])
            result[rule] = analyze_result[1]

            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, result[0])

    # Determine the overall score of this CDR dimension
    amount = 0
    for rule in result:
        if not rule == 'score':
            result['score'] += result[rule]['weight'] * result[rule]['score']
            amount += result[rule]['weight']
    result['score'] = result['score'] / amount

    return result


def analyze_survey(analyze, company, survey_companies, survey_services):
    """
    Analyzes and interprets the results of the rule get_survey(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param survey_companies: Results from company websites
    :param survey_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in survey_companies:
        survey_companies[company] = 0
    if company not in survey_services:
        survey_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Umfrage',
        'description': 'Es wird geprüft, ob Produkt- oder Serviceumfragen durchgeführt werden.',
        'result': (survey_companies[company], survey_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if survey_companies[company] is None:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die benötigten Webseiten wurden nicht gefunden, Prüfung nicht möglich.'
        interpretation_result['recommendation'] = ''
    elif survey_companies[company]:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Es werden Umfragen durchgeführt.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Es werden keine Umfragen durchgeführt.'
        interpretation_result['recommendation'] = 'Wir empfehlen Kundenumfragen durchzuführen, um dem Kunden die Möglichkeit zu geben sich direkt in der Firma einzubringen.'

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_newsletter(analyze, company, newsletter_companies, newsletter_services):
    """
    Analyzes and interprets the results of the rule get_newsletter(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param newsletter_companies: Results from company websites
    :param newsletter_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in newsletter_companies:
        newsletter_companies[company] = 0
    if company not in newsletter_services:
        newsletter_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Newsletter',
        'description': 'Es wird geprüft, ob ein Newsletter an den Kunden versendet wird.',
        'result': (newsletter_companies[company], newsletter_companies[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if newsletter_companies[company] is None:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die benötigten Webseiten wurden nicht gefunden, Prüfung nicht möglich.'
        interpretation_result['recommendation'] = ''
    elif newsletter_companies[company]:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Es werden Newsletter an den Kunden versendet.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Es werden keine Newsletter an den Kunden versendet.'
        interpretation_result['recommendation'] = 'Wir empfehlen die Kunden mit Hilfe von Newslettern auf dem Laufenden zu halten um diesen zu ermöglichen sich über alle Neuigkeiten zu informieren.'

    analyze_result = ()

    return analyze_result, interpretation_result
