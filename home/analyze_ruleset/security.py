from home.imports import *


def analyze_security_results(company, config, category, rules, results_companies, results_services, save):
    """
    Executes the analysis part for this CDR dimension
    :param company: The company that currently gets analyzed
    :param config: The current configuration
    :param category: The CDR dimension of the current rule
    :param rules: The rules from the current CDR dimension
    :param results_companies: The results of the company website of the current company
    :param results_services: The results of the service website of the current company
    :param save: True if execution runs over full dataset, False otherwise
    :return: Dictionary with all interpreted results of the company of this CDR dimension
    """
    root_path = 'home/analyze_ruleset/results/security/'
    result = {
        'score': 0
    }

    # Analyze, interpret and save results
    for rule in rules:
        rule_path = root_path + rule + '.txt'
        new_rule = rule.replace('get', 'analyze')

        if new_rule in globals():
            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, config)

            analyze_rule = eval(new_rule)
            analyze_result = analyze_rule(save, company, results_companies[category][rule], results_services[category][rule])
            result[rule] = analyze_result[1]

            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, result[0])

    # Determine the overall score of this CDR dimension
    amount = 0
    for rule in result:
        if not rule == 'score':
            result['score'] += result[rule]['weight'] * result[rule]['score']
            amount += result[rule]['weight']
    result['score'] = result['score'] / amount

    return result


def analyze_certificates(analyze, company, certificate_companies, certificate_services):
    """
    Analyzes and interprets the results of the rule get_certificates(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param certificate_companies: Results from company websites
    :param certificate_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in certificate_companies:
        certificate_companies[company] = 0
    if company not in certificate_services:
        certificate_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Zertifikate',
        'description': 'Es wird geprüft, ob die Zertifizierung der Firma offengelegt wird.',
        'result': (certificate_companies[company], certificate_companies[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if certificate_companies[company] is None:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die benötigten Webseiten wurden nicht gefunden, Prüfung nicht möglich.'
        interpretation_result['recommendation'] = ''
    elif certificate_companies[company]:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Die Zertifizierung der Firma wird offengelegt.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die Zertifizierung der Firma wird nicht offengelegt.'
        interpretation_result['recommendation'] = 'Wir empfehlen die Zertifizierung der Firma offenzulegen um den Kunden die Möglichkeit zu bieten die Qualität Ihres Angebotes zu beurteilen.'

    analyze_result = ()

    return analyze_result, interpretation_result
