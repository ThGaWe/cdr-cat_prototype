from home.imports import *


def analyze_awareness_results(company, config, category, rules, results_companies, results_services, save):
    """
    Executes the analysis part for this CDR dimension
    :param company: The company that currently gets analyzed
    :param config: The current configuration
    :param category: The CDR dimension of the current rule
    :param rules: The rules from the current CDR dimension
    :param results_companies: The results of the company website of the current company
    :param results_services: The results of the service website of the current company
    :param save: True if execution runs over full dataset, False otherwise
    :return: Dictionary with all interpreted results of the company of this CDR dimension
    """
    root_path = 'home/analyze_ruleset/results/awareness/'
    result = {
        'score': 0
    }

    # Analyze, interpret and save results
    for rule in rules:
        rule_path = root_path + rule + '.txt'
        new_rule = rule.replace('get', 'analyze')

        if new_rule in globals():
            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, config)

            analyze_rule = eval(new_rule)
            analyze_result = analyze_rule(save, company, results_companies[category][rule], results_services[category][rule])
            result[rule] = analyze_result[1]

            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, result[0])

    # Determine the overall score of this CDR dimension
    amount = 0
    for rule in result:
        if not rule == 'score':
            result['score'] += result[rule]['weight'] * result[rule]['score']
            amount += result[rule]['weight']
    result['score'] = result['score'] / amount

    return result


def analyze_eco_friendly(analyze, company, eco_companies, eco_services):
    """
    Analyzes and interprets the results of the rule get_eco_friendly(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param eco_companies: Results from service websites
    :param eco_services: Results of analysis part and interpreted results
    :return: Results of analysis part and interpreted results
    """
    if company not in eco_companies:
        eco_companies[company] = 0
    if company not in eco_services:
        eco_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Umweltschutz',
        'description': 'Es wird geprüft, ob Umweltschutz, Klimaschutz und Nachhaltigkeit thematisiert werden.',
        'result': (eco_companies[company], eco_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if eco_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Keine Textbelege zu Umweltschutz, Klimaschutz und Nachhaltigkeit gefunden.'
        interpretation_result['recommendation'] = 'Wir empfehlen, diese Thematisierung noch einmal zu prüfen, und explizit in Ihrem Webauftritt hervorzuheben.'
    elif 0 < eco_companies[company] <= 5:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Wenige Textbelege zu Umweltschutz, Klimaschutz und Nachhaltigkeit gefunden.'
        interpretation_result['recommendation'] = 'Wir empfehlen, diese Thematisierung noch einmal zu prüfen, und deutlich stärker in Ihrem Webauftritt hervorzuheben.'
    elif 5 < eco_companies[company] <= 10:
        interpretation_result['score'] = 2
        interpretation_result['interpretation'] = 'Leicht unterdurchschnittliche Textbelege zu Umweltschutz, Klimaschutz und Nachhaltigkeit gefunden.'
        interpretation_result['recommendation'] = 'Wir empfehlen, diese Thematisierung noch einmal zu prüfen, und noch etwas stärker in Ihrem Webauftritt hervorzuheben.'
    elif 10 < eco_companies[company] <= 15:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Durchschnittliche Textbelege zu Umweltschutz, Klimaschutz und Nachhaltigkeit gefunden.'
        interpretation_result['recommendation'] = ''
    elif 15 < eco_companies[company] <= 20:
        interpretation_result['score'] = 4
        interpretation_result['interpretation'] = 'Überdurchschnittliche Textbelege zu Umweltschutz, Klimaschutz und Nachhaltigkeit gefunden.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Stark überdurchschnittliche Textbelege zu Umweltschutz, Klimaschutz und Nachhaltigkeit gefunden.'
        interpretation_result['recommendation'] = ''

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_energy_efficient(analyze, company, eco_companies, eco_services):
    """
    Analyzes and interprets the results of the rule get_energy_efficient(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param eco_companies: Results from company websites
    :param eco_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in eco_companies:
        eco_companies[company] = 0
    if company not in eco_services:
        eco_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Energie-Effizienz',
        'description': 'Es wird geprüft, ob erneuerbare Energien, Strom- und Wasserverbrauch thematisiert werden.',
        'result': (eco_companies[company], eco_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if eco_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Keine Textbelege zu erneuerbaren Energien, Strom- und Wasserverbrauch gefunden.'
        interpretation_result['recommendation'] = 'Wir empfehlen, diese Thematisierung noch einmal zu prüfen, und explizit in Ihrem Webauftritt hervorzuheben.'
    elif 0 < eco_companies[company] <= 2:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Wenige Textbelege zu erneuerbaren Energien, Strom- und Wasserverbrauch gefunden.'
        interpretation_result['recommendation'] = 'Wir empfehlen, diese Thematisierung noch einmal zu prüfen, und deutlich stärker in Ihrem Webauftritt hervorzuheben.'
    elif 2 < eco_companies[company] <= 4:
        interpretation_result['score'] = 2
        interpretation_result['interpretation'] = 'Leicht unterdurchschnittliche Textbelege zu erneuerbaren Energien, Strom- und Wasserverbrauch gefunden.'
        interpretation_result['recommendation'] = 'Wir empfehlen, diese Thematisierung noch einmal zu prüfen, und noch etwas stärker in Ihrem Webauftritt hervorzuheben.'
    elif 4 < eco_companies[company] <= 6:
        interpretation_result['score'] = 3
        interpretation_result['interpretation'] = 'Durchschnittliche Textbelege zu erneuerbaren Energien, Strom- und Wasserverbrauch gefunden.'
        interpretation_result['recommendation'] = ''
    elif 6 < eco_companies[company] <= 8:
        interpretation_result['score'] = 4
        interpretation_result['interpretation'] = 'Überdurchschnittliche Textbelege zu erneuerbaren Energien, Strom- und Wasserverbrauch gefunden.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Stark überdurchschnittliche Textbelege zu erneuerbaren Energien, Strom- und Wasserverbrauch gefunden.'
        interpretation_result['recommendation'] = ''
    analyze_result = ()

    return analyze_result, interpretation_result

