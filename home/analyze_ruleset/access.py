from home.imports import *


def analyze_access_results(company, config, category, rules, results_companies, results_services, save):
    """
    Executes the analysis part for this CDR dimension
    :param company: The company that currently gets analyzed
    :param config: The current configuration
    :param category: The CDR dimension of the current rule
    :param rules: The rules from the current CDR dimension
    :param results_companies: The results of the company website of the current company
    :param results_services: The results of the service website of the current company
    :param save: True if execution runs over full dataset, False otherwise
    :return: Dictionary with all interpreted results of the company of this CDR dimension
    """
    root_path = 'home/analyze_ruleset/results/access/'
    result = {
        'score': 0
    }

    # Analyze, interpret and save results
    for rule in rules:
        rule_path = root_path + rule + '.txt'
        new_rule = rule.replace('get', 'analyze')

        if new_rule in globals():
            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, config)

            analyze_rule = eval(new_rule)
            # print(results_companies[category])
            # if rule in results_companies[category].keys():
            analyze_result = analyze_rule(save, company, results_companies[category][rule], results_services[category][rule])
            result[rule] = analyze_result[1]
            # else:

            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, result[0])

    # Determine the overall score of this CDR dimension
    amount = 0
    for rule in result:
        if not rule == 'score':
            result['score'] += result[rule]['weight'] * result[rule]['score']
            amount += result[rule]['weight']
    result['score'] = result['score'] / amount

    return result


def analyze_language_check(analyze, company, contact_companies, contact_services):
    """
    Analyzes and interprets the results of the rule get_language_check(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param contact_companies: Results from company websites
    :param contact_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in contact_companies:
        contact_companies[company] = 0
    if company not in contact_services:
        contact_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Mehrsprachigkeit',
        'description': 'Es wird geprüft, ob relevante Dokumente in deutsch und englisch vorliegen.',
        'result': (contact_companies[company], contact_services[company]),
        'weight': 1,
        'score': 5,
        'interpretation': '',
        'recommendation': ''
    }

    only_en = True
    if contact_companies[company] == 0:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die benötigten Webseiten wurden nicht gefunden, Prüfung nicht möglich.'
        interpretation_result['recommendation'] = ''
    elif contact_companies[company] is None:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Alle Dokumente in deutsch und englisch vorhanden.'
        interpretation_result['recommendation'] = ''
    else:
        for key in contact_companies[company].keys():
            interpretation_result['interpretation'] += 'Folgende ' + key + ' Dokumente benötigen eine Übersetzung: '
            for elem in contact_companies[company][key]:
                interpretation_result['interpretation'] += elem[0] + ' in ' + elem[1] + ', '
                if elem[1] == 'DE':
                    only_en = False
            interpretation_result['recommendation'] = ''
        interpretation_result['interpretation'] = interpretation_result['interpretation'][:-2]

        if 'mandatory' in contact_companies[company].keys():
            interpretation_result['score'] -= 1

            if len(contact_companies[company]['mandatory']) == 3:
                interpretation_result['score'] -= 1
        if 'optional' in contact_companies[company].keys():
            interpretation_result['score'] -= 1
        if not only_en:
            interpretation_result['score'] -= 1

    analyze_result = ()

    return analyze_result, interpretation_result

