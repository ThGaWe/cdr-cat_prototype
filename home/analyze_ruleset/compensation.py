from home.imports import *


def analyze_compensation_results(company, config, category, rules, results_companies, results_services, save):
    """
    Executes the analysis part for this CDR dimension
    :param company: The company that currently gets analyzed
    :param config: The current configuration
    :param category: The CDR dimension of the current rule
    :param rules: The rules from the current CDR dimension
    :param results_companies: The results of the company website of the current company
    :param results_services: The results of the service website of the current company
    :param save: True if execution runs over full dataset, False otherwise
    :return: Dictionary with all interpreted results of the company of this CDR dimension
    """
    root_path = 'home/analyze_ruleset/results/compensation/'
    result = {
        'score': 0
    }

    # Analyze, interpret and save results
    for rule in rules:
        rule_path = root_path + rule + '.txt'
        new_rule = rule.replace('get', 'analyze')

        if new_rule in globals():
            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, config)

            analyze_rule = eval(new_rule)
            analyze_result = analyze_rule(save, company, results_companies[category][rule], results_services[category][rule])
            result[rule] = analyze_result[1]

            if save and not helperFunction.is_result_in_file(rule_path, config):
                helperFunction.append_result_file(rule_path, result[0])

    # Determine the overall score of this CDR dimension
    amount = 0
    for rule in result:
        if not rule == 'score':
            result['score'] += result[rule]['weight'] * result[rule]['score']
            amount += result[rule]['weight']
    result['score'] = result['score'] / amount

    return result


def analyze_contact_formular(analyze, company, contact_companies, contact_services):
    """
    Analyzes and interprets the results of the rule get_contact_formular(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param contact_companies: Results from company websites
    :param contact_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in contact_companies:
        contact_companies[company] = 0
    if company not in contact_services:
        contact_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Kontaktformular',
        'description': 'Es wird geprüft, ob ein Kontaktformular vorhanden ist, welches Kunden zur Kontaktaufnahme nutzen können.',
        'result': (contact_companies[company], contact_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if contact_companies[company] is None:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die benötigten Webseiten wurden nicht gefunden, Prüfung nicht möglich.'
        interpretation_result['recommendation'] = ''
    elif contact_companies[company]:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Kontaktformular ist vorhanden.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Kontaktformular fehlt.'
        interpretation_result['recommendation'] = 'Wir empfehlen, ein gut sichtbares Kontaktformular einzufügen, um möglichst einfachen Kundenkontakt zu ermöglichen.'

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_contact_email(analyze, company, contact_companies, contact_services):
    """
    Analyzes and interprets the results of the rule get_contact_email(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param contact_companies: Results from company websites
    :param contact_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in contact_companies:
        contact_companies[company] = 0
    if company not in contact_services:
        contact_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Kontakt-Email',
        'description': 'Es wird geprüft, ob eine Kontakt-eMail-Adresse angegeben wird, welche Kunden zur Kontaktaufnahme nutzen können.',
        'result': (contact_companies[company], contact_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if contact_companies[company] is None:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die benötigten Webseiten wurden nicht gefunden, Prüfung nicht möglich.'
        interpretation_result['recommendation'] = ''
    elif contact_companies[company]:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Kontakt-eMail-Adresse ist vorhanden.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Kontakt-eMail-Adresse fehlt.'
        interpretation_result['recommendation'] = 'Wir empfehlen, eine gut sichtbare Kontakt-eMail-Adresse einzufügen, um möglichst einfachen Kundenkontakt zu ermöglichen.'

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_contact_phone(analyze, company, contact_companies, contact_services):
    """
    Analyzes and interprets the results of the rule get_contact_phone(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param contact_companies: Results from company websites
    :param contact_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in contact_companies:
        contact_companies[company] = 0
    if company not in contact_services:
        contact_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Kontakt-Telefonnummer',
        'description': 'Es wird geprüft, ob eine Kontakt-Telefonnummer angegeben wird, welche Kunden zur Kontaktaufnahme nutzen können.',
        'result': (contact_companies[company], contact_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if contact_companies[company] is None:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die benötigten Webseiten wurden nicht gefunden, Prüfung nicht möglich.'
        interpretation_result['recommendation'] = ''
    elif contact_companies[company]:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Kontakt-Telefonnummer ist vorhanden.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Kontakt-Telefonnummer fehlt.'
        interpretation_result['recommendation'] = 'Wir empfehlen, eine gut sichtbare Kontakt-Telefonnummer einzufügen, um möglichst einfachen Kundenkontakt zu ermöglichen.'

    analyze_result = ()

    return analyze_result, interpretation_result


def analyze_contact_privacy(analyze, company, contact_companies, contact_services):
    """
    Analyzes and interprets the results of the rule get_contact_privacy(...)
    :param analyze: True whether analysis part should be executed or not
    :param company: The company that currently gets analyzed
    :param contact_companies: Results from company websites
    :param contact_services: Results from service websites
    :return: Results of analysis part and interpreted results
    """
    if company not in contact_companies:
        contact_companies[company] = 0
    if company not in contact_services:
        contact_services[company] = 0

    interpretation_result = {
        'name': 'Prüfung Kontakt zu Datenschutzbeauftragtem',
        'description': 'Es wird geprüft, ob ein Kontakt zum Datenschutzbeauftragten angegeben wird, welche Kunden zu Themen des Datenschutzes nutzen können.',
        'result': (contact_companies[company], contact_services[company]),
        'weight': 1,
        'score': 0,
        'interpretation': '',
        'recommendation': ''
    }

    if contact_companies[company] is None:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Die benötigten Webseiten wurden nicht gefunden, Prüfung nicht möglich.'
        interpretation_result['recommendation'] = ''
    elif contact_companies[company]:
        interpretation_result['score'] = 5
        interpretation_result['interpretation'] = 'Kontakt zum Datenschutzbeauftragten ist vorhanden.'
        interpretation_result['recommendation'] = ''
    else:
        interpretation_result['score'] = 1
        interpretation_result['interpretation'] = 'Kontakt zum Datenschutzbeauftragten fehlt.'
        interpretation_result['recommendation'] = 'Wir empfehlen, Kontaktdaten (Name, eMail, Telefon) zum Datenschutzbeauftragten einzufügen.'

    analyze_result = ()

    return analyze_result, interpretation_result
