from django import template

register = template.Library()


@register.filter
def get_keyvalue(dic, key):
    return dic[key]


@register.filter
def is_smaller(val1, val2):
    return int(val1) < int(val2)


@register.filter
def is_bigger(val1, val2):
    return int(val1) > int(val2)


@register.filter
def is_smaller_equal(val1, val2):
    return int(val1) <= int(val2)


@register.filter
def is_bigger_equal(val1, val2):
    return int(val1) >= int(val2)
