import json
import pickle
import os.path


# def is_empty(doc_list):
#     doc_elems = []
#
#     for doc in doc_list:
#         doc_elems += doc[2] + doc[3]
#
#     if len(doc_elems) == 0:
#         return True
#     else:
#         return False


def get_company_names(companies, services):
    """
    Return all companies with a company and service website
    :param companies: List of company websites
    :param services: List of service websites
    :return: All names that occur in both lists
    """
    if companies < services:
        result = companies

        for service in services:
            if service not in companies:
                result.append(service)

        return result
    elif companies > services:
        result = services

        for company in companies:
            if company not in services:
                result.append(company)

        return result
    else:
        return companies


def get_paragraphs(doc):
    """
    Separate a text into its paragraphs. A paragraph ends with two end of line tokens '\n\n'
    :param doc: Text to separate into paragraphs
    :return: List of paragraphs
    """
    paragraph_list = []
    paragraph_start = 0
    header = True

    for token_counter, token in enumerate(doc):
        # End of a paragraph
        if "\n\n" in token.text and not header or token_counter == len(doc) - 1:
            paragraph_list.append(doc[paragraph_start:token_counter].as_doc())
            paragraph_start = token_counter + 1
            header = True
        # Header of a paragraph
        elif token.text == "\n":
            header = False
    return paragraph_list


def new_doc(config, companies_or_services, companies, documents):
    """
    Checks if the configuration changed to previous iteration
    :param config: Path of the document containing the previous configuration
    :param companies_or_services: Contains the URL parameter cos from new configuration
    :param companies: Contains the URL parameter comps from new configuration
    :param documents: Contains the URL parameter docs from new configuration
    :return: True or False, whether the configuration has changed or not
    """
    save_to_file = False

    # Open previous configuration
    if os.path.isfile(config):
        with open(config, 'r', encoding='utf-8') as config_file:
            # Check whether the configuration has changed
            if str(config_file.readline().replace('\"', '\'')) != (str(companies_or_services) + str(companies) + str(documents)):
                save_to_file = True
    else:
        # If the config document doesn't exist in the first iteration
        save_to_file = True

    if save_to_file:
        # Save the current configuration to the config file
        with open(config, 'w') as config_file:
            config_file.write(json.dumps(companies_or_services))
            config_file.write(json.dumps(companies))
            config_file.write(json.dumps(documents))
    return save_to_file


def save_doc(path, doc):
    """
    Saves the Spacy doc to a .pkl file
    :param path: Path of the file to store the doc
    :param doc: The doc which gets saved
    """
    with open(path, 'wb') as doc_file:
        pickle.dump(doc, doc_file)


def open_doc(path):
    """
    Loades a saved Spacy doc from a .pkl file
    :param path: Path to the file which gets loaded
    """
    with open(path, 'rb') as doc_file:
        return pickle.load(doc_file)


def is_result_in_file(path, config):
    """
    Checks whether the analysis result is already stored for the given configuration
    :param path: Path of the file where the analysis is stored
    :param config: The configuration to check for
    :return: True or False, whether the analysis for the given configuration is stored or not
    """
    prev_line = "None"

    if os.path.exists(path):
        with open(path, 'r') as result_file:
            for line in result_file:
                line = line.replace('\n', '')
                if prev_line in config and line in config:
                    return True

                prev_line = line
    return False


def append_result_file(path, results):
    """
    Saves the results of the analysis part into a .txt file
    :param path: Path of the file to store in
    :param results: Results to store
    """
    with open(path, 'a+') as result_file:
        for line in results:
            result_file.write(str(line))
            result_file.write('\n')
        result_file.write('\n')
