def get_industry_branch(doc):
    """
    (Placeholder Function) A helper function that is needed for other rules. Determines the industry branch of a company
    :param doc: The Spacy doc all rules work with
    :return: None
    """
    return None


def get_costs(doc):
    """
    (Placeholder Function) Checks which costs for the customer are mentioned by the company
    :param doc: The Spacy doc all rules work with
    :return: None
    """
    return None
