from home.imports import *

pattern_email = [{'LIKE_EMAIL': True}]
matcher.add("Email", [pattern_email])

last_doc = None

contact_formular = False
contact_email = False
contact_phone = False
contact_privacy = False


def get_compensation_rules(paragraphs):
    """
    Executes all rules from CDR dimension compensation to save runtime
    :param paragraphs: List of all paragraphs of a text
    """
    global contact_formular
    contact_formular = False
    global contact_email
    contact_email = False
    global contact_phone
    contact_phone = False
    global contact_privacy
    contact_privacy = False

    for paragraph in paragraphs:
        if ("kontakt" or "e-mail" or "telefon" or "formular" or "anfragen") in paragraph.text.lower():
            # Execute code for get_contact_formular(...)
            if "kontaktformular" in paragraph.text.lower():
                contact_formular = True
            # Execute code for get_contact_email(...)
            if len(matcher(paragraph)) != 0:
                contact_email = True
            # Execute code for get_contact_phone(...)
            if "tel" in paragraph.text.lower():
                for token_counter, token in enumerate(paragraph):
                    if "tel" in token.text.lower() and token_counter < len(paragraph) - 2:
                        if (paragraph[token_counter + 1].pos_ == "PUNCT" and any(
                                char.isdigit() for char in paragraph[token_counter + 2].text)) or any(
                                char.isdigit() for char in paragraph[token_counter + 1].text):
                            contact_phone = True
        # Execute code for get_contact_privacy(...)
        if ("datenschutz" or "verantwortlich") in paragraph.text.lower():
            if "datenschutzbeauftragt" in paragraph.text.lower() and len(matcher(paragraph)) != 0:
                contact_privacy = True
            elif "tel" in paragraph.text.lower():
                for token_counter, token in enumerate(paragraph):
                    if "tel" in token.text.lower() and token_counter < len(paragraph) - 2:
                        if (paragraph[token_counter + 1].pos_ == "PUNCT" and any(char.isdigit() for char in paragraph[token_counter + 2].text)) or any(char.isdigit() for char in paragraph[token_counter + 1].text):
                            contact_privacy = True


def get_contact_formular(doc):
    """
    Checks if a contact formular is used on the website
    :param doc: The Spacy doc all rules work with
    :return: True or False, whether a contact formular is used
    """
    global last_doc

    if 'data_priv' in doc['de']['mandatory'].keys():
        if not doc == last_doc:
            paragraphs = helperFunction.get_paragraphs(doc['de']['mandatory']['data_priv'][1])
            get_compensation_rules(paragraphs)

            last_doc = doc
    else:
        return None

    return contact_formular


def get_contact_email(doc):
    """
    Checks if a contact email is given on the website
    :param doc: The Spacy doc all rules work with
    :return: True or False, whether a contact email is given
    """
    global last_doc

    if 'data_priv' in doc['de']['mandatory'].keys():
        if not doc == last_doc:
            paragraphs = helperFunction.get_paragraphs(doc['de']['mandatory']['data_priv'][1])
            get_compensation_rules(paragraphs)

            last_doc = doc
    else:
        return None

    return contact_email


def get_contact_phone(doc):
    """
    Checks if a contact phone number is given on the website
    :param doc: The Spacy doc all rules work with
    :return: True or False, whether a contact phone number is given
    """
    global last_doc

    if 'data_priv' in doc['de']['mandatory'].keys():
        if not doc == last_doc:
            paragraphs = helperFunction.get_paragraphs(doc['de']['mandatory']['data_priv'][1])
            get_compensation_rules(paragraphs)

            last_doc = doc
    else:
        return None

    return contact_phone


def get_contact_privacy(doc):
    """
    Checks if the contact data of the data privacy officer is given on the website
    :param doc: The Spacy doc all rules work with
    :return: True or False, whether the contact data of the data privacy officer is given
    """
    global last_doc

    if 'data_priv' in doc['de']['mandatory'].keys():
        if not doc == last_doc:
            paragraphs = helperFunction.get_paragraphs(doc['de']['mandatory']['data_priv'][1])
            get_compensation_rules(paragraphs)

            last_doc = doc
    else:
        return None

    return contact_privacy
