from home.imports import *


def get_language_check(doc):
    """
    Checks if all documents are translated into German and English
    :param doc: The Spacy doc all rules work with
    :return: List with all documents that need a translation
    """
    lang_docs = {
        'mandatory': [],
        'optional': []
    }

    if len(doc['de']['mandatory']) > len(doc['en']['mandatory']):
        for doc_de in doc['de']['mandatory']:
            if doc_de not in doc['en']['mandatory']:
                lang_docs['mandatory'].append((doc_de, 'EN'))
    else:
        for doc_en in doc['en']['mandatory']:
            if doc_en not in doc['de']['mandatory']:
                lang_docs['mandatory'].append((doc_en, 'DE'))

    if len(doc['de']['optional']) > len(doc['en']['optional']):
        for doc_de in doc['de']['optional']:
            if doc_de not in doc['en']['optional']:
                lang_docs['optional'].append((doc_de, 'EN'))
    else:
        for doc_en in doc['en']['optional']:
            if doc_en not in doc['de']['optional']:
                lang_docs['optional'].append((doc_en, 'DE'))

    if len(lang_docs['mandatory']) == 0 and len(lang_docs['optional']) == 0:
        return None

    if len(lang_docs['mandatory']) == 0:
        del lang_docs['mandatory']
    if len(lang_docs['optional']) == 0:
        del lang_docs['optional']

    return lang_docs


def get_tac(doc):
    """
    (Placeholder Function) Checks if the topic access is addressed in the terms and conditions
    :param doc: The Spacy doc all rules work with
    :return: None
    """
    return None
