from home.imports import *


def get_liability(doc):
    """
    (Placeholder Function) Checks if the company addresses the topic liability
    :param doc: The Spacy doc all rules work with
    :return: None
    """
    return None


def get_certificates(doc):
    """
    Checks if the company mentions their certificates. Should be extended to also return said certificates, but that's hard to extract just from the files
    :param doc: The Spacy doc all rules work with
    :return: True or False, whether the company mentions their certificates
    """
    if len(doc['de']['optional']['full']) == 0:
        relevant_docs = doc['de']['mandatory']['full']
    elif len(doc['de']['mandatory']['full']) == 0:
        relevant_docs = doc['de']['optional']['full']
    else:
        relevant_docs = Doc.from_docs([doc['de']['mandatory']['full'], doc['de']['optional']['full']])

    if not len(relevant_docs) == 0:
        for sent in list(relevant_docs.sents):
            if 'zertifi' in sent.text.lower():
                # print(sent.text + '\n')
                return True
        return False
    return None
