from home.imports import *

last_trad_doc = None
last_ratio_doc = None

average_num_char = 0
average_sent_len = 0
average_comma_count = 0

token_ratio = 0
verb_ratio = 0
noun_ratio = 0
verb_noun_ratio = 0

lix = 0


def get_traditional_features(doc):
    """
    Executes all feature functions to determine the readability of a text
    :param doc: The Spacy doc all rules work with
    """
    global average_num_char
    average_num_char = 0
    global average_sent_len
    average_sent_len = 0
    global average_comma_count
    average_comma_count = 0

    curr_sent_len = 0
    sent_count = 0
    word_count = 0
    comma_count = 0

    for index, token in enumerate(doc):
        if token.pos_ != "PUNCT" and token.pos_ != "SPACE" and token.text != "\n":
            average_num_char += len(token)
            word_count += 1

        if token.pos_ == "PUNCT" and token.text == ",":
            comma_count += 1

        if token.pos_ == "PUNCT" and (token.text == "." or token.text == "?" or token.text == "!") or index == len(doc) - 1:
            sent_count += 1
            average_sent_len += curr_sent_len
            curr_sent_len = 0

            average_comma_count += comma_count
            comma_count = 0
        elif token.pos_ != "PUNCT" and token.pos_ != "SPACE" and token.text != "\n":
            curr_sent_len += 1
    average_num_char = average_num_char / word_count
    average_sent_len = average_sent_len / sent_count
    average_comma_count = average_comma_count / sent_count


def get_ratios(doc):
    """
    Executes all token ratios to determine the readability of a text
    :param doc: The Spacy doc all rules work with
    """
    global token_ratio
    token_ratio = 0
    global verb_ratio
    verb_ratio = 0
    global noun_ratio
    noun_ratio = 0
    global verb_noun_ratio
    verb_noun_ratio = 0

    global lix
    lix = 0

    bow = {}
    verb_count = 0
    noun_count = 0
    word_count = 0
    long_word_count = 0  # More than 6 characters
    sentences = []
    curr_sent = []

    for index, token in enumerate(doc):
        if token.pos_ == "PUNCT" and (token.text == "." or token.text == "?" or token.text == "!") or index == len(doc) - 1:
            sentences.append(curr_sent)
            curr_sent = []
        else:
            curr_sent.append(token)
        if token.pos_ != "PUNCT" and token.pos_ != "SPACE" and token.text != "\n" and not token.text.isdigit():
            word_count += 1

            if len(token.text) > 6:
                long_word_count += 1

            if token.text not in bow:
                bow[token.text] = (token.pos_, 1)

                if token.pos_ == "VERB":
                    verb_count += 1
                elif token.pos_ == "NOUN":
                    noun_count += 1
            else:
                bow[token.text] = (bow[token.text][0], bow[token.text][1] + 1)

    token_ratio = len(bow) / word_count
    verb_ratio = verb_count / word_count
    noun_ratio = noun_count / word_count
    verb_noun_ratio = noun_count / (noun_count + verb_count)

    lix = round((word_count / len(sentences)) + (long_word_count / word_count) * 100)


def get_average_num_char(doc):
    """
    Determines the average number of characters from all words of a text
    :param doc: The Spacy doc all rules work with
    :return: Average number of characters
    """
    global last_trad_doc

    if len(doc["de"]["mandatory"]["full"]) != 0:
        if not doc == last_trad_doc:
            # get_traditional_features(Doc.from_docs([doc["de"]["mandatory"]["full"], doc["de"]["optional"]["full"]]))
            get_traditional_features(doc["de"]["mandatory"]["full"])

            last_trad_doc = doc
    else:
        return 0

    return average_num_char


def get_average_sent_len(doc):
    """
    Determine the average words per sentence
    :param doc: The Spacy doc all rules work with
    :return: Average words per sentence
    """
    global last_trad_doc

    if len(doc["de"]["mandatory"]["full"]) != 0:
        if not doc == last_trad_doc:
            get_traditional_features(doc["de"]["mandatory"]["full"])

            last_trad_doc = doc
    else:
        return 0

    return average_sent_len


def get_average_comma_count(doc):
    """
    Determines the average number of commas in a sentence
    :param doc: The Spacy doc all rules work with
    :return: Average number of commas in a sentence
    """
    global last_trad_doc

    if len(doc["de"]["mandatory"]["full"]) != 0:
        if not doc == last_trad_doc:
            get_traditional_features(doc["de"]["mandatory"]["full"])

            last_trad_doc = doc
    else:
        return 0

    return average_comma_count


def get_lix(doc):
    """
    Determines the Readability Index (Lesbarkeitsindex) of a text to get a concrete score on the readability
    :param doc: The Spacy doc all rules work with
    :return: Readability Index
    """
    global last_ratio_doc

    if len(doc["de"]["mandatory"]["full"]) != 0:
        if not doc == last_ratio_doc:
            get_ratios(doc["de"]["mandatory"]["full"])

            last_ratio_doc = doc
    else:
        return 0

    return lix


def get_token_ratio(doc):
    """
    Determines the use of unique words in a text
    :param doc: The Spacy doc all rules work with
    :return: Ratio of unique words
    """
    global last_ratio_doc

    if len(doc["de"]["mandatory"]["full"]) != 0:
        if not doc == last_ratio_doc:
            get_ratios(doc["de"]["mandatory"]["full"])

            last_ratio_doc = doc
    else:
        return 0

    return token_ratio


def get_verb_ratio(doc):
    """
    Determines the ratio between verbs and non verbs
    :param doc: The Spacy doc all rules work with
    :return: Ratio between verbs and non verbs
    """
    global last_ratio_doc

    if len(doc["de"]["mandatory"]["full"]) != 0:
        if not doc == last_ratio_doc:
            get_ratios(doc["de"]["mandatory"]["full"])

            last_ratio_doc = doc
    else:
        return 0

    return verb_ratio


def get_noun_ratio(doc):
    """
    Determines the ratio between nouns and non nouns
    :param doc: The Spacy doc all rules work with
    :return: Ratio between nouns and non nouns
    """
    global last_ratio_doc

    if len(doc["de"]["mandatory"]["full"]) != 0:
        if not doc == last_ratio_doc:
            get_ratios(doc["de"]["mandatory"]["full"])

            last_ratio_doc = doc
    else:
        return 0

    return noun_ratio


def get_verb_noun_ratio(doc):
    """
    Determines the ratio between verbs and nouns
    :param doc: The Spacy doc all rules work with
    :return: Ratio between verbs and nouns
    """
    global last_ratio_doc

    if len(doc["de"]["mandatory"]["full"]) != 0:
        if not doc == last_ratio_doc:
            get_ratios(doc["de"]["mandatory"]["full"])

            last_ratio_doc = doc
    else:
        return 0

    return verb_noun_ratio


def get_data_transfer(doc):
    """
    (Placeholder Function) Checks if user data is passed on to third parties
    :param doc: Überprüft die Weitergabe von Daten an Dritte
    :return: None
    """
    return None
