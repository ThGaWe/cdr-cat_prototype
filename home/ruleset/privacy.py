def privacy_test(doc):
    """
    An unused function to analyze the text to come up with fitting rules
    :param doc: The Spacy doc all rules work with
    :return: False
    """
    if 'data_priv' in doc['de']['mandatory'].keys():
        relevant_docs = doc['de']['mandatory']['data_priv']

        if not len(relevant_docs) == 0:
            for sent in list(relevant_docs[1].sents):
                if 'dsgvo' in sent.text.lower():
                    print(sent.text + '\n')

    return False


def get_saved_data(doc):
    """
    Returns the user data a company saves and checks if it will get deleted again. Can be easily extended by adding new conditions
    :param doc: The Spacy doc all rules work with
    :return: Tuple with the data that gets saved on first place and True or False on the second place, which indicates if said data will get deleted again
    """
    result = []

    if 'data_priv' in doc['de']['mandatory'].keys():
        relevant_docs = doc['de']['mandatory']['data_priv']

        if not len(relevant_docs) == 0:
            for sent in list(relevant_docs[1].sents):
                if ('cookies', False) not in result and 'cookies' in sent.text.lower():
                    result.append(('cookies', False))
                if ('personenbezogene daten', False) not in result and \
                        'keine' not in sent.text.lower() and 'personenbezogene' in sent.text.lower() and 'daten' in sent.text.lower():
                    result.append(('personenbezogene daten', False))
                if ('ip-adresse', False) not in result and 'ip-adresse' in sent.text.lower() and not\
                        ('nicht' in sent.text.lower() and ('speicher' in sent.text.lower() or 'erhoben' in sent.text.lower())):
                    result.append(('ip-adresse', False))

        for sent in list(relevant_docs[1].sents):
            for index, elem in enumerate(result):
                if elem[0] in sent.text.lower() and ('lösch' in sent.text.lower() or 'wiederrufen' in sent.text.lower()):
                    result[index] = (elem[0], True)

    # TODO: Unterscheiden zwischen Firmen die keine Daten speichern und Firmen ohne Datenschutzerklärung
    if len(result) == 0:
        return None

    return result
