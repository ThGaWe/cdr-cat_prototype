from home.imports import *

survey = False
newsletter = False

last_doc = None


def participation_rules(doc):
    """
    Executes all rules from CDR dimension participation to save runtime
    :param doc: The Spacy doc all rules work with
    """
    global survey
    survey = False
    global newsletter
    newsletter = False

    if len(doc['de']['optional']['full']) == 0:
        relevant_docs = doc['de']['mandatory']['full']
    elif len(doc['de']['mandatory']['full']) == 0:
        relevant_docs = doc['de']['optional']['full']
    else:
        relevant_docs = Doc.from_docs([doc['de']['mandatory']['full'], doc['de']['optional']['full']])

    if not len(relevant_docs) == 0:
        for sent in list(relevant_docs.sents):
            # Execute code for get_newsletter(...)
            if 'newsletter' in sent.text.lower():
                newsletter = True
            # Execute code for get_survey(...)
            if 'umfrage' in sent.text.lower():
                # if 'produkt' in sent.text.lower():
                #     print('Produktumfrage:\n' + sent.text + '\n')
                # if 'kunde' in sent.text.lower():
                #     print('Kundenzufriedenheitsumfrage:\n' + sent.text + '\n')
                # else:
                survey = True


def get_survey(doc):
    """
    Checks if the company executes user or product surveys
    :param doc: The Spacy doc all rules work with
    :return: True or False, whether the company executes surveys
    """
    global last_doc

    if (len(doc['de']['mandatory']) + len(doc['de']['optional'])) != 0:
        if not doc == last_doc:
            participation_rules(doc)

            last_doc = doc
    else:
        return None

    return survey


def get_newsletter(doc):
    """
    Checks if the company offers a newsletter
    :param doc: The Spacy doc all rules work with
    :return: True or False, whether the company offers a newsletter
    """
    global last_doc

    if (len(doc['de']['mandatory']) + len(doc['de']['optional'])) != 0:
        if not doc == last_doc:
            participation_rules(doc)

            last_doc = doc
    else:
        return None

    return newsletter


def get_customer_type(doc):
    """
    (Placeholder Function) Checks if the company differs between private and business customers
    :param doc: The Spacy doc all rules work with
    :return: None
    """
    return None
