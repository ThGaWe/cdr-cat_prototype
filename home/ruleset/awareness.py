def get_eco_friendly(doc):
    """
    Checks if the topic eco friendliness is addressed
    :param doc: The Spacy doc all rules work with
    :return: Number of relevant found words
    """
    relevant_docs = []
    relevant_words = 0

    for key in doc['de']['optional'].keys():
        if 'self_imposed' in key or 'contents' in key:
            relevant_docs.append(doc['de']['optional'][key])

    if not len(relevant_docs) == 0:
        for curr_doc in relevant_docs:
            for sent in list(curr_doc[1].sents):
                if 'öko' in sent.text.lower():
                    relevant_words += 1
                if 'fair' in sent.text.lower():
                    relevant_words += 1
                if 'nachhaltig' in sent.text.lower():
                    relevant_words += 1
                if 'klimaschutz' in sent.text.lower() or 'umweltschutz' in sent.text.lower():
                    relevant_words += 1
                if 'gesund' in sent.text.lower():
                    relevant_words += 1

    return relevant_words


def get_energy_efficient(doc):
    """
    Checks if the topic energy efficiency is addressed
    :param doc: The Spacy doc all rules work with
    :return: Number of relevant found words
    """
    relevant_docs = []
    relevant_words = 0

    for key in doc['de']['optional'].keys():
        if 'self_imposed' in key or 'contents' in key:
            relevant_docs.append(doc['de']['optional'][key])

    if not len(relevant_docs) == 0:
        for curr_doc in relevant_docs:
            for sent in list(curr_doc[1].sents):
                if 'erneuerbare' in sent.text.lower() and 'energie' in sent.text.lower():
                    relevant_words += 1
                if ('weniger' in sent.text.lower() or 'minimal' in sent.text.lower()) and \
                        ('energie' in sent.text.lower() or 'wasser' in sent.text.lower() or 'strom' in sent.text.lower()):
                    relevant_words += 1
                if 'verbrauch' in sent.text.lower() and 'senken' in sent.text.lower():
                    relevant_words += 1
                if 'effizient' in sent.text.lower():
                    relevant_words += 1

    return relevant_words
